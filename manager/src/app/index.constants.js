/* global malarkey:false, moment:false */
(function () {
  'use strict';

  angular
    .module('app')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    /*测试环境*/
    .constant('restServer', 'http://apilibrary.com/')
})();
