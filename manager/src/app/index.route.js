(function () {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login");
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'login.html',
        controller: 'loginController',
        controllerAs: 'loginCtrl'
      })
      .state('home', {
        url: '/',
        templateUrl: 'app/main/content.html',
        controller: 'mainController',
        controllerAs: 'mainCtrl'
      })
      /*欢迎页面*/
      .state('home.welcome', {
        url: 'welcome',
        templateUrl: 'app/main/welcome.html'
      })
      /*订单管理*/
      .state('home.order', {
        url: 'order',
        templateUrl: 'app/main/main.html'
      })
      //订单列表
      .state('home.order.orderList', {
        url: '/orderList',
        templateUrl: 'app/components/order/views/orderList.html',
        controller: 'orderListController',
        controllerAs: 'orderListCtrl'
      })
      //订单详情
      .state('home.order.orderAdd', {
        url: '/orderAdd?orderNo',
        templateUrl: 'app/components/order/views/orderAdd.html',
        controller: 'orderAddController',
        controllerAs: 'orderAddCtrl'
      })
      /*产品管理*/
      .state('home.commodity', {
        url: 'commodity',
        templateUrl: 'app/main/main.html'
      })
      //产品列表
      .state('home.commodity.commodityList', {
        url: '/commodityList',
        templateUrl: 'app/components/commodity/views/commodityList.html',
        controller: 'commodityListController',
        controllerAs: 'commodityListCtrl'
      })
      //添加产品
      .state('home.commodity.commodityAdd', {
        url: '/commodityAdd?commodityId',
        templateUrl: 'app/components/commodity/views/commodityAdd.html',
        controller: 'commodityAddController',
        controllerAs: 'commodityAddCtrl'
      })
      /*用户管理*/
      .state('home.user', {
        url: 'user',
        templateUrl: 'app/main/main.html'
      })
      //用户列表
      .state('home.user.userList', {
        url: '/userList',
        templateUrl: 'app/components/user/views/userList.html',
        controller: 'userListController',
        controllerAs: 'userListCtrl'
      })
      //用户详情
      .state('home.user.userAdd', {
        url: '/userAdd?uid',
        templateUrl: 'app/components/user/views/userAdd.html',
        controller: 'userAddController',
        controllerAs: 'userAddCtrl'
      })
      /*新闻资讯管理*/
      .state('home.news', {
        url: 'news',
        templateUrl: 'app/main/main.html'
      })
      //新闻资讯列表
      .state('home.news.newsList', {
        url: '/newsList',
        templateUrl: 'app/components/news/views/newsList.html',
        controller: 'newsListController',
        controllerAs: 'newsListCtrl'
      })
      //添加新闻资讯
      .state('home.news.newsAdd', {
        url: '/newsAdd?newsId',
        templateUrl: 'app/components/news/views/newsAdd.html',
        controller: 'newsAddController',
        controllerAs: 'newsAddCtrl'
      })
      /*留言管理*/
      .state('home.message', {
        url: 'message',
        templateUrl: 'app/main/main.html'
      })
      //订购留言
      .state('home.message.messageList', {
        url: '/messageList?status',
        templateUrl: 'app/components/message/views/messageList.html',
        controller: 'messageListController',
        controllerAs: 'messageListCtrl'
      })
      //查看订购留言
      .state('home.message.messageAdd', {
        url: '/messageAdd?messageId&status',
        templateUrl: 'app/components/message/views/messageAdd.html',
        controller: 'messageAddController',
        controllerAs: 'messageAddCtrl'
      })
      /*管理员管理*/
      .state('home.manager', {
        url: 'manager',
        templateUrl: 'app/main/main.html'
      })
      //管理员列表
      .state('home.manager.managerList', {
        url: '/managerList',
        templateUrl: 'app/components/manager/views/managerList.html',
        controller: 'managerListController',
        controllerAs: 'managerListCtrl'
      })
      //管理员列表
      .state('home.manager.roleList', {
        url: '/roleList',
        templateUrl: 'app/components/manager/views/roleList.html',
        controller: 'roleListController',
        controllerAs: 'roleListCtrl'
      })
    $urlRouterProvider.otherwise('/welcome');
  }

})();
