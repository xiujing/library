(function () {
  'use strict';

  angular
    .module('app', ['ngAnimate', 'ngCookies', 'dndLists', 'cgNotify', 'ngLoadingSpinner', 'monospaced.elastic', 'ngTouch', 'daterangepicker', 'ngSanitize', 'ngLocale', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr', 'angularFileUpload', 'LocalStorageModule', 'angular-md5', 'oitozero.ngSweetAlert', 'datePicker', 'ngTagsInput', 'chart.js', 'highcharts-ng', 'inputDropdown', 'summernote']);

})();
