(function () {
  'use strict';

  angular
    .module('app')
    .controller('orderAddController', orderAddController);

  function orderAddController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService) {
    var vm = this;
    /*获取头部参数*/
    vm.orderNo = $stateParams.orderNo ?$stateParams.orderNo : 0;

    /*获取订单详情*/
    if (vm.orderNo) {
      var getReq = {
        method: 'GET',
        url: restServer + 'v1/manager/mall/order/' + vm.orderNo
      };
      $http(getReq)
        .success(function (res) {
          if (res.resultStatus) {
            vm.orderNo = res.resultData.orderNo;
            if(res.resultData.orderStatus=='WAITRECEIVE'){
              vm.orderStatus = "待支付"
            }else{
              vm.orderStatus = "已支付"
            }
            vm.province = res.resultData.province;
            vm.city = res.resultData.city;
            vm.district = res.resultData.district;
            vm.detailAddress = res.resultData.detailAddress;
            vm.reciverName = res.resultData.reciverName;
            vm.phoneNumber = res.resultData.phoneNumber;
            vm.totalPrice = res.resultData.totalPrice;
            vm.createTime = res.resultData.createTime;
            vm.username = res.resultData.username;
            vm.commodities = res.resultData.commodities;
          }
        }).error(function (res) {
        $log.log('error', res);
      });

    }


  }
})();
