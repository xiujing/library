(function () {
  'use strict';

  angular
    .module('app')
    .controller('orderListController', orderListController);

  /** @ngInject */
  function orderListController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;
    vm.pageNo = 1;

    /*获取用户列表*/
    vm.getOrderList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/mall/order',
        params:{
          pageNo: vm.pageNo,
        }
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.orderList = res.resultData.BoxOrderList;
            /*分页*/
            vm.totalNum = res.resultData.BoxCount;
            vm.pageResult = pageService.calculatePage(res.resultData.BoxCount, vm.pageNo, 20);
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getOrderList();



    /*分页按钮点击*/
    vm.changePage = function (page) {
      vm.pageNo = page;
      vm.getOrderList();
    }
    
    // 确认付款
    vm.changeStatus1 = function(orderNo){
      alertService.confirm("确定修改为已付款？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'post',
            url: restServer + 'v1/manager/mall/order?orderNo=' + orderNo + '&orderStatus=WAITDELIVER',
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "付款成功");
                vm.getOrderList();
              } else {
                alertService.alert(res.errorMessage);
              }
              
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });
    }
    
    // 修改订单状态
    vm.changeStatus2 = function(orderNo){
      alertService.confirm("确认发货？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'post',
            url: restServer + 'v1/manager/mall/order?orderNo=' + orderNo + '&orderStatus=WAITRECEIVE',
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "发货成功");
                vm.getOrderList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });
    }

		

  }
})();
