(function () {
  'use strict';

  angular
    .module('app')
    .controller('messageAddController', messageAddController);

  function messageAddController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService) {
    var vm = this;
    /*获取头部参数*/
    vm.messageId = $stateParams.messageId ?$stateParams.messageId : 0;
    vm.status = $stateParams.status?$stateParams.status:1;


    /*获取往期回顾详情*/
    if (vm.messageId) {
      if(vm.status==1){
        var getReq = {
          method: 'GET',
          url: restServer + 'backstage/ordered/' + vm.messageId
        };
        $http(getReq)
          .success(function (res) {
            if (res.resultStatus) {
              vm.name = res.resultData.name;
              vm.contactTel = res.resultData.contactTel;
              vm.buyTheme = res.resultData.buyTheme;
              vm.content = res.resultData.content;
              vm.IP = res.resultData.IP;
              vm.createTime = res.resultData.createTime;
            }
          }).error(function (res) {
          $log.log('error', res);
        });
      }else if(vm.status==2){
        var getReq = {
          method: 'GET',
          url: restServer + 'backstage/subscribe/' + vm.messageId
        };
        $http(getReq)
          .success(function (res) {
            if (res.resultStatus) {
              vm.childrenName = res.resultData.childrenName;
              vm.childrenAge = res.resultData.childrenAge;
              vm.contactTel = res.resultData.contactTel;
              vm.IP = res.resultData.IP;
              vm.createTime = res.resultData.createTime;
            }
          }).error(function (res) {
          $log.log('error', res);
        });
      }else if(vm.status==3){
        var getReq = {
          method: 'GET',
          url: restServer + 'backstage/online/' + vm.messageId
        };
        $http(getReq)
          .success(function (res) {
            if (res.resultStatus) {
              vm.name = res.resultData.name;
              vm.tel = res.resultData.tel;
              vm.email = res.resultData.email;
              vm.content = res.resultData.content;
              vm.IP = res.resultData.IP;
              vm.createTime = res.resultData.createTime;
            }
          }).error(function (res) {
          $log.log('error', res);
        });
      }
    }



  }
})();
