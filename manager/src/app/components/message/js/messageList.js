(function () {
  'use strict';

  angular
    .module('app')
    .controller('messageListController', messageListController);

  /** @ngInject */
  function messageListController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;
    vm.pageNo = 1;
    /*获取头部参数*/
    vm.status = $stateParams.status ?$stateParams.status : 3;  //1代表科学实验室    2代表创客课程
    /*获取留言列表*/
    vm.getMessageList = function () {
      if(vm.status==1){  //订购留言列表
        var ajaxData = {
          method: 'GET',
          url: restServer + 'backstage/ordered',
          params:{
            pageNo: vm.pageNo,
          }
        };
        $http(ajaxData)
          .success(function (res) {
            if (res.resultStatus) {
              vm.messageList = res.resultData.boxMessageList.data;
              /*分页*/
              vm.totalNum = res.resultData.boxMessageList.total;
              vm.pageResult = pageService.calculatePage(res.resultData.boxMessageList.total, vm.pageNo, 20);
            }
          }).error(function (res) {
          $log.log('error', res);
        });

     }else if(vm.status==2){  //预定课程列表
        var ajaxData = {
          method: 'GET',
          url: restServer + 'backstage/subscribe',
          params:{
            pageNo: vm.pageNo,
          }
        };
        $http(ajaxData)
          .success(function (res) {
            if (res.resultStatus) {
              vm.messageList = res.resultData.subscribeList.data;
              /*分页*/
              vm.totalNum = res.resultData.subscribeList.total;
              vm.pageResult = pageService.calculatePage(res.resultData.subscribeList.total, vm.pageNo, 20);
            }
          }).error(function (res) {
          $log.log('error', res);
        });
      }else if(vm.status==3){  //在线留言列表
        var ajaxData = {
          method: 'GET',
          url: restServer + 'backstage/online',
          params:{
            pageNo: vm.pageNo,
          }
        };
        $http(ajaxData)
          .success(function (res) {
            if (res.resultStatus) {
              vm.messageList = res.resultData.onlineMessageList.data;
              /*分页*/
              vm.totalNum = res.resultData.onlineMessageList.total;
              vm.pageResult = pageService.calculatePage(res.resultData.onlineMessageList.total, vm.pageNo, 20);
            }
          }).error(function (res) {
          $log.log('error', res);
        });
      }          
    }
    
    vm.getMessageList();


    /*分页按钮点击*/
    vm.changePage = function (page) {
      vm.pageNo = page;
      vm.getMessageList();
    }

    // 点击切换往期回顾类型
    vm.changeStatus = function(status){
      vm.status = status;
      vm.getMessageList();
    }

    /*删除常见问题*/
    vm.delete = function (id) {
      alertService.confirm("确定删除？", "", function (isConfirm) {
        if (isConfirm) {
          if(vm.status==1){
            var delClass = {
              method: 'DELETE',
              url: restServer + 'backstage/ordered/' + id,
            };
          }else if(vm.status==2){
            var delClass = {
              method: 'DELETE',
              url: restServer + 'backstage/subscribe/' + id,
            };
          }else if(vm.status==3){
            var delClass = {
              method: 'DELETE',
              url: restServer + 'backstage/online/' + id,
            };
          }
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "删除成功");
                vm.getMessageList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }

  }
})();
