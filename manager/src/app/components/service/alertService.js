(function () {
  'use strict';

  angular
    .module('app')
    .service('alertService', alertService);

  /** @ngInject */
  function alertService(notify, $rootScope, SweetAlert) {

    this.confirm = confirm;
    this.alert = alert;
    this.success = success;
    this.addAlert = addAlert;
    this.inputAlert = inputAlert;

    function inputAlert(title, text, inputName, callback) {
      SweetAlert.swal({
          title: title,
          text: text,
          type: "input",
          showCancelButton: true,
          confirmButtonText: "确认",
          cancelButtonText: "取消",
          confirmButtonColor: "#1ab394",
          closeOnConfirm: true,
          animation: "slide-from-top",
          inputPlaceholder: inputName
        },
        function (inputValue) {
          if (inputValue === "") {
            swal.showInputError("不能为空!");
          } else {
            var callStatus = callback(inputValue);
            /* swal({
             title: "已提交",
             text: "",
             timer: 0,
             showConfirmButton: false
             });*/
          }
        });
    }

    function confirm(title, tip, cb) {
      SweetAlert.swal({
          title: title,
          text: tip,
          showCancelButton: true,
          confirmButtonText: "确认",
          cancelButtonText: "取消",
          confirmButtonColor: "#1ab394",
          closeOnConfirm: true
        }, function (isConfirm) {
          cb(isConfirm);
        }
      );
    }

    function alert(title) {
      SweetAlert.swal({
        title: title,
        confirmButtonText: "确认",
        confirmButtonColor: "#1ab394"
      });
    }

    function success() {
      SweetAlert.swal("提交成功", "", "success");
    }

    /**
     *
     * @param type danger or success
     * @param msg
     */
    function addAlert(type, msg) {
      /*  $rootScope.alert = {type:type,msg: msg};
       $timeout(function(){
       $rootScope.alert = null;
       },2000);*/
      $rootScope.inspiniaTemplate = 'app/main/notify.html';
      notify({message: msg, classes: 'alert-info', templateUrl: $rootScope.inspiniaTemplate});
    }


    $rootScope.closeAlert = function () {
      $rootScope.alert = null;
    };
  }
})();
