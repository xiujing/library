(function () {
  'use strict';
  angular.module("app")
    .service('expressService', expressService);
  function expressService() {
    var vm = this;
    vm.defaultList = [
      {
        express: "顺丰",
        readableName: "顺丰",
        expressCode: "shunfeng"
      },
      {
        express: "申通",
        readableName: "申通",
        expressCode: "shentong"
      },
      {
        express: "圆通",
        readableName: "圆通",
        expressCode: "yuantong"
      },
      {
        express: "中通",
        readableName: "中通",
        expressCode: "zhongtong"
      },
      {
        express: "汇通快递",
        readableName: "汇通快递",
        expressCode: "huitongkuaidi"
      },
      {
        express: "韵达",
        readableName: "韵达",
        expressCode: "yunda"
      },
      {
        express: "天天",
        readableName: "天天",
        expressCode: "tiantian"
      },
      {
        express: "优速",
        readableName: "优速",
        expressCode: "youshuwuliu"
      },
      {
        express: "全峰",
        readableName: "全峰",
        expressCode: "quanfengkuaidi"
      },
      {
        express: "EMS",
        readableName: "EMS",
        expressCode: "ems"
      }
    ];
    vm.defaultDropdownObjects =[
      {
        "express": "邮政包裹/平邮",
        "readableName": "邮政包裹/平邮",
        "expressCode": "youzhengguonei"
      },
      {
        "express": "国际包裹",
        "readableName": "国际包裹",
        "expressCode": "youzhengguoji"
      },
      {
        "express": "EMS",
        "readableName": "EMS",
        "expressCode": "ems"
      },
      {
        "express": "EMS-国际件",
        "readableName": "EMS-国际件",
        "expressCode": "emsguoji"
      },
      {
        "express": "EMS-国际件-英文结果",
        "readableName": "EMS-国际件-英文结果",
        "expressCode": "emsinten"
      },
      {
        "express": "北京EMS",
        "readableName": "北京EMS",
        "expressCode": "bjemstckj"
      },
      {
        "express": "顺丰",
        "readableName": "顺丰",
        "expressCode": "shunfeng"
      },
      {
        "express": "申通",
        "readableName": "申通",
        "expressCode": "shentong"
      },
      {
        "express": "圆通",
        "readableName": "圆通",
        "expressCode": "yuantong"
      },
      {
        "express": "中通",
        "readableName": "中通",
        "expressCode": "zhongtong"
      },
      {
        "express": "汇通",
        "readableName": "汇通",
        "expressCode": "huitongkuaidi"
      },
      {
        "express": "韵达",
        "readableName": "韵达",
        "expressCode": "yunda"
      },
      {
        "express": "宅急送",
        "readableName": "宅急送",
        "expressCode": "zhaijisong"
      },
      {
        "express": "天天",
        "readableName": "天天",
        "expressCode": "tiantian"
      },
      {
        "express": "德邦",
        "readableName": "德邦",
        "expressCode": "debangwuliu"
      },
      {
        "express": "国通",
        "readableName": "国通",
        "expressCode": "guotongkuaidi"
      },
      {
        "express": "增益",
        "readableName": "增益",
        "expressCode": "zengyisudi"
      },
      {
        "express": "速尔",
        "readableName": "速尔",
        "expressCode": "suer"
      },
      {
        "express": "中铁物流",
        "readableName": "中铁物流",
        "expressCode": "ztky"
      },
      {
        "express": "中铁快运",
        "readableName": "中铁快运",
        "expressCode": "zhongtiewuliu"
      },
      {
        "express": "能达",
        "readableName": "能达",
        "expressCode": "ganzhongnengda"
      },
      {
        "express": "优速",
        "readableName": "优速",
        "expressCode": "youshuwuliu"
      },
      {
        "express": "全峰",
        "readableName": "全峰",
        "expressCode": "quanfengkuaidi"
      },
      {
        "express": "京东",
        "readableName": "京东",
        "expressCode": "jd"
      },
      {
        "express": "FedEx-国际",
        "readableName": "FedEx-国际",
        "expressCode": "fedex"
      },
      {
        "express": "FedEx-美国",
        "readableName": "FedEx-美国",
        "expressCode": "fedexus"
      },
      {
        "express": "DHL全球件",
        "readableName": "DHL全球件",
        "expressCode": "dhlen"
      },
      {
        "express": "DHL",
        "readableName": "DHL",
        "expressCode": "dhl"
      },
      {
        "express": "DHL-德国",
        "readableName": "DHL-德国",
        "expressCode": "dhlde"
      },
      {
        "express": "TNT全球件",
        "readableName": "TNT全球件",
        "expressCode": "tnten"
      },
      {
        "express": "TNT",
        "readableName": "TNT",
        "expressCode": "tnt"
      },
      {
        "express": "UPS全球件",
        "readableName": "UPS全球件",
        "expressCode": "upsen"
      },
      {
        "express": "UPS",
        "readableName": "UPS",
        "expressCode": "ups"
      },
      {
        "express": "USPS",
        "readableName": "USPS",
        "expressCode": "usps"
      },
      {
        "express": "DPD",
        "readableName": "DPD",
        "expressCode": "dpd"
      },
      {
        "express": "DPD Germany",
        "readableName": "DPD Germany",
        "expressCode": "dpdgermany"
      },
      {
        "express": "DPD Poland",
        "readableName": "DPD Poland",
        "expressCode": "dpdpoland"
      },
      {
        "express": "DPD UK",
        "readableName": "DPD UK",
        "expressCode": "dpduk"
      },
      {
        "express": "GLS",
        "readableName": "GLS",
        "expressCode": "gls"
      },
      {
        "express": "Toll",
        "readableName": "Toll",
        "expressCode": "dpexen"
      },
      {
        "express": "Toll Priority(Toll Online)",
        "readableName": "Toll Priority(Toll Online)",
        "expressCode": "tollpriority"
      },
      {
        "express": "Aramex",
        "readableName": "Aramex",
        "expressCode": "aramex"
      },
      {
        "express": "DPEX",
        "readableName": "DPEX",
        "expressCode": "dpex"
      },
      {
        "express": "宅急便",
        "readableName": "宅急便",
        "expressCode": "zhaijibian"
      },
      {
        "express": "黑猫雅玛多",
        "readableName": "黑猫雅玛多",
        "expressCode": "yamato"
      },
      {
        "express": "香港邮政(HongKong Post)",
        "readableName": "香港邮政(HongKong Post)",
        "expressCode": "hkpost"
      },
      {
        "express": "英国大包、EMS（Parcel Force）",
        "readableName": "英国大包、EMS（Parcel Force）",
        "expressCode": "parcelforce"
      },
      {
        "express": "英国小包（Royal Mail）",
        "readableName": "英国小包（Royal Mail）",
        "expressCode": "royalmail"
      },
      {
        "express": "澳大利亚邮政-英文",
        "readableName": "澳大利亚邮政-英文",
        "expressCode": "auspost"
      },
      {
        "express": "加拿大邮政-英文版",
        "readableName": "加拿大邮政-英文版",
        "expressCode": "canpost"
      },
      {
        "express": "一统飞鸿",
        "readableName": "一统飞鸿",
        "expressCode": "yitongfeihong"
      },
      {
        "express": "如风达",
        "readableName": "如风达",
        "expressCode": "rufengda"
      },
      {
        "express": "海红网送",
        "readableName": "海红网送",
        "expressCode": "haihongwangsong"
      },
      {
        "express": "通和天下",
        "readableName": "通和天下",
        "expressCode": "tonghetianxia"
      },
      {
        "express": "郑州建华",
        "readableName": "郑州建华",
        "expressCode": "zhengzhoujianhua"
      },
      {
        "express": "红马甲",
        "readableName": "红马甲",
        "expressCode": "sxhongmajia"
      },
      {
        "express": "芝麻开门",
        "readableName": "芝麻开门",
        "expressCode": "zhimakaimen"
      },
      {
        "express": "乐捷递",
        "readableName": "乐捷递",
        "expressCode": "lejiedi"
      },
      {
        "express": "立即送",
        "readableName": "立即送",
        "expressCode": "lijisong"
      },
      {
        "express": "银捷",
        "readableName": "银捷",
        "expressCode": "yinjiesudi"
      },
      {
        "express": "门对门",
        "readableName": "门对门",
        "expressCode": "menduimen"
      },
      {
        "express": "河北建华",
        "readableName": "河北建华",
        "expressCode": "hebeijianhua"
      },
      {
        "express": "微特派",
        "readableName": "微特派",
        "expressCode": "weitepai"
      },
      {
        "express": "风行天下",
        "readableName": "风行天下",
        "expressCode": "fengxingtianxia"
      },
      {
        "express": "尚橙",
        "readableName": "尚橙",
        "expressCode": "shangcheng"
      },
      {
        "express": "新蛋奥硕",
        "readableName": "新蛋奥硕",
        "expressCode": "neweggozzo"
      },
      {
        "express": "鑫飞鸿",
        "readableName": "鑫飞鸿",
        "expressCode": "xinhongyukuaidi"
      },
      {
        "express": "全一",
        "readableName": "全一",
        "expressCode": "quanyikuaidi"
      },
      {
        "express": "彪记",
        "readableName": "彪记",
        "expressCode": "biaojikuaidi"
      },
      {
        "express": "星晨急便",
        "readableName": "星晨急便",
        "expressCode": "xingchengjibian"
      },
      {
        "express": "亚风",
        "readableName": "亚风",
        "expressCode": "yafengsudi"
      },
      {
        "express": "源伟丰",
        "readableName": "源伟丰",
        "expressCode": "yuanweifeng"
      },
      {
        "express": "全日通",
        "readableName": "全日通",
        "expressCode": "quanritongkuaidi"
      },
      {
        "express": "安信达",
        "readableName": "安信达",
        "expressCode": "anxindakuaixi"
      },
      {
        "express": "民航",
        "readableName": "民航",
        "expressCode": "minghangkuaidi"
      },
      {
        "express": "凤凰",
        "readableName": "凤凰",
        "expressCode": "fenghuangkuaidi"
      },
      {
        "express": "京广",
        "readableName": "京广",
        "expressCode": "jinguangsudikuaijian"
      },
      {
        "express": "配思货运",
        "readableName": "配思货运",
        "expressCode": "peisihuoyunkuaidi"
      },
      {
        "express": "AAE-中国件",
        "readableName": "AAE-中国件",
        "expressCode": "aae"
      },
      {
        "express": "大田",
        "readableName": "大田",
        "expressCode": "datianwuliu"
      },
      {
        "express": "新邦",
        "readableName": "新邦",
        "expressCode": "xinbangwuliu"
      },
      {
        "express": "龙邦",
        "readableName": "龙邦",
        "expressCode": "longbanwuliu"
      },
      {
        "express": "一邦",
        "readableName": "一邦",
        "expressCode": "yibangwuliu"
      },
      {
        "express": "联昊通",
        "readableName": "联昊通",
        "expressCode": "lianhaowuliu"
      },
      {
        "express": "广东邮政",
        "readableName": "广东邮政",
        "expressCode": "guangdongyouzhengwuliu"
      },
      {
        "express": "中邮",
        "readableName": "中邮",
        "expressCode": "zhongyouwuliu"
      },
      {
        "express": "天地华宇",
        "readableName": "天地华宇",
        "expressCode": "tiandihuayu"
      },
      {
        "express": "盛辉",
        "readableName": "盛辉",
        "expressCode": "shenghuiwuliu"
      },
      {
        "express": "长宇",
        "readableName": "长宇",
        "expressCode": "changyuwuliu"
      },
      {
        "express": "飞康达",
        "readableName": "飞康达",
        "expressCode": "feikangda"
      },
      {
        "express": "元智捷诚",
        "readableName": "元智捷诚",
        "expressCode": "yuanzhijiecheng"
      },
      {
        "express": "万家",
        "readableName": "万家",
        "expressCode": "wanjiawuliu"
      },
      {
        "express": "远成",
        "readableName": "远成",
        "expressCode": "yuanchengwuliu"
      },
      {
        "express": "信丰",
        "readableName": "信丰",
        "expressCode": "xinfengwuliu"
      },
      {
        "express": "文捷航空",
        "readableName": "文捷航空",
        "expressCode": "wenjiesudi"
      },
      {
        "express": "全晨",
        "readableName": "全晨",
        "expressCode": "quanchenkuaidi"
      },
      {
        "express": "佳怡",
        "readableName": "佳怡",
        "expressCode": "jiayiwuliu"
      },
      {
        "express": "快捷",
        "readableName": "快捷",
        "expressCode": "kuaijiesudi"
      },
      {
        "express": "D速",
        "readableName": "D速",
        "expressCode": "dsukuaidi"
      },
      {
        "express": "全际通",
        "readableName": "全际通",
        "expressCode": "quanjitong"
      },
      {
        "express": "青岛安捷",
        "readableName": "青岛安捷",
        "expressCode": "anjiekuaidi"
      },
      {
        "express": "越丰",
        "readableName": "越丰",
        "expressCode": "yuefengwuliu"
      },
      {
        "express": "急先达",
        "readableName": "急先达",
        "expressCode": "jixianda"
      },
      {
        "express": "百福东方",
        "readableName": "百福东方",
        "expressCode": "baifudongfang"
      },
      {
        "express": "BHT",
        "readableName": "BHT",
        "expressCode": "bht"
      },
      {
        "express": "伍圆",
        "readableName": "伍圆",
        "expressCode": "wuyuansudi"
      },
      {
        "express": "蓝镖",
        "readableName": "蓝镖",
        "expressCode": "lanbiaokuaidi"
      },
      {
        "express": "COE",
        "readableName": "COE",
        "expressCode": "coe"
      },
      {
        "express": "南京100",
        "readableName": "南京100",
        "expressCode": "nanjing"
      },
      {
        "express": "恒路",
        "readableName": "恒路",
        "expressCode": "hengluwuliu"
      },
      {
        "express": "金大",
        "readableName": "金大",
        "expressCode": "jindawuliu"
      },
      {
        "express": "华夏龙",
        "readableName": "华夏龙",
        "expressCode": "huaxialongwuliu"
      },
      {
        "express": "运通中港",
        "readableName": "运通中港",
        "expressCode": "yuntongkuaidi"
      },
      {
        "express": "佳吉",
        "readableName": "佳吉",
        "expressCode": "jiajiwuliu"
      },
      {
        "express": "盛丰",
        "readableName": "盛丰",
        "expressCode": "shengfengwuliu"
      },
      {
        "express": "源安达",
        "readableName": "源安达",
        "expressCode": "yuananda"
      },
      {
        "express": "加运美",
        "readableName": "加运美",
        "expressCode": "jiayunmeiwuliu"
      },
      {
        "express": "万象",
        "readableName": "万象",
        "expressCode": "wanxiangwuliu"
      },
      {
        "express": "宏品",
        "readableName": "宏品",
        "expressCode": "hongpinwuliu"
      },
      {
        "express": "上大",
        "readableName": "上大",
        "expressCode": "shangda"
      },
      {
        "express": "中铁",
        "readableName": "中铁",
        "expressCode": "zhongtiewuliu"
      },
      {
        "express": "原飞航",
        "readableName": "原飞航",
        "expressCode": "yuanfeihangwuliu"
      },
      {
        "express": "海外环球",
        "readableName": "海外环球",
        "expressCode": "haiwaihuanqiu"
      },
      {
        "express": "三态",
        "readableName": "三态",
        "expressCode": "santaisudi"
      },
      {
        "express": "晋越",
        "readableName": "晋越",
        "expressCode": "jinyuekuaidi"
      },
      {
        "express": "联邦",
        "readableName": "联邦",
        "expressCode": "lianbangkuaidi"
      },
      {
        "express": "飞快达",
        "readableName": "飞快达",
        "expressCode": "feikuaida"
      },
      {
        "express": "忠信达",
        "readableName": "忠信达",
        "expressCode": "zhongxinda"
      },
      {
        "express": "共速达",
        "readableName": "共速达",
        "expressCode": "gongsuda"
      },
      {
        "express": "嘉里大通",
        "readableName": "嘉里大通",
        "expressCode": "jialidatong"
      },
      {
        "express": "OCS",
        "readableName": "OCS",
        "expressCode": "ocs"
      },
      {
        "express": "美国",
        "readableName": "美国",
        "expressCode": "meiguokuaidi"
      },
      {
        "express": "成都立即送",
        "readableName": "成都立即送",
        "expressCode": "lijisong"
      },
      {
        "express": "递四方",
        "readableName": "递四方",
        "expressCode": "disifang"
      },
      {
        "express": "康力",
        "readableName": "康力",
        "expressCode": "kangliwuliu"
      },
      {
        "express": "跨越",
        "readableName": "跨越",
        "expressCode": "kuayue"
      },
      {
        "express": "海盟",
        "readableName": "海盟",
        "expressCode": "haimengsudi"
      },
      {
        "express": "圣安",
        "readableName": "圣安",
        "expressCode": "shenganwuliu"
      },
      {
        "express": "中速",
        "readableName": "中速",
        "expressCode": "zhongsukuaidi"
      },
      {
        "express": "OnTrac",
        "readableName": "OnTrac",
        "expressCode": "ontrac"
      },
      {
        "express": "七天连锁",
        "readableName": "七天连锁",
        "expressCode": "sevendays"
      },
      {
        "express": "明亮",
        "readableName": "明亮",
        "expressCode": "mingliangwuliu"
      },
      {
        "express": "凡客配送（作废）",
        "readableName": "凡客配送（作废）",
        "expressCode": "vancl"
      },
      {
        "express": "华企",
        "readableName": "华企",
        "expressCode": "huaqikuaiyun"
      },
      {
        "express": "城市100",
        "readableName": "城市100",
        "expressCode": "city100"
      },
      {
        "express": "穗佳",
        "readableName": "穗佳",
        "expressCode": "suijiawuliu"
      },
      {
        "express": "飞豹",
        "readableName": "飞豹",
        "expressCode": "feibaokuaidi"
      },
      {
        "express": "传喜",
        "readableName": "传喜",
        "expressCode": "chuanxiwuliu"
      },
      {
        "express": "捷特",
        "readableName": "捷特",
        "expressCode": "jietekuaidi"
      },
      {
        "express": "隆浪",
        "readableName": "隆浪",
        "expressCode": "longlangkuaidi"
      },
      {
        "express": "EMS-英文",
        "readableName": "EMS-英文",
        "expressCode": "emsen"
      },
      {
        "express": "中天万运",
        "readableName": "中天万运",
        "expressCode": "zhongtianwanyun"
      },
      {
        "express": "邦送",
        "readableName": "邦送",
        "expressCode": "bangsongwuliu"
      },
      {
        "express": "澳大利亚(Australia Post)",
        "readableName": "澳大利亚(Australia Post)",
        "expressCode": "auspost"
      },
      {
        "express": "加拿大(Canada Post)",
        "readableName": "加拿大(Canada Post)",
        "expressCode": "canpost"
      },
      {
        "express": "加拿大邮政",
        "readableName": "加拿大邮政",
        "expressCode": "canpostfr"
      },
      {
        "express": "顺丰-美国件",
        "readableName": "顺丰-美国件",
        "expressCode": "shunfengen"
      },
      {
        "express": "汇强",
        "readableName": "汇强",
        "expressCode": "huiqiangkuaidi"
      },
      {
        "express": "希优特",
        "readableName": "希优特",
        "expressCode": "xiyoutekuaidi"
      },
      {
        "express": "昊盛",
        "readableName": "昊盛",
        "expressCode": "haoshengwuliu"
      },
      {
        "express": "亿领",
        "readableName": "亿领",
        "expressCode": "yilingsuyun"
      },
      {
        "express": "大洋",
        "readableName": "大洋",
        "expressCode": "dayangwuliu"
      },
      {
        "express": "递达",
        "readableName": "递达",
        "expressCode": "didasuyun"
      },
      {
        "express": "易通达",
        "readableName": "易通达",
        "expressCode": "yitongda"
      },
      {
        "express": "邮必佳",
        "readableName": "邮必佳",
        "expressCode": "youbijia"
      },
      {
        "express": "亿顺航",
        "readableName": "亿顺航",
        "expressCode": "yishunhang"
      },
      {
        "express": "飞狐",
        "readableName": "飞狐",
        "expressCode": "feihukuaidi"
      },
      {
        "express": "潇湘晨报",
        "readableName": "潇湘晨报",
        "expressCode": "xiaoxiangchenbao"
      },
      {
        "express": "巴伦支",
        "readableName": "巴伦支",
        "expressCode": "balunzhi"
      },
      {
        "express": "闽盛",
        "readableName": "闽盛",
        "expressCode": "minshengkuaidi"
      },
      {
        "express": "佳惠尔",
        "readableName": "佳惠尔",
        "expressCode": "syjiahuier"
      },
      {
        "express": "民邦",
        "readableName": "民邦",
        "expressCode": "minbangsudi"
      },
      {
        "express": "上海快通",
        "readableName": "上海快通",
        "expressCode": "shanghaikuaitong"
      },
      {
        "express": "北青小红帽",
        "readableName": "北青小红帽",
        "expressCode": "xiaohongmao"
      },
      {
        "express": "GSM",
        "readableName": "GSM",
        "expressCode": "gsm"
      },
      {
        "express": "安能",
        "readableName": "安能",
        "expressCode": "annengwuliu"
      },
      {
        "express": "KCS",
        "readableName": "KCS",
        "expressCode": "kcs"
      },
      {
        "express": "City-Link",
        "readableName": "City-Link",
        "expressCode": "citylink"
      },
      {
        "express": "店通",
        "readableName": "店通",
        "expressCode": "diantongkuaidi"
      },
      {
        "express": "凡宇",
        "readableName": "凡宇",
        "expressCode": "fanyukuaidi"
      },
      {
        "express": "平安达腾飞",
        "readableName": "平安达腾飞",
        "expressCode": "pingandatengfei"
      },
      {
        "express": "广东通路",
        "readableName": "广东通路",
        "expressCode": "guangdongtonglu"
      },
      {
        "express": "中睿",
        "readableName": "中睿",
        "expressCode": "zhongruisudi"
      },
      {
        "express": "快达",
        "readableName": "快达",
        "expressCode": "kuaidawuliu"
      },
      {
        "express": "ADP国际",
        "readableName": "ADP国际",
        "expressCode": "adp"
      },
      {
        "express": "颿达国际",
        "readableName": "颿达国际",
        "expressCode": "fardarww"
      },
      {
        "express": "颿达国际-英文",
        "readableName": "颿达国际-英文",
        "expressCode": "fandaguoji"
      },
      {
        "express": "林道国际",
        "readableName": "林道国际",
        "expressCode": "shlindao"
      },
      {
        "express": "中外运-中文",
        "readableName": "中外运-中文",
        "expressCode": "sinoex"
      },
      {
        "express": "中外运",
        "readableName": "中外运",
        "expressCode": "zhongwaiyun"
      },
      {
        "express": "深圳德创",
        "readableName": "深圳德创",
        "expressCode": "dechuangwuliu"
      },
      {
        "express": "林道国际-英文",
        "readableName": "林道国际-英文",
        "expressCode": "ldxpres"
      },
      {
        "express": "瑞典（Sweden Post）",
        "readableName": "瑞典（Sweden Post）",
        "expressCode": "ruidianyouzheng"
      },
      {
        "express": "PostNord(Posten AB)",
        "readableName": "PostNord(Posten AB)",
        "expressCode": "postenab"
      },
      {
        "express": "偌亚奥国际",
        "readableName": "偌亚奥国际",
        "expressCode": "nuoyaao"
      },
      {
        "express": "祥龙运通",
        "readableName": "祥龙运通",
        "expressCode": "xianglongyuntong"
      },
      {
        "express": "品速心达",
        "readableName": "品速心达",
        "expressCode": "pinsuxinda"
      },
      {
        "express": "宇鑫",
        "readableName": "宇鑫",
        "expressCode": "yuxinwuliu"
      },
      {
        "express": "陪行",
        "readableName": "陪行",
        "expressCode": "peixingwuliu"
      },
      {
        "express": "户通",
        "readableName": "户通",
        "expressCode": "hutongwuliu"
      },
      {
        "express": "西安城联",
        "readableName": "西安城联",
        "expressCode": "xianchengliansudi"
      },
      {
        "express": "煜嘉",
        "readableName": "煜嘉",
        "expressCode": "yujiawuliu"
      },
      {
        "express": "一柒国际",
        "readableName": "一柒国际",
        "expressCode": "yiqiguojiwuliu"
      },
      {
        "express": "Fedex-国际件-中文",
        "readableName": "Fedex-国际件-中文",
        "expressCode": "fedexcn"
      },
      {
        "express": "联邦-英文",
        "readableName": "联邦-英文",
        "expressCode": "lianbangkuaidien"
      },
      {
        "express": "中通（带电话）",
        "readableName": "中通（带电话）",
        "expressCode": "zhongtongphone"
      },
      {
        "express": "赛澳递for买卖宝",
        "readableName": "赛澳递for买卖宝",
        "expressCode": "saiaodimmb"
      },
      {
        "express": "上海无疆for买卖宝",
        "readableName": "上海无疆for买卖宝",
        "expressCode": "shanghaiwujiangmmb"
      },
      {
        "express": "新加坡小包(Singapore Post)",
        "readableName": "新加坡小包(Singapore Post)",
        "expressCode": "singpost"
      },
      {
        "express": "音素",
        "readableName": "音素",
        "expressCode": "yinsu"
      },
      {
        "express": "南方传媒",
        "readableName": "南方传媒",
        "expressCode": "ndwl"
      },
      {
        "express": "速呈宅配",
        "readableName": "速呈宅配",
        "expressCode": "sucheng"
      },
      {
        "express": "创一",
        "readableName": "创一",
        "expressCode": "chuangyi"
      },
      {
        "express": "云南滇驿",
        "readableName": "云南滇驿",
        "expressCode": "dianyi"
      },
      {
        "express": "重庆星程",
        "readableName": "重庆星程",
        "expressCode": "cqxingcheng"
      },
      {
        "express": "四川星程",
        "readableName": "四川星程",
        "expressCode": "scxingcheng"
      },
      {
        "express": "贵州星程",
        "readableName": "贵州星程",
        "expressCode": "gzxingcheng"
      },
      {
        "express": "运通中港(作废)",
        "readableName": "运通中港(作废)",
        "expressCode": "ytkd"
      },
      {
        "express": "Gati-英文",
        "readableName": "Gati-英文",
        "expressCode": "gatien"
      },
      {
        "express": "Gati-中文",
        "readableName": "Gati-中文",
        "expressCode": "gaticn"
      },
      {
        "express": "jcex",
        "readableName": "jcex",
        "expressCode": "jcex"
      },
      {
        "express": "派尔",
        "readableName": "派尔",
        "expressCode": "peex"
      },
      {
        "express": "凯信达",
        "readableName": "凯信达",
        "expressCode": "kxda"
      },
      {
        "express": "安达信",
        "readableName": "安达信",
        "expressCode": "advancing"
      },
      {
        "express": "汇文",
        "readableName": "汇文",
        "expressCode": "huiwen"
      },
      {
        "express": "亿翔",
        "readableName": "亿翔",
        "expressCode": "yxexpress"
      },
      {
        "express": "东红",
        "readableName": "东红",
        "expressCode": "donghong"
      },
      {
        "express": "飞远配送",
        "readableName": "飞远配送",
        "expressCode": "feiyuanvipshop"
      },
      {
        "express": "好运来",
        "readableName": "好运来",
        "expressCode": "hlyex"
      },
      {
        "express": "四川快优达",
        "readableName": "四川快优达",
        "expressCode": "kuaiyouda"
      },
      {
        "express": "日昱",
        "readableName": "日昱",
        "expressCode": "riyuwuliu"
      },
      {
        "express": "速通",
        "readableName": "速通",
        "expressCode": "sutongwuliu"
      },
      {
        "express": "晟邦",
        "readableName": "晟邦",
        "expressCode": "nanjingshengbang"
      },
      {
        "express": "爱尔兰(An Post)",
        "readableName": "爱尔兰(An Post)",
        "expressCode": "anposten"
      },
      {
        "express": "日本（Japan Post）",
        "readableName": "日本（Japan Post）",
        "expressCode": "japanposten"
      },
      {
        "express": "丹麦(Post Denmark)",
        "readableName": "丹麦(Post Denmark)",
        "expressCode": "postdanmarken"
      },
      {
        "express": "巴西(Brazil Post/Correios)",
        "readableName": "巴西(Brazil Post/Correios)",
        "expressCode": "brazilposten"
      },
      {
        "express": "荷兰挂号信(PostNL international registered mail)",
        "readableName": "荷兰挂号信(PostNL international registered mail)",
        "expressCode": "postnlcn"
      },
      {
        "express": "荷兰挂号信(PostNL international registered mail)",
        "readableName": "荷兰挂号信(PostNL international registered mail)",
        "expressCode": "postnl"
      },
      {
        "express": "乌克兰EMS-中文(EMS Ukraine)",
        "readableName": "乌克兰EMS-中文(EMS Ukraine)",
        "expressCode": "emsukrainecn"
      },
      {
        "express": "乌克兰EMS(EMS Ukraine)",
        "readableName": "乌克兰EMS(EMS Ukraine)",
        "expressCode": "emsukraine"
      },
      {
        "express": "乌克兰邮政包裹",
        "readableName": "乌克兰邮政包裹",
        "expressCode": "ukrpostcn"
      },
      {
        "express": "乌克兰小包、大包(UkrPost)",
        "readableName": "乌克兰小包、大包(UkrPost)",
        "expressCode": "ukrpost"
      },
      {
        "express": "海红for买卖宝",
        "readableName": "海红for买卖宝",
        "expressCode": "haihongmmb"
      },
      {
        "express": "FedEx-英国件（FedEx UK)",
        "readableName": "FedEx-英国件（FedEx UK)",
        "expressCode": "fedexuk"
      },
      {
        "express": "FedEx-英国件",
        "readableName": "FedEx-英国件",
        "expressCode": "fedexukcn"
      },
      {
        "express": "叮咚",
        "readableName": "叮咚",
        "expressCode": "dingdong"
      },
      {
        "express": "UPS Freight",
        "readableName": "UPS Freight",
        "expressCode": "upsfreight"
      },
      {
        "express": "ABF",
        "readableName": "ABF",
        "expressCode": "abf"
      },
      {
        "express": "Purolator",
        "readableName": "Purolator",
        "expressCode": "purolator"
      },
      {
        "express": "比利时（Bpost）",
        "readableName": "比利时（Bpost）",
        "expressCode": "bpost"
      },
      {
        "express": "比利时国际(Bpost international)",
        "readableName": "比利时国际(Bpost international)",
        "expressCode": "bpostinter"
      },
      {
        "express": "LaserShip",
        "readableName": "LaserShip",
        "expressCode": "lasership"
      },
      {
        "express": "YODEL",
        "readableName": "YODEL",
        "expressCode": "yodel"
      },
      {
        "express": "DHL-荷兰（DHL Netherlands）",
        "readableName": "DHL-荷兰（DHL Netherlands）",
        "expressCode": "dhlnetherlands"
      },
      {
        "express": "MyHermes",
        "readableName": "MyHermes",
        "expressCode": "myhermes"
      },
      {
        "express": "Fastway Ireland",
        "readableName": "Fastway Ireland",
        "expressCode": "fastway"
      },
      {
        "express": "法国大包、EMS-法文（Chronopost France）",
        "readableName": "法国大包、EMS-法文（Chronopost France）",
        "expressCode": "chronopostfra"
      },
      {
        "express": "Selektvracht",
        "readableName": "Selektvracht",
        "expressCode": "selektvracht"
      },
      {
        "express": "蓝弧",
        "readableName": "蓝弧",
        "expressCode": "lanhukuaidi"
      },
      {
        "express": "比利时(Belgium Post)",
        "readableName": "比利时(Belgium Post)",
        "expressCode": "belgiumpost"
      },
      {
        "express": "UPS Mail Innovations",
        "readableName": "UPS Mail Innovations",
        "expressCode": "upsmailinno"
      },
      {
        "express": "挪威（Posten Norge）",
        "readableName": "挪威（Posten Norge）",
        "expressCode": "postennorge"
      },
      {
        "express": "瑞士邮政",
        "readableName": "瑞士邮政",
        "expressCode": "swisspostcn"
      },
      {
        "express": "瑞士(Swiss Post)",
        "readableName": "瑞士(Swiss Post)",
        "expressCode": "swisspost"
      },
      {
        "express": "英国邮政小包",
        "readableName": "英国邮政小包",
        "expressCode": "royalmailcn"
      },
      {
        "express": "DHL Benelux",
        "readableName": "DHL Benelux",
        "expressCode": "dhlbenelux"
      },
      {
        "express": "Nova Poshta",
        "readableName": "Nova Poshta",
        "expressCode": "novaposhta"
      },
      {
        "express": "DHL-波兰（DHL Poland）",
        "readableName": "DHL-波兰（DHL Poland）",
        "expressCode": "dhlpoland"
      },
      {
        "express": "Estes",
        "readableName": "Estes",
        "expressCode": "estes"
      },
      {
        "express": "TNT UK",
        "readableName": "TNT UK",
        "expressCode": "tntuk"
      },
      {
        "express": "Deltec Courier",
        "readableName": "Deltec Courier",
        "expressCode": "deltec"
      },
      {
        "express": "OPEK",
        "readableName": "OPEK",
        "expressCode": "opek"
      },
      {
        "express": "Italy SDA",
        "readableName": "Italy SDA",
        "expressCode": "italysad"
      },
      {
        "express": "MRW",
        "readableName": "MRW",
        "expressCode": "mrw"
      },
      {
        "express": "Chronopost Portugal",
        "readableName": "Chronopost Portugal",
        "expressCode": "chronopostport"
      },
      {
        "express": "西班牙(Correos de Espa?a)",
        "readableName": "西班牙(Correos de Espa?a)",
        "expressCode": "correosdees"
      },
      {
        "express": "Direct Link",
        "readableName": "Direct Link",
        "expressCode": "directlink"
      },
      {
        "express": "ELTA Hellenic Post",
        "readableName": "ELTA Hellenic Post",
        "expressCode": "eltahell"
      },
      {
        "express": "捷克（?eská po?ta）",
        "readableName": "捷克（?eská po?ta）",
        "expressCode": "ceskaposta"
      },
      {
        "express": "Siodemka",
        "readableName": "Siodemka",
        "expressCode": "siodemka"
      },
      {
        "express": "International Seur",
        "readableName": "International Seur",
        "expressCode": "seur"
      },
      {
        "express": "久易",
        "readableName": "久易",
        "expressCode": "jiuyicn"
      },
      {
        "express": "克罗地亚（Hrvatska Posta）",
        "readableName": "克罗地亚（Hrvatska Posta）",
        "expressCode": "hrvatska"
      },
      {
        "express": "保加利亚（Bulgarian Posts）",
        "readableName": "保加利亚（Bulgarian Posts）",
        "expressCode": "bulgarian"
      },
      {
        "express": "Portugal Seur",
        "readableName": "Portugal Seur",
        "expressCode": "portugalseur"
      },
      {
        "express": "EC-Firstclass",
        "readableName": "EC-Firstclass",
        "expressCode": "ecfirstclass"
      },
      {
        "express": "DTDC India",
        "readableName": "DTDC India",
        "expressCode": "dtdcindia"
      },
      {
        "express": "Safexpress",
        "readableName": "Safexpress",
        "expressCode": "safexpress"
      },
      {
        "express": "韩国（Korea Post）",
        "readableName": "韩国（Korea Post）",
        "expressCode": "koreapost"
      },
      {
        "express": "TNT Australia",
        "readableName": "TNT Australia",
        "expressCode": "tntau"
      },
      {
        "express": "泰国（Thailand Thai Post）",
        "readableName": "泰国（Thailand Thai Post）",
        "expressCode": "thailand"
      },
      {
        "express": "SkyNet Malaysia",
        "readableName": "SkyNet Malaysia",
        "expressCode": "skynetmalaysia"
      },
      {
        "express": "马来西亚小包（Malaysia Post(Registered)）",
        "readableName": "马来西亚小包（Malaysia Post(Registered)）",
        "expressCode": "malaysiapost"
      },
      {
        "express": "马来西亚大包、EMS（Malaysia Post(parcel,EMS)）",
        "readableName": "马来西亚大包、EMS（Malaysia Post(parcel,EMS)）",
        "expressCode": "malaysiaems"
      },
      {
        "express": "沙特阿拉伯(Saudi Post)",
        "readableName": "沙特阿拉伯(Saudi Post)",
        "expressCode": "saudipost"
      },
      {
        "express": "南非（South African Post Office）",
        "readableName": "南非（South African Post Office）",
        "expressCode": "southafrican"
      },
      {
        "express": "OCA Argentina",
        "readableName": "OCA Argentina",
        "expressCode": "ocaargen"
      },
      {
        "express": "尼日利亚(Nigerian Postal)",
        "readableName": "尼日利亚(Nigerian Postal)",
        "expressCode": "nigerianpost"
      },
      {
        "express": "智利(Correos Chile)",
        "readableName": "智利(Correos Chile)",
        "expressCode": "chile"
      },
      {
        "express": "以色列(Israel Post)",
        "readableName": "以色列(Israel Post)",
        "expressCode": "israelpost"
      },
      {
        "express": "Estafeta",
        "readableName": "Estafeta",
        "expressCode": "estafeta"
      },
      {
        "express": "港快",
        "readableName": "港快",
        "expressCode": "gdkd"
      },
      {
        "express": "墨西哥（Correos de Mexico）",
        "readableName": "墨西哥（Correos de Mexico）",
        "expressCode": "mexico"
      },
      {
        "express": "罗马尼亚（Posta Romanian）",
        "readableName": "罗马尼亚（Posta Romanian）",
        "expressCode": "romanian"
      },
      {
        "express": "TNT Italy",
        "readableName": "TNT Italy",
        "expressCode": "tntitaly"
      },
      {
        "express": "Mexico Multipack",
        "readableName": "Mexico Multipack",
        "expressCode": "multipack"
      },
      {
        "express": "葡萄牙（Portugal CTT）",
        "readableName": "葡萄牙（Portugal CTT）",
        "expressCode": "portugalctt"
      },
      {
        "express": "Interlink Express",
        "readableName": "Interlink Express",
        "expressCode": "interlink"
      },
      {
        "express": "华航",
        "readableName": "华航",
        "expressCode": "hzpl"
      },
      {
        "express": "Gati-KWE",
        "readableName": "Gati-KWE",
        "expressCode": "gatikwe"
      },
      {
        "express": "Red Express",
        "readableName": "Red Express",
        "expressCode": "redexpress"
      },
      {
        "express": "Mexico Senda Express",
        "readableName": "Mexico Senda Express",
        "expressCode": "mexicodenda"
      },
      {
        "express": "TCI XPS",
        "readableName": "TCI XPS",
        "expressCode": "tcixps"
      },
      {
        "express": "高铁",
        "readableName": "高铁",
        "expressCode": "hre"
      },
      {
        "express": "新加坡EMS、大包(Singapore Speedpost)",
        "readableName": "新加坡EMS、大包(Singapore Speedpost)",
        "expressCode": "speedpost"
      },
      {
        "express": "EMS-国际件-英文",
        "readableName": "EMS-国际件-英文",
        "expressCode": "emsinten"
      },
      {
        "express": "Asendia USA",
        "readableName": "Asendia USA",
        "expressCode": "asendiausa"
      },
      {
        "express": "法国大包、EMS-英文(Chronopost France)",
        "readableName": "法国大包、EMS-英文(Chronopost France)",
        "expressCode": "chronopostfren"
      },
      {
        "express": "意大利(Poste Italiane)",
        "readableName": "意大利(Poste Italiane)",
        "expressCode": "italiane"
      },
      {
        "express": "冠达",
        "readableName": "冠达",
        "expressCode": "gda"
      },
      {
        "express": "出口易",
        "readableName": "出口易",
        "expressCode": "chukou1"
      },
      {
        "express": "黄马甲",
        "readableName": "黄马甲",
        "expressCode": "huangmajia"
      },
      {
        "express": "新干线",
        "readableName": "新干线",
        "expressCode": "anlexpress"
      },
      {
        "express": "飞洋",
        "readableName": "飞洋",
        "expressCode": "shipgce"
      },
      {
        "express": "贝海国际",
        "readableName": "贝海国际",
        "expressCode": "xlobo"
      },
      {
        "express": "阿联酋(Emirates Post)",
        "readableName": "阿联酋(Emirates Post)",
        "expressCode": "emirates"
      },
      {
        "express": "新顺丰（NSF）",
        "readableName": "新顺丰（NSF）",
        "expressCode": "nsf"
      },
      {
        "express": "巴基斯坦(Pakistan Post)",
        "readableName": "巴基斯坦(Pakistan Post)",
        "expressCode": "pakistan"
      },
      {
        "express": "世运",
        "readableName": "世运",
        "expressCode": "shiyunkuaidi"
      },
      {
        "express": "合众(UCS）",
        "readableName": "合众(UCS）",
        "expressCode": "ucs"
      },
      {
        "express": "阿富汗(Afghan Post)",
        "readableName": "阿富汗(Afghan Post)",
        "expressCode": "afghan"
      },
      {
        "express": "白俄罗斯(Belpochta)",
        "readableName": "白俄罗斯(Belpochta)",
        "expressCode": "belpost"
      },
      {
        "express": "全通",
        "readableName": "全通",
        "expressCode": "quantwl"
      },
      {
        "express": "EFS Post",
        "readableName": "EFS Post",
        "expressCode": "efs"
      },
      {
        "express": "TNT Post",
        "readableName": "TNT Post",
        "expressCode": "tntpostcn"
      },
      {
        "express": "英脉",
        "readableName": "英脉",
        "expressCode": "gml"
      },
      {
        "express": "广通",
        "readableName": "广通",
        "expressCode": "gtongsudi"
      },
      {
        "express": "东瀚",
        "readableName": "东瀚",
        "expressCode": "donghanwl"
      },
      {
        "express": "rpx",
        "readableName": "rpx",
        "expressCode": "rpx"
      },
      {
        "express": "日日顺",
        "readableName": "日日顺",
        "expressCode": "rrs"
      },
      {
        "express": "华通",
        "readableName": "华通",
        "expressCode": "htongexpress"
      },
      {
        "express": "吉尔吉斯斯坦(Kyrgyz Post)",
        "readableName": "吉尔吉斯斯坦(Kyrgyz Post)",
        "expressCode": "kyrgyzpost"
      },
      {
        "express": "拉脱维亚(Latvijas Pasts)",
        "readableName": "拉脱维亚(Latvijas Pasts)",
        "expressCode": "latvia"
      },
      {
        "express": "黎巴嫩(Liban Post)",
        "readableName": "黎巴嫩(Liban Post)",
        "expressCode": "libanpost"
      },
      {
        "express": "立陶宛（Lietuvos pa?tas）",
        "readableName": "立陶宛（Lietuvos pa?tas）",
        "expressCode": "lithuania"
      },
      {
        "express": "马尔代夫(Maldives Post)",
        "readableName": "马尔代夫(Maldives Post)",
        "expressCode": "maldives"
      },
      {
        "express": "马耳他（Malta Post）",
        "readableName": "马耳他（Malta Post）",
        "expressCode": "malta"
      },
      {
        "express": "马其顿(Macedonian Post)",
        "readableName": "马其顿(Macedonian Post)",
        "expressCode": "macedonia"
      },
      {
        "express": "新西兰（New Zealand Post）",
        "readableName": "新西兰（New Zealand Post）",
        "expressCode": "newzealand"
      },
      {
        "express": "摩尔多瓦(Posta Moldovei)",
        "readableName": "摩尔多瓦(Posta Moldovei)",
        "expressCode": "moldova"
      },
      {
        "express": "孟加拉国(EMS)",
        "readableName": "孟加拉国(EMS)",
        "expressCode": "bangladesh"
      },
      {
        "express": "塞尔维亚(PE Post of Serbia)",
        "readableName": "塞尔维亚(PE Post of Serbia)",
        "expressCode": "serbia"
      },
      {
        "express": "塞浦路斯(Cyprus Post)",
        "readableName": "塞浦路斯(Cyprus Post)",
        "expressCode": "cypruspost"
      },
      {
        "express": "突尼斯EMS(Rapid-Poste)",
        "readableName": "突尼斯EMS(Rapid-Poste)",
        "expressCode": "tunisia"
      },
      {
        "express": "乌兹别克斯坦(Post of Uzbekistan)",
        "readableName": "乌兹别克斯坦(Post of Uzbekistan)",
        "expressCode": "uzbekistan"
      },
      {
        "express": "新喀里多尼亚[法国](New Caledonia)",
        "readableName": "新喀里多尼亚[法国](New Caledonia)",
        "expressCode": "caledonia"
      },
      {
        "express": "叙利亚(Syrian Post)",
        "readableName": "叙利亚(Syrian Post)",
        "expressCode": "republic"
      },
      {
        "express": "亚美尼亚(Haypost-Armenian Postal)",
        "readableName": "亚美尼亚(Haypost-Armenian Postal)",
        "expressCode": "haypost"
      },
      {
        "express": "也门(Yemen Post)",
        "readableName": "也门(Yemen Post)",
        "expressCode": "yemen"
      },
      {
        "express": "印度(India Post)",
        "readableName": "印度(India Post)",
        "expressCode": "india"
      },
      {
        "express": "英国(大包,EMS)",
        "readableName": "英国(大包,EMS)",
        "expressCode": "england"
      },
      {
        "express": "约旦(Jordan Post)",
        "readableName": "约旦(Jordan Post)",
        "expressCode": "jordan"
      },
      {
        "express": "越南小包(Vietnam Posts)",
        "readableName": "越南小包(Vietnam Posts)",
        "expressCode": "vietnam"
      },
      {
        "express": "黑山(Po?ta Crne Gore)",
        "readableName": "黑山(Po?ta Crne Gore)",
        "expressCode": "montenegro"
      },
      {
        "express": "哥斯达黎加(Correos de Costa Rica)",
        "readableName": "哥斯达黎加(Correos de Costa Rica)",
        "expressCode": "correos"
      },
      {
        "express": "西安喜来",
        "readableName": "西安喜来",
        "expressCode": "xilaikd"
      },
      {
        "express": "格陵兰[丹麦]（TELE Greenland A/S）",
        "readableName": "格陵兰[丹麦]（TELE Greenland A/S）",
        "expressCode": "greenland"
      },
      {
        "express": "菲律宾（Philippine Postal）",
        "readableName": "菲律宾（Philippine Postal）",
        "expressCode": "phlpost"
      },
      {
        "express": "厄瓜多尔(Correos del Ecuador)",
        "readableName": "厄瓜多尔(Correos del Ecuador)",
        "expressCode": "ecuador"
      },
      {
        "express": "冰岛(Iceland Post)",
        "readableName": "冰岛(Iceland Post)",
        "expressCode": "iceland"
      },
      {
        "express": "波兰小包(Poczta Polska)",
        "readableName": "波兰小包(Poczta Polska)",
        "expressCode": "emonitoring"
      },
      {
        "express": "阿尔巴尼亚(Posta shqipatre)",
        "readableName": "阿尔巴尼亚(Posta shqipatre)",
        "expressCode": "albania"
      },
      {
        "express": "阿鲁巴[荷兰]（Post Aruba）",
        "readableName": "阿鲁巴[荷兰]（Post Aruba）",
        "expressCode": "aruba"
      },
      {
        "express": "埃及（Egypt Post）",
        "readableName": "埃及（Egypt Post）",
        "expressCode": "egypt"
      },
      {
        "express": "爱沙尼亚(Eesti Post)",
        "readableName": "爱沙尼亚(Eesti Post)",
        "expressCode": "omniva"
      },
      {
        "express": "云豹国际货运",
        "readableName": "云豹国际货运",
        "expressCode": "leopard"
      },
      {
        "express": "中外运空运",
        "readableName": "中外运空运",
        "expressCode": "sinoairinex"
      },
      {
        "express": "上海昊宏国际货物",
        "readableName": "上海昊宏国际货物",
        "expressCode": "hyk"
      },
      {
        "express": "城晓国际",
        "readableName": "城晓国际",
        "expressCode": "ckeex"
      },
      {
        "express": "匈牙利（Magyar Posta）",
        "readableName": "匈牙利（Magyar Posta）",
        "expressCode": "hungary"
      },
      {
        "express": "澳门(Macau Post)",
        "readableName": "澳门(Macau Post)",
        "expressCode": "macao"
      },
      {
        "express": "台湾（中华邮政）",
        "readableName": "台湾（中华邮政）",
        "expressCode": "postserv"
      },
      {
        "express": "快淘",
        "readableName": "快淘",
        "expressCode": "kuaitao"
      },
      {
        "express": "秘鲁(SERPOST)",
        "readableName": "秘鲁(SERPOST)",
        "expressCode": "peru"
      },
      {
        "express": "印度尼西亚EMS(Pos Indonesia-EMS)",
        "readableName": "印度尼西亚EMS(Pos Indonesia-EMS)",
        "expressCode": "indonesia"
      },
      {
        "express": "哈萨克斯坦(Kazpost)",
        "readableName": "哈萨克斯坦(Kazpost)",
        "expressCode": "kazpost"
      },
      {
        "express": "立白宝凯",
        "readableName": "立白宝凯",
        "expressCode": "lbbk"
      },
      {
        "express": "百千诚",
        "readableName": "百千诚",
        "expressCode": "bqcwl"
      },
      {
        "express": "皇家",
        "readableName": "皇家",
        "expressCode": "pfcexpress"
      },
      {
        "express": "法国(La Poste)",
        "readableName": "法国(La Poste)",
        "expressCode": "csuivi"
      },
      {
        "express": "奥地利(Austrian Post)",
        "readableName": "奥地利(Austrian Post)",
        "expressCode": "austria"
      },
      {
        "express": "乌克兰小包、大包(UkrPoshta)",
        "readableName": "乌克兰小包、大包(UkrPoshta)",
        "expressCode": "ukraine"
      },
      {
        "express": "乌干达(Posta Uganda)",
        "readableName": "乌干达(Posta Uganda)",
        "expressCode": "uganda"
      },
      {
        "express": "阿塞拜疆EMS(EMS AzerExpressPost)",
        "readableName": "阿塞拜疆EMS(EMS AzerExpressPost)",
        "expressCode": "azerbaijan"
      },
      {
        "express": "芬兰(Itella Posti Oy)",
        "readableName": "芬兰(Itella Posti Oy)",
        "expressCode": "finland"
      },
      {
        "express": "斯洛伐克(Slovenská Posta)",
        "readableName": "斯洛伐克(Slovenská Posta)",
        "expressCode": "slovak"
      },
      {
        "express": "埃塞俄比亚(Ethiopian postal)",
        "readableName": "埃塞俄比亚(Ethiopian postal)",
        "expressCode": "ethiopia"
      },
      {
        "express": "卢森堡(Luxembourg Post)",
        "readableName": "卢森堡(Luxembourg Post)",
        "expressCode": "luxembourg"
      },
      {
        "express": "毛里求斯(Mauritius Post)",
        "readableName": "毛里求斯(Mauritius Post)",
        "expressCode": "mauritius"
      },
      {
        "express": "文莱(Brunei Postal)",
        "readableName": "文莱(Brunei Postal)",
        "expressCode": "brunei"
      },
      {
        "express": "Quantium",
        "readableName": "Quantium",
        "expressCode": "quantium"
      },
      {
        "express": "坦桑尼亚(Tanzania Posts)",
        "readableName": "坦桑尼亚(Tanzania Posts)",
        "expressCode": "tanzania"
      },
      {
        "express": "阿曼(Oman Post)",
        "readableName": "阿曼(Oman Post)",
        "expressCode": "oman"
      },
      {
        "express": "直布罗陀[英国]( Royal Gibraltar Post)",
        "readableName": "直布罗陀[英国]( Royal Gibraltar Post)",
        "expressCode": "gibraltar"
      },
      {
        "express": "博源恒通",
        "readableName": "博源恒通",
        "expressCode": "byht"
      },
      {
        "express": "越南EMS(VNPost Express)",
        "readableName": "越南EMS(VNPost Express)",
        "expressCode": "vnpost"
      },
      {
        "express": "安迅",
        "readableName": "安迅",
        "expressCode": "anxl"
      },
      {
        "express": "达方",
        "readableName": "达方",
        "expressCode": "dfpost"
      },
      {
        "express": "兰州伙伴",
        "readableName": "兰州伙伴",
        "expressCode": "huoban"
      },
      {
        "express": "天纵",
        "readableName": "天纵",
        "expressCode": "tianzong"
      },
      {
        "express": "波黑(JP BH Posta)",
        "readableName": "波黑(JP BH Posta)",
        "expressCode": "bohei"
      },
      {
        "express": "玻利维亚",
        "readableName": "玻利维亚",
        "expressCode": "bolivia"
      },
      {
        "express": "柬埔寨(Cambodia Post)",
        "readableName": "柬埔寨(Cambodia Post)",
        "expressCode": "cambodia"
      },
      {
        "express": "巴林(Bahrain Post)",
        "readableName": "巴林(Bahrain Post)",
        "expressCode": "bahrain"
      },
      {
        "express": "纳米比亚(NamPost)",
        "readableName": "纳米比亚(NamPost)",
        "expressCode": "namibia"
      },
      {
        "express": "卢旺达(Rwanda i-posita)",
        "readableName": "卢旺达(Rwanda i-posita)",
        "expressCode": "rwanda"
      },
      {
        "express": "莱索托(Lesotho Post)",
        "readableName": "莱索托(Lesotho Post)",
        "expressCode": "lesotho"
      },
      {
        "express": "肯尼亚(POSTA KENYA)",
        "readableName": "肯尼亚(POSTA KENYA)",
        "expressCode": "kenya"
      },
      {
        "express": "喀麦隆(CAMPOST)",
        "readableName": "喀麦隆(CAMPOST)",
        "expressCode": "cameroon"
      },
      {
        "express": "伯利兹(Belize Postal)",
        "readableName": "伯利兹(Belize Postal)",
        "expressCode": "belize"
      },
      {
        "express": "巴拉圭(Correo Paraguayo)",
        "readableName": "巴拉圭(Correo Paraguayo)",
        "expressCode": "paraguay"
      },
      {
        "express": "十方通",
        "readableName": "十方通",
        "expressCode": "sfift"
      },
      {
        "express": "飞鹰",
        "readableName": "飞鹰",
        "expressCode": "hnfy"
      },
      {
        "express": "UPS i-parcle",
        "readableName": "UPS i-parcle",
        "expressCode": "iparcel"
      },
      {
        "express": "鑫锐达",
        "readableName": "鑫锐达",
        "expressCode": "bjxsrd"
      },
      {
        "express": "麦力",
        "readableName": "麦力",
        "expressCode": "mailikuaidi"
      },
      {
        "express": "瑞丰",
        "readableName": "瑞丰",
        "expressCode": "rfsd"
      },
      {
        "express": "美联",
        "readableName": "美联",
        "expressCode": "letseml"
      },
      {
        "express": "CNPEX中邮",
        "readableName": "CNPEX中邮",
        "expressCode": "cnpex"
      },
      {
        "express": "鑫世锐达",
        "readableName": "鑫世锐达",
        "expressCode": "xsrd"
      },
      {
        "express": "同舟行",
        "readableName": "同舟行",
        "expressCode": "chinatzx"
      },
      {
        "express": "秦邦",
        "readableName": "秦邦",
        "expressCode": "qbexpress"
      },
      {
        "express": "大达",
        "readableName": "大达",
        "expressCode": "idada"
      },
      {
        "express": "skynet",
        "readableName": "skynet",
        "expressCode": "skynet"
      },
      {
        "express": "红马",
        "readableName": "红马",
        "expressCode": "nedahm"
      },
      {
        "express": "云南中诚",
        "readableName": "云南中诚",
        "expressCode": "czwlyn"
      },
      {
        "express": "万博",
        "readableName": "万博",
        "expressCode": "wanboex"
      },
      {
        "express": "腾达",
        "readableName": "腾达",
        "expressCode": "nntengda"
      },
      {
        "express": "郑州速捷",
        "readableName": "郑州速捷",
        "expressCode": "sujievip"
      },
      {
        "express": "UBI Australia",
        "readableName": "UBI Australia",
        "expressCode": "gotoubi"
      },
      {
        "express": "ECMS Express",
        "readableName": "ECMS Express",
        "expressCode": "ecmsglobal"
      },
      {
        "express": "速派(FastGo)",
        "readableName": "速派(FastGo)",
        "expressCode": "fastgo"
      },
      {
        "express": "易客满",
        "readableName": "易客满",
        "expressCode": "ecmscn"
      },
      {
        "express": "俄顺达",
        "readableName": "俄顺达",
        "expressCode": "eshunda"
      },
      {
        "express": "广东速腾",
        "readableName": "广东速腾",
        "expressCode": "suteng"
      },
      {
        "express": "新鹏",
        "readableName": "新鹏",
        "expressCode": "gdxp"
      },
      {
        "express": "美国韵达",
        "readableName": "美国韵达",
        "expressCode": "yundaexus"
      },
      {
        "express": "深圳DPEX",
        "readableName": "深圳DPEX",
        "expressCode": "szdpex"
      },
      {
        "express": "百世",
        "readableName": "百世",
        "expressCode": "baishiwuliu"
      },
      {
        "express": "荷兰包裹(PostNL International Parcels)",
        "readableName": "荷兰包裹(PostNL International Parcels)",
        "expressCode": "postnlpacle"
      },
      {
        "express": "乐天",
        "readableName": "乐天",
        "expressCode": "ltexp"
      },
      {
        "express": "智通",
        "readableName": "智通",
        "expressCode": "ztong"
      },
      {
        "express": "鑫通宝",
        "readableName": "鑫通宝",
        "expressCode": "xtb"
      },
      {
        "express": "airpak expresss",
        "readableName": "airpak expresss",
        "expressCode": "airpak"
      },
      {
        "express": "荷兰邮政-中国件",
        "readableName": "荷兰邮政-中国件",
        "expressCode": "postnlchina"
      },
      {
        "express": "法国小包（colissimo）",
        "readableName": "法国小包（colissimo）",
        "expressCode": "colissimo"
      },
      {
        "express": "PCA Express",
        "readableName": "PCA Express",
        "expressCode": "pcaexpress"
      },
      {
        "express": "韩润",
        "readableName": "韩润",
        "expressCode": "hanrun"
      },
      {
        "express": "中远e环球",
        "readableName": "中远e环球",
        "expressCode": "cosco"
      },
      {
        "express": "顺达",
        "readableName": "顺达",
        "expressCode": "sundarexpress"
      },
      {
        "express": "捷记方舟",
        "readableName": "捷记方舟",
        "expressCode": "ajexpress"
      },
      {
        "express": "方舟",
        "readableName": "方舟",
        "expressCode": "arkexpress"
      },
      {
        "express": "明大",
        "readableName": "明大",
        "expressCode": "adaexpress"
      },
      {
        "express": "长江国际",
        "readableName": "长江国际",
        "expressCode": "changjiang"
      },
      {
        "express": "八达通",
        "readableName": "八达通",
        "expressCode": "bdatong"
      },
      {
        "express": "美国申通",
        "readableName": "美国申通",
        "expressCode": "stoexpress"
      },
      {
        "express": "泛捷国际",
        "readableName": "泛捷国际",
        "expressCode": "epanex"
      },
      {
        "express": "顺捷丰达",
        "readableName": "顺捷丰达",
        "expressCode": "shunjiefengda"
      },
      {
        "express": "华赫",
        "readableName": "华赫",
        "expressCode": "nmhuahe"
      },
      {
        "express": "德国(Deutsche Post)",
        "readableName": "德国(Deutsche Post)",
        "expressCode": "deutschepost"
      },
      {
        "express": "百腾",
        "readableName": "百腾",
        "expressCode": "baitengwuliu"
      },
      {
        "express": "品骏",
        "readableName": "品骏",
        "expressCode": "pjbest"
      },
      {
        "express": "全速通",
        "readableName": "全速通",
        "expressCode": "quansutong"
      },
      {
        "express": "中技",
        "readableName": "中技",
        "expressCode": "zhongjiwuliu"
      },
      {
        "express": "九曳供应链",
        "readableName": "九曳供应链",
        "expressCode": "jiuyescm"
      },
      {
        "express": "天翼",
        "readableName": "天翼",
        "expressCode": "tykd"
      },
      {
        "express": "德意思",
        "readableName": "德意思",
        "expressCode": "dabei"
      },
      {
        "express": "城际",
        "readableName": "城际",
        "expressCode": "chengji"
      },
      {
        "express": "程光",
        "readableName": "程光",
        "expressCode": "chengguangkuaidi"
      },
      {
        "express": "佐川急便",
        "readableName": "佐川急便",
        "expressCode": "sagawa"
      },
      {
        "express": "蓝天",
        "readableName": "蓝天",
        "expressCode": "lantiankuaidi"
      },
      {
        "express": "永昌",
        "readableName": "永昌",
        "expressCode": "yongchangwuliu"
      },
      {
        "express": "笨鸟海淘",
        "readableName": "笨鸟海淘",
        "expressCode": "birdex"
      },
      {
        "express": "一正达",
        "readableName": "一正达",
        "expressCode": "yizhengdasuyun"
      },
      {
        "express": "优配",
        "readableName": "优配",
        "expressCode": "sdyoupei"
      },
      {
        "express": "TRAKPAK",
        "readableName": "TRAKPAK",
        "expressCode": "trakpak"
      },
      {
        "express": "GTS",
        "readableName": "GTS",
        "expressCode": "gts"
      },
      {
        "express": "AOL澳通",
        "readableName": "AOL澳通",
        "expressCode": "aolau"
      },
      {
        "express": "宜送",
        "readableName": "宜送",
        "expressCode": "yiex"
      },
      {
        "express": "通达兴",
        "readableName": "通达兴",
        "expressCode": "tongdaxing"
      },
      {
        "express": "香港(HongKong Post)英文",
        "readableName": "香港(HongKong Post)英文",
        "expressCode": "hkposten"
      },
      {
        "express": "飞力士",
        "readableName": "飞力士",
        "expressCode": "flysman"
      },
      {
        "express": "转运四方",
        "readableName": "转运四方",
        "expressCode": "zhuanyunsifang"
      },
      {
        "express": "logen路坚",
        "readableName": "logen路坚",
        "expressCode": "ilogen"
      },
      {
        "express": "成都东骏",
        "readableName": "成都东骏",
        "expressCode": "dongjun"
      },
      {
        "express": "日本郵便",
        "readableName": "日本郵便",
        "expressCode": "japanpost"
      },
      {
        "express": "佳家通货运",
        "readableName": "佳家通货运",
        "expressCode": "jiajiatong56"
      },
      {
        "express": "吉日优派",
        "readableName": "吉日优派",
        "expressCode": "jrypex"
      },
      {
        "express": "西安胜峰",
        "readableName": "西安胜峰",
        "expressCode": "xaetc"
      },
      {
        "express": "CJ",
        "readableName": "CJ",
        "expressCode": "doortodoor"
      },
      {
        "express": "信天捷",
        "readableName": "信天捷",
        "expressCode": "xintianjie"
      },
      {
        "express": "泰国138国际",
        "readableName": "泰国138国际",
        "expressCode": "sd138"
      },
      {
        "express": "猴急送",
        "readableName": "猴急送",
        "expressCode": "hjs"
      },
      {
        "express": "全信通",
        "readableName": "全信通",
        "expressCode": "quanxintong"
      },
      {
        "express": "amazon-国际订单",
        "readableName": "amazon-国际订单",
        "expressCode": "amusorder"
      },
      {
        "express": "骏丰国际",
        "readableName": "骏丰国际",
        "expressCode": "junfengguoji"
      },
      {
        "express": "货运皇",
        "readableName": "货运皇",
        "expressCode": "kingfreight"
      },
      {
        "express": "速必达",
        "readableName": "速必达",
        "expressCode": "subida"
      },
      {
        "express": "特急便",
        "readableName": "特急便",
        "expressCode": "sucmj"
      },
      {
        "express": "亚马逊中国",
        "readableName": "亚马逊中国",
        "expressCode": "yamaxunwuliu"
      },
      {
        "express": "锦程",
        "readableName": "锦程",
        "expressCode": "jinchengwuliu"
      },
      {
        "express": "景光",
        "readableName": "景光",
        "expressCode": "jgwl"
      },
      {
        "express": "御风",
        "readableName": "御风",
        "expressCode": "yufeng"
      },
      {
        "express": "至诚通达",
        "readableName": "至诚通达",
        "expressCode": "zhichengtongda"
      },
      {
        "express": "日益通",
        "readableName": "日益通",
        "expressCode": "rytsd"
      },
      {
        "express": "航宇",
        "readableName": "航宇",
        "expressCode": "hangyu"
      },
      {
        "express": "急顺通",
        "readableName": "急顺通",
        "expressCode": "pzhjst"
      },
      {
        "express": "优速通达",
        "readableName": "优速通达",
        "expressCode": "yousutongda"
      },
      {
        "express": "秦远",
        "readableName": "秦远",
        "expressCode": "qinyuan"
      },
      {
        "express": "澳邮中国",
        "readableName": "澳邮中国",
        "expressCode": "auexpress"
      },
      {
        "express": "众辉达",
        "readableName": "众辉达",
        "expressCode": "zhdwl"
      },
      {
        "express": "飞邦",
        "readableName": "飞邦",
        "expressCode": "fbkd"
      },
      {
        "express": "华达",
        "readableName": "华达",
        "expressCode": "huada"
      },
      {
        "express": "FOX GLOBAL EXPRESS",
        "readableName": "FOX GLOBAL EXPRESS",
        "expressCode": "fox"
      },
      {
        "express": "环球",
        "readableName": "环球",
        "expressCode": "huanqiu"
      },
      {
        "express": "辉联",
        "readableName": "辉联",
        "expressCode": "huilian"
      },
      {
        "express": "A2U",
        "readableName": "A2U",
        "expressCode": "a2u"
      },
      {
        "express": "UEQ",
        "readableName": "UEQ",
        "expressCode": "ueq"
      },
      {
        "express": "中加国际",
        "readableName": "中加国际",
        "expressCode": "scic"
      },
      {
        "express": "易达通",
        "readableName": "易达通",
        "expressCode": "yidatong"
      },
      {
        "express": "捷网俄全通",
        "readableName": "捷网俄全通",
        "expressCode": "ruexp"
      },
      {
        "express": "华通务达",
        "readableName": "华通务达",
        "expressCode": "htwd"
      },
      {
        "express": "申必达",
        "readableName": "申必达",
        "expressCode": "speedoex"
      },
      {
        "express": "联运",
        "readableName": "联运",
        "expressCode": "lianyun"
      },
      {
        "express": "捷安达",
        "readableName": "捷安达",
        "expressCode": "jieanda"
      },
      {
        "express": "SHL畅灵国际",
        "readableName": "SHL畅灵国际",
        "expressCode": "shlexp"
      },
      {
        "express": "EWE全球",
        "readableName": "EWE全球",
        "expressCode": "ewe"
      },
      {
        "express": "全球",
        "readableName": "全球",
        "expressCode": "abcglobal"
      },
      {
        "express": "芒果",
        "readableName": "芒果",
        "expressCode": "mangguo"
      },
      {
        "express": "金海淘",
        "readableName": "金海淘",
        "expressCode": "goldhaitao"
      },
      {
        "express": "极光",
        "readableName": "极光",
        "expressCode": "jiguang"
      },
      {
        "express": "富腾达国际",
        "readableName": "富腾达国际",
        "expressCode": "ftd"
      },
      {
        "express": "DCS",
        "readableName": "DCS",
        "expressCode": "dcs"
      },
      {
        "express": "成达国际",
        "readableName": "成达国际",
        "expressCode": "chengda"
      },
      {
        "express": "中环",
        "readableName": "中环",
        "expressCode": "zhonghuan"
      },
      {
        "express": "顺邦国际",
        "readableName": "顺邦国际",
        "expressCode": "shunbang"
      },
      {
        "express": "启辰国际",
        "readableName": "启辰国际",
        "expressCode": "qichen"
      },
      {
        "express": "澳货通",
        "readableName": "澳货通",
        "expressCode": "auex"
      },
      {
        "express": "澳速",
        "readableName": "澳速",
        "expressCode": "aosu"
      },
      {
        "express": "澳世",
        "readableName": "澳世",
        "expressCode": "aus"
      },
      {
        "express": "天马迅达",
        "readableName": "天马迅达",
        "expressCode": "tianma"
      },
      {
        "express": "美龙快递",
        "readableName": "美龙快递",
        "expressCode": "mjexp"
      },
      {
        "express": "香港骏辉物流",
        "readableName": "香港骏辉物流",
        "expressCode": "chunfai"
      },
      {
        "express": "三三国际物流",
        "readableName": "三三国际物流",
        "expressCode": "zenzen"
      },
      {
        "express": "淼信快递",
        "readableName": "淼信快递",
        "expressCode": "mxe56"
      },
      {
        "express": "海派通",
        "readableName": "海派通",
        "expressCode": "hipito"
      },
      {
        "express": "鹏程快递",
        "readableName": "鹏程快递",
        "expressCode": "pengcheng"
      },
      {
        "express": "冠庭国际物流",
        "readableName": "冠庭国际物流",
        "expressCode": "guanting"
      },
      {
        "express": "金岸物流",
        "readableName": "金岸物流",
        "expressCode": "jinan"
      },
      {
        "express": "海带宝",
        "readableName": "海带宝",
        "expressCode": "haidaibao"
      },
      {
        "express": "澳通华人物流",
        "readableName": "澳通华人物流",
        "expressCode": "cllexpress"
      },
      {
        "express": "斑马物流",
        "readableName": "斑马物流",
        "expressCode": "banma"
      },
      {
        "express": "友家速递",
        "readableName": "友家速递",
        "expressCode": "youjia"
      },
      {
        "express": "百通物流",
        "readableName": "百通物流",
        "expressCode": "buytong"
      },
      {
        "express": "新元快递",
        "readableName": "新元快递",
        "expressCode": "xingyuankuaidi"
      },
      {
        "express": "全速物流",
        "readableName": "全速物流",
        "expressCode": "quansu"
      },
      {
        "express": "新杰物流",
        "readableName": "新杰物流",
        "expressCode": "sunjex"
      },
      {
        "express": "鲁通快运",
        "readableName": "鲁通快运",
        "expressCode": "lutong"
      },
      {
        "express": "新元国际",
        "readableName": "新元国际",
        "expressCode": "xynyc"
      },
      {
        "express": "小C海淘",
        "readableName": "小C海淘",
        "expressCode": "xiaocex"
      },
      {
        "express": "航空快递",
        "readableName": "航空快递",
        "expressCode": "airgtc"
      },
      {
        "express": "叮咚澳洲转运",
        "readableName": "叮咚澳洲转运",
        "expressCode": "dindon"
      },
      {
        "express": "环球通达",
        "readableName": "环球通达",
        "expressCode": "hqtd"
      },
      {
        "express": "好又快物流",
        "readableName": "好又快物流",
        "expressCode": "haoyoukuai"
      },
      {
        "express": "永旺达快递",
        "readableName": "永旺达快递",
        "expressCode": "yongwangda"
      },
      {
        "express": "木春货运",
        "readableName": "木春货运",
        "expressCode": "mchy"
      },
      {
        "express": "程光快递",
        "readableName": "程光快递",
        "expressCode": "flyway"
      },
      {
        "express": "全之鑫物流",
        "readableName": "全之鑫物流",
        "expressCode": "qzx56"
      },
      {
        "express": "百事亨通",
        "readableName": "百事亨通",
        "expressCode": "bsht"
      },
      {
        "express": "ILYANG",
        "readableName": "ILYANG",
        "expressCode": "ilyang"
      },
      {
        "express": "先锋快递",
        "readableName": "先锋快递",
        "expressCode": "xianfeng"
      },
      {
        "express": "万家通快递",
        "readableName": "万家通快递",
        "expressCode": "timedg"
      },
      {
        "express": "美快国际物流",
        "readableName": "美快国际物流",
        "expressCode": "meiquick"
      },
      {
        "express": "泰中物流",
        "readableName": "泰中物流",
        "expressCode": "tny"
      },
      {
        "express": "美通",
        "readableName": "美通",
        "expressCode": "valueway"
      },
      {
        "express": "新速航",
        "readableName": "新速航",
        "expressCode": "sunspeedy"
      },
      {
        "express": "速方",
        "readableName": "速方",
        "expressCode": "bphchina"
      }
    ];
  }
})();

