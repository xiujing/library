/**
 * Created by fanyaowu on 2016-7-5       .
 */
(function () {
  'use strict';

  angular
    .module('app')
    .service('fileUpload', fileUpload);

  /** @ngInject */
  function fileUpload(FileUploader, $log, alertService, restServer) {

    // 图片上传
    var pictureUploader = new FileUploader({
      // url: restServer + 'v3/mall/upload/picture',
      url: 'http://apilibrary.com/common/upload',
      autoUpload: true
    });

    this.pictureUploader = pictureUploader;

    // 视频上传
    var videoUploader = new FileUploader({
      url: 'http://apilibrary.com/common/upload',
      //url: restServer + 'v3/mall/upload/video',
      autoUpload: true
    });


    pictureUploader.onAfterAddingFile = function (fileItem) {
      $log.log('size:', fileItem._file.size / 1024 / 1024);
      if (fileItem._file.size / 1024 / 1024 > 2) {
        alertService.alert("图片大小超过2M！");
        fileItem.uploader.queue.splice(0, 1);
      }
      if (!/image/.test(fileItem.file.type)) {
        alertService.alert("格式错误", "只允许上传图片");
        fileItem.uploader.queue.splice(0, 1);
      }
      $log.info('onAfterAddingFile', fileItem);
    };

    this.videoUploader = videoUploader;

  }

})();
