(function () {
  'use strict';

  angular
    .module('app')
    .service('cityListService', cityListService);

  /** @ngInject */
  function cityListService() {
    this.cityList = [
      {
        "cityCode": "010",
        "cityName": "北京市",
        "readableName": "北京市",
        "shortName": "北京",
        "province": "北京",
        "cityPinyin": "Beijing"
      },
      {
        "cityCode": "020",
        "cityName": "广州市",
        "readableName": "广州市",
        "shortName": "广州",
        "province": "广东省",
        "cityPinyin": "Guangzhou"
      },
      {
        "cityCode": "021",
        "cityName": "上海市",
        "readableName": "上海市",
        "shortName": "上海",
        "province": "上海",
        "cityPinyin": "Shanghai"
      },
      {
        "cityCode": "022",
        "cityName": "天津市",
        "readableName": "天津市",
        "shortName": "天津",
        "province": "天津",
        "cityPinyin": "Tianjin"
      },
      {
        "cityCode": "023",
        "cityName": "重庆市",
        "readableName": "重庆市",
        "shortName": "重庆",
        "province": "重庆",
        "cityPinyin": "Chongqing"
      },
      {
        "cityCode": "024",
        "cityName": "沈阳市",
        "readableName": "沈阳市",
        "shortName": "沈阳",
        "province": "辽宁省",
        "cityPinyin": "Shenyang"
      },
      {
        "cityCode": "025",
        "cityName": "南京市",
        "readableName": "南京市",
        "shortName": "南京",
        "province": "江苏省",
        "cityPinyin": "Nanjing"
      },
      {
        "cityCode": "027",
        "cityName": "江岸区",
        "readableName": "江岸区",
        "shortName": "江岸",
        "province": "湖北省",
        "cityPinyin": "Jiang'an"
      },
      {
        "cityCode": "028",
        "cityName": "成都市",
        "readableName": "成都市",
        "shortName": "成都",
        "province": "四川省",
        "cityPinyin": "Chengdu"
      },
      {
        "cityCode": "029",
        "cityName": "西安市",
        "readableName": "西安市",
        "shortName": "西安",
        "province": "陕西省",
        "cityPinyin": "Xi'an"
      },
      {
        "cityCode": "0310",
        "cityName": "邯郸市",
        "readableName": "邯郸市",
        "shortName": "邯郸",
        "province": "河北省",
        "cityPinyin": "Handan"
      },
      {
        "cityCode": "0311",
        "cityName": "石家庄市",
        "readableName": "石家庄市",
        "shortName": "石家庄",
        "province": "河北省",
        "cityPinyin": "Shijiazhuang"
      },
      {
        "cityCode": "0312",
        "cityName": "保定市",
        "readableName": "保定市",
        "shortName": "保定",
        "province": "河北省",
        "cityPinyin": "Baoding"
      },
      {
        "cityCode": "0313",
        "cityName": "张家口市",
        "readableName": "张家口市",
        "shortName": "张家口",
        "province": "河北省",
        "cityPinyin": "Zhangjiakou"
      },
      {
        "cityCode": "0314",
        "cityName": "承德市",
        "readableName": "承德市",
        "shortName": "承德",
        "province": "河北省",
        "cityPinyin": "Chengde"
      },
      {
        "cityCode": "0315",
        "cityName": "唐山市",
        "readableName": "唐山市",
        "shortName": "唐山",
        "province": "河北省",
        "cityPinyin": "Tangshan"
      },
      {
        "cityCode": "0316",
        "cityName": "廊坊市",
        "readableName": "廊坊市",
        "shortName": "廊坊",
        "province": "河北省",
        "cityPinyin": "Langfang"
      },
      {
        "cityCode": "0317",
        "cityName": "沧州市",
        "readableName": "沧州市",
        "shortName": "沧州",
        "province": "河北省",
        "cityPinyin": "Cangzhou"
      },
      {
        "cityCode": "0318",
        "cityName": "衡水市",
        "readableName": "衡水市",
        "shortName": "衡水",
        "province": "河北省",
        "cityPinyin": "Hengshui"
      },
      {
        "cityCode": "0319",
        "cityName": "邢台市",
        "readableName": "邢台市",
        "shortName": "邢台",
        "province": "河北省",
        "cityPinyin": "Xingtai"
      },
      {
        "cityCode": "0335",
        "cityName": "秦皇岛市",
        "readableName": "秦皇岛市",
        "shortName": "秦皇岛",
        "province": "河北省",
        "cityPinyin": "Qinhuangdao"
      },
      {
        "cityCode": "0349",
        "cityName": "朔州市",
        "readableName": "朔州市",
        "shortName": "朔州",
        "province": "山西省",
        "cityPinyin": "Shuozhou"
      },
      {
        "cityCode": "0350",
        "cityName": "忻州市",
        "readableName": "忻州市",
        "shortName": "忻州",
        "province": "山西省",
        "cityPinyin": "Xinzhou"
      },
      {
        "cityCode": "0351",
        "cityName": "太原市",
        "readableName": "太原市",
        "shortName": "太原",
        "province": "山西省",
        "cityPinyin": "Taiyuan"
      },
      {
        "cityCode": "0352",
        "cityName": "大同市",
        "readableName": "大同市",
        "shortName": "大同",
        "province": "山西省",
        "cityPinyin": "Datong"
      },
      {
        "cityCode": "0353",
        "cityName": "阳泉市",
        "readableName": "阳泉市",
        "shortName": "阳泉",
        "province": "山西省",
        "cityPinyin": "Yangquan"
      },
      {
        "cityCode": "0354",
        "cityName": "晋中市",
        "readableName": "晋中市",
        "shortName": "晋中",
        "province": "山西省",
        "cityPinyin": "Jinzhong"
      },
      {
        "cityCode": "0355",
        "cityName": "长治市",
        "readableName": "长治市",
        "shortName": "长治",
        "province": "山西省",
        "cityPinyin": "Changzhi"
      },
      {
        "cityCode": "0356",
        "cityName": "晋城市",
        "readableName": "晋城市",
        "shortName": "晋城",
        "province": "山西省",
        "cityPinyin": "Jincheng"
      },
      {
        "cityCode": "0357",
        "cityName": "临汾市",
        "readableName": "临汾市",
        "shortName": "临汾",
        "province": "山西省",
        "cityPinyin": "Linfen"
      },
      {
        "cityCode": "0358",
        "cityName": "吕梁市",
        "readableName": "吕梁市",
        "shortName": "吕梁",
        "province": "山西省",
        "cityPinyin": "Lvliang"
      },
      {
        "cityCode": "0359",
        "cityName": "运城市",
        "readableName": "运城市",
        "shortName": "运城",
        "province": "山西省",
        "cityPinyin": "Yuncheng"
      },
      {
        "cityCode": "0370",
        "cityName": "商丘市",
        "readableName": "商丘市",
        "shortName": "商丘",
        "province": "河南省",
        "cityPinyin": "Shangqiu"
      },
      {
        "cityCode": "0371",
        "cityName": "郑州市",
        "readableName": "郑州市",
        "shortName": "郑州",
        "province": "河南省",
        "cityPinyin": "Zhengzhou"
      },
      {
        "cityCode": "0372",
        "cityName": "安阳市",
        "readableName": "安阳市",
        "shortName": "安阳",
        "province": "河南省",
        "cityPinyin": "Anyang"
      },
      {
        "cityCode": "0373",
        "cityName": "新乡市",
        "readableName": "新乡市",
        "shortName": "新乡",
        "province": "河南省",
        "cityPinyin": "Xinxiang"
      },
      {
        "cityCode": "0374",
        "cityName": "许昌市",
        "readableName": "许昌市",
        "shortName": "许昌",
        "province": "河南省",
        "cityPinyin": "Xuchang"
      },
      {
        "cityCode": "0375",
        "cityName": "平顶山市",
        "readableName": "平顶山市",
        "shortName": "平顶山",
        "province": "河南省",
        "cityPinyin": "Pingdingshan"
      },
      {
        "cityCode": "0376",
        "cityName": "信阳市",
        "readableName": "信阳市",
        "shortName": "信阳",
        "province": "河南省",
        "cityPinyin": "Xinyang"
      },
      {
        "cityCode": "0377",
        "cityName": "南阳市",
        "readableName": "南阳市",
        "shortName": "南阳",
        "province": "河南省",
        "cityPinyin": "Nanyang"
      },
      {
        "cityCode": "0378",
        "cityName": "开封市",
        "readableName": "开封市",
        "shortName": "开封",
        "province": "河南省",
        "cityPinyin": "Kaifeng"
      },
      {
        "cityCode": "0379",
        "cityName": "洛阳市",
        "readableName": "洛阳市",
        "shortName": "洛阳",
        "province": "河南省",
        "cityPinyin": "Luoyang"
      },
      {
        "cityCode": "0391",
        "cityName": "焦作市",
        "readableName": "焦作市",
        "shortName": "焦作",
        "province": "河南省",
        "cityPinyin": "Jiaozuo"
      },
      {
        "cityCode": "0392",
        "cityName": "鹤壁市",
        "readableName": "鹤壁市",
        "shortName": "鹤壁",
        "province": "河南省",
        "cityPinyin": "Hebi"
      },
      {
        "cityCode": "0393",
        "cityName": "濮阳市",
        "readableName": "濮阳市",
        "shortName": "濮阳",
        "province": "河南省",
        "cityPinyin": "Puyang"
      },
      {
        "cityCode": "0394",
        "cityName": "周口市",
        "readableName": "周口市",
        "shortName": "周口",
        "province": "河南省",
        "cityPinyin": "Zhoukou"
      },
      {
        "cityCode": "0395",
        "cityName": "漯河市",
        "readableName": "漯河市",
        "shortName": "漯河",
        "province": "河南省",
        "cityPinyin": "Luohe"
      },
      {
        "cityCode": "0396",
        "cityName": "驻马店市",
        "readableName": "驻马店市",
        "shortName": "驻马店",
        "province": "河南省",
        "cityPinyin": "Zhumadian"
      },
      {
        "cityCode": "0398",
        "cityName": "三门峡市",
        "readableName": "三门峡市",
        "shortName": "三门峡",
        "province": "河南省",
        "cityPinyin": "Sanmenxia"
      },
      {
        "cityCode": "0411",
        "cityName": "大连市",
        "readableName": "大连市",
        "shortName": "大连",
        "province": "辽宁省",
        "cityPinyin": "Dalian"
      },
      {
        "cityCode": "0412",
        "cityName": "鞍山市",
        "readableName": "鞍山市",
        "shortName": "鞍山",
        "province": "辽宁省",
        "cityPinyin": "Anshan"
      },
      {
        "cityCode": "0413",
        "cityName": "铁西区",
        "readableName": "铁西区",
        "shortName": "铁西",
        "province": "辽宁省",
        "cityPinyin": "Tiexi"
      },
      {
        "cityCode": "0414",
        "cityName": "立山区",
        "readableName": "立山区",
        "shortName": "立山",
        "province": "辽宁省",
        "cityPinyin": "Lishan"
      },
      {
        "cityCode": "0415",
        "cityName": "千山区",
        "readableName": "千山区",
        "shortName": "千山",
        "province": "辽宁省",
        "cityPinyin": "Qianshan"
      },
      {
        "cityCode": "0416",
        "cityName": "海城市",
        "readableName": "海城市",
        "shortName": "海城",
        "province": "辽宁省",
        "cityPinyin": "Haicheng"
      },
      {
        "cityCode": "0417",
        "cityName": "台安县",
        "readableName": "台安县",
        "shortName": "台安",
        "province": "辽宁省",
        "cityPinyin": "Tai'an"
      },
      {
        "cityCode": "0418",
        "cityName": "岫岩满族自治县",
        "readableName": "岫岩满族自治县",
        "shortName": "岫岩",
        "province": "辽宁省",
        "cityPinyin": "Xiuyan"
      },
      {
        "cityCode": "0419",
        "cityName": "辽阳市",
        "readableName": "辽阳市",
        "shortName": "辽阳",
        "province": "辽宁省",
        "cityPinyin": "Liaoyang"
      },
      {
        "cityCode": "0421",
        "cityName": "朝阳市",
        "readableName": "朝阳市",
        "shortName": "朝阳",
        "province": "辽宁省",
        "cityPinyin": "Chaoyang"
      },
      {
        "cityCode": "0427",
        "cityName": "盘锦市",
        "readableName": "盘锦市",
        "shortName": "盘锦",
        "province": "辽宁省",
        "cityPinyin": "Panjin"
      },
      {
        "cityCode": "0429",
        "cityName": "葫芦岛市",
        "readableName": "葫芦岛市",
        "shortName": "葫芦岛",
        "province": "辽宁省",
        "cityPinyin": "Huludao"
      },
      {
        "cityCode": "0431",
        "cityName": "长春市",
        "readableName": "长春市",
        "shortName": "长春",
        "province": "吉林省",
        "cityPinyin": "Changchun"
      },
      {
        "cityCode": "0432",
        "cityName": "吉林市",
        "readableName": "吉林市",
        "shortName": "吉林",
        "province": "吉林省",
        "cityPinyin": "Jilin"
      },
      {
        "cityCode": "0433",
        "cityName": "延边朝鲜族自治州",
        "readableName": "延边朝鲜族自治州",
        "shortName": "延边",
        "province": "吉林省",
        "cityPinyin": "Yanbian"
      },
      {
        "cityCode": "0434",
        "cityName": "四平市",
        "readableName": "四平市",
        "shortName": "四平",
        "province": "吉林省",
        "cityPinyin": "Siping"
      },
      {
        "cityCode": "0435",
        "cityName": "通化市",
        "readableName": "通化市",
        "shortName": "通化",
        "province": "吉林省",
        "cityPinyin": "Tonghua"
      },
      {
        "cityCode": "0436",
        "cityName": "白城市",
        "readableName": "白城市",
        "shortName": "白城",
        "province": "吉林省",
        "cityPinyin": "Baicheng"
      },
      {
        "cityCode": "0437",
        "cityName": "辽源市",
        "readableName": "辽源市",
        "shortName": "辽源",
        "province": "吉林省",
        "cityPinyin": "Liaoyuan"
      },
      {
        "cityCode": "0438",
        "cityName": "松原市",
        "readableName": "松原市",
        "shortName": "松原",
        "province": "吉林省",
        "cityPinyin": "Songyuan"
      },
      {
        "cityCode": "0439",
        "cityName": "白山市",
        "readableName": "白山市",
        "shortName": "白山",
        "province": "吉林省",
        "cityPinyin": "Baishan"
      },
      {
        "cityCode": "0451",
        "cityName": "哈尔滨市",
        "readableName": "哈尔滨市",
        "shortName": "哈尔滨",
        "province": "黑龙江省",
        "cityPinyin": "Harbin"
      },
      {
        "cityCode": "0452",
        "cityName": "齐齐哈尔市",
        "readableName": "齐齐哈尔市",
        "shortName": "齐齐哈尔",
        "province": "黑龙江省",
        "cityPinyin": "Qiqihar"
      },
      {
        "cityCode": "0453",
        "cityName": "牡丹江市",
        "readableName": "牡丹江市",
        "shortName": "牡丹江",
        "province": "黑龙江省",
        "cityPinyin": "Mudanjiang"
      },
      {
        "cityCode": "0454",
        "cityName": "佳木斯市",
        "readableName": "佳木斯市",
        "shortName": "佳木斯",
        "province": "黑龙江省",
        "cityPinyin": "Jiamusi"
      },
      {
        "cityCode": "0455",
        "cityName": "绥化市",
        "readableName": "绥化市",
        "shortName": "绥化",
        "province": "黑龙江省",
        "cityPinyin": "Suihua"
      },
      {
        "cityCode": "0456",
        "cityName": "黑河市",
        "readableName": "黑河市",
        "shortName": "黑河",
        "province": "黑龙江省",
        "cityPinyin": "Heihe"
      },
      {
        "cityCode": "0457",
        "cityName": "大兴安岭地区",
        "readableName": "大兴安岭地区",
        "shortName": "大兴安岭",
        "province": "黑龙江省",
        "cityPinyin": "DaXingAnLing"
      },
      {
        "cityCode": "0458",
        "cityName": "伊春市",
        "readableName": "伊春市",
        "shortName": "伊春",
        "province": "黑龙江省",
        "cityPinyin": "Yichun"
      },
      {
        "cityCode": "0459",
        "cityName": "大庆市",
        "readableName": "大庆市",
        "shortName": "大庆",
        "province": "黑龙江省",
        "cityPinyin": "Daqing"
      },
      {
        "cityCode": "0464",
        "cityName": "七台河市",
        "readableName": "七台河市",
        "shortName": "七台河",
        "province": "黑龙江省",
        "cityPinyin": "Qitaihe"
      },
      {
        "cityCode": "0467",
        "cityName": "鸡西市",
        "readableName": "鸡西市",
        "shortName": "鸡西",
        "province": "黑龙江省",
        "cityPinyin": "Jixi"
      },
      {
        "cityCode": "0468",
        "cityName": "鹤岗市",
        "readableName": "鹤岗市",
        "shortName": "鹤岗",
        "province": "黑龙江省",
        "cityPinyin": "Hegang"
      },
      {
        "cityCode": "0469",
        "cityName": "双鸭山市",
        "readableName": "双鸭山市",
        "shortName": "双鸭山",
        "province": "黑龙江省",
        "cityPinyin": "Shuangyashan"
      },
      {
        "cityCode": "0470",
        "cityName": "呼伦贝尔市",
        "readableName": "呼伦贝尔市",
        "shortName": "呼伦贝尔",
        "province": "内蒙古自治区",
        "cityPinyin": "Hulunber"
      },
      {
        "cityCode": "0471",
        "cityName": "呼和浩特市",
        "readableName": "呼和浩特市",
        "shortName": "呼和浩特",
        "province": "内蒙古自治区",
        "cityPinyin": "Hohhot"
      },
      {
        "cityCode": "0472",
        "cityName": "包头市",
        "readableName": "包头市",
        "shortName": "包头",
        "province": "内蒙古自治区",
        "cityPinyin": "Baotou"
      },
      {
        "cityCode": "0473",
        "cityName": "乌海市",
        "readableName": "乌海市",
        "shortName": "乌海",
        "province": "内蒙古自治区",
        "cityPinyin": "Wuhai"
      },
      {
        "cityCode": "0474",
        "cityName": "乌兰察布市",
        "readableName": "乌兰察布市",
        "shortName": "乌兰察布",
        "province": "内蒙古自治区",
        "cityPinyin": "Ulanqab"
      },
      {
        "cityCode": "0475",
        "cityName": "通辽市",
        "readableName": "通辽市",
        "shortName": "通辽",
        "province": "内蒙古自治区",
        "cityPinyin": "Tongliao"
      },
      {
        "cityCode": "0476",
        "cityName": "赤峰市",
        "readableName": "赤峰市",
        "shortName": "赤峰",
        "province": "内蒙古自治区",
        "cityPinyin": "Chifeng"
      },
      {
        "cityCode": "0477",
        "cityName": "鄂尔多斯市",
        "readableName": "鄂尔多斯市",
        "shortName": "鄂尔多斯",
        "province": "内蒙古自治区",
        "cityPinyin": "Ordos"
      },
      {
        "cityCode": "0478",
        "cityName": "巴彦淖尔市",
        "readableName": "巴彦淖尔市",
        "shortName": "巴彦淖尔",
        "province": "内蒙古自治区",
        "cityPinyin": "Bayan Nur"
      },
      {
        "cityCode": "0479",
        "cityName": "锡林郭勒盟",
        "readableName": "锡林郭勒盟",
        "shortName": "锡林郭勒盟",
        "province": "内蒙古自治区",
        "cityPinyin": "Xilin Gol"
      },
      {
        "cityCode": "0482",
        "cityName": "兴安盟",
        "readableName": "兴安盟",
        "shortName": "兴安盟",
        "province": "内蒙古自治区",
        "cityPinyin": "Hinggan"
      },
      {
        "cityCode": "0483",
        "cityName": "阿拉善盟",
        "readableName": "阿拉善盟",
        "shortName": "阿拉善盟",
        "province": "内蒙古自治区",
        "cityPinyin": "Alxa"
      },
      {
        "cityCode": "0510",
        "cityName": "无锡市",
        "readableName": "无锡市",
        "shortName": "无锡",
        "province": "江苏省",
        "cityPinyin": "Wuxi"
      },
      {
        "cityCode": "0511",
        "cityName": "镇江市",
        "readableName": "镇江市",
        "shortName": "镇江",
        "province": "江苏省",
        "cityPinyin": "Zhenjiang"
      },
      {
        "cityCode": "0512",
        "cityName": "苏州市",
        "readableName": "苏州市",
        "shortName": "苏州",
        "province": "江苏省",
        "cityPinyin": "Suzhou"
      },
      {
        "cityCode": "0513",
        "cityName": "南通市",
        "readableName": "南通市",
        "shortName": "南通",
        "province": "江苏省",
        "cityPinyin": "Nantong"
      },
      {
        "cityCode": "0514",
        "cityName": "扬州市",
        "readableName": "扬州市",
        "shortName": "扬州",
        "province": "江苏省",
        "cityPinyin": "Yangzhou"
      },
      {
        "cityCode": "0515",
        "cityName": "盐城市",
        "readableName": "盐城市",
        "shortName": "盐城",
        "province": "江苏省",
        "cityPinyin": "Yancheng"
      },
      {
        "cityCode": "0516",
        "cityName": "徐州市",
        "readableName": "徐州市",
        "shortName": "徐州",
        "province": "江苏省",
        "cityPinyin": "Xuzhou"
      },
      {
        "cityCode": "0517",
        "cityName": "淮安市",
        "readableName": "淮安市",
        "shortName": "淮安",
        "province": "江苏省",
        "cityPinyin": "Huai'an"
      },
      {
        "cityCode": "0518",
        "cityName": "连云港市",
        "readableName": "连云港市",
        "shortName": "连云港",
        "province": "江苏省",
        "cityPinyin": "Lianyungang"
      },
      {
        "cityCode": "0519",
        "cityName": "常州市",
        "readableName": "常州市",
        "shortName": "常州",
        "province": "江苏省",
        "cityPinyin": "Changzhou"
      },
      {
        "cityCode": "0523",
        "cityName": "泰州市",
        "readableName": "泰州市",
        "shortName": "泰州",
        "province": "江苏省",
        "cityPinyin": "Taizhou"
      },
      {
        "cityCode": "0527",
        "cityName": "宿迁市",
        "readableName": "宿迁市",
        "shortName": "宿迁",
        "province": "江苏省",
        "cityPinyin": "Suqian"
      },
      {
        "cityCode": "0530",
        "cityName": "菏泽市",
        "readableName": "菏泽市",
        "shortName": "菏泽",
        "province": "山东省",
        "cityPinyin": "Heze"
      },
      {
        "cityCode": "0531",
        "cityName": "济南市",
        "readableName": "济南市",
        "shortName": "济南",
        "province": "山东省",
        "cityPinyin": "Jinan"
      },
      {
        "cityCode": "0532",
        "cityName": "青岛市",
        "readableName": "青岛市",
        "shortName": "青岛",
        "province": "山东省",
        "cityPinyin": "Qingdao"
      },
      {
        "cityCode": "0533",
        "cityName": "淄博市",
        "readableName": "淄博市",
        "shortName": "淄博",
        "province": "山东省",
        "cityPinyin": "Zibo"
      },
      {
        "cityCode": "0534",
        "cityName": "德州市",
        "readableName": "德州市",
        "shortName": "德州",
        "province": "山东省",
        "cityPinyin": "Dezhou"
      },
      {
        "cityCode": "0536",
        "cityName": "潍坊市",
        "readableName": "潍坊市",
        "shortName": "潍坊",
        "province": "山东省",
        "cityPinyin": "Weifang"
      },
      {
        "cityCode": "0537",
        "cityName": "济宁市",
        "readableName": "济宁市",
        "shortName": "济宁",
        "province": "山东省",
        "cityPinyin": "Jining"
      },
      {
        "cityCode": "0538",
        "cityName": "泰安市",
        "readableName": "泰安市",
        "shortName": "泰安",
        "province": "山东省",
        "cityPinyin": "Tai'an"
      },
      {
        "cityCode": "0539",
        "cityName": "临沂市",
        "readableName": "临沂市",
        "shortName": "临沂",
        "province": "山东省",
        "cityPinyin": "Linyi"
      },
      {
        "cityCode": "0543",
        "cityName": "滨州市",
        "readableName": "滨州市",
        "shortName": "滨州",
        "province": "山东省",
        "cityPinyin": "Binzhou"
      },
      {
        "cityCode": "0546",
        "cityName": "东营市",
        "readableName": "东营市",
        "shortName": "东营",
        "province": "山东省",
        "cityPinyin": "Dongying"
      },
      {
        "cityCode": "0550",
        "cityName": "滁州市",
        "readableName": "滁州市",
        "shortName": "滁州",
        "province": "安徽省",
        "cityPinyin": "Chuzhou"
      },
      {
        "cityCode": "0551",
        "cityName": "合肥市",
        "readableName": "合肥市",
        "shortName": "合肥",
        "province": "安徽省",
        "cityPinyin": "Hefei"
      },
      {
        "cityCode": "0552",
        "cityName": "蚌埠市",
        "readableName": "蚌埠市",
        "shortName": "蚌埠",
        "province": "安徽省",
        "cityPinyin": "Bengbu"
      },
      {
        "cityCode": "0553",
        "cityName": "镜湖区",
        "readableName": "镜湖区",
        "shortName": "镜湖",
        "province": "安徽省",
        "cityPinyin": "Jinghu"
      },
      {
        "cityCode": "0554",
        "cityName": "淮南市",
        "readableName": "淮南市",
        "shortName": "淮南",
        "province": "安徽省",
        "cityPinyin": "Huainan"
      },
      {
        "cityCode": "0555",
        "cityName": "马鞍山市",
        "readableName": "马鞍山市",
        "shortName": "马鞍山",
        "province": "安徽省",
        "cityPinyin": "Ma'anshan"
      },
      {
        "cityCode": "0556",
        "cityName": "安庆市",
        "readableName": "安庆市",
        "shortName": "安庆",
        "province": "安徽省",
        "cityPinyin": "Anqing"
      },
      {
        "cityCode": "0557",
        "cityName": "宿州市",
        "readableName": "宿州市",
        "shortName": "宿州",
        "province": "安徽省",
        "cityPinyin": "Suzhou"
      },
      {
        "cityCode": "0558",
        "cityName": "阜阳市",
        "readableName": "阜阳市",
        "shortName": "阜阳",
        "province": "安徽省",
        "cityPinyin": "Fuyang"
      },
      {
        "cityCode": "0559",
        "cityName": "黄山市",
        "readableName": "黄山市",
        "shortName": "黄山",
        "province": "安徽省",
        "cityPinyin": "Huangshan"
      },
      {
        "cityCode": "0561",
        "cityName": "淮北市",
        "readableName": "淮北市",
        "shortName": "淮北",
        "province": "安徽省",
        "cityPinyin": "Huaibei"
      },
      {
        "cityCode": "0562",
        "cityName": "铜陵市",
        "readableName": "铜陵市",
        "shortName": "铜陵",
        "province": "安徽省",
        "cityPinyin": "Tongling"
      },
      {
        "cityCode": "0563",
        "cityName": "宣城市",
        "readableName": "宣城市",
        "shortName": "宣城",
        "province": "安徽省",
        "cityPinyin": "Xuancheng"
      },
      {
        "cityCode": "0564",
        "cityName": "六安市",
        "readableName": "六安市",
        "shortName": "六安",
        "province": "安徽省",
        "cityPinyin": "Lu'an"
      },
      {
        "cityCode": "0565",
        "cityName": "庐江县",
        "readableName": "庐江县",
        "shortName": "庐江",
        "province": "安徽省",
        "cityPinyin": "Lujiang"
      },
      {
        "cityCode": "0566",
        "cityName": "池州市",
        "readableName": "池州市",
        "shortName": "池州",
        "province": "安徽省",
        "cityPinyin": "Chizhou"
      },
      {
        "cityCode": "0570",
        "cityName": "衢州市",
        "readableName": "衢州市",
        "shortName": "衢州",
        "province": "浙江省",
        "cityPinyin": "Quzhou"
      },
      {
        "cityCode": "0571",
        "cityName": "杭州市",
        "readableName": "杭州市",
        "shortName": "杭州",
        "province": "浙江省",
        "cityPinyin": "Hangzhou"
      },
      {
        "cityCode": "0572",
        "cityName": "湖州市",
        "readableName": "湖州市",
        "shortName": "湖州",
        "province": "浙江省",
        "cityPinyin": "Huzhou"
      },
      {
        "cityCode": "0573",
        "cityName": "嘉兴市",
        "readableName": "嘉兴市",
        "shortName": "嘉兴",
        "province": "浙江省",
        "cityPinyin": "Jiaxing"
      },
      {
        "cityCode": "0574",
        "cityName": "宁波市",
        "readableName": "宁波市",
        "shortName": "宁波",
        "province": "浙江省",
        "cityPinyin": "Ningbo"
      },
      {
        "cityCode": "0575",
        "cityName": "绍兴市",
        "readableName": "绍兴市",
        "shortName": "绍兴",
        "province": "浙江省",
        "cityPinyin": "Shaoxing"
      },
      {
        "cityCode": "0576",
        "cityName": "台州市",
        "readableName": "台州市",
        "shortName": "台州",
        "province": "浙江省",
        "cityPinyin": "Taizhou"
      },
      {
        "cityCode": "0577",
        "cityName": "温州市",
        "readableName": "温州市",
        "shortName": "温州",
        "province": "浙江省",
        "cityPinyin": "Wenzhou"
      },
      {
        "cityCode": "0578",
        "cityName": "丽水市",
        "readableName": "丽水市",
        "shortName": "丽水",
        "province": "浙江省",
        "cityPinyin": "Lishui"
      },
      {
        "cityCode": "0579",
        "cityName": "金华市",
        "readableName": "金华市",
        "shortName": "金华",
        "province": "浙江省",
        "cityPinyin": "Jinhua"
      },
      {
        "cityCode": "0580",
        "cityName": "舟山市",
        "readableName": "舟山市",
        "shortName": "舟山",
        "province": "浙江省",
        "cityPinyin": "Zhoushan"
      },
      {
        "cityCode": "0591",
        "cityName": "福州市",
        "readableName": "福州市",
        "shortName": "福州",
        "province": "福建省",
        "cityPinyin": "Fuzhou"
      },
      {
        "cityCode": "0592",
        "cityName": "厦门市",
        "readableName": "厦门市",
        "shortName": "厦门",
        "province": "福建省",
        "cityPinyin": "Xiamen"
      },
      {
        "cityCode": "0593",
        "cityName": "宁德市",
        "readableName": "宁德市",
        "shortName": "宁德",
        "province": "福建省",
        "cityPinyin": "Ningde"
      },
      {
        "cityCode": "0594",
        "cityName": "莆田市",
        "readableName": "莆田市",
        "shortName": "莆田",
        "province": "福建省",
        "cityPinyin": "Putian"
      },
      {
        "cityCode": "0595",
        "cityName": "泉州市",
        "readableName": "泉州市",
        "shortName": "泉州",
        "province": "福建省",
        "cityPinyin": "Quanzhou"
      },
      {
        "cityCode": "0596",
        "cityName": "漳州市",
        "readableName": "漳州市",
        "shortName": "漳州",
        "province": "福建省",
        "cityPinyin": "Zhangzhou"
      },
      {
        "cityCode": "0597",
        "cityName": "龙岩市",
        "readableName": "龙岩市",
        "shortName": "龙岩",
        "province": "福建省",
        "cityPinyin": "Longyan"
      },
      {
        "cityCode": "0598",
        "cityName": "三明市",
        "readableName": "三明市",
        "shortName": "三明",
        "province": "福建省",
        "cityPinyin": "Sanming"
      },
      {
        "cityCode": "0599",
        "cityName": "南平市",
        "readableName": "南平市",
        "shortName": "南平",
        "province": "福建省",
        "cityPinyin": "Nanping"
      },
      {
        "cityCode": "0600",
        "cityName": "延平区",
        "readableName": "延平区",
        "shortName": "延平",
        "province": "福建省",
        "cityPinyin": "Yanping"
      },
      {
        "cityCode": "0601",
        "cityName": "邵武市",
        "readableName": "邵武市",
        "shortName": "邵武",
        "province": "福建省",
        "cityPinyin": "Shaowu"
      },
      {
        "cityCode": "0602",
        "cityName": "武夷山市",
        "readableName": "武夷山市",
        "shortName": "武夷山",
        "province": "福建省",
        "cityPinyin": "Wuyishan"
      },
      {
        "cityCode": "0603",
        "cityName": "建瓯市",
        "readableName": "建瓯市",
        "shortName": "建瓯",
        "province": "福建省",
        "cityPinyin": "Jianou"
      },
      {
        "cityCode": "0605",
        "cityName": "顺昌县",
        "readableName": "顺昌县",
        "shortName": "顺昌",
        "province": "福建省",
        "cityPinyin": "Shunchang"
      },
      {
        "cityCode": "0606",
        "cityName": "浦城县",
        "readableName": "浦城县",
        "shortName": "浦城",
        "province": "福建省",
        "cityPinyin": "Pucheng"
      },
      {
        "cityCode": "0607",
        "cityName": "光泽县",
        "readableName": "光泽县",
        "shortName": "光泽",
        "province": "福建省",
        "cityPinyin": "Guangze"
      },
      {
        "cityCode": "0608",
        "cityName": "松溪县",
        "readableName": "松溪县",
        "shortName": "松溪",
        "province": "福建省",
        "cityPinyin": "Songxi"
      },
      {
        "cityCode": "0609",
        "cityName": "政和县",
        "readableName": "政和县",
        "shortName": "政和",
        "province": "福建省",
        "cityPinyin": "Zhenghe"
      },
      {
        "cityCode": "0631",
        "cityName": "威海市",
        "readableName": "威海市",
        "shortName": "威海",
        "province": "山东省",
        "cityPinyin": "Weihai"
      },
      {
        "cityCode": "0632",
        "cityName": "枣庄市",
        "readableName": "枣庄市",
        "shortName": "枣庄",
        "province": "山东省",
        "cityPinyin": "Zaozhuang"
      },
      {
        "cityCode": "0633",
        "cityName": "日照市",
        "readableName": "日照市",
        "shortName": "日照",
        "province": "山东省",
        "cityPinyin": "Rizhao"
      },
      {
        "cityCode": "0634",
        "cityName": "莱芜市",
        "readableName": "莱芜市",
        "shortName": "莱芜",
        "province": "山东省",
        "cityPinyin": "Laiwu"
      },
      {
        "cityCode": "0635",
        "cityName": "烟台市",
        "readableName": "烟台市",
        "shortName": "烟台",
        "province": "山东省",
        "cityPinyin": "Yantai"
      },
      {
        "cityCode": "0660",
        "cityName": "汕尾市",
        "readableName": "汕尾市",
        "shortName": "汕尾",
        "province": "广东省",
        "cityPinyin": "Shanwei"
      },
      {
        "cityCode": "0662",
        "cityName": "阳江市",
        "readableName": "阳江市",
        "shortName": "阳江",
        "province": "广东省",
        "cityPinyin": "Yangjiang"
      },
      {
        "cityCode": "0668",
        "cityName": "茂名市",
        "readableName": "茂名市",
        "shortName": "茂名",
        "province": "广东省",
        "cityPinyin": "Maoming"
      },
      {
        "cityCode": "0691",
        "cityName": "西双版纳傣族自治州",
        "readableName": "西双版纳傣族自治州",
        "shortName": "西双版纳",
        "province": "云南省",
        "cityPinyin": "Xishuangbanna"
      },
      {
        "cityCode": "0692",
        "cityName": "德宏傣族景颇族自治州",
        "readableName": "德宏傣族景颇族自治州",
        "shortName": "德宏",
        "province": "云南省",
        "cityPinyin": "Dehong"
      },
      {
        "cityCode": "0701",
        "cityName": "鹰潭市",
        "readableName": "鹰潭市",
        "shortName": "鹰潭",
        "province": "江西省",
        "cityPinyin": "Yingtan"
      },
      {
        "cityCode": "0710",
        "cityName": "襄阳市",
        "readableName": "襄阳市",
        "shortName": "襄阳",
        "province": "湖北省",
        "cityPinyin": "Xiangyang"
      },
      {
        "cityCode": "0711",
        "cityName": "鄂州市",
        "readableName": "鄂州市",
        "shortName": "鄂州",
        "province": "湖北省",
        "cityPinyin": "Ezhou"
      },
      {
        "cityCode": "0712",
        "cityName": "孝感市",
        "readableName": "孝感市",
        "shortName": "孝感",
        "province": "湖北省",
        "cityPinyin": "Xiaogan"
      },
      {
        "cityCode": "0713",
        "cityName": "黄冈市",
        "readableName": "黄冈市",
        "shortName": "黄冈",
        "province": "湖北省",
        "cityPinyin": "Huanggang"
      },
      {
        "cityCode": "0714",
        "cityName": "黄石市",
        "readableName": "黄石市",
        "shortName": "黄石",
        "province": "湖北省",
        "cityPinyin": "Huangshi"
      },
      {
        "cityCode": "0715",
        "cityName": "咸宁市",
        "readableName": "咸宁市",
        "shortName": "咸宁",
        "province": "湖北省",
        "cityPinyin": "Xianning"
      },
      {
        "cityCode": "0716",
        "cityName": "荆州市",
        "readableName": "荆州市",
        "shortName": "荆州",
        "province": "湖北省",
        "cityPinyin": "Jingzhou"
      },
      {
        "cityCode": "0717",
        "cityName": "宜昌市",
        "readableName": "宜昌市",
        "shortName": "宜昌",
        "province": "湖北省",
        "cityPinyin": "Yichang"
      },
      {
        "cityCode": "0718",
        "cityName": "恩施土家族苗族自治州",
        "readableName": "恩施土家族苗族自治州",
        "shortName": "恩施",
        "province": "湖北省",
        "cityPinyin": "Enshi"
      },
      {
        "cityCode": "0719",
        "cityName": "十堰市",
        "readableName": "十堰市",
        "shortName": "十堰",
        "province": "湖北省",
        "cityPinyin": "Shiyan"
      },
      {
        "cityCode": "0722",
        "cityName": "随州市",
        "readableName": "随州市",
        "shortName": "随州",
        "province": "湖北省",
        "cityPinyin": "Suizhou"
      },
      {
        "cityCode": "0724",
        "cityName": "荆门市",
        "readableName": "荆门市",
        "shortName": "荆门",
        "province": "湖北省",
        "cityPinyin": "Jingmen"
      },
      {
        "cityCode": "0728",
        "cityName": "仙桃市",
        "readableName": "仙桃市",
        "shortName": "仙桃",
        "province": "湖北省",
        "cityPinyin": "Xiantao"
      },
      {
        "cityCode": "0730",
        "cityName": "岳阳市",
        "readableName": "岳阳市",
        "shortName": "岳阳",
        "province": "湖南省",
        "cityPinyin": "Yueyang"
      },
      {
        "cityCode": "0731",
        "cityName": "长沙市",
        "readableName": "长沙市",
        "shortName": "长沙",
        "province": "湖南省",
        "cityPinyin": "Changsha"
      },
      {
        "cityCode": "0734",
        "cityName": "衡阳市",
        "readableName": "衡阳市",
        "shortName": "衡阳",
        "province": "湖南省",
        "cityPinyin": "Hengyang"
      },
      {
        "cityCode": "0735",
        "cityName": "郴州市",
        "readableName": "郴州市",
        "shortName": "郴州",
        "province": "湖南省",
        "cityPinyin": "Chenzhou"
      },
      {
        "cityCode": "0736",
        "cityName": "常德市",
        "readableName": "常德市",
        "shortName": "常德",
        "province": "湖南省",
        "cityPinyin": "Changde"
      },
      {
        "cityCode": "0737",
        "cityName": "益阳市",
        "readableName": "益阳市",
        "shortName": "益阳",
        "province": "湖南省",
        "cityPinyin": "Yiyang"
      },
      {
        "cityCode": "0738",
        "cityName": "娄底市",
        "readableName": "娄底市",
        "shortName": "娄底",
        "province": "湖南省",
        "cityPinyin": "Loudi"
      },
      {
        "cityCode": "0739",
        "cityName": "邵阳市",
        "readableName": "邵阳市",
        "shortName": "邵阳",
        "province": "湖南省",
        "cityPinyin": "Shaoyang"
      },
      {
        "cityCode": "0743",
        "cityName": "湘西土家族苗族自治州",
        "readableName": "湘西土家族苗族自治州",
        "shortName": "湘西",
        "province": "湖南省",
        "cityPinyin": "Xiangxi"
      },
      {
        "cityCode": "0744",
        "cityName": "张家界市",
        "readableName": "张家界市",
        "shortName": "张家界",
        "province": "湖南省",
        "cityPinyin": "Zhangjiajie"
      },
      {
        "cityCode": "0745",
        "cityName": "怀化市",
        "readableName": "怀化市",
        "shortName": "怀化",
        "province": "湖南省",
        "cityPinyin": "Huaihua"
      },
      {
        "cityCode": "0746",
        "cityName": "永州市",
        "readableName": "永州市",
        "shortName": "永州",
        "province": "湖南省",
        "cityPinyin": "Yongzhou"
      },
      {
        "cityCode": "0750",
        "cityName": "江门市",
        "readableName": "江门市",
        "shortName": "江门",
        "province": "广东省",
        "cityPinyin": "Jiangmen"
      },
      {
        "cityCode": "0751",
        "cityName": "韶关市",
        "readableName": "韶关市",
        "shortName": "韶关",
        "province": "广东省",
        "cityPinyin": "Shaoguan"
      },
      {
        "cityCode": "0752",
        "cityName": "惠州市",
        "readableName": "惠州市",
        "shortName": "惠州",
        "province": "广东省",
        "cityPinyin": "Huizhou"
      },
      {
        "cityCode": "0753",
        "cityName": "梅州市",
        "readableName": "梅州市",
        "shortName": "梅州",
        "province": "广东省",
        "cityPinyin": "Meizhou"
      },
      {
        "cityCode": "0754",
        "cityName": "汕头市",
        "readableName": "汕头市",
        "shortName": "汕头",
        "province": "广东省",
        "cityPinyin": "Shantou"
      },
      {
        "cityCode": "0755",
        "cityName": "深圳市",
        "readableName": "深圳市",
        "shortName": "深圳",
        "province": "广东省",
        "cityPinyin": "Shenzhen"
      },
      {
        "cityCode": "0756",
        "cityName": "珠海市",
        "readableName": "珠海市",
        "shortName": "珠海",
        "province": "广东省",
        "cityPinyin": "Zhuhai"
      },
      {
        "cityCode": "0757",
        "cityName": "佛山市",
        "readableName": "佛山市",
        "shortName": "佛山",
        "province": "广东省",
        "cityPinyin": "Foshan"
      },
      {
        "cityCode": "0758",
        "cityName": "肇庆市",
        "readableName": "肇庆市",
        "shortName": "肇庆",
        "province": "广东省",
        "cityPinyin": "Zhaoqing"
      },
      {
        "cityCode": "0759",
        "cityName": "湛江市",
        "readableName": "湛江市",
        "shortName": "湛江",
        "province": "广东省",
        "cityPinyin": "Zhanjiang"
      },
      {
        "cityCode": "0760",
        "cityName": "中山市",
        "readableName": "中山市",
        "shortName": "中山",
        "province": "广东省",
        "cityPinyin": "Zhongshan"
      },
      {
        "cityCode": "0762",
        "cityName": "河源市",
        "readableName": "河源市",
        "shortName": "河源",
        "province": "广东省",
        "cityPinyin": "Heyuan"
      },
      {
        "cityCode": "0763",
        "cityName": "清远市",
        "readableName": "清远市",
        "shortName": "清远",
        "province": "广东省",
        "cityPinyin": "Qingyuan"
      },
      {
        "cityCode": "0766",
        "cityName": "云浮市",
        "readableName": "云浮市",
        "shortName": "云浮",
        "province": "广东省",
        "cityPinyin": "Yunfu"
      },
      {
        "cityCode": "0768",
        "cityName": "潮州市",
        "readableName": "潮州市",
        "shortName": "潮州",
        "province": "广东省",
        "cityPinyin": "Chaozhou"
      },
      {
        "cityCode": "0769",
        "cityName": "东莞市",
        "readableName": "东莞市",
        "shortName": "东莞",
        "province": "广东省",
        "cityPinyin": "Dongguan"
      },
      {
        "cityCode": "0770",
        "cityName": "防城港市",
        "readableName": "防城港市",
        "shortName": "防城港",
        "province": "广西壮族自治区",
        "cityPinyin": "Fangchenggang"
      },
      {
        "cityCode": "0771",
        "cityName": "南宁市",
        "readableName": "南宁市",
        "shortName": "南宁",
        "province": "广西壮族自治区",
        "cityPinyin": "Nanning"
      },
      {
        "cityCode": "0772",
        "cityName": "柳州市",
        "readableName": "柳州市",
        "shortName": "柳州",
        "province": "广西壮族自治区",
        "cityPinyin": "Liuzhou"
      },
      {
        "cityCode": "0773",
        "cityName": "桂林市",
        "readableName": "桂林市",
        "shortName": "桂林",
        "province": "广西壮族自治区",
        "cityPinyin": "Guilin"
      },
      {
        "cityCode": "0774",
        "cityName": "梧州市",
        "readableName": "梧州市",
        "shortName": "梧州",
        "province": "广西壮族自治区",
        "cityPinyin": "Wuzhou"
      },
      {
        "cityCode": "0775",
        "cityName": "贵港市",
        "readableName": "贵港市",
        "shortName": "贵港",
        "province": "广西壮族自治区",
        "cityPinyin": "Guigang"
      },
      {
        "cityCode": "0776",
        "cityName": "百色市",
        "readableName": "百色市",
        "shortName": "百色",
        "province": "广西壮族自治区",
        "cityPinyin": "Baise"
      },
      {
        "cityCode": "0777",
        "cityName": "钦州市",
        "readableName": "钦州市",
        "shortName": "钦州",
        "province": "广西壮族自治区",
        "cityPinyin": "Qinzhou"
      },
      {
        "cityCode": "0778",
        "cityName": "河池市",
        "readableName": "河池市",
        "shortName": "河池",
        "province": "广西壮族自治区",
        "cityPinyin": "Hechi"
      },
      {
        "cityCode": "0779",
        "cityName": "北海市",
        "readableName": "北海市",
        "shortName": "北海",
        "province": "广西壮族自治区",
        "cityPinyin": "Beihai"
      },
      {
        "cityCode": "0780",
        "cityName": "宜州市",
        "readableName": "宜州市",
        "shortName": "宜州",
        "province": "广西壮族自治区",
        "cityPinyin": "Yizhou"
      },
      {
        "cityCode": "0781",
        "cityName": "南丹县",
        "readableName": "南丹县",
        "shortName": "南丹",
        "province": "广西壮族自治区",
        "cityPinyin": "Nandan"
      },
      {
        "cityCode": "0782",
        "cityName": "天峨县",
        "readableName": "天峨县",
        "shortName": "天峨",
        "province": "广西壮族自治区",
        "cityPinyin": "Tiane"
      },
      {
        "cityCode": "0783",
        "cityName": "凤山县",
        "readableName": "凤山县",
        "shortName": "凤山",
        "province": "广西壮族自治区",
        "cityPinyin": "Fengshan"
      },
      {
        "cityCode": "0784",
        "cityName": "东兰县",
        "readableName": "东兰县",
        "shortName": "东兰",
        "province": "广西壮族自治区",
        "cityPinyin": "Donglan"
      },
      {
        "cityCode": "0785",
        "cityName": "罗城仫佬族自治县",
        "readableName": "罗城仫佬族自治县",
        "shortName": "罗城",
        "province": "广西壮族自治区",
        "cityPinyin": "Luocheng"
      },
      {
        "cityCode": "0786",
        "cityName": "环江毛南族自治县",
        "readableName": "环江毛南族自治县",
        "shortName": "环江",
        "province": "广西壮族自治区",
        "cityPinyin": "Huanjiang"
      },
      {
        "cityCode": "0787",
        "cityName": "巴马瑶族自治县",
        "readableName": "巴马瑶族自治县",
        "shortName": "巴马",
        "province": "广西壮族自治区",
        "cityPinyin": "Bama"
      },
      {
        "cityCode": "0788",
        "cityName": "都安瑶族自治县",
        "readableName": "都安瑶族自治县",
        "shortName": "都安",
        "province": "广西壮族自治区",
        "cityPinyin": "Du'an"
      },
      {
        "cityCode": "0789",
        "cityName": "大化瑶族自治县",
        "readableName": "大化瑶族自治县",
        "shortName": "大化",
        "province": "广西壮族自治区",
        "cityPinyin": "Dahua"
      },
      {
        "cityCode": "0790",
        "cityName": "新余市",
        "readableName": "新余市",
        "shortName": "新余",
        "province": "江西省",
        "cityPinyin": "Xinyu"
      },
      {
        "cityCode": "0791",
        "cityName": "南昌市",
        "readableName": "南昌市",
        "shortName": "南昌",
        "province": "江西省",
        "cityPinyin": "Nanchang"
      },
      {
        "cityCode": "0792",
        "cityName": "九江市",
        "readableName": "九江市",
        "shortName": "九江",
        "province": "江西省",
        "cityPinyin": "Jiujiang"
      },
      {
        "cityCode": "0793",
        "cityName": "上饶市",
        "readableName": "上饶市",
        "shortName": "上饶",
        "province": "江西省",
        "cityPinyin": "Shangrao"
      },
      {
        "cityCode": "0794",
        "cityName": "抚州市",
        "readableName": "抚州市",
        "shortName": "抚州",
        "province": "江西省",
        "cityPinyin": "Fuzhou"
      },
      {
        "cityCode": "0795",
        "cityName": "宜春市",
        "readableName": "宜春市",
        "shortName": "宜春",
        "province": "江西省",
        "cityPinyin": "Yichun"
      },
      {
        "cityCode": "0796",
        "cityName": "吉安市",
        "readableName": "吉安市",
        "shortName": "吉安",
        "province": "江西省",
        "cityPinyin": "Ji'an"
      },
      {
        "cityCode": "0797",
        "cityName": "赣州市",
        "readableName": "赣州市",
        "shortName": "赣州",
        "province": "江西省",
        "cityPinyin": "Ganzhou"
      },
      {
        "cityCode": "0798",
        "cityName": "景德镇市",
        "readableName": "景德镇市",
        "shortName": "景德镇",
        "province": "江西省",
        "cityPinyin": "Jingdezhen"
      },
      {
        "cityCode": "0799",
        "cityName": "昌江区",
        "readableName": "昌江区",
        "shortName": "昌江",
        "province": "江西省",
        "cityPinyin": "Changjiang"
      },
      {
        "cityCode": "0800",
        "cityName": "珠山区",
        "readableName": "珠山区",
        "shortName": "珠山",
        "province": "江西省",
        "cityPinyin": "Zhushan"
      },
      {
        "cityCode": "0801",
        "cityName": "乐平市",
        "readableName": "乐平市",
        "shortName": "乐平",
        "province": "江西省",
        "cityPinyin": "Leping"
      },
      {
        "cityCode": "0802",
        "cityName": "浮梁县",
        "readableName": "浮梁县",
        "shortName": "浮梁",
        "province": "江西省",
        "cityPinyin": "Fuliang"
      },
      {
        "cityCode": "0803",
        "cityName": "上栗县",
        "readableName": "上栗县",
        "shortName": "上栗",
        "province": "江西省",
        "cityPinyin": "Shangli"
      },
      {
        "cityCode": "0804",
        "cityName": "芦溪县",
        "readableName": "芦溪县",
        "shortName": "芦溪",
        "province": "江西省",
        "cityPinyin": "Luxi"
      },
      {
        "cityCode": "0812",
        "cityName": "攀枝花市",
        "readableName": "攀枝花市",
        "shortName": "攀枝花",
        "province": "四川省",
        "cityPinyin": "Panzhihua"
      },
      {
        "cityCode": "0813",
        "cityName": "自贡市",
        "readableName": "自贡市",
        "shortName": "自贡",
        "province": "四川省",
        "cityPinyin": "Zigong"
      },
      {
        "cityCode": "0816",
        "cityName": "绵阳市",
        "readableName": "绵阳市",
        "shortName": "绵阳",
        "province": "四川省",
        "cityPinyin": "Mianyang"
      },
      {
        "cityCode": "0817",
        "cityName": "南充市",
        "readableName": "南充市",
        "shortName": "南充",
        "province": "四川省",
        "cityPinyin": "Nanchong"
      },
      {
        "cityCode": "0818",
        "cityName": "达州市",
        "readableName": "达州市",
        "shortName": "达州",
        "province": "四川省",
        "cityPinyin": "Dazhou"
      },
      {
        "cityCode": "0825",
        "cityName": "遂宁市",
        "readableName": "遂宁市",
        "shortName": "遂宁",
        "province": "四川省",
        "cityPinyin": "Suining"
      },
      {
        "cityCode": "0826",
        "cityName": "广安市",
        "readableName": "广安市",
        "shortName": "广安",
        "province": "四川省",
        "cityPinyin": "Guang'an"
      },
      {
        "cityCode": "0827",
        "cityName": "巴中市",
        "readableName": "巴中市",
        "shortName": "巴中",
        "province": "四川省",
        "cityPinyin": "Bazhong"
      },
      {
        "cityCode": "0830",
        "cityName": "泸州市",
        "readableName": "泸州市",
        "shortName": "泸州",
        "province": "四川省",
        "cityPinyin": "Luzhou"
      },
      {
        "cityCode": "0831",
        "cityName": "宜宾市",
        "readableName": "宜宾市",
        "shortName": "宜宾",
        "province": "四川省",
        "cityPinyin": "Yibin"
      },
      {
        "cityCode": "0832",
        "cityName": "内江市",
        "readableName": "内江市",
        "shortName": "内江",
        "province": "四川省",
        "cityPinyin": "Neijiang"
      },
      {
        "cityCode": "0833",
        "cityName": "乐山市",
        "readableName": "乐山市",
        "shortName": "乐山",
        "province": "四川省",
        "cityPinyin": "Leshan"
      },
      {
        "cityCode": "0834",
        "cityName": "凉山彝族自治州",
        "readableName": "凉山彝族自治州",
        "shortName": "凉山",
        "province": "四川省",
        "cityPinyin": "Liangshan"
      },
      {
        "cityCode": "0835",
        "cityName": "雅安市",
        "readableName": "雅安市",
        "shortName": "雅安",
        "province": "四川省",
        "cityPinyin": "Ya'an"
      },
      {
        "cityCode": "0836",
        "cityName": "甘孜藏族自治州",
        "readableName": "甘孜藏族自治州",
        "shortName": "甘孜",
        "province": "四川省",
        "cityPinyin": "Garze"
      },
      {
        "cityCode": "0837",
        "cityName": "阿坝藏族羌族自治州",
        "readableName": "阿坝藏族羌族自治州",
        "shortName": "阿坝",
        "province": "四川省",
        "cityPinyin": "Aba"
      },
      {
        "cityCode": "0838",
        "cityName": "德阳市",
        "readableName": "德阳市",
        "shortName": "德阳",
        "province": "四川省",
        "cityPinyin": "Deyang"
      },
      {
        "cityCode": "0839",
        "cityName": "广元市",
        "readableName": "广元市",
        "shortName": "广元",
        "province": "四川省",
        "cityPinyin": "Guangyuan"
      },
      {
        "cityCode": "0840",
        "cityName": "宁南县",
        "readableName": "宁南县",
        "shortName": "宁南",
        "province": "四川省",
        "cityPinyin": "Ningnan"
      },
      {
        "cityCode": "0841",
        "cityName": "普格县",
        "readableName": "普格县",
        "shortName": "普格",
        "province": "四川省",
        "cityPinyin": "Puge"
      },
      {
        "cityCode": "0842",
        "cityName": "布拖县",
        "readableName": "布拖县",
        "shortName": "布拖",
        "province": "四川省",
        "cityPinyin": "Butuo"
      },
      {
        "cityCode": "0843",
        "cityName": "金阳县",
        "readableName": "金阳县",
        "shortName": "金阳",
        "province": "四川省",
        "cityPinyin": "Jinyang"
      },
      {
        "cityCode": "0844",
        "cityName": "昭觉县",
        "readableName": "昭觉县",
        "shortName": "昭觉",
        "province": "四川省",
        "cityPinyin": "Zhaojue"
      },
      {
        "cityCode": "0845",
        "cityName": "喜德县",
        "readableName": "喜德县",
        "shortName": "喜德",
        "province": "四川省",
        "cityPinyin": "Xide"
      },
      {
        "cityCode": "0846",
        "cityName": "冕宁县",
        "readableName": "冕宁县",
        "shortName": "冕宁",
        "province": "四川省",
        "cityPinyin": "Mianning"
      },
      {
        "cityCode": "0847",
        "cityName": "越西县",
        "readableName": "越西县",
        "shortName": "越西",
        "province": "四川省",
        "cityPinyin": "Yuexi"
      },
      {
        "cityCode": "0848",
        "cityName": "甘洛县",
        "readableName": "甘洛县",
        "shortName": "甘洛",
        "province": "四川省",
        "cityPinyin": "Ganluo"
      },
      {
        "cityCode": "0849",
        "cityName": "美姑县",
        "readableName": "美姑县",
        "shortName": "美姑",
        "province": "四川省",
        "cityPinyin": "Meigu"
      },
      {
        "cityCode": "0850",
        "cityName": "雷波县",
        "readableName": "雷波县",
        "shortName": "雷波",
        "province": "四川省",
        "cityPinyin": "Leibo"
      },
      {
        "cityCode": "0851",
        "cityName": "木里藏族自治县",
        "readableName": "木里藏族自治县",
        "shortName": "木里",
        "province": "四川省",
        "cityPinyin": "Muli"
      },
      {
        "cityCode": "0852",
        "cityName": "遵义市",
        "readableName": "遵义市",
        "shortName": "遵义",
        "province": "贵州省",
        "cityPinyin": "Zunyi"
      },
      {
        "cityCode": "0853",
        "cityName": "安顺市",
        "readableName": "安顺市",
        "shortName": "安顺",
        "province": "贵州省",
        "cityPinyin": "Anshun"
      },
      {
        "cityCode": "0854",
        "cityName": "黔南布依族苗族自治州",
        "readableName": "黔南布依族苗族自治州",
        "shortName": "黔南",
        "province": "贵州省",
        "cityPinyin": "Qiannan"
      },
      {
        "cityCode": "0855",
        "cityName": "黔东南苗族侗族自治州",
        "readableName": "黔东南苗族侗族自治州",
        "shortName": "黔东南",
        "province": "贵州省",
        "cityPinyin": "Qiandongnan"
      },
      {
        "cityCode": "0856",
        "cityName": "铜仁市",
        "readableName": "铜仁市",
        "shortName": "铜仁",
        "province": "贵州省",
        "cityPinyin": "Tongren"
      },
      {
        "cityCode": "0857",
        "cityName": "毕节市",
        "readableName": "毕节市",
        "shortName": "毕节",
        "province": "贵州省",
        "cityPinyin": "Bijie"
      },
      {
        "cityCode": "0858",
        "cityName": "六盘水市",
        "readableName": "六盘水市",
        "shortName": "六盘水",
        "province": "贵州省",
        "cityPinyin": "Liupanshui"
      },
      {
        "cityCode": "0859",
        "cityName": "黔西南布依族苗族自治州",
        "readableName": "黔西南布依族苗族自治州",
        "shortName": "黔西南",
        "province": "贵州省",
        "cityPinyin": "Qianxinan"
      },
      {
        "cityCode": "0870",
        "cityName": "昭通市",
        "readableName": "昭通市",
        "shortName": "昭通",
        "province": "云南省",
        "cityPinyin": "Zhaotong"
      },
      {
        "cityCode": "0871",
        "cityName": "昆明市",
        "readableName": "昆明市",
        "shortName": "昆明",
        "province": "云南省",
        "cityPinyin": "Kunming"
      },
      {
        "cityCode": "0872",
        "cityName": "大理白族自治州",
        "readableName": "大理白族自治州",
        "shortName": "大理",
        "province": "云南省",
        "cityPinyin": "Dali"
      },
      {
        "cityCode": "0873",
        "cityName": "红河哈尼族彝族自治州",
        "readableName": "红河哈尼族彝族自治州",
        "shortName": "红河",
        "province": "云南省",
        "cityPinyin": "Honghe"
      },
      {
        "cityCode": "0874",
        "cityName": "曲靖市",
        "readableName": "曲靖市",
        "shortName": "曲靖",
        "province": "云南省",
        "cityPinyin": "Qujing"
      },
      {
        "cityCode": "0875",
        "cityName": "保山市",
        "readableName": "保山市",
        "shortName": "保山",
        "province": "云南省",
        "cityPinyin": "Baoshan"
      },
      {
        "cityCode": "0876",
        "cityName": "文山壮族苗族自治州",
        "readableName": "文山壮族苗族自治州",
        "shortName": "文山",
        "province": "云南省",
        "cityPinyin": "Wenshan"
      },
      {
        "cityCode": "0877",
        "cityName": "玉溪市",
        "readableName": "玉溪市",
        "shortName": "玉溪",
        "province": "云南省",
        "cityPinyin": "Yuxi"
      },
      {
        "cityCode": "0878",
        "cityName": "楚雄彝族自治州",
        "readableName": "楚雄彝族自治州",
        "shortName": "楚雄",
        "province": "云南省",
        "cityPinyin": "Chuxiong"
      },
      {
        "cityCode": "0879",
        "cityName": "普洱市",
        "readableName": "普洱市",
        "shortName": "普洱",
        "province": "云南省",
        "cityPinyin": "Pu'er"
      },
      {
        "cityCode": "0883",
        "cityName": "临沧市",
        "readableName": "临沧市",
        "shortName": "临沧",
        "province": "云南省",
        "cityPinyin": "Lincang"
      },
      {
        "cityCode": "0886",
        "cityName": "怒江傈僳族自治州",
        "readableName": "怒江傈僳族自治州",
        "shortName": "怒江",
        "province": "云南省",
        "cityPinyin": "Nujiang"
      },
      {
        "cityCode": "0887",
        "cityName": "迪庆藏族自治州",
        "readableName": "迪庆藏族自治州",
        "shortName": "迪庆",
        "province": "云南省",
        "cityPinyin": "Deqen"
      },
      {
        "cityCode": "0888",
        "cityName": "丽江市",
        "readableName": "丽江市",
        "shortName": "丽江",
        "province": "云南省",
        "cityPinyin": "Lijiang"
      },
      {
        "cityCode": "0891",
        "cityName": "拉萨市",
        "readableName": "拉萨市",
        "shortName": "拉萨",
        "province": "西藏自治区",
        "cityPinyin": "Lhasa"
      },
      {
        "cityCode": "0892",
        "cityName": "日喀则市",
        "readableName": "日喀则市",
        "shortName": "日喀则",
        "province": "西藏自治区",
        "cityPinyin": "Rikaze"
      },
      {
        "cityCode": "0893",
        "cityName": "山南地区",
        "readableName": "山南地区",
        "shortName": "山南",
        "province": "西藏自治区",
        "cityPinyin": "Shannan"
      },
      {
        "cityCode": "0894",
        "cityName": "林芝地区",
        "readableName": "林芝地区",
        "shortName": "林芝",
        "province": "西藏自治区",
        "cityPinyin": "Nyingchi"
      },
      {
        "cityCode": "0895",
        "cityName": "昌都市",
        "readableName": "昌都市",
        "shortName": "昌都",
        "province": "西藏自治区",
        "cityPinyin": "Qamdo"
      },
      {
        "cityCode": "0896",
        "cityName": "那曲地区",
        "readableName": "那曲地区",
        "shortName": "那曲",
        "province": "西藏自治区",
        "cityPinyin": "Nagqu"
      },
      {
        "cityCode": "0897",
        "cityName": "阿里地区",
        "readableName": "阿里地区",
        "shortName": "阿里",
        "province": "西藏自治区",
        "cityPinyin": "Ngari"
      },
      {
        "cityCode": "0898",
        "cityName": "海口市",
        "readableName": "海口市",
        "shortName": "海口",
        "province": "海南省",
        "cityPinyin": "Haikou"
      },
      {
        "cityCode": "0901",
        "cityName": "塔城地区",
        "readableName": "塔城地区",
        "shortName": "塔城",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Qoqek"
      },
      {
        "cityCode": "0902",
        "cityName": "哈密地区",
        "readableName": "哈密地区",
        "shortName": "哈密",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Hami"
      },
      {
        "cityCode": "0903",
        "cityName": "和田地区",
        "readableName": "和田地区",
        "shortName": "和田",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Hotan"
      },
      {
        "cityCode": "0906",
        "cityName": "阿勒泰地区",
        "readableName": "阿勒泰地区",
        "shortName": "阿勒泰",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Altay"
      },
      {
        "cityCode": "0908",
        "cityName": "克孜勒苏柯尔克孜自治州",
        "readableName": "克孜勒苏柯尔克孜自治州",
        "shortName": "克孜勒苏",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Kizilsu"
      },
      {
        "cityCode": "0909",
        "cityName": "博尔塔拉蒙古自治州",
        "readableName": "博尔塔拉蒙古自治州",
        "shortName": "博尔塔拉",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Bortala"
      },
      {
        "cityCode": "0911",
        "cityName": "延安市",
        "readableName": "延安市",
        "shortName": "延安",
        "province": "陕西省",
        "cityPinyin": "Yan'an"
      },
      {
        "cityCode": "0912",
        "cityName": "榆林市",
        "readableName": "榆林市",
        "shortName": "榆林",
        "province": "陕西省",
        "cityPinyin": "Yulin"
      },
      {
        "cityCode": "0913",
        "cityName": "渭南市",
        "readableName": "渭南市",
        "shortName": "渭南",
        "province": "陕西省",
        "cityPinyin": "Weinan"
      },
      {
        "cityCode": "0914",
        "cityName": "商洛市",
        "readableName": "商洛市",
        "shortName": "商洛",
        "province": "陕西省",
        "cityPinyin": "Shangluo"
      },
      {
        "cityCode": "0915",
        "cityName": "安康市",
        "readableName": "安康市",
        "shortName": "安康",
        "province": "陕西省",
        "cityPinyin": "Ankang"
      },
      {
        "cityCode": "0916",
        "cityName": "汉中市",
        "readableName": "汉中市",
        "shortName": "汉中",
        "province": "陕西省",
        "cityPinyin": "Hanzhong"
      },
      {
        "cityCode": "0917",
        "cityName": "宝鸡市",
        "readableName": "宝鸡市",
        "shortName": "宝鸡",
        "province": "陕西省",
        "cityPinyin": "Baoji"
      },
      {
        "cityCode": "0919",
        "cityName": "铜川市",
        "readableName": "铜川市",
        "shortName": "铜川",
        "province": "陕西省",
        "cityPinyin": "Tongchuan"
      },
      {
        "cityCode": "0930",
        "cityName": "临夏回族自治州",
        "readableName": "临夏回族自治州",
        "shortName": "临夏",
        "province": "甘肃省",
        "cityPinyin": "Linxia"
      },
      {
        "cityCode": "0931",
        "cityName": "兰州市",
        "readableName": "兰州市",
        "shortName": "兰州",
        "province": "甘肃省",
        "cityPinyin": "Lanzhou"
      },
      {
        "cityCode": "0932",
        "cityName": "定西市",
        "readableName": "定西市",
        "shortName": "定西",
        "province": "甘肃省",
        "cityPinyin": "Dingxi"
      },
      {
        "cityCode": "0933",
        "cityName": "平凉市",
        "readableName": "平凉市",
        "shortName": "平凉",
        "province": "甘肃省",
        "cityPinyin": "Pingliang"
      },
      {
        "cityCode": "0934",
        "cityName": "庆阳市",
        "readableName": "庆阳市",
        "shortName": "庆阳",
        "province": "甘肃省",
        "cityPinyin": "Qingyang"
      },
      {
        "cityCode": "0935",
        "cityName": "金昌市",
        "readableName": "金昌市",
        "shortName": "金昌",
        "province": "甘肃省",
        "cityPinyin": "Jinchang"
      },
      {
        "cityCode": "0936",
        "cityName": "张掖市",
        "readableName": "张掖市",
        "shortName": "张掖",
        "province": "甘肃省",
        "cityPinyin": "Zhangye"
      },
      {
        "cityCode": "0937",
        "cityName": "嘉峪关市",
        "readableName": "嘉峪关市",
        "shortName": "嘉峪关",
        "province": "甘肃省",
        "cityPinyin": "Jiayuguan"
      },
      {
        "cityCode": "0938",
        "cityName": "天水市",
        "readableName": "天水市",
        "shortName": "天水",
        "province": "甘肃省",
        "cityPinyin": "Tianshui"
      },
      {
        "cityCode": "0939",
        "cityName": "陇南市",
        "readableName": "陇南市",
        "shortName": "陇南",
        "province": "甘肃省",
        "cityPinyin": "Longnan"
      },
      {
        "cityCode": "0941",
        "cityName": "甘南藏族自治州",
        "readableName": "甘南藏族自治州",
        "shortName": "甘南",
        "province": "甘肃省",
        "cityPinyin": "Gannan"
      },
      {
        "cityCode": "0943",
        "cityName": "白银市",
        "readableName": "白银市",
        "shortName": "白银",
        "province": "甘肃省",
        "cityPinyin": "Baiyin"
      },
      {
        "cityCode": "0951",
        "cityName": "银川市",
        "readableName": "银川市",
        "shortName": "银川",
        "province": "宁夏回族自治区",
        "cityPinyin": "Yinchuan"
      },
      {
        "cityCode": "0952",
        "cityName": "石嘴山市",
        "readableName": "石嘴山市",
        "shortName": "石嘴山",
        "province": "宁夏回族自治区",
        "cityPinyin": "Shizuishan"
      },
      {
        "cityCode": "0953",
        "cityName": "吴忠市",
        "readableName": "吴忠市",
        "shortName": "吴忠",
        "province": "宁夏回族自治区",
        "cityPinyin": "Wuzhong"
      },
      {
        "cityCode": "0954",
        "cityName": "固原市",
        "readableName": "固原市",
        "shortName": "固原",
        "province": "宁夏回族自治区",
        "cityPinyin": "Guyuan"
      },
      {
        "cityCode": "0955",
        "cityName": "中卫市",
        "readableName": "中卫市",
        "shortName": "中卫",
        "province": "宁夏回族自治区",
        "cityPinyin": "Zhongwei"
      },
      {
        "cityCode": "0970",
        "cityName": "海北藏族自治州",
        "readableName": "海北藏族自治州",
        "shortName": "海北",
        "province": "青海省",
        "cityPinyin": "Haibei"
      },
      {
        "cityCode": "0971",
        "cityName": "西宁市",
        "readableName": "西宁市",
        "shortName": "西宁",
        "province": "青海省",
        "cityPinyin": "Xining"
      },
      {
        "cityCode": "0972",
        "cityName": "海东市",
        "readableName": "海东市",
        "shortName": "海东",
        "province": "青海省",
        "cityPinyin": "Haidong"
      },
      {
        "cityCode": "0973",
        "cityName": "黄南藏族自治州",
        "readableName": "黄南藏族自治州",
        "shortName": "黄南",
        "province": "青海省",
        "cityPinyin": "Huangnan"
      },
      {
        "cityCode": "0974",
        "cityName": "海南藏族自治州",
        "readableName": "海南藏族自治州",
        "shortName": "海南",
        "province": "青海省",
        "cityPinyin": "Hainan"
      },
      {
        "cityCode": "0975",
        "cityName": "果洛藏族自治州",
        "readableName": "果洛藏族自治州",
        "shortName": "果洛",
        "province": "青海省",
        "cityPinyin": "Golog"
      },
      {
        "cityCode": "0976",
        "cityName": "玉树藏族自治州",
        "readableName": "玉树藏族自治州",
        "shortName": "玉树",
        "province": "青海省",
        "cityPinyin": "Yushu"
      },
      {
        "cityCode": "0977",
        "cityName": "海西蒙古族藏族自治州",
        "readableName": "海西蒙古族藏族自治州",
        "shortName": "海西",
        "province": "青海省",
        "cityPinyin": "Haixi"
      },
      {
        "cityCode": "0990",
        "cityName": "克拉玛依市",
        "readableName": "克拉玛依市",
        "shortName": "克拉玛依",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Karamay"
      },
      {
        "cityCode": "0991",
        "cityName": "乌鲁木齐市",
        "readableName": "乌鲁木齐市",
        "shortName": "乌鲁木齐",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Urumqi"
      },
      {
        "cityCode": "0992",
        "cityName": "独山子区",
        "readableName": "独山子区",
        "shortName": "独山子",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Dushanzi"
      },
      {
        "cityCode": "0993",
        "cityName": "沙湾县",
        "readableName": "沙湾县",
        "shortName": "沙湾",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Shawan"
      },
      {
        "cityCode": "0994",
        "cityName": "昌吉回族自治州",
        "readableName": "昌吉回族自治州",
        "shortName": "昌吉",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Changji"
      },
      {
        "cityCode": "0995",
        "cityName": "吐鲁番地区",
        "readableName": "吐鲁番地区",
        "shortName": "吐鲁番",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Turpan"
      },
      {
        "cityCode": "0996",
        "cityName": "巴音郭楞蒙古自治州",
        "readableName": "巴音郭楞蒙古自治州",
        "shortName": "巴音郭楞",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Bayingol"
      },
      {
        "cityCode": "0997",
        "cityName": "阿克苏地区",
        "readableName": "阿克苏地区",
        "shortName": "阿克苏",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Aksu"
      },
      {
        "cityCode": "0998",
        "cityName": "喀什地区",
        "readableName": "喀什地区",
        "shortName": "喀什",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Kashgar"
      },
      {
        "cityCode": "0999",
        "cityName": "伊犁哈萨克自治州",
        "readableName": "伊犁哈萨克自治州",
        "shortName": "伊犁",
        "province": "新疆维吾尔自治区",
        "cityPinyin": "Ili"
      }
    ];

    this.provinceList = [{
      "ProID": 1,
      "name": "北京市",
      "ProSort": 1,
      "ProRemark": "直辖市"
    }, {
      "ProID": 2,
      "name": "天津市",
      "ProSort": 2,
      "ProRemark": "直辖市"
    }, {
      "ProID": 3,
      "name": "河北省",
      "ProSort": 5,
      "ProRemark": "省份"
    }, {
      "ProID": 4,
      "name": "山西省",
      "ProSort": 6,
      "ProRemark": "省份"
    }, {
      "ProID": 5,
      "name": "内蒙古自治区",
      "ProSort": 32,
      "ProRemark": "自治区"
    }, {
      "ProID": 6,
      "name": "辽宁省",
      "ProSort": 8,
      "ProRemark": "省份"
    }, {
      "ProID": 7,
      "name": "吉林省",
      "ProSort": 9,
      "ProRemark": "省份"
    }, {
      "ProID": 8,
      "name": "黑龙江省",
      "ProSort": 10,
      "ProRemark": "省份"
    }, {
      "ProID": 9,
      "name": "上海市",
      "ProSort": 3,
      "ProRemark": "直辖市"
    }, {
      "ProID": 10,
      "name": "江苏省",
      "ProSort": 11,
      "ProRemark": "省份"
    }, {
      "ProID": 11,
      "name": "浙江省",
      "ProSort": 12,
      "ProRemark": "省份"
    }, {
      "ProID": 12,
      "name": "安徽省",
      "ProSort": 13,
      "ProRemark": "省份"
    }, {
      "ProID": 13,
      "name": "福建省",
      "ProSort": 14,
      "ProRemark": "省份"
    }, {
      "ProID": 14,
      "name": "江西省",
      "ProSort": 15,
      "ProRemark": "省份"
    }, {
      "ProID": 15,
      "name": "山东省",
      "ProSort": 16,
      "ProRemark": "省份"
    }, {
      "ProID": 16,
      "name": "河南省",
      "ProSort": 17,
      "ProRemark": "省份"
    }, {
      "ProID": 17,
      "name": "湖北省",
      "ProSort": 18,
      "ProRemark": "省份"
    }, {
      "ProID": 18,
      "name": "湖南省",
      "ProSort": 19,
      "ProRemark": "省份"
    }, {
      "ProID": 19,
      "name": "广东省",
      "ProSort": 20,
      "ProRemark": "省份"
    }, {
      "ProID": 20,
      "name": "海南省",
      "ProSort": 24,
      "ProRemark": "省份"
    }, {
      "ProID": 21,
      "name": "广西壮族自治区",
      "ProSort": 28,
      "ProRemark": "自治区"
    }, {
      "ProID": 22,
      "name": "甘肃省",
      "ProSort": 21,
      "ProRemark": "省份"
    }, {
      "ProID": 23,
      "name": "陕西省",
      "ProSort": 27,
      "ProRemark": "省份"
    }, {
      "ProID": 24,
      "name": "新 疆维吾尔自治区",
      "ProSort": 31,
      "ProRemark": "自治区"
    }, {
      "ProID": 25,
      "name": "青海省",
      "ProSort": 26,
      "ProRemark": "省份"
    }, {
      "ProID": 26,
      "name": "宁夏回族自治区",
      "ProSort": 30,
      "ProRemark": "自治区"
    }, {
      "ProID": 27,
      "name": "重庆市",
      "ProSort": 4,
      "ProRemark": "直辖市"
    }, {
      "ProID": 28,
      "name": "四川省",
      "ProSort": 22,
      "ProRemark": "省份"
    }, {
      "ProID": 29,
      "name": "贵州省",
      "ProSort": 23,
      "ProRemark": "省份"
    }, {
      "ProID": 30,
      "name": "云南省",
      "ProSort": 25,
      "ProRemark": "省份"
    }, {
      "ProID": 31,
      "name": "西藏自治区",
      "ProSort": 29,
      "ProRemark": "自治区"
    }, {
      "ProID": 32,
      "name": "台湾省",
      "ProSort": 7,
      "ProRemark": "省份"
    }, {
      "ProID": 33,
      "name": "澳门特别行政区",
      "ProSort": 33,
      "ProRemark": "特别行政区"
    }, {
      "ProID": 34,
      "name": "香港特别行政区",
      "ProSort": 34,
      "ProRemark": "特别行政区"
    }];
  }
})();
