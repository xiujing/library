(function () {
  'use strict';

  angular
    .module('app')
    .controller('commonSetController', commonSetController);

  /** @ngInject */
  function commonSetController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;

    /*获取运费模板*/
    vm.getCommonSetList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/setting',
        async: true
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.commonSet = res.resultData;
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getCommonSetList();


    vm.fileUpload = {pictureUploader: fileUpload.pictureUploader};
    fileUpload.pictureUploader.onCompleteItem = function (fileItem, response) {
      /*判断图片类型*/
       if (fileItem.fileType == 'image') {
        vm.commonSet.customURL = response.resultData;
      }
    };

    /*通用配置修改*/
    vm.edit = function(){
      var change = {
        method: 'POST',
        url: restServer + 'v1/manager/setting',
        params: {
          bannerTime:vm.commonSet.bannerTime,
          orderCancel:parseFloat(vm.commonSet.orderCancel),
          refund:parseFloat(vm.commonSet.refund),
          prohibitRefund:parseFloat(vm.commonSet.prohibitRefund),
          orderConfirm:parseFloat(vm.commonSet.orderConfirm),
          commissionRate:parseFloat(vm.commonSet.commissionRate),
          customURL:vm.commonSet.customURL,
          customWx:parseFloat(vm.commonSet.customWx),
        }
      };
      $http(change)
        .success(function (res) {
          if (res.resultStatus) {
            alertService.addAlert("success", "操作成功");
          }else{
            alertService.alert("操作失败");
          }
        })
        .error(function (res) {
          $log.log('error', res);
        });
    }


  }
})();
