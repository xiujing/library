(function () {
  'use strict';

  angular
    .module('app')
    .controller('freightTemplateController', freightTemplateController);

  /** @ngInject */
  function freightTemplateController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;

    /*获取运费模板*/
    vm.getFreightList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/mall/freight/template',
        async:true,
        params:{
          pageSize:1000
        }
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.freightList = res.resultData.freightTemplate;
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getFreightList();

    /*删除门店*/
    vm.delete = function (id) {
      alertService.confirm("确定删除？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'DELETE',
            url: restServer + 'v1/manager/mall/freight/template/' + id,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "删除成功");
                vm.getFreightList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }


  }
})();
