(function () {
  'use strict';

  angular
    .module('app')
    .controller('freightAddController', freightAddController);

  function freightAddController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService) {
    var vm = this;
    /*获取头部参数*/
    vm.freightTemplateId = $stateParams.freightTemplateId ? $stateParams.freightTemplateId : 0;
    vm.valuationMethod = "1";
    vm.freightTemplateAreaList = [
      {
        commodityNum: "",
        freightPrice: "",
        everyAddNum: "",
        addFreightPrice: "",
        province: "",
      },
      {
        commodityNum: "",
        freightPrice: "",
        everyAddNum: "",
        addFreightPrice: "",
        province: [],
      }
    ];

    /*获取地址列表*/
    $http.get('./assets/data/address.json').success(function (data) {
      vm.address = data;
      vm.provinceList = [];
      vm.cityList = [];
      vm.areaList = [];
      $.each(data, function (index, value) {
        vm.provinceList.push({name: index, checked: false});
      });
      $.each(data, function (index, value) {
        $.each(value, function (key, item) {
          vm.cityList.push({name: key, checked: false, provinceChecked: false, provinceName: index});
        });
      });

      $.each(data, function (index, value) {
        $.each(value, function (key, item) {
          $.each(item, function (ind, val) {
            vm.areaList.push({name: val, checked: false, provinceName: index, cityChecked: false, cityName: key});
          })
        });
      });
    });

    /*获取运费模板详情*/
    vm.getFreightList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/mall/freight/template',
        async: true
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.freightList = res.resultData.freightTemplate;
            $.each(res.resultData.freightTemplate, function (index, value) {
              if (value.freightTemplateId == vm.freightTemplateId) {
                vm.name = value.name;
                vm.valuationMethod = value.valuationMethod;
                vm.overPrice = value.overPrice / 100;
                vm.freightTemplateAreaList = value.freightTemplateAreaList;
                $.each(vm.freightTemplateAreaList,function(index,value){
                  value.addFreightPrice = value.addFreightPrice/100
                  value.freightPrice = value.freightPrice/100
                });
              }
            });
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    if (vm.freightTemplateId !== 0 ) {
      vm.getFreightList();
    }

    /*选择省份*/
    vm.changeProvince = function (name, key) {
      if (vm.provinceList[key].checked) {
        vm.provinceList[key].checked = false;
      } else {
        vm.provinceList[key].checked = true;
      }
      $.each(vm.cityList, function (index, value) {
        if (value.provinceName == name) {
          if (value.provinceChecked) {
            value.provinceChecked = false
          } else {
            value.provinceChecked = true
          }
        }
      });
      $rootScope.$emit('modalParamChange', {
        provinceList: vm.provinceList,
        cityList: vm.cityList,
        areaList: vm.areaList
      });
    };

    /*选择城市*/
    vm.changeCity = function (name, key) {
      if (vm.cityList[key].checked) {
        vm.cityList[key].checked = false;
      } else {
        vm.cityList[key].checked = true;
      }
      $.each(vm.areaList, function (index, value) {
        if (value.cityName == name) {
          if (value.cityChecked) {
            value.cityChecked = false
          } else {
            value.cityChecked = true
          }
        }
      });
      $rootScope.$emit('modalParamChange', {
        provinceList: vm.provinceList,
        cityList: vm.cityList,
        areaList: vm.areaList
      });
    };

    /*选择地区*/
    vm.changeArea = function (key) {
      if (vm.areaList[key].checked) {
        vm.areaList[key].checked = false;
      } else {
        vm.areaList[key].checked = true;
      }
      $rootScope.$emit('modalParamChange', {
        provinceList: vm.provinceList,
        cityList: vm.cityList,
        areaList: vm.areaList
      });
    }

    /*提交信息*/
    vm.submit = function () {
      vm.freightTemplateAreaList[0].province = "";
      $.each(vm.freightTemplateAreaList,function(index,value){
        value.addFreightPrice = value.addFreightPrice*100
        value.freightPrice = value.freightPrice*100
      });
      var postReq = {
        method: 'POST',
        url: restServer + 'v1/manager/mall/freight/template',
        data: {
          "freightTemplateId": vm.freightTemplateId,
          "name": vm.name,
          "valuationMethod": vm.valuationMethod,
          "overPrice": vm.overPrice * 100,
          "freightTemplateAreaList": vm.freightTemplateAreaList,
        }
      };
      $http(postReq)
        .success(function (res) {
          if (res.resultStatus) {
            alertService.addAlert("", "提交成功");
            $state.go("home.common.freightTemplate");
          } else {
            alertService.alert("请完善您填写的参数");
          }
        }).error(function (res) {
        alertService.alert("操作失败！请检查网络");
        console.log("error", res);
      });
    };

    /*新增*/
    vm.addType = function () {
      vm.freightTemplateAreaList.push({
        commodityNum: "",
        freightPrice: "",
        everyAddNum: "",
        addFreightPrice: "",
        province: [],
      })
    }

    /*删除*/
    vm.delete = function (index) {
      vm.freightTemplateAreaList.splice(index, 1)
    }

    /*编辑*/
    vm.freightEdit = function (index) {
      vm.checkedProvince = [];
      vm.checkedCity = [];
      vm.checkedArea = [];
      var list = vm.freightTemplateAreaList[index].province;
      $.each(vm.provinceList, function (index, value) {
        value.checked = false;
      })
      $.each(vm.cityList, function (index, value) {
        value.checked = false;
        value.provinceChecked = false;
      })
      $.each(vm.areaList, function (index, value) {
        value.checked = false;
        value.cityChecked = false;
      })

      $.each(list, function (key, item) {
        vm.checkedProvince.push(item.province);
        $.each(item.cityList, function (ind, val) {
          vm.checkedCity.push(val.city);
          $.each(val.areaList, function (i, v) {
            vm.checkedArea.push(v);
          });
        });
      });

      $.each(vm.provinceList, function (index, value) {
        $.each(vm.checkedProvince, function (key, item) {
          if (item == value.name) {
            value.checked = true;
          }
        });
      });
      $.each(vm.cityList, function (index, value) {
        $.each(vm.checkedCity, function (key, item) {
          if (item == value.name) {
            value.checked = true
          }
        });
      });
      $.each(vm.checkedProvince, function (key, item) {
        $.each(vm.cityList, function (index, value) {
          if (value.provinceName == item) {
            value.provinceChecked = true
          }
        });
      });
      $.each(vm.checkedCity, function (key, item) {
        $.each(vm.areaList, function (index, value) {
          if (value.cityName == item) {
            value.cityChecked = true
          }
        });
      });
      $.each(vm.areaList, function (index, value) {
        $.each(vm.checkedArea, function (key, item) {
          if (item == value.name) {
            value.checked = true
          }
        });
      });
      var size = "lg";
      var templateUrl = "app/components/common/views/freightAddModal.html";
      var params = {
        provinceList: vm.provinceList,
        cityList: vm.cityList,
        areaList: vm.areaList,
      };
      var functions = {
        changeProvince: function (name, index) {
          return vm.changeProvince(name, index);
        },
        changeCity: function (name, index) {
          return vm.changeCity(name, index);
        },
        changeArea: function (index) {
          return vm.changeArea(index);
        },
      };
      var callbackfn = function (flag, result) {
        if (flag) {
          var addressList = [];
          $.each(vm.provinceList, function (index, value) {
            if (value.checked) {
              addressList.push({province: value.name, cityList: []});
            }
          });
          $.each(vm.cityList, function (index, value) {
            $.each(addressList, function (key, item) {
              if (value.checked && value.provinceName == item.province) {
                addressList[key].cityList.push({city: value.name, areaList: []});
              }
            });
          });
          $.each(vm.areaList, function (index, value) {
            $.each(addressList, function (key, item) {
              $.each(item.cityList, function (ind, val) {
                if (value.checked && value.cityName == val.city) {
                  addressList[key].cityList[ind].areaList.push(value.name);
                }
              });
            });
          })
          vm.freightTemplateAreaList[index].province = addressList
          console.log(vm.freightTemplateAreaList);
        }
      };
      modalService.open(size, templateUrl, params, functions, callbackfn);
    }


  }
})();
