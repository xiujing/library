(function () {
  'use strict';

  angular
    .module('app')
    .controller('refundSetController', refundSetController);

  /** @ngInject */
  function refundSetController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;

    /*获取售后申请列表*/
    vm.getRefundSetList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/mall/refund/category',
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.refundList = res.resultData;
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getRefundSetList();

    /*删除售后申请*/
    vm.delete = function (id) {
      alertService.confirm("确定删除？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'DELETE',
            url: restServer + 'v1/manager/mall/refund/category/' + id,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "删除成功");
                vm.getRefundSetList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }

    /*新增*/
    vm.refundSetAdd = function (refundCategoryId, refundCategory, sort) {
      var size = "md";
      var templateUrl = "app/components/common/views/refundSetEditModal.html";
      var params = {
        refundCategoryId: refundCategoryId,
        refundCategory: refundCategory,
        sort: parseInt(sort),
      };
      var functions = {};
      var callbackfn = function (flag, result) {
        if (flag) {
          var change = {
            method: 'POST',
            url: restServer + 'v1/manager/mall/refund/category',
            params: {
              refundCategoryId: refundCategoryId?refundCategoryId:0,
              refundCategory: result.refundCategory,
              sort: result.sort
            }
          };
          $http(change)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "操作成功");
                vm.getRefundSetList();
              } else {
                alertService.alert("操作失败");
              }
            })
            .error(function (res) {
              $log.log('error', res);
            });
        }
      };
      modalService.open(size, templateUrl, params, functions, callbackfn);
    }

  }
})();
