(function () {
  'use strict';

  angular
    .module('app')
    .controller('newsListController', newsListController);

  /** @ngInject */
  function newsListController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;
    vm.pageNo = 1;
    /*获取新闻媒体列表*/
    vm.getNewsList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'backstage/news',
        params:{
          pageNo: vm.pageNo,
        }
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.newsList = res.resultData.newsList.data;
            /*分页*/
            vm.totalNum = res.resultData.newsList.total;
            vm.pageResult = pageService.calculatePage(res.resultData.newsList.total, vm.pageNo, 20);
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getNewsList();

    /*删除新闻媒体*/
    vm.delete = function (id) {
      alertService.confirm("确定删除？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'DELETE',
            url: restServer + 'backstage/news/' + id,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "删除成功");
                vm.getNewsList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }

    /*分页按钮点击*/
    vm.changePage = function (page) {
      vm.pageNo = page;
      vm.getNewsList();
    }


  }
})();
