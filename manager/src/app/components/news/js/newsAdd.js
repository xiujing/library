(function () {
  'use strict';

  angular
    .module('app')
    .controller('newsAddController', newsAddController);

  function newsAddController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService) {
    var vm = this;
    /*获取头部参数*/
    vm.newsId = $stateParams.newsId ?$stateParams.newsId : 0;
    vm.tag = "赛事新闻"

    /*上传图片*/
    vm.fileUpload = {pictureUploader: fileUpload.pictureUploader};
    fileUpload.pictureUploader.onCompleteItem = function (fileItem, response) {
      /*判断图片类型*/
      if (fileItem.fileType == 'picture') { //学生图片
        vm.picture = response.resultData;
      }
    };

    /*获取新闻媒体详情*/
    if (vm.newsId) {
      var getReq = {
        method: 'GET',
        url: restServer + 'backstage/news/' + vm.newsId
      };
      $http(getReq)
        .success(function (res) {
          if (res.resultStatus) {
            vm.caption = res.resultData.caption;
            vm.picture = res.resultData.picture;
            vm.tag = res.resultData.tag;
            vm.content = res.resultData.content;
            vm.detail = res.resultData.detail;
          }
        }).error(function (res) {
        $log.log('error', res);
      });

    }

    

    /*提交信息*/
    vm.submit = function () {
      var postReq = {
        method: 'POST',
        url: restServer + 'backstage/news',
        data: {
          "caption": vm.caption,
          "picture": vm.picture,
          "tag": vm.tag,
          "content": vm.content,
          "newsId": vm.newsId,
          "detail":vm.detail,
        }
      };
      $http(postReq)
        .success(function (res) {
          if (res.resultStatus) {
            alertService.addAlert("", "提交成功");
            $state.go("home.news.newsList");
          } else {
            alertService.alert(res.errorMessage);
          }
        }).error(function (res) {
        alertService.alert("操作失败！请检查网络");
        console.log("error",res);
      });
    };

  }
})();
