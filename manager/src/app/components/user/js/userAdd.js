(function () {
  'use strict';

  angular
    .module('app')
    .controller('userAddController', userAddController);

  function userAddController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService) {
    var vm = this;
    /*获取头部参数*/
    vm.uid = $stateParams.uid ?$stateParams.uid : 0;

    /*获取新闻媒体详情*/
    if (vm.uid) {
      var getReq = {
        method: 'GET',
        url: restServer + 'v1/front/user/user/' + vm.uid
      };
      $http(getReq)
        .success(function (res) {
          if (res.resultStatus) {
            vm.avatar = res.resultData.avatar;
            vm.sina = res.resultData.sina;
            vm.qq = res.resultData.qq;
            vm.wechat = res.resultData.wechat;
            vm.birthday = res.resultData.birthday;
            if(res.resultData.gender==1){
              vm.gender = "男"
            }else{
              vm.gender = "女"
            }
            vm.province = res.resultData.province;
            vm.city = res.resultData.city;
            vm.district = res.resultData.district;
            vm.work = res.resultData.work;
            vm.username = res.resultData.username;
            vm.phoneNumber = res.resultData.phoneNumber;
            vm.email = res.resultData.email;
          }
        }).error(function (res) {
        $log.log('error', res);
      });

    }


  }
})();
