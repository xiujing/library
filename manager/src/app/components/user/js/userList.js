(function () {
  'use strict';

  angular
    .module('app')
    .controller('userListController', userListController);

  /** @ngInject */
  function userListController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;
    vm.pageNo = 1;
    /*获取用户列表*/
    vm.getUserList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/front/user/user',
        params:{
          pageNo: vm.pageNo,
        }
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.userList = res.resultData.userList.data;
            /*分页*/
            vm.totalNum = res.resultData.userList.total;
            vm.pageResult = pageService.calculatePage(res.resultData.userList.total, vm.pageNo, 20);
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getUserList();

    

    /*分页按钮点击*/
    vm.changePage = function (page) {
      vm.pageNo = page;
      vm.getUserList();
    }


  }
})();
