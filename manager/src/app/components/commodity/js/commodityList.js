(function () {
  'use strict';

  angular
    .module('app')
    .controller('commodityListController', commodityListController);

  /** @ngInject */
  function commodityListController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;
    vm.pageNo = 1;
    /*获取盒子列表*/
    vm.getCommodityList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/mall/commodity',
        params:{
          pageNo: vm.pageNo,
        }
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.commodityList = res.resultData.list;
            /*分页*/
            vm.totalNum = res.resultData.count;
            vm.pageResult = pageService.calculatePage(res.resultData.count, vm.pageNo, 20);
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getCommodityList();

    /*删除盒子*/
    vm.delete = function (id) {
      alertService.confirm("确定删除？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'DELETE',
            url: restServer + 'v1/manager/mall/commodity/' + id,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "删除成功");
                vm.getCommodityList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }

    /*分页按钮点击*/
    vm.changePage = function (page) {
      vm.pageNo = page;
      vm.getCommodityList();
    }

    // 修改盒子的推荐状态
    vm.changeStatus = function(id){
      alertService.confirm("确定修改推荐状态？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'PUT',
            url: restServer + 'v1/manager/mall/commodity/' + id + '?recommend=1',
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "修改成功");
                vm.getCommodityList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });
    }
    
    vm.changeStatus1= function(id){
      alertService.confirm("确定审核通过？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'PUT',
            url: restServer + 'backstage/commodity/' + id ,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "审核成功");
                vm.getCommodityList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });
    }

  }
})();
