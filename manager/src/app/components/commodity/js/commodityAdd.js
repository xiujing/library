(function () {
  'use strict';

  angular
    .module('app')
    .controller('commodityAddController', commodityAddController);

  function commodityAddController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService) {
    var vm = this;
    /*获取头部参数*/
    vm.commodityId = $stateParams.commodityId ?$stateParams.commodityId : 0;
    vm.commodityIconList = [];

    /*上传图片*/
    vm.fileUpload = {
      pictureUploader: fileUpload.pictureUploader,
      videoUploader: fileUpload.videoUploader
    };
    fileUpload.pictureUploader.onCompleteItem = function (fileItem, response) {
      /*判断图片类型*/
      if (fileItem.fileType == 'commodityIcon') {
        if (vm.commodityIconList.length < 5) {
          vm.commodityIconList.push(response.resultData);
        } else {
          alertService.alert("最多只能上传5张");
        }
      } else if (fileItem.fileType == 'simple') {//商品主图
        vm.simple = response.resultData;
      } else if (fileItem.fileType == 'bgPicture') {
        vm.bgPicture = response.resultData;
      } else if (fileItem.fileType == 'boxPicture') {
        vm.boxList[fileItem.indexx].boxPicture = response.resultData
      }
    };
    

    /*商品展示图删除*/
    vm.deleteIcon = function (index) {
      vm.commodityIconList.splice(index, 1);
    };

    /*获取新闻媒体详情*/
    if (vm.commodityId) {
      var getReq = {
        method: 'GET',
        url: restServer + 'v1/manager/mall/commodity/' + vm.commodityId
      };
      $http(getReq)
        .success(function (res) {
          if (res.resultStatus) {
            vm.commodityId = res.resultData.commodityId;
            vm.caption = res.resultData.caption;
            vm.content = res.resultData.content;
            vm.commodityIconList = res.resultData.hasPicture.split(";");
            vm.bgPicture = res.resultData.bgPicture;
            vm.type = res.resultData.type;
            vm.originalPrice = res.resultData.originalPrice;
            vm.nowPrice = res.resultData.nowPrice;
            vm.stock = res.resultData.stock;
            vm.detail = res.resultData.detail;
          }
        }).error(function (res) {
        $log.log('error', res);
      });

    }

    

    /*提交信息*/
    vm.submit = function () {
      var postReq = {
        method: 'POST',
        url: restServer + 'v1/manager/mall/commodity',
        data: {
          "commodityId": vm.commodityId,
          "caption": vm.caption,
          "content": vm.content,
          "hasPicture": vm.commodityIconList.join(";"),
          "bgPicture": vm.bgPicture,
					"type": vm.type,
					"originalPrice" : vm.originalPrice,
					"detail":vm.detail,
          "nowPrice":vm.nowPrice,
          "stock":vm.stock,
         
        }
      };
      $http(postReq)
        .success(function (res) {
          if (res.resultStatus) {
            alertService.addAlert("", "提交成功");
            $state.go("home.commodity.commodityList");
          } else {
            alertService.alert(res.errorMessage);
          }
        }).error(function (res) {
        alertService.alert("操作失败！请检查网络");
        console.log("error",res);
      });
    };
  }
})();
