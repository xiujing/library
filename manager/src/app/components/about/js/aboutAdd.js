(function () {
  'use strict';

  angular
    .module('app')
    .controller('aboutAddController', aboutAddController);

  function aboutAddController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService) {
    var vm = this;

    /*获取创客课程详情*/
    var getReq = {
      method: 'GET',
      url: restServer + 'backstage/about'
    };
    $http(getReq)
      .success(function (res) {
        if (res.resultStatus) {
          vm.companySyn = res.resultData[0].companySyn;
          vm.idea = res.resultData[0].idea;
        }
      }).error(function (res) {
      $log.log('error', res);
    });


    /*提交信息*/
    vm.submit = function () {
      var postReq = {
        method: 'POST',
        url: restServer + 'backstage/about',
        data: {
          "aboutId": 1,
          "companySyn": vm.companySyn,
          "idea": vm.idea
        }
      };
      $http(postReq)
        .success(function (res) {
          if (res.resultStatus) {
            alertService.addAlert("", "提交成功");
            $state.go("home.about.aboutAdd");
          } else {
            alertService.alert(res.errorMessage);
          }
        }).error(function (res) {
        alertService.alert("操作失败！请检查网络");
        console.log("error",res);
      });
    };

  }
})();
