(function () {
  'use strict';

  angular
    .module('app')
    .controller('roleListController', roleListController);

  /** @ngInject */
  function roleListController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope, $state, localStorageService, cityListService) {
    var vm = this;
    vm.resourcesList = [
      {
        name: "爱乐猫产品",
        list: [
          {
            name: "产品列表",
            checked: false
          }
        ]
      },
      {
        name: "老师管理",
        list: [
          {
            name: "老师列表",
            checked: false
          }
        ]
      },
      // {
      //   name: "合作申请",
      //   list: [
      //     {
      //       name: "合作加盟",
      //       checked: false
      //     },
      //     {
      //       name: "供应商合作",
      //       checked: false
      //     },
      //     {
      //       name: "合作申请详情",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "图片管理",
      //   list: [
      //     {
      //       name: "图片列表",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "售后服务",
      //   list: [
      //     {
      //       name: "售后服务",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "充值管理",
      //   list: [
      //     {
      //       name: "充值配置",
      //       checked: false
      //     },
      //     {
      //       name: "充值记录",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "评论管理",
      //   list: [
      //     {
      //       name: "评论列表",
      //       checked: false
      //     },
      //     {
      //       name: "晒单列表",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "会员管理",
      //   list: [
      //     {
      //       name: "会员列表",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "公告管理",
      //   list: [
      //     {
      //       name: "公告列表",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "订单管理",
      //   list: [
      //     {
      //       name: "订单列表",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "营销管理",
      //   list: [
      //     {
      //       name: "代金券列表",
      //       checked: false
      //     },
      //     {
      //       name: "红包列表",
      //       checked: false
      //     },
      //     {
      //       name: "门店促销",
      //       checked: false
      //     },
      //     {
      //       name: "门店促销分类",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "活动管理",
      //   list: [
      //     {
      //       name: "限时抢购",
      //       checked: false
      //     },
      //     {
      //       name: "活动分类",
      //       checked: false
      //     },
      //     {
      //       name: "活动商品",
      //       checked: false
      //     }
      //   ]
      // },
      {
        name: "管理员管理",
        list: [
          {
            name: "管理员列表",
            checked: false
          },
          {
            name: "角色列表",
            checked: false
          }
        ]
      },
      {
        name: "商品管理",
        list: [
          {
            name: "商品列表",
            checked: false
          }
        ]
      },
      // {
      //   name: "通用设置",
      //   list: [
      //     {
      //       name: "运费模板",
      //       checked: false
      //     },
      //     {
      //       name: "通用配置",
      //       checked: false
      //     },
      //     {
      //       name: "退货配置",
      //       checked: false
      //     }
      //   ]
      // },
      // {
      //   name: "销售统计",
      //   list: [
      //     {
      //       name: "销售统计",
      //       checked: false
      //     }
      //   ]
      // },
    ];

    /*获取角色列表*/
    vm.getRoleList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/user/role',
        params: {
          managerId: vm.managerId
        }
      }
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.roleLIst = res.resultData;
            vm.roleDetail = vm.roleLIst[0];
            vm.changeSource();
          }
        }).error(function (res) {
        $log.log('error', res);
      })
    }
    vm.getRoleList();

    /*角色删除*/
    vm.delete = function (id) {
      alertService.confirm("确定删除？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'DELETE',
            url: restServer + 'v1/manager/user/role/' + id,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "删除成功");
                vm.getRoleList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }

    /*修改按钮点击*/
    vm.edit = function (index) {
      if (!vm.roleLIst[index].resources) {
        vm.roleLIst[index].resources = "";
      }
      vm.roleDetail = vm.roleLIst[index];
      $.each(vm.resourcesList, function (index, value) {
        $.each(value.list, function (key, item) {
          item.checked = false;
        });
      })
      vm.changeSource();
    }

    /*改变权限列表样式*/
    vm.changeSource = function () {
      $.each(vm.resourcesList, function (index, value) {
        $.each(value.list, function (key, item) {
          if (vm.roleDetail.resources.indexOf(item.name) != -1) {
            item.checked = true;
          }
        });
      })
    }

    /*选中点击*/
    vm.changeChecked = function (key, index) {
      if (vm.resourcesList[key].list[index].checked) {
        vm.resourcesList[key].list[index].checked = false
      } else {
        vm.resourcesList[key].list[index].checked = true
      }
    }

    /*提交按钮点击*/
    vm.submit = function () {
      var resources = [];
      $.each(vm.resourcesList, function (index, value) {
        $.each(value.list, function (key, item) {
          if (item.checked) {
            resources.push(item.name);
          }
        });
      })
      var ajaxData = {
        method: 'POST',
        url: restServer + 'v1/manager/user/role',
        params: {
          roleId: vm.roleDetail.roleId,
          roleName: vm.roleDetail.roleName,
          roleDesc: vm.roleDetail.roleDesc,
          resources: resources.join(";")
        }
      }
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            alertService.addAlert("success", "修改成功");
            vm.getRoleList();
          } else {
            alertService.alert(res.errorMessage);
          }
        }).error(function (res) {
        $log.log('error', res);
      })
    }

    /*新增按钮点击*/
    vm.addRole = function () {
      var connectDetail = {};
      connectDetail.roleId = 0;
      connectDetail.roleName = "";
      connectDetail.roleDesc = "";
      connectDetail.resources = "";
      vm.roleDetail = connectDetail;
      $.each(vm.resourcesList, function (index, value) {
        $.each(value.list, function (key, item) {
          item.checked = false;
        });
      })
      vm.changeSource();
    }


  }
})();
