(function () {
  'use strict';

  angular
    .module('app')
    .controller('managerListController', managerListController);

  /** @ngInject */
  function managerListController($log, fileUpload, restServer, $stateParams, $http, alertService, pageService, $location, modalService, $rootScope) {
    var vm = this;

    /*获取管理员列表*/
    vm.getManagerList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/user/account',
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.managerList = res.resultData;
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getManagerList();

    /*删除管理员*/
    vm.delete = function (id) {
      alertService.confirm("确定删除？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'DELETE',
            url: restServer + 'v1/manager/user/account/' + id,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "删除成功");
                vm.getManagerList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }

    /*获取角色列表*/
    vm.getRoleList = function () {
      var ajaxData = {
        method: 'GET',
        url: restServer + 'v1/manager/user/role',
      };
      $http(ajaxData)
        .success(function (res) {
          if (res.resultStatus) {
            vm.roleList = res.resultData;
          }
        }).error(function (res) {
        $log.log('error', res);
      });
    }
    vm.getRoleList();

    /*新增*/
    vm.managerAdd= function (managerId, userName,realName,connectTel,roleId) {
      console.log(roleId);
      var size = "md";
      var templateUrl = "app/components/manager/views/managerEditModal.html";
      var params = {
        managerId: managerId,
        userName: userName,
        password: "",
        realName: realName,
        connectTel: connectTel,
        roleId: roleId,
        roleList:vm.roleList
      };
      var functions = {};
      var callbackfn = function (flag, result) {
        if (flag) {
          var change = {
            method: 'POST',
            url: restServer + 'v1/manager/user/account',
            params: managerId?{
              managerId: managerId,
              userName: result.userName,
              realName: result.realName,
              connectTel: result.connectTel,
              roleId: result.roleId
            }:{
              managerId: 0,
              userName: result.userName,
              password: result.password,
              realName: result.realName,
              connectTel: result.connectTel,
              roleId: result.roleId
            }
          };
          $http(change)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "操作成功");
                vm.getManagerList();
              } else {
                alertService.alert(res.errorMessage);
              }
            })
            .error(function (res) {
              $log.log('error', res);
            });
        }
      };
      modalService.open(size, templateUrl, params, functions, callbackfn);
    }

    /*修改状态*/
    vm.changeStatus = function (id) {
      alertService.confirm("确定修改？", "", function (isConfirm) {
        if (isConfirm) {
          var delClass = {
            method: 'PUT',
            url: restServer + 'v1/manager/user/states/' + id,
          };
          $http(delClass)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "修改成功");
                vm.getManagerList();
              } else {
                alertService.alert(res.errorMessage);
              }
            }).error(function (res) {
            $log.log("error", res);
          });
        }
      });

    }

    /*管理员修改密码*/
    vm.changePassword= function (managerId) {
      var size = "md";
      var templateUrl = "app/components/manager/views/changePassword.html";
      var params = {
        managerId: managerId,
        oldPassword: "",
        newPassword: "",
      };
      var functions = {};
      var callbackfn = function (flag, result) {
        if (flag) {
          var change = {
            method: 'PUT',
            url: restServer + 'v1/manager/user/password',
            params:{
              managerId: managerId,
              oldPassword: result.oldPassword,
              newPassword: result.newPassword,
            }
          };
          $http(change)
            .success(function (res) {
              if (res.resultStatus) {
                alertService.addAlert("success", "操作成功");
                vm.getManagerList();
              } else {
                alertService.alert(res.errorMessage);
              }
            })
            .error(function (res) {
              $log.log('error', res);
            });
        }
      };
      modalService.open(size, templateUrl, params, functions, callbackfn);
    }
  }
})();
