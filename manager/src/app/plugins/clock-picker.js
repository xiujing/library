function clockPicker() {
  return {
    restrict: 'A',
    link: function(scope, element) {
      element.clockpicker();
    }
  };
}

angular
  .module('app')
  .directive('clockPicker', clockPicker);
