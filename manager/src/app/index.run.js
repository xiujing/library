(function() {
  'use strict';

  angular
    .module('app')
    .run(runBlock)
  .run(function ($rootScope, $state) {
    $rootScope.$state = $state;
  })
  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
