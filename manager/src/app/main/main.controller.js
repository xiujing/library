(function () {
  'use strict';

  angular
    .module('app')
    .controller('mainController', mainController);
  /** @ngInject */
  function mainController($log, localStorageService, $state) {
    var vm = this;
    vm.platValue = 1;
    // if (!localStorageService.get("ywscUserInfo")) {
    //   $state.go("login");
    // } else {
    //   var ywscUserInfo = localStorageService.get("ywscUserInfo")

    //   if(ywscUserInfo.resources){
    //     vm.resources = ywscUserInfo.resources;
    //   }else{
    //     vm.resources =""
    //   }
    // }
    vm.platform = function(plat){
      vm.platValue = plat;
    }
    console.log(vm.platValue)
  }

  angular
    .module('app').config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({color: '#1ab394'});
  }]);
})();
