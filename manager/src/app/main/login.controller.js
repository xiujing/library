(function () {
  'use strict';

  angular
    .module('app')
    .controller('loginController', loginController);

  /** @ngInject */
  function loginController($log, restServer, $http, $state, localStorageService) {
    var vm = this;
    vm.login = function () {
      var postLogin = {
        method: 'POST',
        url: restServer + 'v1/manager/user/login',
        params: {
          userName: vm.username,
          password: vm.password,
        }
      };
      $http(postLogin)
        .success(function (res) {
          if(res.resultStatus){
            localStorageService.set("ywscUserInfo",res.resultData);
            $state.go('home.welcome');
          }else{
            vm.errorMsg = res.errorMessage
          }
        })
        .error(function (res) {
          $log.log('error', res);
        });
    };
  }
})();
