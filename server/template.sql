/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : template

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-02-10 17:03:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mall_address
-- ----------------------------
DROP TABLE IF EXISTS `mall_address`;
CREATE TABLE `mall_address` (
  `userAccess` varchar(32) DEFAULT NULL,
  `addressId` int(11) NOT NULL AUTO_INCREMENT COMMENT '地址ID',
  `accountId` int(11) DEFAULT NULL COMMENT '用户ID',
  `reciverName` varchar(32) NOT NULL COMMENT '收件人姓名',
  `phoneNumber` varchar(32) NOT NULL COMMENT '联系电话',
  `province` varchar(32) NOT NULL COMMENT '所在省',
  `city` varchar(32) NOT NULL COMMENT '所在市',
  `district` varchar(32) DEFAULT NULL COMMENT '所在区/镇',
  `detailAddress` varchar(128) NOT NULL COMMENT '详细街道地址',
  `defaultStatus` tinyint(1) DEFAULT '0' COMMENT '地址类型(1默认地址/0常用地址)',
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='收货地址表';

-- ----------------------------
-- Records of mall_address
-- ----------------------------
INSERT INTO `mall_address` VALUES (null, '1', '1', '李琮4', '18296154802', '广东省', '深圳', '龙华', '民治大道', '1');
INSERT INTO `mall_address` VALUES ('1', '2', null, '李琮4', '18296154802', '广东省', '深圳', '龙华', '民治大道', '1');

-- ----------------------------
-- Table structure for mall_cart
-- ----------------------------
DROP TABLE IF EXISTS `mall_cart`;
CREATE TABLE `mall_cart` (
  `cartId` int(11) NOT NULL AUTO_INCREMENT COMMENT '购物车ID',
  `accountId` int(11) DEFAULT NULL COMMENT '用户ID',
  `commodityId` int(11) NOT NULL COMMENT '商品ID',
  `amounts` int(5) NOT NULL COMMENT '购买数量',
  `skuId` int(11) NOT NULL COMMENT '商品属性对应的价格表ID',
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userAccess` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`cartId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='购物车表';

-- ----------------------------
-- Records of mall_cart
-- ----------------------------
INSERT INTO `mall_cart` VALUES ('3', '1', '8', '4', '4', '2017-08-31 15:50:06', '1');
INSERT INTO `mall_cart` VALUES ('4', '1', '11', '1', '8', '2017-09-20 10:35:58', null);

-- ----------------------------
-- Table structure for mall_commodity
-- ----------------------------
DROP TABLE IF EXISTS `mall_commodity`;
CREATE TABLE `mall_commodity` (
  `commodityId` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `commodityName` varchar(128) NOT NULL COMMENT '商品名称',
  `description` varchar(512) DEFAULT NULL COMMENT '商品描述',
  `commodityIcon` varchar(512) NOT NULL COMMENT '商品图片展示URL，多个图片用分号;隔开',
  `descPictures` mediumtext COMMENT '商品详情图片URL(多个URL用;隔开)',
  `saleStatus` tinyint(1) DEFAULT '1' COMMENT '上下架状态(1为上架，0为下架，默认上架)',
  `categoryId` int(5) DEFAULT NULL COMMENT '商品所属类别',
  `maxPrice` int(11) NOT NULL COMMENT '原价最大值',
  `minPrice` int(11) NOT NULL COMMENT '原价最小值',
  `maxDiscount` int(11) NOT NULL COMMENT '优惠价最高',
  `minDiscount` int(11) NOT NULL COMMENT '优惠价最低',
  `realSaleCount` int(11) NOT NULL DEFAULT '0' COMMENT '真实销售量',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `optionValue` varchar(255) DEFAULT NULL COMMENT '商品属性',
  PRIMARY KEY (`commodityId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='商品表';

-- ----------------------------
-- Records of mall_commodity
-- ----------------------------
INSERT INTO `mall_commodity` VALUES ('14', '手机', '销量最好', 'http://30days.img-cn-shenzhen.aliyuncs.com/237e1fcae2e14ca9b05b838a34b5da9d_origin.jpg', 'http://pic.naildaka.com/images/000/000/009/201602/56c6a766095e5.jpg', '1', '1', '95000', '1', '95000', '1', '0', '2017-09-20 14:55:46', '2017-09-20 14:55:46', '颜色:土豪金,红色;型号:8G,4G');

-- ----------------------------
-- Table structure for mall_commodity_category
-- ----------------------------
DROP TABLE IF EXISTS `mall_commodity_category`;
CREATE TABLE `mall_commodity_category` (
  `categoryId` int(5) NOT NULL AUTO_INCREMENT COMMENT '类别ID',
  `categoryName` varchar(64) NOT NULL COMMENT '类别名称',
  `sort` int(11) DEFAULT '0' COMMENT '排序号',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='商品类别表';

-- ----------------------------
-- Records of mall_commodity_category
-- ----------------------------
INSERT INTO `mall_commodity_category` VALUES ('1', '商品类别1', '0', '2017-08-30 15:24:49');
INSERT INTO `mall_commodity_category` VALUES ('2', '商品类别2', '0', '2017-08-30 15:24:54');
INSERT INTO `mall_commodity_category` VALUES ('4', '商品类别', '0', '2017-08-30 15:57:34');

-- ----------------------------
-- Table structure for mall_commodity_comment
-- ----------------------------
DROP TABLE IF EXISTS `mall_commodity_comment`;
CREATE TABLE `mall_commodity_comment` (
  `commentId` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品评价ID',
  `content` varchar(256) DEFAULT NULL COMMENT '评价内容',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '评价时间',
  `accountId` int(11) DEFAULT NULL COMMENT '用户昵称',
  `orderNo` varchar(64) DEFAULT NULL COMMENT '评价的订单号',
  `descPictures` mediumtext COMMENT '图片描述，容纳3张图片URL，多个URL之间用;隔开',
  `score` int(1) DEFAULT NULL COMMENT '评分',
  `commodityId` int(11) DEFAULT NULL COMMENT '商品ID',
  `reply` varchar(512) DEFAULT NULL COMMENT '客服回复',
  `userAccess` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`commentId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='商城商品评价';

-- ----------------------------
-- Records of mall_commodity_comment
-- ----------------------------
INSERT INTO `mall_commodity_comment` VALUES ('3', '非常不錯', '2017-09-07 09:35:17', null, '20170906151454414671', null, '5', '10', '欢迎下次再来', '9797D71C02826C332A85DE87A12AC2E3');
INSERT INTO `mall_commodity_comment` VALUES ('4', '很好的一次体验', '2017-09-07 12:02:48', null, '20170906151454414671', null, '4', '0', '欢迎下次再来', '9797D71C02826C332A85DE87A12AC2E3');
INSERT INTO `mall_commodity_comment` VALUES ('5', '非常不錯', '2017-09-06 20:10:22', null, '20170906151454414671', null, '5', '10', null, '9797D71C02826C332A85DE87A12AC2E3');
INSERT INTO `mall_commodity_comment` VALUES ('6', '很好的一次体验', '2017-09-06 20:10:22', null, '20170906151454414671', null, '4', '0', null, '9797D71C02826C332A85DE87A12AC2E3');
INSERT INTO `mall_commodity_comment` VALUES ('7', '非常不錯', '2017-09-07 12:10:23', null, '20170906151454414671', null, '5', '10', null, '9797D71C02826C332A85DE87A12AC2E3');
INSERT INTO `mall_commodity_comment` VALUES ('8', '很好的一次体验', '2017-09-07 12:10:23', null, '20170906151454414671', null, '4', '0', null, '9797D71C02826C332A85DE87A12AC2E3');

-- ----------------------------
-- Table structure for mall_commodity_sku
-- ----------------------------
DROP TABLE IF EXISTS `mall_commodity_sku`;
CREATE TABLE `mall_commodity_sku` (
  `skuId` int(22) NOT NULL AUTO_INCREMENT COMMENT '价格ID',
  `commodityId` int(11) NOT NULL COMMENT '商品ID',
  `skuDesc` varchar(256) NOT NULL COMMENT '商品属性描述json格式串',
  `commodityPrice` int(11) NOT NULL COMMENT '属性组合之后的价格',
  `discount` int(11) NOT NULL COMMENT '优惠价',
  `stock` int(11) NOT NULL DEFAULT '1' COMMENT '商品库存',
  PRIMARY KEY (`skuId`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COMMENT='商品价格，根据型号和颜色组合商品价格';

-- ----------------------------
-- Records of mall_commodity_sku
-- ----------------------------
INSERT INTO `mall_commodity_sku` VALUES ('21', '14', 'a:2:{i:0;a:2:{s:6:\"skuKey\";s:6:\"颜色\";s:8:\"skuValue\";s:6:\"红色\";}i:1;a:2:{s:6:\"skuKey\";s:6:\"型号\";s:8:\"skuValue\";s:2:\"4G\";}}', '1', '1', '11');
INSERT INTO `mall_commodity_sku` VALUES ('22', '14', 'a:2:{i:0;a:2:{s:6:\"skuKey\";s:6:\"颜色\";s:8:\"skuValue\";s:9:\"土豪金\";}i:1;a:2:{s:6:\"skuKey\";s:6:\"型号\";s:8:\"skuValue\";s:2:\"4G\";}}', '95000', '95000', '10');
INSERT INTO `mall_commodity_sku` VALUES ('39', '14', 'a:2:{i:0;a:2:{s:6:\"skuKey\";s:6:\"颜色\";s:8:\"skuValue\";s:9:\"土豪金\";}i:1;a:2:{s:6:\"skuKey\";s:6:\"型号\";s:8:\"skuValue\";s:2:\"8G\";}}', '11', '1', '11');

-- ----------------------------
-- Table structure for mall_order
-- ----------------------------
DROP TABLE IF EXISTS `mall_order`;
CREATE TABLE `mall_order` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `orderNo` varchar(64) NOT NULL COMMENT '订单号',
  `orderStatus` varchar(32) NOT NULL COMMENT '订单状态（NONPAYMENT待付款/OVERDUE订单过期/WAITDELIVER待发货/WAITRECEIVE待收货/ORDERFINISH交易完成/WAITREFUND待退款/REFUNDEND退款成功/WAITCOMMENT待评论）(三个最终状态：REFUNDEND，ORDERFINISH)',
  `province` varchar(32) NOT NULL COMMENT '所在省',
  `city` varchar(32) NOT NULL COMMENT '所在市',
  `district` varchar(32) NOT NULL COMMENT '所在区/镇',
  `detailAddress` varchar(128) NOT NULL COMMENT '详细街道地址',
  `accountId` int(11) DEFAULT NULL COMMENT '用户ID',
  `reciverName` varchar(32) NOT NULL COMMENT '收件人姓名',
  `phoneNumber` varchar(16) NOT NULL COMMENT '手机号码',
  `deliverCode` varchar(64) DEFAULT NULL COMMENT '快递编号',
  `deliverName` varchar(64) DEFAULT NULL COMMENT '快递公司名称',
  `deliverNo` varchar(64) DEFAULT NULL COMMENT '快递单号',
  `payType` varchar(32) DEFAULT NULL COMMENT '支付方式',
  `totalPrice` int(11) DEFAULT NULL COMMENT '商品总价格',
  `postage` int(4) NOT NULL DEFAULT '0' COMMENT '邮费',
  `totalFee` int(11) NOT NULL DEFAULT '0' COMMENT '订单的总支付价格',
  `userRemark` varchar(128) DEFAULT NULL COMMENT '买家留言',
  `createTime` datetime DEFAULT NULL COMMENT '下单时间',
  `payTime` datetime DEFAULT NULL COMMENT '支付时间',
  `deliveryTime` datetime DEFAULT NULL COMMENT '发货时间',
  `finishTime` datetime DEFAULT NULL COMMENT '订单完成时间',
  `userAccess` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='商品订单';

-- ----------------------------
-- Records of mall_order
-- ----------------------------
INSERT INTO `mall_order` VALUES ('3', '20170829151505727350', 'WAITRECEIVE', '广东省', '深圳市', '龙华新区', '民治大道展滔', '1', '李琮', '18296154802', '', '', '', 'ALI', '15', '1000', '1015', null, null, null, null, null, '1');
INSERT INTO `mall_order` VALUES ('4', '20170831174126704750', 'NONPAYMENT', '广东省', '深圳市', '龙华新区', '民治大道展滔', '1', '李琮', '18296154802', null, null, null, null, '15', '1000', '1015', null, '2017-08-31 17:41:26', null, null, null, '1');
INSERT INTO `mall_order` VALUES ('5', '20170906100407867332', 'REFUNDEND', '广东省', '深圳市', '龙华新区', '民治大道展滔', '1', '李琮', '18296154802', 'shunfeng', '顺丰', '261038405024', 'WEPAY', '99900', '0', '99900', null, '2017-09-06 10:04:07', null, '2017-09-06 11:06:34', null, '9797D71C02826C332A85DE87A12AC2E3');
INSERT INTO `mall_order` VALUES ('6', '20170906151454414671', 'ORDERFINISH', '广东省', '深圳市', '龙华新区', '民治大道展滔', '1', '李琮', '18296154802', null, null, null, null, '89000', '0', '89000', null, '2017-09-06 15:14:54', null, null, null, '9797D71C02826C332A85DE87A12AC2E3');

-- ----------------------------
-- Table structure for mall_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `mall_order_detail`;
CREATE TABLE `mall_order_detail` (
  `detailId` int(11) NOT NULL AUTO_INCREMENT COMMENT '详情ID',
  `orderNo` varchar(64) NOT NULL COMMENT '所属订单号',
  `skuId` int(11) DEFAULT NULL COMMENT '商品对应的价格ID',
  `commodityName` varchar(128) NOT NULL COMMENT '商品名称',
  `commodityPrice` int(11) NOT NULL COMMENT '商品单价',
  `commodityIcon` varchar(512) NOT NULL COMMENT '商品图片展示URL，多个图片用分号;隔开',
  `descPictures` varchar(2048) NOT NULL COMMENT '商品详情图片',
  `amounts` int(11) NOT NULL COMMENT '购买的商品数量',
  `commodityId` int(11) NOT NULL COMMENT '商品ID',
  `skuDesc` varchar(256) NOT NULL COMMENT '商品属性描述json格式串',
  `discount` int(11) DEFAULT NULL,
  `totalPrice` int(11) DEFAULT NULL COMMENT '商品的购买价格（单价*数量）',
  PRIMARY KEY (`detailId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='商品订单详情';

-- ----------------------------
-- Records of mall_order_detail
-- ----------------------------
INSERT INTO `mall_order_detail` VALUES ('1', '20170829151505727350', '3', '拼团商品1', '4', 'http://pic.yilos.com/7a314a1f650a0af2bf230732f36a46a3.jpg', 'http://pic.naildaka.com/images/000/000/009/201602/56c6a766095e5.jpg', '3', '8', '[{\"attrKey\":\"净含量\",\"attrValue\":\"3ML\"},{\"attrKey\":\"型号\",\"attrValue\":\"低胶\"}]', '5', '15');
INSERT INTO `mall_order_detail` VALUES ('2', '20170831174126704750', '3', '拼团商品1', '4', 'http://pic.yilos.com/7a314a1f650a0af2bf230732f36a46a3.jpg', 'http://pic.naildaka.com/images/000/000/009/201602/56c6a766095e5.jpg', '3', '8', '[{\"attrKey\":\"净含量\",\"attrValue\":\"3ML\"},{\"attrKey\":\"型号\",\"attrValue\":\"低胶\"}]', '5', '15');
INSERT INTO `mall_order_detail` VALUES ('3', '20170906100407867332', '7', '手机', '100000', 'http://30days.img-cn-shenzhen.aliyuncs.com/237e1fcae2e14ca9b05b838a34b5da9d_origin.jpg', 'http://pic.naildaka.com/images/000/000/009/201602/56c6a766095e5.jpg', '1', '10', '[{\"skuKey\":\"颜色\",\"skuValue\":\"土豪金\"},{\"skuKey\":\"型号\",\"skuValue\":\"8G\"}]', '99900', '99900');
INSERT INTO `mall_order_detail` VALUES ('4', '20170906151454414671', '8', '手机', '90000', 'http://30days.img-cn-shenzhen.aliyuncs.com/237e1fcae2e14ca9b05b838a34b5da9d_origin.jpg', 'http://pic.naildaka.com/images/000/000/009/201602/56c6a766095e5.jpg', '1', '10', '[{\"skuKey\":\"颜色\",\"skuValue\":\"红色\"},{\"skuKey\":\"型号\",\"skuValue\":\"4G\"}]', '89000', '89000');

-- ----------------------------
-- Table structure for mall_order_refund
-- ----------------------------
DROP TABLE IF EXISTS `mall_order_refund`;
CREATE TABLE `mall_order_refund` (
  `refundId` int(11) NOT NULL AUTO_INCREMENT COMMENT '退款申请ID',
  `refundNo` varchar(64) NOT NULL COMMENT '退款单号',
  `orderNo` varchar(64) NOT NULL COMMENT '交易完成的订单单号',
  `accountId` int(11) DEFAULT NULL COMMENT '用户ID',
  `refundPrice` int(8) NOT NULL DEFAULT '0' COMMENT '老版本整单申请退款时为剩余可退款金额（实际支付金额包括邮费-已经退还的部分金额），新版本为订单中退款的商品的总价格',
  `refundStatus` varchar(32) NOT NULL COMMENT '退款状态（PENDING处理中等待审核/REVOKED撤销申请/WAITRETURN等待退货/WAITCONFIRM等待确认/WAITREFUND等待退款/SUCCEEDED成功/FAILED失败）',
  `refundReason` varchar(512) DEFAULT NULL COMMENT '退款理由',
  `refuseReason` varchar(512) DEFAULT NULL COMMENT '拒绝退款原因',
  `payType` varchar(32) DEFAULT NULL COMMENT '支付方式',
  `applyTime` datetime NOT NULL COMMENT '申请退款时间',
  `finishTime` datetime DEFAULT NULL COMMENT '退款成功时间',
  `descPictures` varchar(384) DEFAULT NULL COMMENT '退款原因之图片描述',
  `remark` varchar(256) DEFAULT NULL COMMENT '用户备注',
  `deliverName` varchar(32) DEFAULT NULL COMMENT '物流公司',
  `deliverNo` varchar(32) DEFAULT NULL COMMENT '物流单号',
  `deliverCode` varchar(32) DEFAULT NULL,
  `commodityId` int(11) DEFAULT NULL COMMENT '商品Id',
  `skuId` int(11) DEFAULT NULL COMMENT '商品对应的价格Id',
  `postage` int(11) DEFAULT '0' COMMENT '邮费',
  `userAccess` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`refundId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='订单退款';

-- ----------------------------
-- Records of mall_order_refund
-- ----------------------------
INSERT INTO `mall_order_refund` VALUES ('1', 'R2017090112283825395', '20170829151505727350', '1', '15', 'REVOKED', '就是要退款，任性', null, 'ALIPAY', '2017-09-01 12:28:38', null, '', '', 'AA', 'AA', 'AA', '8', '3', '0', '1');
INSERT INTO `mall_order_refund` VALUES ('2', 'R2017090112283825395', '20170829151505727350', '1', '15', 'REVOKED', '就是要退款，任性', null, 'ALIPAY', '2017-09-01 12:28:38', null, '', '', 'AA', 'AA', 'AA', '8', '3', '0', '1');
INSERT INTO `mall_order_refund` VALUES ('12', 'R2017090611154395133', '20170906100407867332', '1', '99900', 'SUCCEEDED', '就是要退款，任性', null, 'WEPAY', '2017-09-06 11:15:43', null, '', '不喜欢', '顺丰', '261038405024', 'shunfeng', '10', '7', '0', '9797D71C02826C332A85DE87A12AC2E3');

-- ----------------------------
-- Table structure for mall_payment_callback
-- ----------------------------
DROP TABLE IF EXISTS `mall_payment_callback`;
CREATE TABLE `mall_payment_callback` (
  `callbackId` int(22) NOT NULL AUTO_INCREMENT COMMENT '回调结果ID',
  `callbackTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '支付回调时间',
  `callbackResult` varchar(4096) NOT NULL COMMENT '支付回调结果JSON串或XML文档',
  `payType` varchar(16) NOT NULL COMMENT '支付方式(ALI支付宝/WX微信）',
  `orderNo` varchar(64) DEFAULT NULL COMMENT '订单号',
  PRIMARY KEY (`callbackId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付回调结果表';

-- ----------------------------
-- Records of mall_payment_callback
-- ----------------------------

-- ----------------------------
-- Table structure for mall_setting
-- ----------------------------
DROP TABLE IF EXISTS `mall_setting`;
CREATE TABLE `mall_setting` (
  `settingId` int(11) NOT NULL AUTO_INCREMENT,
  `commonPostage` int(11) NOT NULL DEFAULT '10' COMMENT '一般邮费',
  `remotePostage` int(11) NOT NULL DEFAULT '20' COMMENT '偏远以及港澳地区邮费',
  `limitTotalFee` int(11) NOT NULL DEFAULT '99' COMMENT '满99减邮费',
  `orderRefresh` int(11) NOT NULL DEFAULT '7200' COMMENT '订单状态修改时间',
  PRIMARY KEY (`settingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城配置模板';

-- ----------------------------
-- Records of mall_setting
-- ----------------------------

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `accountId` int(11) NOT NULL AUTO_INCREMENT COMMENT '账户ID',
  `userName` varchar(20) DEFAULT NULL COMMENT '用户名',
  `email` varchar(30) DEFAULT NULL COMMENT '用户注册邮箱',
  `password` varchar(35) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(16) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(256) DEFAULT NULL COMMENT '头像',
  `realName` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `gender` varchar(1) DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `phoneNumber` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `registTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  `privilege` varchar(8) DEFAULT NULL COMMENT '第三方登录类别',
  `userAccess` varchar(1024) NOT NULL,
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES ('1', 'leo', '1169551923@qq.com', '123456', null, 'http://30days.img-cn-shenzhen.aliyuncs.com/a85407f27edf4128a57a79419df9e7b0_origin.jpg', null, null, null, null, '2017-08-28 12:11:22', null, '1');
INSERT INTO `user_account` VALUES ('11', 'leo1', null, '123456', null, 'http://30days.img-cn-shenzhen.aliyuncs.com/a85407f27edf4128a57a79419df9e7b0_origin.jpg', null, null, null, '18296154802', '2017-08-30 17:48:53', null, '9797D71C02826C332A85DE87A12AC2E3');
INSERT INTO `user_account` VALUES ('12', null, null, null, 'XXX', 'http://30days.img-cn-shenzhen.aliyuncs.com/cbf99be690b04399abd38a3476d6ab24_origin.jpg', null, '男', null, null, '2017-09-20 11:23:59', null, 'eyJpdiI6IlkwS0dPMFJGNExObTdPTGdzV09FOUE9PSIsInZhbHVlIjoiRGZ2K2JuRlBUbVdEWmJjeE5BWFdoa2tZTnRwQjhiVU82RllvbFNEWDFkeE90SkpVeVpxS3JVdHpEMk1ZVFR5OXNMZ3VhY29cL0lBaTVvcDg4WUIxM2NBPT0iLCJtYWMiOiI0NzA3OTJmMjdiNzUyYmU5YWRiMWNhMTg2YWNkNTEzZDU3MzVlN2E4ZGM2MjMwNzkzZWIxZDJjODMzNTc2ZmNhIn0=');
INSERT INTO `user_account` VALUES ('13', null, null, null, 'XXX', 'http://30days.img-cn-shenzhen.aliyuncs.com/cbf99be690b04399abd38a3476d6ab24_origin.jpg', null, '男', null, null, '2017-09-20 11:24:08', null, 'eyJpdiI6Im5aZGxJTTRhS0N0S0RJclMrWlhZS0E9PSIsInZhbHVlIjoiaVpQMEhiSDBhbFFSTUh5TXdPZVRjZHZqMHhtY3ZKeVkrbis1TExjbGdKSFZPOTZhT2NncUVLeHByQXBMU0k1TVVnOVNSWVVYblVHbUFXRWFnNDY2VlE9PSIsIm1hYyI6Ijc0NjE2NTY2YTU0MTIyNGZkZGIwNWQwNzQ3MThiMmY0MDdkZWU2ZmY2ODk3ODY4ZTAzZGUwNGY0NTFkNGUwMmYifQ==');
INSERT INTO `user_account` VALUES ('14', null, null, null, 'XXX', 'http://30days.img-cn-shenzhen.aliyuncs.com/cbf99be690b04399abd38a3476d6ab24_origin.jpg', null, '男', null, null, '2017-09-20 11:24:23', null, 'eyJpdiI6ImVHdU1hOGpYYWpkOTIzY1Q1NkxkOUE9PSIsInZhbHVlIjoiU3pRRUhmM0M4NVBBUVBvODk3QmZVblQyT2MyYkhLSGNkY0VqZXVzb1NoTm1vejM5cXZhck1VdVlwZ01OMXlETFVNTUNJQTVZYVYwZmJ0UlZPOVM0ZVE9PSIsIm1hYyI6IjYzNWZkYTgxMTMzOTc5YmI1OGYzYzAyOGMzNTc3MGNlZDNkZTcwYzQzZjZiYzhmYzE3OGFiMzZmYzdkMWJlZDIifQ==');
INSERT INTO `user_account` VALUES ('15', null, null, null, 'XXX', 'http://30days.img-cn-shenzhen.aliyuncs.com/cbf99be690b04399abd38a3476d6ab24_origin.jpg', null, '男', null, null, '2017-09-20 11:24:47', null, 'eyJpdiI6InVzR1NNTXNFSkdlM0VocnJSaE4yRnc9PSIsInZhbHVlIjoiM1FVT0U3U2JQVlZHSlV2bWNSQjBHeHViN2ZCejFcL0NJVWtpUWd1NG1TRENvWjZxcGFLbDZLWWhRK0tDSGxwQ3A0aGs0SFZ6WGViTUNRMFdmcWRLWnNBPT0iLCJtYWMiOiI2MGQ3NzNhNzMyNjlmYzhjNmEzMWYzNjEyYjE2MmVjMDZhODMzYWU3MDhlM2ZjOGZhZjllZGVjMjg4ZjBkNTQxIn0=');
INSERT INTO `user_account` VALUES ('20', null, null, null, 'XXX', 'http://30days.img-cn-shenzhen.aliyuncs.com/cbf99be690b04399abd38a3476d6ab24_origin.jpg', null, '男', null, null, '2017-09-20 11:29:59', null, 'eyJpdiI6InNBaUdQaFFYbFhhYUp3bjdMK1lEU1E9PSIsInZhbHVlIjoib2NTUmFDVVlpSkJEUjhGU3JsbGc3R29weEc2Mkx2WndUUkdqNFYwYVdKRGQyT0dQVTdNY1E1SDhkNzA0WUJ6VHF1YWpsYjI1U25PaXZVMDQ5QnVUOUE9PSIsIm1hYyI6ImY2Y2E5YTU0MGFhZjI1NzY4ZDk1ZDA5MzE2ZjlmNzczOWQ2NjA2N2YzODFmOTI5MDY2NzViODVkOWU3YTk5NDIifQ==');
INSERT INTO `user_account` VALUES ('21', null, null, null, 'XXX', 'http://30days.img-cn-shenzhen.aliyuncs.com/cbf99be690b04399abd38a3476d6ab24_origin.jpg', null, '男', null, null, '2017-09-20 11:30:34', null, 'eyJpdiI6Im80SGJScFVnc2Z5YTlXUDA4aXZEMlE9PSIsInZhbHVlIjoiNzVlTjVCSElJT0RoU1hkaUs1TXZJT3lBcWU5c3hCXC9jeGFqSnJ2S2hOQm9jUUNLQ3M0SGlmaHcwaHBBMFZ0Q1BmMm5KbmpTNytDYUNDWDBvdkVcLzZhQT09IiwibWFjIjoiY2YwNjhjYjc2YTc5YzYzOWZhNDNhNDAxZmE0YzU3OThhYzJiMzUxMGE2NmI2NGRjYmY2MjdkMTU2NWY1NDQyOSJ9');
INSERT INTO `user_account` VALUES ('22', null, null, null, 'XXX', 'http://30days.img-cn-shenzhen.aliyuncs.com/cbf99be690b04399abd38a3476d6ab24_origin.jpg', null, '男', null, null, '2017-09-20 11:31:02', null, 'eyJpdiI6IjhrQjRKdlVCRjc2czlDVkhtUTdvc2c9PSIsInZhbHVlIjoiN0QybkdrMThvK2JMMG5nWmY5R08yTFZpVEJFZ3BNUytjdTFxaisrWlJZZEQ2c1wvMmJpemFyTDFhM3lUWnUxT2pYODhHQ28rd1lpNXV2Qyt0T2duU2hBPT0iLCJtYWMiOiJjZmQzNTc3ODcwODEzYWFkYzhiZTIxNzIwOTExZmIxMmY0NWNiODU4Y2U0NjM4ZjdkYTE4OWZlOWQyMzc2NzRiIn0=');

-- ----------------------------
-- Table structure for user_account_info
-- ----------------------------
DROP TABLE IF EXISTS `user_account_info`;
CREATE TABLE `user_account_info` (
  `infoId` int(11) NOT NULL AUTO_INCREMENT,
  `openId` varchar(128) NOT NULL,
  `unionId` varchar(128) DEFAULT NULL,
  `accountId` int(11) NOT NULL,
  PRIMARY KEY (`infoId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='用户三方信息表';

-- ----------------------------
-- Records of user_account_info
-- ----------------------------
INSERT INTO `user_account_info` VALUES ('3', 'CE3A270DD266429061ADC756CFD3CFE3', null, '20');
INSERT INTO `user_account_info` VALUES ('4', 'CE3A270DD266429061ADC756CFD3CFE13', null, '21');
INSERT INTO `user_account_info` VALUES ('5', 'CE3A270DD266429061ADC756CFD3CF1E13', null, '22');

-- ----------------------------
-- Table structure for user_manager
-- ----------------------------
DROP TABLE IF EXISTS `user_manager`;
CREATE TABLE `user_manager` (
  `managerId` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `userName` varchar(32) NOT NULL COMMENT '登录名',
  `realName` varchar(20) DEFAULT NULL,
  `connectTel` varchar(20) DEFAULT NULL,
  `password` varchar(64) NOT NULL COMMENT '登录密码',
  `roleId` int(11) DEFAULT '0' COMMENT '用户所属角色ID',
  `enabledStatus` tinyint(1) NOT NULL DEFAULT '1' COMMENT '账号状态（1为可使用，0为禁用）',
  `createTime` datetime DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`managerId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Records of user_manager
-- ----------------------------
INSERT INTO `user_manager` VALUES ('1', '管理员', '李琮', '18296154802', '123456', '1', '0', '2017-08-30 14:54:35', '2017-08-30 14:54:35');
INSERT INTO `user_manager` VALUES ('3', '管理员1', '李琮', '18296154802', '123456', '3', '0', '2017-08-30 14:54:56', '2017-08-30 14:54:56');
INSERT INTO `user_manager` VALUES ('5', '管理员2', '李琮', '18296154802', '$2y$10$hZmpRq2YrVitpl.OKmSP/uwH6lXJ06cTO6N.KKamJFAmf1gpBoLeC', '3', '1', '2017-08-30 14:57:40', '2017-09-28 16:38:42');
INSERT INTO `user_manager` VALUES ('7', '管理员3', '李琮', '18296154802', '123456', '3', '1', '2017-08-30 14:58:35', '2017-09-19 16:29:07');

-- ----------------------------
-- Table structure for user_manager_role
-- ----------------------------
DROP TABLE IF EXISTS `user_manager_role`;
CREATE TABLE `user_manager_role` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(32) NOT NULL COMMENT '角色名称',
  `roleDesc` varchar(256) NOT NULL COMMENT '角色描述',
  `resources` varchar(2048) DEFAULT NULL COMMENT '拥有资源，多个资源之间用英文分号;隔开',
  `createTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of user_manager_role
-- ----------------------------
INSERT INTO `user_manager_role` VALUES ('3', '超级管理员2', '管理系统所有资源', '资源1;资源2;资源3', '2017-08-30 14:53:23', '2017-08-30 14:53:23');
INSERT INTO `user_manager_role` VALUES ('4', '超级管理员', '管理系统所有资源', '资源1;资源2;资源3', null, '2017-09-19 16:06:37');
INSERT INTO `user_manager_role` VALUES ('5', '超级管理员', '管理系统所有资源', '资源1;资源2;资源3', null, '2017-09-19 16:09:23');
