<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2018-02-06
 * Time: 14:12
 */

namespace App\Services\Email;

use Mail;

class EmailService
{
//  public function send()
//  {
//      $subject = "退款成功通知";
//      $message = "失败";
//      Mail::raw($message,function ($m) use ($subject){
//          $m->subject($subject);
//          $m->to("1063446909@qq.com");
//      });
//  }
    public function send()
    {
        $name = '测试';
        // Mail::send()的返回值为空，所以可以其他方法进行判断
        Mail::send('emails.test',['name'=>$name],function($message){
            $to = '1063446909@qq.com';
            $message ->to($to)->subject('邮件测试');
        });
        // 返回的一个错误数组，利用此可以判断是否发送成功
        dd(Mail::failures());
    }
}