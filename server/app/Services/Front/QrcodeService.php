<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-10-14
 * Time: 11:32
 */

namespace App\Services\Front;

use App\Libs\OSS;
use Illuminate\Support\Facades\Crypt;

class QrcodeService
{
    /**
     * 返回二维码地址
     * @param $accessToken
     * @param $orderNo
     * @return string
     */
    public function qrcode($accessToken, $orderNo)
    {
        $fileName = time().'.jpg';
       // $orderNo =Crypt::encrypt($orderNo);
        $url = "http://qr.topscan.com/api.php?text=".$orderNo;
        file_put_contents('static/'.$fileName, file_get_contents($url));
        $newName = md5(date('ymdhis') . $fileName) . ".jpg" ;    //定义上传文件的新名称
        $path = 'groupon/'.$newName ;
        $model = new OSS();
        $model->publicUpload('xcxhn',$path,'static/'.$fileName,[ 'ContentType' => "image/jpeg"]);
        $data = $model->url.$newName;
       @unlink('static/'.$fileName);

        return $data;
    }
}