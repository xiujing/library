<?php
/**
 * 商品 业务层
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-05
 * Time: 17:58
 */

namespace App\Services\Manager;


use App\Models\CommoditySku;

class Commodity
{
    /**
     * 计算原价的最大以及最小价格
     * @param $param
     * @return array
     */
    private function commodityPrice($param)
    {
        $maxPrice = 0;
        $minPrice = 999999999;
        foreach ($param as $value){
            if($value['commodityPrice'] > $maxPrice){
                $maxPrice = $value['commodityPrice'];
            }
            if($value['commodityPrice'] < $minPrice){
                $minPrice = $value['commodityPrice'];
            }
        }
        return [$minPrice, $maxPrice];
    }

    /**
     * 计算优惠价的最大以及最小价格
     * @param $param
     * @return array
     */
    private function commodityDiscountPrice($param)
    {
        $maxPrice = 0;
        $minPrice = 999999999;
        foreach ($param as $value){
            if($value['discount'] > $maxPrice){
                $maxPrice = $value['discount'];
            }
            if($value['discount'] < $minPrice){
                $minPrice = $value['discount'];
            }
        }
        return [$minPrice, $maxPrice];
    }

    /**
     * 根据参数初始化通用商品数据
     */
    public function _initCommonGoodsByParam($param) {
        $common_array = array();
        list($mimPrice, $maxPrice) = $this->commodityPrice($param['skuValueList']);
        list($minDiscount, $maxDiscount)             = $this->commodityDiscountPrice($param['skuValueList']);
        $common_array['categoryId']                  = $param['categoryId'];
        $common_array['commodityName']               = $param['commodityName'];
        $common_array['commodityIcon']               = $param['commodityIcon'];
        $common_array['descPictures']                = $param['descPictures'];
        $common_array['maxPrice']                    = $maxPrice;
        $common_array['minPrice']                    = $mimPrice;
        $common_array['maxDiscount']                 = $maxDiscount;
        $common_array['minDiscount']                 = $minDiscount;
        $common_array['optionValue']                 = $param['optionValue'];
        $common_array['description']                 = $param['description'];
        $common_array['createTime']                  = date("Y-m-d H:i:s",time());
        return $common_array;
    }

    /**
     * 规格属性
     * @param $attr
     * @param $goodsId
     * @return array
     */
    public function addGoodsPrice($sku, $commodityId)
    {
        $result = [];
        foreach ($sku as $key => $value){
            $result[$key]['commodityId'] = $commodityId;
            $result[$key]['stock'] = $sku[$key]['stock'];
            $result[$key]['skuDesc'] = serialize($sku[$key]['skuDesc']);
            $result[$key]['commodityPrice'] = $sku[$key]['commodityPrice'];
            $result[$key]['discount'] = $sku[$key]['discount'];
            $result[$key]['skuId'] = $sku[$key]['skuId'];
        }
        return $result;
    }

    /**
     * 修改规格
     * @param $commoditySku
     * @param $status
     * @return bool
     * @throws \Exception
     */
    public function updateCommoditySku($commoditySku,$status)
    {

        $skuIdArr = [];
        if($status){//修改
            foreach ($commoditySku as $key => $value){
                $skuId = $value['skuId'];
                unset($value['skuId']);
                if($skuId !=0){
                    $skuIdArr[] = $skuId;
                    CommoditySku::where('skuId',$skuId)->update($value);
                }else{
                    $status = CommoditySku::create($value);
                    if(!$status){
                        throw new \Exception('插入商品规格失败');
                    }
                    $skuIdArr[] = $status['skuId'];
                }
            }
            if($skuIdArr){
                CommoditySku::whereNotIn('skuId',$skuIdArr)->delete();
            }

            return true;
        }else{//新增
            return CommoditySku::insert($commoditySku);
        }
    }
}