<?php
/**
 * 退款处理
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-07
 * Time: 15:02
 */

namespace App\Services\Manager;


use App\Libs\Contracts\PayMent\AliPay\AliWeb;
use App\Libs\Contracts\PayMent\Wxpay\Refund;
use App\Models\Order;
use App\Models\OrderRefund;

class RefundService
{
    /**
     * 开始退款 原路返回
     * @param $order  orderNo 订单号、payType原支付方式
     * @param $type  1 代表最后一笔订单
     * @return bool
     * @throws \Exception
     */
    public function onLineRefund($order, $type=0, $postage=0)
    {
        //开始退款
        $totalPrice = 0 ;
        $refund = OrderRefund::find($order['refundId']);
        if(!$refund){
            throw new \Exception("退款参数错误");
        }
        if($type){
            $totalPrice = $refund['refundPrice'] + $postage;
        }else{
            $totalPrice = $refund['refundPrice'];
        }
        $refundPayType = $order['payType'];
        //修改状态
        $result['orderNo'] = $order['orderNo'];
        $result['refundNo'] = $refund['refundNo'];
        $result['totalPrice'] = $order['totalPrice'];
        $result['refundPrice'] = $totalPrice;
        //退款
        if($refundPayType === "WX"){//微信退款
            $model = new Refund();
            $model->refund($result);
        }else{//支付宝退款
            $model = new AliWeb();
            $result['orderNo'] = $order['orderNo'];
            $result['refundPrice'] = $totalPrice / 100;
            $result['refundReason'] = $refund['refundReason'];
            $result['refundNo'] = $refund['refundNo'];
            $model->refund($result);
        }

        if($type){
            $status = OrderRefund::where(['refundId'=>$order['refundId']])->update(['refundStatus'=>'SUCCEEDED','postage'=>$postage,'finishTime'=>date("Y-m-d H:i:s",time())]);
        }else{
            $status = OrderRefund::where(['refundId'=>$order['refundId']])->update(['refundStatus'=>'SUCCEEDED']);
        }
        if(!$status){
            throw new \Exception("修改退款状态失败");
        }
        return true;
    }

    /**
     * 修改订单状态
     * @param $order
     * @return bool
     * @throws \Exception
     */
    public function updateOrder($order)
    {
        $status = Order::where(['orderNo'=>$order['orderNo']])->first();
        if(!$status){
            throw new \Exception("订单号错误");
        }
        if($status['orderStatus'] === 'WAITREFUND') {//证明全部退款，需要修改订单状态
            if($status['deliverNo']){//证明已经发货，返回到待收货状态
                $orderStatus = Order::where('orderNo',$order['orderNo'])->update(['orderStatus'=>'WAITRECEIVE']);
                if(!$orderStatus){
                    throw new \Exception("修改订单状态失败");
                }
            }else{//没有发货，返回到发货状态
                $orderStatus = Order::where('orderNo',$order['orderNo'])->update(['orderStatus'=>'WAITDELIVER']);
                if(!$orderStatus){
                    throw new \Exception("修改订单状态失败");
                }
            }
        }

        return true;
    }
}