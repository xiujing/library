<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-10-19
 * Time: 11:03
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserManagerLog extends Model
{
    protected $table = 'user_manager_log';
    protected $primaryKey = 'managerLogId';
    protected $guarded = [''];
    public $timestamps = false;
}