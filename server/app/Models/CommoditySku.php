<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 11:11
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CommoditySku extends Model
{
    protected $table = 'mall_commodity_sku';
    protected $primaryKey = 'skuId';
    protected $guarded = [];
    public $timestamps = false;
}