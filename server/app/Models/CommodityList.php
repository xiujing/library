<?php
/**
 *
 * User: 刘利达  damonlld@126.com
 * Date: 2018-12-20
 * Time: 18:52
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CommodityList extends Model
{
    protected $table = 'mall_commodity';
    protected $primaryKey = 'commodityId';
    protected $guarded = [];
    public $timestamps = false;
}