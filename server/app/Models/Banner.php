<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-10-11
 * Time: 18:14
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner';
    protected $primaryKey = 'bannerId';
    protected $guarded = [];
    public $timestamps = false;
}