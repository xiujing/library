<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 18:07
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderRefund extends Model
{
    protected $table = 'mall_order_refund';
    protected $primaryKey = 'refundId';
    protected $guarded = [];
    public $timestamps = false;
}