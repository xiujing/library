<?php
/**
 *
 * User: 刘利达  damonlld@126.com
 * Date: 2018-12-20
 * Time: 19:13
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class QuestionsAnswers extends Model
{
    protected $table = 'questions_answers';
    protected $primaryKey = 'qaId';
    protected $guarded = [];
    public $timestamps = false;
}