<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-05
 * Time: 11:36
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserManagerRole extends Model
{
    protected $table = 'user_manager_role';
    protected $primaryKey = 'roleId';
    protected $guarded = [];
    public $timestamps = false;

}