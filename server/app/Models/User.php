<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-10-11
 * Time: 15:25
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'userId';
    protected $guarded = [''];
    public $timestamps = false;
	
	public function field()
    {
        return $this->belongsToMany('App\Models\AdditionalFields','user_addition' ,'userId','fieldsId')->withPivot('value');
    }
}