<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 11:08
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    protected $primaryKey = 'commodityId';
    protected $table = 'commodity';
    protected $guarded = [];
    public $timestamps = false;

}