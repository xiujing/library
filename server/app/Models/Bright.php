<?php
/**
 *
 * User: 刘利达  damonlld@126.com
 * Date: 2018-12-20
 * Time: 18:49
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Bright extends Model
{
    protected $table = 'fwBright';
    protected $primaryKey = 'fwBrightId';
    protected $guarded = [];
    public $timestamps = false;
}