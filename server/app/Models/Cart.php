<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-07
 * Time: 22:36
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
        protected $table = 'mall_cart';
        protected $primaryKey = 'cartId';
        protected $guarded = [];
        public $timestamps = false;
}