<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 15:15
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'mall_order_detail';
    protected $primaryKey = 'detailId';
    protected $guarded = [];
    public $timestamps = false;

    public function orderReturn()
    {
        return $this->hasOne("App\Models\OrderRefund",'orderNo','orderNo');
    }
}