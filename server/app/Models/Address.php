<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-15
 * Time: 15:22
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'mall_address';
    protected $primaryKey = 'addressId';
    protected $guarded = [];
    public $timestamps = false;
}