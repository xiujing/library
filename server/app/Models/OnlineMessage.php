<?php
/**
 *
 * User: 刘利达  damonlld@126.com
 * Date: 2018-12-20
 * Time: 19:10
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OnlineMessage extends Model
{
    protected $table = 'onlineMessage';
    protected $primaryKey = 'onlineMessageId';
    protected $guarded = [];
    public $timestamps = false;
}