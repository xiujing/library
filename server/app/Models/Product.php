<?php
/**
 *
 * User: 刘利达  damonlld@126.com
 * Date: 2018-12-20
 * Time: 19:01
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'indexProduct';
    protected $primaryKey = 'productId';
    protected $guarded = [];
    public $timestamps = false;
}