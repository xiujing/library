<?php
/**
 * 管理员
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-05
 * Time: 10:21
 */

namespace App\Models;

use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject as AuthenticatableUserContract;


class UserManager extends Authenticatable implements AuthenticatableUserContract
{

    protected $table = 'user_manager';
    protected $primaryKey = 'managerId';
    protected  $guarded = [];
    protected $hidden = ['password'];
    public $timestamps = false;

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey(); // Eloquent model method
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
