<?php
/**
 *
 * User: 刘利达  damonlld@126.com
 * Date: 2018-12-20
 * Time: 19:04
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $primaryKey = 'newsId';
    protected $guarded = [];
    public $timestamps = false;
}