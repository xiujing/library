<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'user_account';
    protected $primaryKey = 'userId';
    protected $guarded = [];
    public $timestamps = false;
}