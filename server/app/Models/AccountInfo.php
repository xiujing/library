<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-20
 * Time: 11:13
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AccountInfo extends Model
{
    protected $table = 'user_account_info';
    protected $primaryKey = 'infoId';
    protected $guarded = [];
    public $timestamps = true;
}