<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 15:07
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'mall_order';
    protected $primaryKey = 'orderId';
    protected $guarded = [];
    public $timestamps = false;

    /**
     * 一对多
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderDetail()
    {
        return $this->hasMany("App\Models\OrderDetail","orderNo","orderNo");
    }
}