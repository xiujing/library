<?php
/**
 * 支付回调模型
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-11
 * Time: 15:52
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PaymentCallback extends Model
{
    protected $table = 'mall_payment_callback';
    protected $primaryKey = 'callbackId';
    protected $guarded = [];
    public $timestamps = false;
}