<?php
/**
 *
 * User: 刘利达  damonlld@126.com
 * Date: 2018-12-20
 * Time: 18:49
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';
    protected $primaryKey = 'aboutId';
    protected $guarded = [];
    public $timestamps = false;
}