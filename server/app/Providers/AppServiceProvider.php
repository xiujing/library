<?php

namespace App\Providers;

use App\Libs\Contracts\Express\ExpressManager;
use App\Libs\Contracts\SMS\SMSManager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->registerKuaidi();
        $this->registerSMS();
    }
    protected function registerKuaidi()
    {
        $this->app->singleton('kuaidi',function ($app){
            return new ExpressManager($app);
        });
    }
    protected function registerSMS()
    {
        $this->app->singleton('sms', function ($app){
            return new SMSManager($app);
        });
    }
}
