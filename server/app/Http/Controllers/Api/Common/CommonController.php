<?php
/**
 * Created by PhpStorm.
 * User: Xiaowei Liang
 * Date: 2017-09-01
 * Time: 15:26
 */

namespace App\Http\Controllers\Api\Common;


use App\Http\Controllers\Controller;
use App\Libs\Contracts\Orc\AipOcr;
use App\Libs\Contracts\PayMent\Wxpay\H5NativePay;
use App\Libs\Contracts\PayMent\Wxpay\NativePay;
use App\Libs\Contracts\PayMent\Wxpay\Refund;
use App\Libs\Contracts\PayMent\Wxpay\ScanNativePay;
use App\Libs\Contracts\Qrcode\QRencode;
use App\Libs\OSS;
use App\Services\Wx\WxService;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function ossUpload(Request $request)
    {
        $file = $request->file('file');
        if ($file->isValid()) {

            $realPath = $file->getRealPath();
            $entension = $file -> getClientOriginalExtension();
            $clientName = $file->getClientOriginalName();    //客户端文件名称
            $newName = md5(date('ymdhis') . $clientName) . "." . $entension;    //定义上传文件的新名称

            //$mimeTye = $file->getMimeType();
            $path = env("STATIC_URL")."/uploads/image/" . $newName;
            move_uploaded_file($realPath, $path);

            $data = env("BATH_PATH")."/uploads/image/" . $newName;
            return $this->apiResponse($data,"上传文件成功");

        }
        return $this->apiResponse('','上传文件失败',10000);
    }

    public function kuaidi(Request $request)
    {
        $name = $request->name;
        $code  = $request->code;
        $data = app("kuaidi")->getData($name,$code);
        return $this->apiResponse($data,"获取快递成功");
    }

    /**
     * 通过手机号获取验证码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function code(Request $request)
    {
        $phone = $request->phone;
        if(!$phone){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $code = rand(10000,99999);
        $status = app('sms')->send($phone, $code);
        if($status == 100){
            return $this->apiResponse('');
        }else{
            return $this->apiResponse('', config('errorCode.CODE_FAILS'));
        }
    }

    /**
     * 身份证识别
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function idcard(Request $request)
    {
        $certificatesNumber = $request->certificatesNumber;
        $certificatesPositive = $request->certificatesPositive;
       /* if(!$certificatesNumber || !$certificatesPositive){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }*/
        $wxService = new WxService();
        $url = $wxService->getFilePath($certificatesPositive);
        $aipOcr = new AipOcr(env("BAIDU_APPID"), env("BAIDU_API_KEY"), env("BAIDU_SECRET_KEY"));
        $data = $aipOcr->idcard(file_get_contents($url), true);
        if(!empty($data['error_code'])){
            return $this->apiResponse('', config('errorCode.CARD_NO'));
        }
        $result['certificatesNumber'] = $data['words_result']['公民身份号码']['words'];
        $result['userName'] = $data['words_result']['姓名']['words'];
        $result['birthday'] = date("Y-m-d",strtotime($data['words_result']['出生']['words']));
        $result['sex'] =  str_replace(array("男", "女"), array('1', '2'), $data['words_result']['性别']['words']);
        $result['nation'] = $data['words_result']['民族']['words'];
        return $this->apiResponse($result);
        /*$cards = $data['words_result']['公民身份号码']['words'];
        if($cards != $certificatesNumber){
            return $this->apiResponse('', config('errorCode.CARD_ERROR'));
        }*/
    }
}