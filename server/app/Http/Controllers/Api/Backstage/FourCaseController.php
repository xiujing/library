<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\FourCase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FourCaseController extends Controller{

    public function index(){
        $FourCaseList = FourCase::paginate(20);
        return $this->apiResponse($FourCaseList);
    }


    public function  show($fwActivityId){
        $data = FourCase::find($fwActivityId);
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'fwActivityId' => 'integer|required',
            'picture' => 'required',
            'synopsis' => 'required',
            'content' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }

        $data['createTime'] = date("Y-m-d H:i:s");
        $fwActivityId = @$data['fwActivityId'];
        unset($data['$fwActivityId']);

        if ($fwActivityId){
            $status = FourCase::where('fwActivityId', $fwActivityId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $status = FourCase::create($data);

            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);

    }

//    public function update(Request $request, $fwActivityId)
//    {
//        $data = $request->all();
//        $validator = Validator::make($data, [
//        ]);
//        if ($validator->fails()) {
//            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
//        }
//        $status = FourCase::where('fwActivityId', $fwActivityId)->update($data);
//        if (!$status) {
//            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
//        }else{
//            return $this->apiResponse(true);
//        }
//    }


    public function destroy($fwActivityId)
    {
        $data = FourCase::destroy($fwActivityId);
        return $this->apiResponse($data);
    }
}
