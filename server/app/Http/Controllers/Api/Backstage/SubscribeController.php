<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\SubscribeMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscribeController extends Controller
{
    public function index()
    {
        $data['subscribeList'] = SubscribeMessage::paginate(20);
        return $this->apiResponse($data);
    }

    public function destroy($subscribeId)
    {
        $data = SubscribeMessage::destroy($subscribeId);
        return $this->apiResponse($data);
    }

    public function show($subscribeId){
        $data = SubscribeMessage::find($subscribeId);
        return $this->apiResponse($data);
    }

}
