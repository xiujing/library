<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends Controller
{
    public function index()
    {
        $data['feedbackList'] = Feedback::paginate(20);
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'feedbackId' => 'integer|required',
            'name' => 'required',
            'content' => 'required',
            'avatar' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['createTime'] = date("Y-m-d H:i:s");
        $feedbackId = @$data['feedbackId'];
        unset($data['feedbackId']);
        if ($feedbackId){
            $status = Feedback::where('feedbackId', $feedbackId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $status = Feedback::create($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);
    }

    public function show($feedbackId)
    {
        $data = Feedback::find($feedbackId);
        return $this->apiResponse($data);
    }

    public function destroy($feedbackId)
    {
        $data = Feedback::destroy($feedbackId);
        return $this->apiResponse($data);
    }
}
