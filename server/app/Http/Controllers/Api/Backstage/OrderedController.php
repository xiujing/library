<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\BoxMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderedController extends Controller
{
    public function index()
    {
        $data['boxMessageList'] = BoxMessage::paginate(20);
        return $this->apiResponse($data);
    }

    public function destroy($boxMessageId)
    {
        $data = BoxMessage::destroy($boxMessageId);
        return $this->apiResponse($data);
    }

    public function show($boxMessageId){
        $data = BoxMessage::find($boxMessageId);
        return $this->apiResponse($data);
    }
}
