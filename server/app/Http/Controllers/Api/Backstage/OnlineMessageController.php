<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\OnlineMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OnlineMessageController extends Controller
{
    public function index()
    {
        $data['onlineMessageList'] = OnlineMessage::paginate(20);
        return $this->apiResponse($data);
    }

    public function destroy($onlineMessageId)
    {
        $data = OnlineMessage::destroy($onlineMessageId);
        return $this->apiResponse($data);
    }

    public function show($onlineMessageId){
        $data = OnlineMessage::find($onlineMessageId);
        return $this->apiResponse($data);
    }
}
