<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\SwCamp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SwcampController extends Controller
{
    public function index()
    {
        $data['swcampList'] = SwCamp::paginate(20);
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'swcampId' => 'integer|required',
            'picture' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['createTime'] = date("Y-m-d H:i:s");
        $swcampId = @$data['swcampId'];
        unset($data['swcampId']);

        if ($swcampId){
            $status = Swcamp::where('swcampId', $swcampId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $status = Swcamp::create($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);

    }

    public function show($swcampId)
    {
        $data = Swcamp::find($swcampId);
        return $this->apiResponse($data);
    }

    public function destroy($swcampId)
    {
        $data = Swcamp::destroy($swcampId);
        return $this->apiResponse($data);
    }
}
