<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Bright;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BrightController extends Controller
{

    public function index(){
        $BrightList = Bright::get();
        return $this->apiResponse($BrightList);
    }

    public function  show($BrightId){
        $data = Bright::find($BrightId);
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'fwBrightId' => 'integer|required',
            'picture' => 'required',
            'synopsis' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['createTime'] = date("Y-m-d H:i:s");
        $fwBrightId = @$data['fwBrightId'];
        unset($data['fwBrightId']);

        if ($fwBrightId){
            $status = Bright::where('fwBrightId', $fwBrightId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $status = Bright::create($data);

            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);

    }

//    public function update(Request $request, $BrightId)
//    {
//        $data = $request->all();
//        $validator = Validator::make($data, [
//        ]);
//        if ($validator->fails()) {
//            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
//        }
//        $status = Bright::where('Bright', $BrightId)->update($data);
//        if (!$status) {
//            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
//        }else{
//            return $this->apiResponse(true);
//        }
//    }


    public function destroy($BrightId)
    {
        $data = Bright::destroy($BrightId);
        return $this->apiResponse($data);
    }
}
