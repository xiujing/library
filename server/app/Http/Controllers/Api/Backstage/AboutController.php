<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{
    public function index()
    {
        $data = About::get();
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'aboutId' => 'integer',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }

        $aboutId = @$data['aboutId'];
        unset($data['aboutId']);
        $status = About::where('aboutId', $aboutId)->update($data);

        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }
}
