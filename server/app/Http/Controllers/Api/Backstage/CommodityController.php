<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Commodity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommodityController extends Controller
{
    public function index()
    {
        $data['CommodityList'] = Commodity::where('commodityClass',1)
            ->orderBy('commodityId','Desc')->paginate(20);
        $i = 0;
        while(@$data['CommodityList'][$i]){
            $data['CommodityList'][$i]['hasVideo'] = unserialize($data['CommodityList'][$i]['hasVideo']);
            $i++;
        }
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'commodityId' => 'integer',
            "commodityName" =>'required',
            "description" =>'required',
            "commodityIcon" =>'required',
            "bgPicture" =>'required',
//            "boxVideo" =>'required',
            "boxContent" =>'required',
//            "hasVideo" =>'required',
            "recommendFirst" =>'required',
            "recommendSecond" =>'required',
            "recommendThird" =>'required',
            "simple" =>'required',
            "maxPrice" =>'required|numeric',
            "minPrice" =>'required|numeric',
            "optionValue" =>'required',
            ''
        ]);
//        return $data;
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['createTime'] = date("Y-m-d H:i:s");
        $data['hasVideo'] = serialize($data['hasVideo']);
        $commodityId = @$data['commodityId'];
        unset($data['commodityId']);

        if ($commodityId){
            $status = Commodity::where('commodityId', $commodityId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $data['commodityClass'] = 1;
            $status = Commodity::create($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);

    }

    public function show($commodityId)
    {
        $data = Commodity::find($commodityId);
        $data['hasVideo'] = unserialize($data['hasVideo']);
        return $this->apiResponse($data);
    }

    public function update(Request $request, $commodityId)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            "recommend" =>'integer|required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }

        Commodity::where('recommend','=','1')->update(['recommend'=>0]);

        $status = Commodity::where('commodityId', $commodityId)->update(['recommend'=>$data['recommend']]);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        }
        return $this->apiResponse(true);
    }

    public function updateStatus(Request $request, $commodityId)
    {
        $status = Commodity::find($commodityId);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        }
        if ($status['saleStatus'] == 1){
            Commodity::where('commodityId', $commodityId)->update(['saleStatus'=> 2]);
        }else{
            Commodity::where('commodityId', $commodityId)->update(['saleStatus'=> 1]);

        }

        return $this->apiResponse(true);
    }

    public function destroy($commodityId)
    {
        $data = Commodity::destroy($commodityId);
        return $this->apiResponse($data);
    }
}
