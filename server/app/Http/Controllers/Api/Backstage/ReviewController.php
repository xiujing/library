<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    public function index()
    {
        $data['reviewList'] = Review::paginate(20);
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'reviewId' => 'integer|required',
            'picture' =>  'required',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['createTime'] = date("Y-m-d H:i:s");
        $reviewId = @$data['reviewId'];
        unset($data['reviewId']);

        if ($reviewId){
            $status = Review::where('reviewId', $reviewId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $status = Review::create($data);

            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);

    }

    public function show($reviewId)
    {
        $data = Review::find($reviewId);
        return $this->apiResponse($data);
    }

    public function destroy($reviewId)
    {
        $data = Review::destroy($reviewId);
        return $this->apiResponse($data);
    }
}
