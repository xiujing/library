<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Commodity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommodityFController extends Controller
{
    public function index(Request $request)
    {
        $commodityClass = $request->commodityClass;
        $data['CommodityList'] = Commodity::select('commodityId','commodityName','description','commodityIcon','maxPrice','minPrice','simple','boxContent','commodityClass','createTime')->where('commodityClass','!=',1)
            ->orderBy('commodityId','Desc')->paginate(20);
        if (@$commodityClass){
            $data['CommodityList'] = Commodity::select('commodityId','commodityName','description','commodityIcon','maxPrice','minPrice','simple','commodityClass','createTime')->where('commodityClass',$commodityClass)
                ->orderBy('commodityId','Desc')->paginate(20);
        }

        $class = array();
        $class['1'] = '盒子';
        $class['2'] = '四驱车';
        $class['3'] = '双轨跑道';
        $i = 0;
        while(@$data['CommodityList'][$i]){
            $data['CommodityList'][$i]['commodityClass'] = $class[$data['CommodityList'][$i]['commodityClass']];
            $i++;
        }
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'commodityId' => 'integer',
            "commodityName" =>'required',
            "description" =>'required',
            "commodityIcon" =>'required',
            "maxPrice" =>'required|numeric',
            "minPrice" =>'required|numeric',
            'simple'   =>'required',
            "commodityClass" => 'required|integer'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['createTime'] = date("Y-m-d H:i:s");
        $commodityId = @$data['commodityId'];
        unset($data['commodityId']);

        if ($commodityId){
            $status = Commodity::where('commodityId', $commodityId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $status = Commodity::create($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);

    }

    public function show($commodityId)
    {
        $data = Commodity::select('commodityId','commodityName','description','commodityIcon','maxPrice','minPrice','simple','boxContent','commodityClass','createTime')->find($commodityId);
        return $this->apiResponse($data);
    }

    public function update(Request $request, $commodityId)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            "recommend" =>'integer|required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }

        Commodity::where('recommend','=','1')->update(['recommend'=>0]);

        $status = Commodity::where('commodityId', $commodityId)->update(['recommend'=>$data['recommend']]);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        }
        return $this->apiResponse(true);
    }

    public function updateState(Request $request, $commodityId)
    {
        $status = Commodity::find($commodityId);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        }

        $status = Commodity::where('commodityId', $commodityId)->update(['valueState'=> !$status['valueState']]);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        }
        return $this->apiResponse(true);
    }

    public function destroy($commodityId)
    {
        $data = Commodity::destroy($commodityId);
        return $this->apiResponse($data);
    }
}
