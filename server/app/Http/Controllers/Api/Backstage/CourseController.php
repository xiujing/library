<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function index()
    {
        $data['courseList'] = Course::paginate(20);
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'courseId' => 'integer|required',
            'picture' => 'required',
            'code' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['createTime'] = date("Y-m-d H:i:s");
        $courseId = @$data['courseId'];
        unset($data['courseId']);

        if ($courseId){
            $status = Course::where('courseId', $courseId)->update($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
            }
        }else{
            $status = Course::create($data);
            if (!$status) {
                return $this->apiResponse('', config('errorCode.INSERT_REQ_FAILED'));
            }

        }
        return $this->apiResponse(true);

    }

    public function show($courseId)
    {
        $data = Course::find($courseId);
        return $this->apiResponse($data);
    }

    public function destroy($courseId)
    {
        $data = Course::destroy($courseId);
        return $this->apiResponse($data);
    }
}
