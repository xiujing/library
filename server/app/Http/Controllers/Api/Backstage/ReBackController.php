<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Commodity;
use App\Models\ReBack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ReBackController extends Controller
{

    public function index()
    {
        $data = ReBack::select('t1.*','t2.commodityName','t2.hasVideo')
                    ->from((new ReBack())->getTable().' as t1')
                    ->join((new Commodity())->getTable().' as t2','t1.commodityId','=','t2.commodityId')
                    ->paginate(20)->toArray();
        foreach ($data['data'] as &$value ){
            $value['videoName'] = unserialize($value['hasVideo'])[$value['videoOrder']-1]['boxName'];
            $value['videoPicture'] = unserialize($value['hasVideo'])[$value['videoOrder']-1]['boxPicture'];
            unset($value['hasVideo']);
        }
        $result['data'] = $data['data'];
        $result['totalNum'] = $data['total'];
        return $this->apiResponse($result);
    }

    public function show($rebackId)
    {
        $data = ReBack::where('rebackId',$rebackId)->get();
        return $this->apiResponse($data);
    }

    public function destroy($rebackId)
    {
        $data = ReBack::destroy($rebackId);
        return $this->apiResponse($data);
    }
}
