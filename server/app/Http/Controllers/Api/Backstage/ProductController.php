<?php

namespace App\Http\Controllers\Api\Backstage;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index()
    {//GET  backstage/product show
        $data['productList'] = Product::get();
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {//POST backstage/product  edi update
        $data = $request->all();
        $productId = $data['productId'];
        $validator = Validator::make($data,[
            'productId' => 'integer|required',
            'caption'    => 'required',
            'content' => 'required',
            'picture' => 'required',
            'smallPicture' => 'required',
            'link' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        if ($productId) {//修改
            $status = Product::where('productId', $productId)->update($data);
        }

        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        } else {
            return $this->apiResponse(true);
        }
    }

    public function show($productId)
    {//GET backstage/product/{productId} show solo
        $data = Product::find($productId);
        return $this->apiResponse($data);
    }

}
