<?php
namespace App\Http\Controllers\Api\Web;

use App\Http\Controllers\Controller;
use App\Models\Commodity;
use App\Models\Feedback;
use App\Models\News;
use App\Models\Product;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IndexController extends Controller{

    public function index(){
        $data['indexProduct'] = Commodity::where('recommend',1)->get();
        $data['newsList'] = News::get();

        return $this->apiResponse($data);
    }

}