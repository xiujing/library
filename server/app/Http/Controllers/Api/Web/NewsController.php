<?php
    namespace App\Http\Controllers\Api\Web;

    use App\Http\Controllers\Controller;
    use App\Models\Commodity;
    use App\Models\CommodityList;
    use App\Models\News;
    use App\Models\ReBack;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;

    class NewsController extends Controller{

        public function index(Request $request){
            $tag = $request->tag;
            $sql = News::select("newsId","caption","picture","tag","content","createTime");

            if ($tag){
              $sql->where('tag',$tag);
            }
            $data = $sql->orderBy('newsId','Desc')
                ->get()->toArray();
            foreach ($data as $k=>$v){
                $data[$k]['day'] = substr($v['createTime'],8,2);
                $data[$k]['createTime'] = date('Y-m',strtotime($v['createTime']));
            }
            $result['newsList'] = $data;

            return $this->apiResponse($result);
        }

        public function show($newsId){
            $data = News::find($newsId);
            return $this->apiResponse($data);

        }


    }