<?php
namespace App\Http\Controllers\Api\Web;

use App\Http\Controllers\Controller;
use App\Models\OnlineMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller{
    public function store(Request $request){
        $data = $request->all();
        $validator = Validator::make($data,[
            'name' => 'required',
            'tel' => 'required',
            'email' => 'required',
            'content' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $data['IP'] = getIp();

        $data['createTime'] = date("Y-m-d H:i:s");

            $status = OnlineMessage::create($data);
            if (!$status){
                return $this->apiResponse('', config('errorCode.MESSAGE_INSERT_FAILS'));
            }
//            var_dump($status);
        if ($status){
            return $this->apiResponse(true);
        }else{
            return $this->apiResponse('', config('errorCode.TIME_REQ_FORBID'));
        }
    }
}