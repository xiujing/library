<?php
    namespace App\Http\Controllers\Api\Web;

    use App\Http\Controllers\Controller;
    use App\Models\About;
    use App\Models\Teacher;

    class AboutController extends Controller{
        public function index(){
            $data['about'] = About::get();
            return $this->apiResponse($data);
        }
    }