<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-11-27
 * Time: 15:34
 */

namespace App\Http\Controllers\Api\V1\Manager\Word;


use App\Http\Controllers\Controller;
use App\Models\AdditionalFields;
use App\Models\UserAddition;
use App\Models\UserManagerLog;
use App\Libs\Word;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Course;
use App\Models\Settlement;
use App\Models\LifeExcel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class WordController extends Controller
{
    /**
     * 传承班学员
     * @param $userId
     */
    public function inheritance(Request $request)
    {
        $userId = $request->userId;
        $user = User::with('field')->where('userid', '=', $userId)->first();
        $field = $user->field;
        $field = json_decode($field, true);

        foreach ($field as $value) {
            if ($value['id'] == 9) {
                $zc = $value['pivot']['value'];   //专长
            } else {
                $zc = '未填写';
            }
            if ($value['id'] == 13) {
                $profession = $value['pivot']['value'];  //专业
            } else {
                $profession = '未填写';
            }
            if ($value['id'] == 18) {
                $QQ = $value['pivot']['value'];      //qq
            } else {
                $QQ = '未填写';
            }
            if ($value['id'] == 20) {
                $wx = $value['pivot']['value'];   //微信号
            } else {
                $wx = '未填写';
            }
            if ($value['id'] == 38) {
                $xl = $value['pivot']['value'];     //学历
            } else {
                $xl = '未填写';
            }
            if ($value['id'] == 49) {
                $other = $value['pivot']['value'];    //其他情况
            } else {
                $other = '未填写';
            }
            if ($value['id'] == 55) {
                $jjpeople = $value['pivot']['value'];  //紧急联系人
            } else {
                $jjpeople = '未填写';
            }
            if ($value['id'] == 56) {
                $jjtel = $value['pivot']['value'];   //紧急电话
            } else {
                $jjtel = '未填写';
            }
            if ($value['id'] == 57) {
                $guanxi = $value['pivot']['value'];   //关系
            } else {
                $guanxi = '未填写';
            }
            if ($value['id'] == 72) {
                $zy = $value['pivot']['value'];    //职业
            } else {
                $zy = '未填写';
            }
            if ($value['id'] == 73) {
                $hy = $value['pivot']['value'];     //行业
            } else {
                $hy = '未填写';
            }
            if ($value['id'] == 88) {
                $Student_ID = $value['pivot']['value'];  //学号
            } else {
                $Student_ID = '未填写';
            }

            if ($value['id'] == 92) {
                $wxq = $value['pivot']['value'];      //微信群
            } else {
                $wxq = '未填写';
            }
            if ($value['id'] == 94) {
                $name_c = $value['pivot']['value'];    //常用名
            } else {
                $name_c = '未填写';
            }
            if ($value['id'] == 91) {
                $qqq = $value['pivot']['value'];    //qq群
            } else {
                $qqq = '未填写';
            }
            if ($value['id'] == 90) {
                $religion = $value['pivot']['value'];   //宗教信仰
            } else {
                $religion = '未填写';
            }
            if ($value['id'] == 93) {
                $bs_date = @date("Y-m-d", $value['pivot']['value']);  //拜师日期
            } else {
                $bs_date = '未填写';
            }
        }
        $user = json_decode($user, true);
        $field = $user['field'];
        $sex = $user['sex'];
        if ($sex = 1) {
            $sex = '男';
        } else {
            $sex = '女';
        }
        $photo = $user['photo']; //免冠照
        $certificatesPositive = $user['certificatesPositive'];  //证件正面
        $certificatesOpposite = $user['certificatesOpposite'];  //证件反面
        $time = date('Y-m-d', time());
        $managerId = $request->managerId;
        $userName = \DB::table('user_manager')->select('userName')->where('managerId', '=', $managerId)->first();
        if ($userName) {
            $userName = $userName->userName;
        }
        echo '  
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
<xml><w:WordDocument><w:View>Print</w:View></xml>  
<style>
		caption{
			font-size: 24px;
			padding: 10px 0;
		}
		.table{
			text-align: left;
			border-spacing: 0;
            border-collapse: collapse;
            border: 1px solid #000;
        }
		tbody tr td{
			font-size: 16px;
			height: 26px;
			padding: 0 10px;
			vertical-align:middle;		
		}
		.content{
			height: 150px;
			vertical-align: top;
		}
		p{
		    width: 40%;
	        text-align: right;
		}
		.table2,
		.table2 tr,
		.table2 tr td{
		   border: none;
		}
		.info p {
			display: inline-block;
			font-weight: bold;
			font-size: 13px;
		}
		.info p span {
			font-weight: normal;
		}
	</style>
</head>';
        echo '
<body>
<table border="1" cellspacing="0" cellpadding="0" style="margin: 0 auto;" width="100%" class="table">
	<caption>
	    传承班学员表
	</caption>
	<tbody>
		<tr>
			<td colspan="2">学号：        ' . $Student_ID . '</td>
			<td rowspan="6" width="22%" align="center"><img width="123" height="169" src="' . $photo . '"/></td>
		</tr>
		<tr>
			<td colspan="2">身份证姓名：      ' . $user['userName'] . '</td>
		</tr>
		<tr>
			<td colspan="2">常用名：        ' . $name_c . '</td>
		</tr>
		<tr>
			<td colspan="2">性别：            ' . $sex . '</td>
		</tr>
		<tr>
			<td colspan="2">手机号码：        ' . $user['phone'] . '</td>
		</tr>
		<tr>
			<td colspan="2">出生日期：        ' . $user['birthday'] . '</td>
		</tr>
		<tr>
			<td width="200">微信号：         ' . $wx . '</td>
			<td rowspan="6" colspan="2" width="44%" align="center"><img width="253" height="165" src="' . $certificatesPositive . '"/></td>
		</tr>
		<tr>
			<td>微信群：             ' . $wxq . ' </td>
		</tr>
		<tr>
			<td>QQ：                  ' . $QQ . '</td>
		</tr>
		<tr>
			<td>QQ群：                ' . $qqq . '</td>
		</tr>
		<tr>
			<td>现居省市：        ' . $user['province'] . $user['city'] . '</td>
		</tr>
		<tr>
			<td>身份证号：       ' . $user['certificatesNumber'] . '</td>
		</tr>
		<tr>
			<td>宗教信仰：      ' . $religion . '</td>
			
		</tr>
		<tr>
			<td>学历：          ' . $xl . '</td>
		</tr>
		<tr>
			<td>专业：        ' . $profession . '</td>
		</tr>
		<tr>
			<td>行业：       ' . $hy . '</td>
		</tr>
		<tr>
			<td>职业：       ' . $zy . '</td>
		</tr>
		<tr>
			<td>专长：       ' . $zc . '</td>
		</tr>
		<tr>
			<td colspan="3">拜师日期：          ' . $bs_date . '</td>
		</tr>
		<tr>
			<td colspan="3">紧急联系人：         ' . $jjpeople . '</td>
		</tr>
		<tr>
			<td colspan="3">紧急人联系电话：      ' . $jjtel . '</td>
		</tr>
		<tr>
			<td colspan="3">关系：            ' . $guanxi . '</td>
		</tr>
		<tr>
			<td class="content" colspan="3">其他说明：     ' . $other . '</td>
		</tr>
	</tbody>
</table>
<br><br>
<table align="left" border="0" cellspacing="0" cellpadding="0" class="table2">
<tbody>
    <tr>
        <td>管理员名称：  ' . $userName . '</td>
        <td>导出时间： ' . $time . '</td>
    </tr>
 </tbody>
</table>
</body>';
        ob_start(); //打开缓冲区
        header("Cache-Control: public");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        if (strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "传承班学员表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=传承班学员表.doc');
        } else if (strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "传承班学员表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            Header('Content-Disposition: attachment; filename=传承班学员表.doc');
        } else {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "传承班学员表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=传承班学员表.doc');
        }
        header("Pragma:no-cache");
        header("Expires:0");
        ob_end_flush();//输出全部内容到浏览器
    }

    /**
     * 各类视频课程承办申请表导出
     * @param $courseId
     * @return mixed
     */
    public function course(Request $request)
    {
        $contractorCourseId = $request->contractorCourseId;
        $course = \DB::table('contractor_course')
            ->join('course', 'contractor_course.courseId', '=', 'course.courseId')
            ->join('contractor', 'contractor_course.contractorId', '=', 'contractor.contractorId')
            ->leftJoin('user_manager', 'contractor_course.managerId', '=', 'user_manager.managerId')
            //  ->join('contractor_course_detail', 'contractor_course.contractorCourseId', '=', 'contractor_course_detail.contractorCourseId')
            ->select('course.*', 'contractor_course.*',
                'contractor.contractorAllName', 'contractor.companyCertificatesStatus', 'contractor.companyCertificatesNumber', 'contractor.legalName', 'contractor.legalCertificatesStatus',
                'contractor.legalCertificatesNumber', 'contractor.contractorName', 'contractor.sex', 'contractor.nation', 'contractor.phone', 'contractor.email', 'contractor.phone', 'contractor.certificatesStatus',
                'contractor.certificatesNumber', 'contractor.province as contractorProvince', 'contractor.city as contractorCity', 'contractor.area as contractorArea', 'contractor.detail as contractorDetail',
                'curriculumVitae', 'certificatesPositive', 'certificatesOpposite', 'legalCertificatesPositive', 'legalCertificatesOpposite', 'companyCertificatesPositive', 'companyCertificatesOpposite',
                'companyCertificatesPositivePhoto', 'contractor.photo',
                'user_manager.userName as managerName')
            ->get();
        //return $course;
        //$result = [];

        foreach ($course as $key => &$value) {
            if ($value->contractorCourseId == $contractorCourseId) {
                $result = $value;
            }
        }

        $result = json_decode(json_encode($result), true);
        $managerId = $request->managerId;
        $userName = \DB::table('user_manager')->select('userName')->where('managerId', '=', $managerId)->first();
        if ($userName) {
            $userName = $userName->userName;
        }
        $certificatesPositive = $result['certificatesPositive'];
        $certificatesOpposite = $result['certificatesOpposite'];
        $legalCertificatesPositive = $result['legalCertificatesPositive'];
        $legalCertificatesOpposite = $result['legalCertificatesOpposite'];

        $companyCertificatesPositive = $result['companyCertificatesPositive'];
        $companyCertificatesOpposite = $result['companyCertificatesOpposite'];
        $companyCertificatesPositivePhoto = $result['companyCertificatesPositivePhoto'];
        $contactInformation = unserialize(@$result['contactInformation']);
        $companyCertificatesStatus = str_replace(array('SOCIALCREDITCODE', 'INSTITUTION', 'SOCIALGROUP', 'ENTERPRICE'),
            array('社会信用代码', '事业单位登记证', '社会团体证', '民办非企业单位登记证'), $result['companyCertificatesStatus']);
        $legalCertificatesStatus = str_replace(array('CARDS', 'PASSPORT', 'HKPASS', 'OFFICERS', 'OTHER'),
            array('身份证', '护照', '港澳通行证', '军官证', '其他'), $result['legalCertificatesStatus']);
        $certificatesStatus = str_replace(array('CARDS', 'PASSPORT', 'HKPASS', 'OFFICERS', 'OTHER'),
            array('身份证', '护照', '港澳通行证', '军官证', '其他'), $result['certificatesStatus']);
        $sex = str_replace(array('1', '2', '0', ''), array("男性", "女性", "未知", ''), $result['sex']);
        $address = $result['contractorProvince'] . $result['contractorCity'] . $result['contractorArea'] . $result['contractorDetail'];
        $openCity = $result['province'] . $result['city']; //开课城市
        $courseAddress = $result['province'] . $result['city'] . $result['area'] . $result['detail']; //会场地址
        ob_start(); //打开缓冲区
        echo '  
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
<xml><w:WordDocument><w:View>Print</w:View></xml>  
</head>';
        echo '<body>
			<style>
				div{
					text-align:center;
					font-size:24px;
					margin-bottom:10px;
					font-weight:bold;
				}
				.table1{
					border-right: 1px solid #000;
					border-bottom: 1px solid #000;
					border-collapse:collapse;
					font-size: 24px;
				}
				.table1 td{
					border-left: 1px solid #000;
					border-top: 1px solid #000;
					font-size:16px;
					padding: 0 10px;
					height: 30px;
				}
				.table2{
					border:none;
					width:100%;
					border-collapse:collapse;
				}
				.table1 td.height{
					height:100px;
				}
				.height_center{
					height:200px;
					text-align:center;
					vertical-align:top;
				}
				.span-un{
				    text-decoration:underline;
				}
				.table2,
		.table2 tr,
		.table2 tr td{
		   border: none;
		}
			</style>
			<div><span class="span-un">' . $result['courseName'] . '</span>视频课程承办申请表</div>
			<p>填表日期：　　年　　月　　日</p>
		<table class="table1" cellspacing="0" cellpadding="0" style="width:100%;">
			<tbody>
				<tr>
					<td width="25%">承办人姓名</td>
					<td width="15%">' . $result['contractorName'] . '</td>
					<td width="14%">性别</td>
					<td width="11%">' . $sex . '</td>
					<td width="11%">民族</td>
					<td width="15%">' . $result['nation'] . '</td>
					<td width="14%" rowspan="5"><img width="71" height="99" src="' . $result['photo'] . '"  alt="近半年一寸免冠照片"/></td>
				</tr>
				<tr>
					<td>联系电话</td>
					<td colspan="2">' . $result['phone'] . '</td>
					<td>邮箱</td>
					<td colspan="2">' . $result['email'] . '</td>
				</tr>
				<tr>
					<td>承办人证件类型</td>
					<td colspan="5">' . $certificatesStatus . '</td>
				</tr>
				<tr>
					<td>承办人证件号码</td>
					<td colspan="5">' . $result['certificatesNumber'] . '</td>
				</tr>
				<tr>
					<td>常住地址</td>
					<td colspan="5">' . $address . '</td>
				</tr>
				<tr>
					<td>企业名称</td>
					<td colspan="2">' . $result['contractorAllName'] . '</td>
					<td>企业证件类型</td>
					<td colspan="3">' . $companyCertificatesStatus . '</td>
				</tr>
				<tr>
					<td>证件号码</td>
					<td colspan="2">' . $result['companyCertificatesNumber'] . '</td>
					<td>企业法人</td>
					<td colspan="3">' . $result['legalName'] . '</td>
				</tr>
				<tr>
					<td>法人证件类型</td>
					<td colspan="2">' . $legalCertificatesStatus . '</td>
					<td>法人证件号码</td>
					<td colspan="3">' . $result['legalCertificatesNumber'] . '</td>
				</tr>
				<tr>
					<td>法人联系电话</td>
					<td colspan="2">&nbsp;</td>
					<td>会场地址</td>
					<td colspan="3">' . $courseAddress . '</td>
				</tr>
				<tr>
					<td>申请承办时间</td>
					<td colspan="2">' . $result['createTime'] . '</td>
					<td>开课城市</td>
					<td colspan="3">' . $openCity . '</td>
				</tr>
				<tr>
					<td>会场规模（m²）</td>
					<td colspan="2">' . $result['venueScale'] . '</td>
					<td>预计学员人数</td>
					<td colspan="3">' . $result['number'] . '</td>
				</tr>
				<tr>
					<td>宣传渠道</td>
					<td colspan="6">' . $result['propagandaChanel'] . '</td>
				</tr>
				<tr>
					<td rowspan="5">核心团队人员姓名及联系方式（3-5人）</td>
					<td width="100" colspan="2" align="right" style="padding-right:10px;">统筹：</td>
					<td width="100">' . $contactInformation[0]['name'] . '</td>
					<td width="100" colspan="2" align="right" style="padding-right:10px;">联系电话：</td>
					<td width="100">' . $contactInformation[0]['phone'] . '</td>
				</tr>
				<tr>
					<td colspan="2" align="right" style="padding-right:10px;">班主任：</td>
					<td>' . $contactInformation[1]['name'] . '</td>
					<td colspan="2" align="right" style="padding-right:10px;">联系电话：</td>
					<td>' . $contactInformation[1]['phone'] . '</td>
				</tr>
				<tr>
					<td colspan="2" align="right" style="padding-right:10px;">后勤：</td>
					<td>' . $contactInformation[2]['name'] . '</td>
					<td colspan="2" align="right" style="padding-right:10px;">联系电话：</td>
					<td>' . $contactInformation[2]['phone'] . '</td>
				</tr>
				<tr>
					<td colspan="2" align="right" style="padding-right:10px;">主持人：</td>
					<td>' . @$contactInformation[3]['name'] . '</td>
					<td colspan="2" align="right" style="padding-right:10px;">联系电话：</td>
					<td>' . @$contactInformation[3]['phone'] . '</td>
				</tr>
				<tr>
					<td colspan="2" align="right" style="padding-right:10px;">主持助理：</td>
					<td>' . @$contactInformation[4]['name'] . '</td>
					<td colspan="2" align="right" style="padding-right:10px;">联系电话：</td>
					<td>' . @$contactInformation[4]['phone'] . '</td>
				</tr>
				<tr>
					<td colspan="7" align="center">承办人简历</td>
				</tr>
				<tr>
					<td colspan="7" class="height" height="100">' . $result['curriculumVitae'] . '</td>
				</tr>
				<tr>
					<td colspan="7" align="center">申请理由</td>
				</tr>
				<tr>
					<td colspan="7" class="height">' . $result['reason'] . '</td>
				</tr>
				<tr>
					<td colspan="7" style="padding:0;">
						<table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="height_center"><img width="240" src=' . $certificatesPositive . '  alt="承办人证件上传/身份证正面"/></td>
					
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="7" style="padding:0;">
						<table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td width="50%" style="border-left-style:none;border-top-style:none;" class="height_center"><img width="240" src=' . $legalCertificatesPositive . '  alt="法人证件上传/身份证正面"/></td>
								
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="7" style="padding:0;">
						<table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="50%" style="border-left-style:none;border-top-style:none;" class="height_center"><img width="240" src= ' . $companyCertificatesPositive . '  alt="机构证件1上传"/></td>
								<td width="50%" style="border-top-style:none;" class="height_center"><img width="240" src= ' . $companyCertificatesOpposite . '  alt="机构证件2上传"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="7" style="padding:0;">
						<table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="50%" style="border-left-style:none;border-top-style:none;" class="height_center"><img width="240" src=' . $companyCertificatesPositivePhoto . '  alt="机构证件3上传"/></td>
								<td width="50%" style="border-top-style:none;" class="height_center"><img width="240" src=""  alt="机构证件4上传"/></td>
							</tr>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<br><br>
<table  border="0" cellspacing="0" cellpadding="0" class="table2">
<tbody>
    <tr>
        <td>管理员名称：  ' . $userName . '</td>
        <td>导出时间： ' . date("Y-m-d H:i:s",time()) . '</td>
    </tr>
 </tbody>
</table>
		</body>';

        header("Cache-Control: public");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        if (strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "课程承办申请表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename= ' . $result['courseName'] . '课程承办申请表.doc');
        } else if (strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "课程承办申请表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            Header('Content-Disposition: attachment; filename=' . $result['courseName'] . '课程承办申请表.doc');
        } else {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "课程承办申请表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=' . $result['courseName'] . '课程承办申请表.doc');
        }
        header("Pragma:no-cache");
        header("Expires:0");
        ob_end_flush();//输出全部内容到浏览器
    }

    /**
     * 生命与国学高峰论坛学者信息表导出
     */
    public function life(Request $request)
    {
        $life = \DB::table('life_excel')->first();
        $managerId = $request->managerId;
        $userName = \DB::table('user_manager')->select('userName')->where('managerId', '=', $managerId)->first();
        if ($userName) {
            $userName = $userName->userName;
        }
        echo '  
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
<xml><w:WordDocument><w:View>Print</w:View></xml>  
</head>';
        echo '<body>
	<style>
	div{
					text-align:center;
					font-size:24px;
					margin-bottom:10px;
					font-weight:bold;
				}
		table{
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
			border-collapse:collapse;
		}
		table td{
			border-left: 1px solid #000;
			border-top: 1px solid #000;
			text-align: center;
			height: 34px;
			font-size:14px;
			line-height: 30px;
		}
		.table2,
		.table2 tr,
		.table2 tr td{
		   border: none;
		}
	</style>
	<div>“生命与国学”高峰论坛学者信息表</div>
	<table border="1" cellspacing="0" cellpadding="0" style="margin: 0 auto;width: 100%;">
	<tbody>
		<tr>
			<td width="19%">姓名</td>
			<td width="14%">' . $life->name . '</td>
			<td width="14%">性别</td>
			<td width="12%">' . $life->sex . '</td>
			<td width="14%">出生年</td>
			<td>' . $life->birthday . '</td>
			<td width="14%" rowspan="4">
			<img width="71" height="99" src="https://gss3.bdstatic.com/-Po3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike80%2C5%2C5%2C80%2C26/sign=a082293acd8065386fe7ac41f6b4ca21/8694a4c27d1ed21b3d658291aa6eddc451da3f11.jpg"  alt="照片"/></td>
		</tr>
		<tr>
			<td>笔名等</td>
			<td colspan="2">' . $life->pseudonym . '</td>
			<td>籍贯</td>
			<td colspan="2">' . $life->nativeplace . '</td>
		</tr>
		<tr>
			<td>国籍</td>
			<td>' . $life->nationality . '</td>
			<td>民族</td>
			<td>' . $life->nation . '</td>
			<td>常住地</td>
			<td>' . $life->obode . '</td>
		</tr>
		<tr>
			<td>身份证号码</td>
			<td colspan="5">' . $life->card . '</td>
		</tr>
		<tr>
			<td>工作单位</td>
			<td colspan="6">' . $life->workunit . '</td>
		</tr>
		<tr>
			<td>职称/职位</td>
			<td colspan="6">' . $life->job . '</td>
		</tr>
		<tr>
			<td>社会兼职</td>
			<td colspan="6">' . $life->socialappointments . '</td>
		</tr>
		<tr>
			<td>联系电话</td>
			<td colspan="3">' . $life->phone . '</td>
			
			<td>电子邮箱</td>
			<td colspan="2">' . $life->emile . '</td>
		</tr>
		<tr>
			<td>助理姓名</td>
			<td colspan="3">' . $life->assistantname . '</td>
			
			<td>助理电话</td>
			<td colspan="2">' . $life->assistanttel . '</td>
		</tr>
		<tr>
			<td>邮寄地址</td>
			<td colspan="6">' . $life->address . '</td>
		</tr>
		<tr>
			<td>微信</td>
			<td colspan="3">' . $life->wx . '</td>
			
			<td>头条号</td>
			<td colspan="2">' . $life->headlinenumber . '</td>
		</tr>
		<tr>
			<td>QQ号</td>
			<td colspan="3">' . $life->qq . '</td>
			
			<td>个人主页</td>
			<td colspan="2">' . $life->ersonalhomepage . '</td>
		</tr>
		<tr>
			<td>博客或其他</td>
			<td colspan="6">' . $life->blog . '</td>
		</tr>
		<tr>
			<td>师承</td>
			<td colspan="3">' . $life->teach . '</td>
			
			<td>学术领域及研究方向</td>
			<td colspan="2">' . $life->xsly . '</td>
		</tr>
		<tr>
			<td>代表著作</td>
			<td colspan="3">' . $life->dbzz . '</td>
			
			<td>代表文章</td>
			<td colspan="2">' . $life->dbwz . '</td>
		</tr>
		<tr>
			<td>宗教信仰或宗教倾向</td>
			<td colspan="3">' . $life->zjxy . '</td>
			
			<td>学术圈（喜恶）</td>
			<td colspan="2">' . $life->xsq . '</td>
		</tr>
		<tr>
			<td>学术观点</td>
			<td colspan="6">' . $life->xsgd . '</td>
		</tr>
		<tr>
			<td>社会履历及关系背景</td>
			<td colspan="6">' . $life->shll . '</td>
		</tr>
		<tr>
			<td>参加的学术会议及主题演讲</td>
			<td colspan="6">' . $life->ztyj . '</td>
		</tr>
		<tr>
			<td>采访文章、视频链接</td>
			<td colspan="6">' . $life->cfwz . '</td>
		</tr>
		<tr>
			<td>家庭关系</td>
			<td colspan="6">' . $life->jtgx . '</td>
		</tr>
		<tr>
			<td>兴趣爱好</td>
			<td colspan="6">' . $life->hobby . '</td>
		</tr>
		<tr>
			<td>推荐人</td>
			<td colspan="3">' . $life->tjr . '</td>
			
			<td>对接人</td>
			<td colspan="2">' . $life->djr . '</td>
		</tr>
		<tr>
			<td>沟通及互动记录</td>
			<td colspan="6">' . $life->gt . '</td>
		</tr>
		<tr>
			<td>其他</td>
			<td colspan="6">' . $life->other . '</td>
		</tr>
	</tbody>
</table>
		<br><br>
<table  border="0" cellspacing="0" cellpadding="0" class="table2">
<tbody>
    <tr>
        <td>管理员名称：  ' . $userName . '</td>
        <td>导出时间： ' . date("Y-m-d H:i:s",time()) . '</td>
    </tr>
 </tbody>
</table>
</body>';
        ob_start(); //打开缓冲区
        header("Cache-Control: public");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        if (strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "生命与国学高峰论坛学者信息word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=生命与国学高峰论坛学者信息表.doc');
        } else if (strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "生命与国学高峰论坛学者信息表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            Header('Content-Disposition: attachment; filename=生命与国学高峰论坛学者信息表.doc');
        } else {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "生命与国学高峰论坛学者信息表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=生命与国学高峰论坛学者信息表.doc');
        }
        header("Pragma:no-cache");
        header("Expires:0");
        ob_end_flush();//输出全部内容到浏览器
    }

    /**
     * 视频课程使用费核对表
     */
    public function check_course(Request $request)
    {
        //$courseName = '111';
        $settlementOrderId = $request->settlementOrderId;
        $managerId = $request->managerId;
        $userName = \DB::table('user_manager')->select('userName')->where('managerId', '=', $managerId)->first();
        if ($userName) {
            $userName = $userName->userName;
        }
        $courseName = \DB::table('settlement_order')->where('settlementOrderId', '=', $settlementOrderId)->first();
        $courseName = json_decode(json_encode($courseName), true);
        //return $courseName;
        //return $settlementOrderId;
//        $data = Settlement::select('*')->where('settlementOrderId', $settlementOrderId)->first();
//        return $data;
        echo '  
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
<xml><w:WordDocument><w:View>Print</w:View></xml>  
</head>';
        echo '<body>
		<style>
	        div{
				text-align:center;
				font-size:21px;
				margin-bottom:10px;
				font-weight:bold;
			}
			table{
				border-right: 1px solid #000;
				border-bottom: 1px solid #000;
				border-collapse:collapse;
			}
			table td{
				border-left: 1px solid #000;
				border-top: 1px solid #000;
				font-size:16px;
				padding: 0 10px;
				height: 40px;
				line-height: 30px;
			}
			.yc,
			.yc tr,
			.yc tr td{
				border:none;
			}
			.span-un{
				    text-decoration:underline;
				}
				.table2,
		.table2 tr,
		.table2 tr td{
		   border: none;
		}
		</style>
		
		<div><span class="span-un">' . $courseName['courseName'] . '</span>视频课程----<span class="span-un">' . $courseName['city'] . '</span>(开课城市)使用费用核对表</div>
		<br><br>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td rowspan="2">课程编号</td>
					<td rowspan="2">开课时间</td>
					<td rowspan="2">开课地点</td>
					<td rowspan="2">负责人</td>
					<td colspan="6" align="center">课程人数及费用情况</td>
				</tr>
				<tr>
					<td>新学员</td>
					<td>传承班弟子</td>
					<td>复修学员</td>
					<td>人数合计</td>
					<td>总收入（元）</td>
					<td>使用费 金额（元）</td>
				</tr>
				<tr>
					<td>' . $courseName['courseNumber'] . '</td>
					<td>' . $courseName['startTime'] . '</td>
					<td>' . $courseName['contractorAllName'] . '</td>
					<td>' . $courseName['contractorName'] . '</td>
					<td>' . $courseName['newUserNum'] . '</td>
					<td>' . $courseName['seniorUserNum'] . '</td>
					<td>' . $courseName['secondUserNum'] . '</td>
					<td>' . $courseName['totalNum'] . '</td>
					<td>' . $courseName['totalPrice'] . '</td>
					<td>' . $courseName['platFormPrice'] . '</td>
				</tr>
				<tr>
					<td>备注</td>
					<td colspan="9">按实际人数收取课程使用费<u>' . $courseName['rate'] . '</u>%（必填）</td>
				</tr>
			</tbody>
		</table>
		<br />
		<br />
		<br />
		<table width="80%" border="0" cellspacing="0" cellpadding="0" align="right">
			<tbody class="yc">
				<tr>
					<td width="160">研究院审核人：</td>
					<td width="100">&nbsp;</td>
					<td width="140">承办方签字：</td>
					<td width="100">&nbsp;</td>
					<td width="100">（印章）</td>
				</tr>
				<tr>
					<td>汇款人：</td>
					<td>&nbsp;</td>
					<td>汇款日期：</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
				<br><br>
<table border="0" cellspacing="0" cellpadding="0" class="table2">
<tbody>
    <tr>
        <td>管理员名称：  ' . $userName . '</td>
        <td>导出时间： ' . date("Y-m-d H:i:s",time()) . '</td>
    </tr>
 </tbody>
</table>
		</body>';
        ob_start(); //打开缓冲区
        header("Cache-Control: public");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        if (strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "视频课程使用费核对表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=视频课程使用费核对表.doc');
        } else if (strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "视频课程使用费核对表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            Header('Content-Disposition: attachment; filename=视频课程使用费核对表.doc');
        } else {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "视频课程使用费核对表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=视频课程使用费核对表.doc');
        }
        header("Pragma:no-cache");
        header("Expires:0");
        ob_end_flush();//输出全部内容到浏览器
    }

    /**
     * 视频课程学员报名表
     */
    public function user_enroll(Request $request)
    {
        $courseOrderId = $request->courseOrderId;
        //$courseOrderId = 93;
        $managerId = $request->managerId;
        $userName = \DB::table('user_manager')->select('userName')->where('managerId', '=', $managerId)->first();
        if ($userName) {
            $userName = $userName->userName;
        }
        $enroll = \DB::table('course_order')
            ->join('course', 'course_order.courseId', '=', 'course.courseId')
            ->join('user', 'course_order.userId', '=', 'user.userId')
            ->select( 'course.*', 'course_order.*','user.*')->where('course_order.orderNo', '=', $courseOrderId)
            ->first();
        $info = unserialize($enroll->additionalInformation);
        foreach ($info as $value) {
            if ($value['id'] == 1) {
                $nation = $value['value'];   //名族
            } else {
                $nation = '';
            }
            if ($value['id'] == 20) {
                $wx = $value['value'];    //微信号
            } else {
                $wx = '';
            }
            if ($value['id'] == 18) {
                $QQ = $value['value'];   //qq
            } else {
                $QQ = '';
            }
            if ($value['id'] == 90) {
                $zjxy = $value['value'];   //宗教信仰
            } else {
                $zjxy = '';
            }
            if ($value['id'] == 38) {
                $xl = $value['value'];    //学历
            } else {
                $xl = '';
            }
            if ($value['id'] == 72) {
                $zy = $value['value'];     //职业
            } else {
                $zy = '';
            }
            if ($value['id'] == 40) {
                $byyx = $value['value'];      //毕业院校
            } else {
                $byyx = '';
            }
            if ($value['id'] == 13) {
                $zhuanye = $value['value'];     //专业
            } else {
                $zhuanye = '';
            }
            if ($value['id'] == 93) {
                $bs_date = $value['value'];      //拜师日期
            } else {
                $bs_date = '';
            }
            if ($value['id'] == 95) {
                $huoqu = $value['value'];      //获取信息渠道
            } else {
                $huoqu = '';
            }
            if ($value['id'] == 16) {
                $jkzk = $value['value'];       //健康状况
            } else {
                $jkzk = '';
            }
            if ($value['id'] == 8) {
                $clothes = $value['value'];    //服装尺码
            } else {
                $clothes = '';
            }
            if ($value['id'] == 96) {
                $hobby = $value['value'];    //兴趣爱好
            } else {
                $hobby = '';
            }
            if ($value['id'] == 97) {
                $aaa = $value['value'];    //可参加复试时间安排
            } else {
                $aaa = '';
            }
            if ($value['id'] == 98) {
                $bbb = $value['value'];     //阅读过哪些，，，
            } else {
                $bbb = '';
            }
            if ($value['id'] == 99) {
                $ccc = $value['value'];    //对传统文化的认识
            } else {
                $ccc = '';
            }
            if ($value['id'] == 55) {
                $ddd = $value['value'];      //紧急联系人
            } else {
                $ddd = '';
            }
            if ($value['id'] == 56) {
                $eee = $value['value'];    //紧急联系人电话
            } else {
                $eee = '';
            }
            if ($value['id'] == 57) {
                $fff = $value['value'];    //关系
            } else {
                $fff = '';
            }
            if ($value['id'] == 10) {
                $bz = $value['value'];   //备注
            } else {
                $bz = '';
            }
        }
        $sex =  str_replace(array('1', '2', '0'), array("男", "女", ""), $enroll->sex);
//        var_dump($info);exit;
//        var_dump($enroll);exit;
        echo '  
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
<xml><w:WordDocument><w:View>Print</w:View></xml>  
</head>';
        echo '<body>
		<style>
			div{
				text-align:center;
				font-size:21px;
				margin-bottom:10px;
				font-weight:bold;
			}
			table{
				border-right: 1px solid #000;
				border-bottom: 1px solid #000;
				border-collapse:collapse;
			}
			table td{
				border-left: 1px solid #000;
				border-top: 1px solid #000;
				font-size:16px;
				height:30px;
				padding: 0 10px;
			}
			.span-un{
				    text-decoration:underline;
				}
				.table2,
		.table2 tr,
		.table2 tr td{
		   border: none;
		}
		</style>
		
		<div><span class="span-un">' . $enroll->courseName . '</span>视频课程学员报名表</div>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td colspan="2">姓名:     ' . $enroll->userName . '</td>
					<td rowspan="5" width="18%"><img width="71" height="99" src="https://gss3.bdstatic.com/-Po3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike80%2C5%2C5%2C80%2C26/sign=a082293acd8065386fe7ac41f6b4ca21/8694a4c27d1ed21b3d658291aa6eddc451da3f11.jpg" alt="近半年一寸免冠照片"/></td>
				</tr>
				<tr>
					<td colspan="2">性别:     ' .$sex  . '</td>
				</tr>
				<tr>
					<td colspan="2">手机号码:     ' . $enroll->phone . '</td>
				</tr>
				<tr>
					<td colspan="2">出生日期:     ' . $enroll->birthday . '</td>
				</tr>
				<tr>
					<td colspan="2">民族:     ' . $nation . '</td>
				</tr>
				<tr>
					<td>微信号:     ' . $wx . '</td>
					<td colspan="2" rowspan="5" width="36%" align="center"><img width="200" src="' . $enroll->certificatesPositive . '"  alt="身份证正面"/></td>
				</tr>
				<tr>
					<td>QQ:     ' . $QQ . '</td>
				</tr>
				<tr>
					<td>所在省市:     ' . $enroll->province . $enroll->city . '</td>
				</tr>
				<tr>
					<td>联系地址：  ' . $enroll->province . $enroll->city .$enroll->area .$enroll->detail . '</td>
				</tr>
				<tr>
					<td>身份证号:     ' . $enroll->certificatesNumber . '</td>
				</tr>
				<tr>
					<td  colspan="3"> 宗教信仰:     ' . $zjxy . '</td>
				</tr>
				<tr>
					<td  colspan="3"> 学历:     ' . $xl . '</td>
				</tr>
				<tr>
					<td  colspan="3">职业:     ' . $zy . '</td>
				</tr>
				<tr>
					<td  colspan="3">毕业院校:     ' . $byyx . '</td>
				</tr>
				<tr>
					<td  colspan="3">专业:     ' . $zhuanye . '</td>
				</tr>
				<tr>
					<td colspan="3">拜师日期:     ' . $bs_date . '</td>
				</tr>
				<tr>
					<td colspan="3">获取信息渠道:     ' . $huoqu . '</td>
				</tr>
				<tr>
					<td colspan="3">健康状态:     ' . $jkzk . '</td>
				</tr>
				<tr>
					<td colspan="3">服装尺码:     ' . $clothes . '</td>
				</tr>
				<tr>
					<td colspan="3">兴趣爱好:     ' . $hobby . '</td>
				</tr>
				<tr>
					<td colspan="3">可参加复修时间安排:     ' . $aaa . '</td>
				</tr>
				<tr>
					<td colspan="3">阅读过哪些潘麟导师生命科学系列经典丛书:     ' . $bbb . '</td>
				</tr>
				<tr>
					<td colspan="3">对传统文化的认识:     ' . $ccc . '</td>
				</tr>
				<tr>
					<td colspan="3">紧急联系人:     ' . $ddd . '</td>
				</tr>
				<tr>
					<td colspan="3">紧急联系人电话:     ' . $eee . '</td>
				</tr>
				<tr>
					<td colspan="3">关系:     ' . $fff . '</td>
				</tr>
				<tr>
					<td colspan="3">备注:     ' . $bz . '</td>
				</tr>
			</tbody>
		</table>
				<br><br>
<table border="0" cellspacing="0" cellpadding="0" class="table2">
<tbody>
    <tr>
        <td>管理员名称：  ' . $userName . '</td>
        <td>导出时间： ' . date("Y-m-d H:i:s",time()) . '</td>
    </tr>
 </tbody>
</table>
		</body>';
        ob_start(); //打开缓冲区
        header("Cache-Control: public");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        if (strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "视频课程学员报名表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=视频课程学员报名表.doc');
        } else if (strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "视频课程学员报名表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            Header('Content-Disposition: attachment; filename=视频课程学员报名表.doc');
        } else {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "视频课程学员报名表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=视频课程学员报名表.doc');
        }
        header("Pragma:no-cache");
        header("Expires:0");
        ob_end_flush();//输出全部内容到浏览器

    }

    /**
     * 义工报名表
     */
    public function volunteer(Request $request)
    {
        $userId = $request->userId;
        $user = User::with('field')->where('userid', '=', $userId)->first();
        if ($user->sex = 1) {
            $sex = '男';
        } else {
            $sex = '女';
        }

        $field = $user->field;
        $num = count($field);
        if ($num != 0) {
            $field = json_decode($field, true);
            foreach ($field as &$value) {
                if ($value['id'] == 6) {
                    $job = $value['pivot']['value'];    //申请岗位/职位
                } else {
                    $job = '未填写';
                }
                if ($value['id'] == 7) {
                    $dd_time = $value['pivot']['value'];     //到达时间
                } else {
                    $dd_time = '未填写';
                }
                if ($value['id'] == 8) {
                    $clothes = $value['pivot']['value'];   //服装尺码
                } else {
                    $clothes = '未填写';
                }
                if ($value['id'] == 9) {
                    $zhuanchang = $value['pivot']['value'];     //专长
                } else {
                    $zhuanchang = '未填写';
                }
                if ($value['id'] == 10) {
                    $beizhu = $value['pivot']['value'];     //备注
                } else {
                    $beizhu = '未填写';
                }
            }
        } else {
            $job = '未填写';
            $dd_time = '未填写';
            $clothes = '未填写';
            $zhuanchang = '未填写';
            $beizhu = '未填写';
        }
        $time = date('Y-m-d', time());
        $managerId = $request->managerId;
        $userName = \DB::table('user_manager')->select('userName')->where('managerId', '=', $managerId)->first();
        if ($userName) {
            $userName = $userName->userName;
        }

        echo '  
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
<xml><w:WordDocument><w:View>Print</w:View></xml>  
</head>';
        echo '<body>
		<style>
		div{
			text-align:center;
			font-size:21px;
			margin-bottom:10px;
			font-weight:bold;
		}
		table{
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
			border-collapse:collapse;
		}
		table td{
			border-left: 1px solid #000;
			border-top: 1px solid #000;
			font-size:14px;
			padding: 0 10px;
			height:26px;
		}
		.table2,
		.table2 tr,
		.table2 tr td{
		   border: none;
		}
		
	</style>
	
	<div>义工报名表</div>
	<table border="1" cellspacing="0" cellpadding="0" style="margin: 0 auto;width: 100%;">
		<tbody>
			<tr>
				<td>姓名：   ' . $user->userName . '</td>
				<td width="16%" rowspan="4"><img width="71" height="99" src="' . $user->photo . '"  alt="近半年一寸免冠照片"/></td>
			</tr>
			<tr>
				<td>性别：   ' . $sex . '</td>
			</tr>
			<tr>
				<td>身份：   ' . $user->userFlag . '</td>
			</tr>
			<tr>
				<td>手机号码：   ' . $user->phone . '</td>
			</tr>
			<tr>
				<td colspan="2">出生日期：   ' . $user->birthday . '</td>
			</tr>
			<tr>
				<td colspan="2">所在省市：   ' . $user->province . $user->city . '</td>
			</tr>
			<tr>
				<td colspan="2">身份证号码：   ' . $user->certificatesNumber . '</td>
			</tr>
			<tr>
				<td colspan="2">申请岗位/职位：   ' . $job . '</td>
			</tr>
			<tr>
				<td colspan="2">到达时间：   ' . $dd_time . '</td>
			</tr>
			<tr>
				<td colspan="2">服装尺码：   ' . $clothes . '</td>
			</tr>
			<tr>
				<td colspan="2">专长：   ' . $zhuanchang . '</td>
			</tr>
			<tr>
				<td colspan="2">备注：   ' . $beizhu . '</td>
			</tr>
		</tbody>
	</table>
	<br><br>
<table border="0" cellspacing="0" cellpadding="0" class="table2">
<tbody>
    <tr>
        <td>管理员名称：  ' . $userName . '</td>
        <td>导出时间： ' . $time . '</td>
    </tr>
 </tbody>
</table>
	</body>';
        ob_start(); //打开缓冲区
        header("Cache-Control: public");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        if (strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "义工报名表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=义工报名表.doc');
        } else if (strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "义工报名表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            Header('Content-Disposition: attachment; filename=义工报名表.doc');
        } else {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "义工报名表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=义工报名表.doc');
        }
        header("Pragma:no-cache");
        header("Expires:0");
        ob_end_flush();//输出全部内容到浏览器
    }


    public function work(Request $request)
    {
        $userId = $request->userId;
        $user = User::with('field')->where('userid', '=', $userId)->get();
        $user = json_decode($user, true);
        foreach ($user as $key => &$value) {
            if ($value['level'] == 'SERIOR') {
                $level = '高级会员';
            }
            if ($value['level'] == 'GENERAL') {
                $level = '普通会员';
            }
            if ($value['level'] == 'ACCURATE_SENIOR') {
                $level = '准高级会员';
            }
            if ($value['sex'] == '0') {
                $sex = '未知';
            }
            if ($value['sex'] == '1') {
                $sex = '男';
            }
            if ($value['sex'] == '2') {
                $sex = '女';
            }
            $name = $value['userName'];
            $workStatus = $value['workStatus'];
            $birthday = $value['birthday'];
            $certificatesNumber = $value['certificatesNumber'];
            $phone = $value['phone'];
            $sqgw = '';                //申请岗位
            $sqrq = '';                 //申请日期
            $firstxl = '';              //第一学历及所学专业
            $highxl = '';            //最高学历及所学专业
            $hignFlag = '';             //最高学历是否全日制
            $jn = '';                //技能资格证书（无/名称
            $zzmm = '';              //政治面貌
            $xy = '';                //信仰
            $hj = '';            //户籍所在省市
            $mz = '';            //民族
            $sg = '';             //身高
            $jiankang = '';         //健康状况
            $hyzt = '';          //婚姻状态
            $qq = '';             //qq
            $email = '';             //email
            $wx = '';             //微信
            $nowAddress = '';           //目前住址
            $zp = '';         //获取招聘信息渠道
            $qwxz = '';             //期望薪资
            $zdxz = '';           //最低接受薪资
            $jtqk = '';          //家庭情况
            $workjl = '';             //工作经历
            $jyjl = '';             //学历教育经历
            $otherjl = '';             //其他培训经历
            $homeFlag = '';           //常居地有无住房
            $english = '';         //英语水平
            $otherwj = '';           //其他外语
            $jzlx = '';             //驾照类型
            $jl = '';           //驾龄
            $wordStatus = '';          //会操作何种办公软件
            $dgdate = '';             //何时到岗
            $jjpeople = '';             //紧急联系人及与其关系
            $jjphone = '';             //紧急联系电话
            $jb = '';           //可否出差或加班
            $zwjs = '';         //自我介绍
            $csqk = '';           //初试情况
            $fsqk = '';             //复试情况
            $rzrq= '';           //入职日期
            $rzgw = '';          //入职岗位
            $department = '';             //所属部门
            $sysc = '';             //试用时长
            $syyx = '';             //试用月薪
            $zzrq = '';           //转正日期
            $zzyx = '';         //转正月薪
            $fldy = '';           //薪资福利待遇
            $yglx = '';             //用工类型
            $departmentyj = '';           //用人部门负责人意见
            $departmentldyj = '';          //用人部门分管领导意见
            $rzbyj = '';             //人资部意见
            $rzbldyj = '';             //人资部分管领导意见
            $fyzyj = '';             //副院长意见
            $yzyj = '';           //院长意见
            $cqjl = '';         //出勤记录
            $sykhjl = '';           //试用考核记录
            $khjl = '';             //考核记录
            $gwydjl = '';           //岗位异动记录
            $xzydjl = '';          //薪资异动记录
            $jfjl = '';             //奖罚记录
            $companypxjl = '';             //参加公司培训记录
            $ldqdjl = '';             //劳动合同签订记录
            $remark = '';           //其他说明

            if (count($value['field']) > 0) {
                foreach ($user[$key]['field'] as $k => $v) {
                    if ($user[$key]['field'][$k]['id'] == 6) {
                        $sqgw = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 74) {
                        $sqrq = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 12) {
                        $firstxl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 111) {
                        $highxl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 112) {
                        $hignFlag = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 113) {
                        $jn = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 90) {
                        $xy = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 15) {
                        $hj = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 1) {
                        $mz = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 2) {
                        $sg = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 16) {
                        $jiankang = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 114) {
                        $hyzt = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 18) {
                        $qq = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 19) {
                        $email= $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 20) {
                        $wx = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 21) {
                        $nowAddress = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 115) {
                        $zp = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 24) {
                        $qwxz = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 116) {
                        $zdxz = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 117) {
                        $jtqk = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 118) {
                        $workjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 119) {
                        $jyjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 120) {
                        $otherjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 121) {
                        $homeFlag = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 122) {
                        $english = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 123) {
                        $otherwj = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 124) {
                        $jzlx = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 125) {
                        $jl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 126) {
                        $wordStatus = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 54) {
                        $dgdate = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 127) {
                        $jjpeople = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 56) {
                        $jjphone = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 128) {
                        $jb = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 129) {
                        $zwjs = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 130) {
                        $csqk = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 131) {
                        $fsqk = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 132) {
                        $rzrq = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 133) {
                        $rzgw = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 62) {
                        $department = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 134) {
                        $sysc = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 135) {
                        $syyx= $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 136) {
                        $zzrq = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 137) {
                        $zzyx = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 138) {
                        $fldy = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 139) {
                        $yglx = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 140) {
                        $departmentyj = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 141) {
                        $departmentldyj = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 69) {
                        $rzbyj = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 142) {
                        $rzbldyj = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 143) {
                        $fyzyj = $v['pivot']['value'];
                    }


                    if ($user[$key]['field'][$k]['id'] == 144) {
                        $yzyj = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 145) {
                        $cqjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 146) {
                        $sykhjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 147) {
                        $khjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 148) {
                        $gwydjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 149) {
                        $xzydjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 150) {
                        $jfjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 151) {
                        $companypxjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 152) {
                        $ldqdjl = $v['pivot']['value'];
                    }
                    if ($user[$key]['field'][$k]['id'] == 153) {
                        $remark = $v['pivot']['value'];
                    }
                }
            }
        }
        $result = $user;
        foreach ($result as &$v) {
            unset($v['field']);
        }
        if(!$result){
            return;
        }
        $time = date('Y-m-d', time());
        $managerId = $request->managerId;
        $userName = \DB::table('user_manager')->select('userName')->where('managerId', '=', $managerId)->first();
        if ($userName) {
            $userName = $userName->userName;
        }

        echo '  
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
<xml><w:WordDocument><w:View>Print</w:View></xml>  
</head>';
        echo '<body>
		<style>
		.wrap {
			width: 860px;
			margin: 0 auto;
		}
		.wrap h3 {
			text-align: center;
		}
		.info p {
			display: inline-block;
			font-weight: bold;
			font-size: 13px;
		}
		.info p span {
			font-weight: normal;
		}
		table{
			width: 100%;
			margin: 0 auto;
			border: 1px solid #000;
			border-collapse:collapse;
			text-align: center;
			font-size: 13px;
			border-bottom: 0;
			font-family: "宋体";
		}
		td,th {
			border: 1px solid #000;
			min-width: 72px;
			height: 30px;
		}
		.single {
			padding-left: 10px; 
			vertical-align: top;
			text-align: left;
			text-indent: 2em;
			line-height: 1.5
		}
		.none-border {
			border: 0;
		}
		.align-left {
			text-align: left;
			padding-left: 10px;
		}
		.text-block {
			height: 100px;
		}
		.none-border-top {
			border-top: 0;
		}
		.top td {
		border: 0;
		font-size: 10px;
		font-weight: bold;
		}
		.table2,
		.table2 tr,
		.table2 tr td{
		   border: none;
		}
	</style>
	<div class="wrap">
		<table>
			<tbody>
		<tr class="top">
		<td style="border: 0;font-size: 10px;font-weight: bold;" >申请岗位：</td>
		<td style="border: 0;font-size: 10px;font-weight: bold;">'.$sqgw.'</td>
		<td colspan="2" style="border: 0;font-size: 10px;font-weight: bold;">申请日期：</td>
		<td style="border: 0;font-size: 10px;font-weight: bold;">'.$sqrq.'</td>
		<td style="border: 0;font-size: 10px;font-weight: bold;">员工状态：</td>
		<td style="border: 0;font-size: 10px;font-weight: bold;">'.$workStatus.'</td>
		<td colspan="4" style="border: 0;font-size: 10px;font-weight: bold;">(相关个人信息均以身份证所示为准)：</td>
		</tr>
	
				<tr>
					<td>姓名</td>
					<td >'.$name.'</td>
					<td>性别</td>
					<td>'.$sex.'</td>
					<td>出生日期</td>
					<td>'.$birthday.'</td>
					<td>民族</td>
					<td>'.$mz.'</td>
					<td>信仰</td>
					<td>'.$xy.'</td>
					<td style="min-width: 100px" rowspan="4">近期一寸免冠照</td> <!-- 照片 -->
				</tr>
				<tr>
					<td>政治面貌</td>
					<td>'.$zzmm.'</td>
					<td>身高</td>
					<td>'.$sg.'</td>
					<td>健康状况</td>
					<td colspan="5">'.$jiankang.'</td>
				</tr>
				<tr>
					<td>第一学历及所学专业</td>
					<td colspan="3">'.$firstxl.'</td>
					<td>最高学历及所学专业</td>
					<td colspan="2">'.$highxl.'</td>
					<td>最高学历是否全日制</td>
					<td colspan="2">'.$hignFlag.'</td>
				</tr>
				<tr>
					<td>身份证号码</td>
					<td colspan="3">'.$certificatesNumber.'</td>
					<td>手机号码</td>
					<td colspan="2">'.$phone.'</td>
					<td>QQ号</td>
					<td colspan="2">'.$qq.'</td>
				</tr>
				<tr>
					<td>微信号</td>
					<td colspan="3">'.$wx.'</td>
					<td>常用邮箱</td>
					<td colspan="2">'.$email.'</td>
					<td>婚姻状态</td>
					<td colspan="3">'.$hyzt.'</td>
				</tr>
				<tr>
					<td>户籍所在省市</td>
					<td colspan="2">'.$hj.'</td>
					<td>目前住址</td>
					<td colspan="3">'.$nowAddress.'</td>
					<td>获取招聘信息渠道</td>
					<td colspan="3">'.$zp.'</td>
				</tr>
				<tr>
					<td>期望薪资</td>
					<td colspan="2">'.$qwxz.'</td>
					<td>最低接受薪资</td>
					<td colspan="2">'.$zdxz.'</td>
					<td>其他说明</td>
					<td colspan="4"></td>
				</tr>
				<tr>
					<td colspan="11" class="align-left">
						<strong>家庭情况</strong>（以父母/夫妻为主，含姓名、年龄、关系、文化程度、职务、家庭氛围介绍）
					</td>
				</tr>
				<tr>
					<td colspan="11" class="text-block single">'.$jtqk.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left">
						<strong>工作经历</strong>（含起止时间段、职位、主要工作内容、离职原因，不包括临时工作、兼职工作）
					</td>
				</tr>
				<tr>
					<td colspan="11" class="text-block single">'.$workjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left">
						<strong>学历教育经历</strong>（填写高中、大学及以上即可，含起止时间段、所学科目、院校名称、获取何种证书）
					</td>
				</tr>
				<tr>
					<td colspan="11" class="text-block single">'.$jyjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left">
						<strong>其他培训经历</strong>（含起止时间段、培训内容、培训机构名称、获取何种证书）
					</td>
				</tr>
				<tr>
					<td colspan="11" class="text-block single">'.$otherjl.'</td>
				</tr>
				<tr>
					<th colspan="11" class="align-left">
						<strong>其他情况</strong>
					</td>
				</tr>
				<tr>
					<td>常居地有无住房</td>
					<td>'.$homeFlag.'</td>
					<td>驾照类型</td>
					<td>'.$jzlx.'</td>
					<td>驾龄</td>
					<td>'.$jl.'</td>
					<td>英语水平</td>
					<td>'.$english.'</td>
					<td>其他外语</td>
					<td colspan="2">'.$otherwj.'</td>
				</tr>
				<tr>
					<td>紧急联系人及与其关系</td>
					<td colspan="3">'.$jjpeople.'</td>
					<td>紧急联系电话</td>
					<td colspan="3">'.$jjphone.'</td>
					<td>何时到岗</td>
					<td colspan="2">'.$dgdate.'</td>
				</tr>
				<tr>
					<td>会操作何种办公软件</td>
					<td colspan="4">'.$wordStatus.'</td>
					<td>技能资格证书</td>
					<td colspan="5">'.$jn.'</td>
				</tr>
				<tr>
					<td>可否出差或加班</td>
					<td colspan="4">'.$jb.'</td>
					<td>其他说明</td>
					<td colspan="5"></td>
				</tr>
				<tr>
					<td colspan="11" class="align-left">
						<strong>自我介绍</strong>（含个人性格特点、特长、爱好、对传统文化的认识等）
					</td>
				</tr>
				<tr>
					<td colspan="11" class="none-border single text-block">'.$zwjs.'</td>
				</tr>
				<tr >
					<td colspan="11" style="height:80px;vertical-align: top;text-align: left;text-indent: 2em;border-bottom: 0">
						本人声明：以上所填内容属实。本人在狮子吼文化传播有限公司就职期间与其他公司或单位无任何劳动关系。如情况不属实愿承担相应责任，同时愿意接受用人单位核查。
					</td>
				</tr>
				<tr>
					<td class="none-border" colspan="7"></td>
					<td class="none-border">陈诺人：</td>
					<td class="none-border"></td>
					<td class="none-border">日期：</td>
					<td style="border-left: 0; border-top: 0"></td>
				</tr>
				<tr>
					<th colspan="11">聘用审批</td>
				</tr>
				<tr>
					<td>入职日期</td>
					<td>'.$rzrq.'</td>
					<td>入职岗位</td>
					<td>'.$rzgw.'</td>
					<td>所属部门</td>
					<td colspan="2">'.$department.'</td>
					<td>用工类型</td>
					<td>'.$yglx.'</td>
					<td>试用时长</td>
					<td>'.$sysc.'</td>
				</tr>
				<tr>
					<td>试用月薪</td>
					<td>'.$syyx.'</td>
					<td>转正月薪</td>
					<td>'.$zzyx.'</td>
					<td colspan="2">薪资发放说明</td>
					<td colspan="5">'.$fldy.'</td>
				</tr>
				<tr>
					<td colspan="6" class="none-border"></td>
					<td colspan="2" class="none-border">入职员工签名确认：</td>
					<td class="none-border"></td>
					<td class="none-border">日期：</td>
					<td style="border-left: 0"></td>
				</tr>
				<tr>
					<td colspan="2">用人部门负责人意见</td>
					<td colspan="3">'.$departmentyj.'</td>
					<td colspan="2">用人部门分管领导意见</td>
					<td colspan="4">'.$departmentldyj.'</td>
				</tr>
				<tr>
					<td colspan="2">人资部负责人意见</td>
					<td colspan="3">'.$rzbyj.'</td>
					<td colspan="2">人资部分管领导意见</td>
					<td colspan="4">'.$rzbldyj.'</td>
				</tr>
				<tr>
					<td colspan="2">副院长意见</td>
					<td colspan="3">'.$fyzyj.'</td>
					<td colspan="2">院长意见</td>
					<td colspan="4">'.$yzyj.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">初试情况</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$csqk.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">复试情况</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$fsqk.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">出勤记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$cqjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">试用考核记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$sykhjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">转正日期</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$zzrq.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">考核记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$khjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">岗位异动记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$gwydjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">薪资异动记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$xzydjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">奖罚记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$jfjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">参加公司培训记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$companypxjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">劳动合同签订记录</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$ldqdjl.'</td>
				</tr>
				<tr>
					<td colspan="11" class="align-left none-border">其他说明</td>
				</tr>
				<tr>
					<td colspan="11" class="single text-block none-border-top">'.$remark.'</td>
				</tr>
			</tbody>
		</table>
		<table border="0" cellspacing="0" cellpadding="0" class="table2">
<tbody>
    <tr>
        <td>管理员名称：  ' . $userName . '</td>
        <td>导出时间： ' . $time . '</td>
    </tr>
 </tbody>
</table>
	</div>
	</body>';
        ob_start(); //打开缓冲区
        header("Cache-Control: public");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        if (strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "员工档案表word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=员工档案.doc');
        } else if (strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox')) {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "员工档案word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            Header('Content-Disposition: attachment; filename=义工报名表.doc');
        } else {
            $ip = getIp();
            UserManagerLog::create(['ip' => $ip, 'operation' => "员工档案word导出成功", 'userName' => $userName, 'managerId' => $managerId]);
            header('Content-Disposition: attachment; filename=员工档案.doc');
        }
        header("Pragma:no-cache");
        header("Expires:0");
        ob_end_flush();//输出全部内容到浏览器
    }
}