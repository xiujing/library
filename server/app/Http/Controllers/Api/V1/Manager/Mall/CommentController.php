<?php
/**
 * 评价
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 17:24
 */

namespace App\Http\Controllers\Api\V1\Manager\Mall;


use App\Http\Controllers\Controller;
use App\Models\CommodityComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends  Controller
{
    public function commentReply(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
           'commentId'      => 'integer|required',
            'reply'         => 'required'
        ]);
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $status = CommodityComment::where('commentId',$data['commentId'])->update(['reply'=>$data['reply']]);
        if(!$status){
            return $this->apiResponse('', config('errorCode.COMMENT_REPLAY_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }
}