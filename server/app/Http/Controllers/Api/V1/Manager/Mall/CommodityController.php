<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-05
 * Time: 17:36
 */

namespace App\Http\Controllers\Api\V1\Manager\Mall;


use App\Http\Controllers\Controller;
use App\Models\CommodityCategory;
use App\Models\CommoditySku;
use App\Services\Manager\Commodity;
use App\Transformers\CommoditySkuTransformer;
use Fractal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommodityController extends Controller
{
    public function index(Request $request)
    {

        $type = $request->type;
        $userId = $request->userId;
        $sql = \App\Models\Commodity::select("*");
        if ($type){
            $sql->where('type',$type);
        }
        if ($userId){
            $sql->where('userId',$userId);
        }
            $data = $sql->paginate(20)->toArray();
        $result['list'] = $data['data'];
        $result['count'] = $data['total'];
        return $this->apiResponse($result);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'commodityId'       => 'integer',
        ]);
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $commodityId = $data['commodityId'];
        unset($data['commodityId']);
        if($commodityId){
            $status = \App\Models\Commodity::where('commodityId',$commodityId)->update($data);
        }else{
            $data['createTime'] = date('Y-m-d H:i:s');
            $status = \App\Models\Commodity::create($data);
        }
        if(!$status){
            return $this->apiResponse('', config('errorCode.UPDATE_FAILED'));
        }
        return $this->apiResponse('');

    }

    public function show($commodityId)
    {
        $commodity = \App\Models\Commodity::find($commodityId);
        if(!$commodity){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        
        return $this->apiResponse($commodity);
    }

    public function destroy($commodityId)
    {
        try{
            \DB::beginTransaction();
            $status = \App\Models\Commodity::destroy($commodityId);
            if(!$status){
                throw new \Exception("删除商品主表失败");
            }
           
            \DB::commit();
            return $this->apiResponse('');
        }catch (\Exception $e){
            \DB::rollback();
            \Log::error("CommodityController destroy".$e->getMessage());
            return $this->apiResponse('', config('errorCode.PRODUCT_DELETE_FAILED'));
        }

    }

    /**
     * 商品上下架
     * @param $commodityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSale($commodityId)
    {
        $status = \App\Models\Commodity::where('commodityId',$commodityId)->first();
        if(!$status){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $status = \App\Models\Commodity::where('commodityId',$commodityId)->update(['saleStatus'=>!($status['saleStatus'])]);
        if(!$status){
            return $this->apiResponse('', config('errorCode.PRODUCT_UPDATE_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }
    public function update($commodityId)
    {
        $status = \App\Models\Commodity::where('commodityId',$commodityId)->first();
        if(!$status){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $status = \App\Models\Commodity::where('commodityId',$commodityId)->update(['recommend'=>!($status['recommend'])]);
        if(!$status){
            return $this->apiResponse('', config('errorCode.PRODUCT_UPDATE_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }
}