<?php
/**
 * 订单管理
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 14:58
 */

namespace App\Http\Controllers\Api\V1\Manager\Mall;


use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Transformers\OrderDetailTransformer;
use Illuminate\Http\Request;
use Fractal;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $dataBox = Order::select('t1.*','t2.username')
            ->from((new Order())->getTable().' as t1')
            ->join((new Account())->getTable().' as t2','t1.userId','=','t2.userId')
            ->orderBy('createTime','DESC')
            ->paginate(20)->toArray();
        $result['BoxOrderList'] = $dataBox['data'];
        $result['BoxCount'] = $dataBox['total'];
//        $dataCar = Order::select('t1.*','t2.username')
//            ->from((new Order())->getTable().' as t1')
//            ->join((new Account())->getTable().' as t2','t1.userId','=','t2.uid')
//            ->orderBy('createTime','DESC')
//            ->where('t1.commodityClass','!=',1)
//            ->paginate(20)->toArray();
//        $result['CarOrderList'] = $dataCar['data'];
//        $result['CarCount'] = $dataCar['total'];
        return $this->apiResponse($result);
    }

    public function show($orderNo)
    {
        $data = Order::select('t1.*','t2.username')
            ->from((new Order())->getTable().' as t1')
            ->join((new Account())->getTable().' as t2','t1.userId','=','t2.userId')
            ->where('orderNo',$orderNo)
            ->first();
        if(!$data){
            return $this->apiResponse('', "参数错误", 2000);
        }
        $orderDetail = Order::find($data['orderId'])->orderDetail()->get();
        $orderDetailTemp= Fractal::collection($orderDetail, new OrderDetailTransformer())->getArray()['data'];
        $data['commodities'] = $orderDetailTemp;
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'orderNo'   =>'required',
            'orderStatus'   =>'required'
        ]);
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $status = Order::where(['orderNo'=>$data['orderNo']])->update(['orderStatus'=>$data['orderStatus']]);
        if(!$status){
            return $this->apiResponse('', config('errorCode.UPDATE_FAILED'));
        }else{
            return $this->apiResponse(true);
        }

    }

    public function deliver(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'orderNo'   =>'required',
            'deliverName'   =>'required',
            'deliverCode'   =>'required',
            'deliverNo'   =>'required'
        ]);
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $status = Order::where(['orderNo'=>$data['orderNo'],'orderStatus'=>'WAITDELIVER'])
            ->update(['orderStatus'=>'WAITRECEIVE','deliverName'=>$data['deliverName'],
                'deliverCode'=>$data['deliverCode'],'deliverNo'=>$data['deliverNo'],
                'deliveryTime'=>date("Y-m-d H:i:s",time())]);
        if(!$status){
             return $this->apiResponse('', config('errorCode.DELIVER_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }

}