<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-11-29
 * Time: 19:48
 */

namespace App\Http\Controllers\Api\V1\Manager\System;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $data = \DB::table("setting")->get();
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $setting = $request->setting;
        foreach ($setting as $value) {
            \DB::table("setting")->where('value', $value['value'])->update(['name' => $value['name']]);
        }
        return $this->apiResponse('');
    }
}