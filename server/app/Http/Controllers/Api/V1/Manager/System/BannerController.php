<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-10-20
 * Time: 17:57
 */

namespace App\Http\Controllers\Api\V1\Manager\System;


use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function index(Request $request)
    {
        $data = Banner::get();
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->only(['bannerId', 'bannerUrl', 'bannerImageUrl', 'blackStatus', 'bannerDetail']);
        $validator = Validator::make($data, [
            'bannerId' => 'integer',
            'bannerImageUrl' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $bannerId = $data['bannerId'];
        unset($data['bannerId']);
        if($bannerId){
            $status = Banner::where('bannerId',$bannerId)->update($data);
        }else{
            $status = Banner::create($data);
        }
        if(!$status){
            return $this->apiResponse('', config('errorCode.UPDATE_FAILED'));
        }
        return $this->apiResponse('');
    }

    public function destroy($bannerId)
    {
        Banner::destroy($bannerId);
        return $this->apiResponse('');
    }
}