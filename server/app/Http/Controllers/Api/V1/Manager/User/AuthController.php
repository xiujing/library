<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-28
 * Time: 16:26
 */

namespace App\Http\Controllers\Api\V1\Manager\User;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use Fractal;


class AuthController extends Controller
{
    protected $guard = 'api';

    public function token(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'userName' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->apiResponse($validator);
        }

        $credentials = $request->only('userName', 'password');

        try {
            if (!$token = Auth::guard($this->guard)->attempt($credentials)) {
                return $this->apiResponse('',config('errorCode.MANAGER_DELETE_ALREADY'));

            }
        } catch (JWTException $e) {
           //
        }

        return response()->json(compact('token'));
    }

    public function refreshToken()
    {

        $old_token = Auth::guard($this->guard)->getToken();
        $token = Auth::guard($this->guard)->refresh($old_token);
       Auth::guard($this->guard)->invalidate($old_token);

        return response()->json(compact('token'));

    }


}