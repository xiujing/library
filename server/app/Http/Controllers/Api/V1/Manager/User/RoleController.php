<?php
/**
 *管理员角色
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-05
 * Time: 12:28
 */

namespace App\Http\Controllers\Api\V1\Manager\User;


use App\Http\Controllers\Controller;
use App\Models\UserManager;
use App\Models\UserManagerRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        $data = UserManagerRole::get();

        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data,[
            'roleName'    => 'required',
            'roleDesc'    => 'required',
           // 'resources'   => 'required',
            'roleId'      => 'integer|required',
        ]);
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $roleId = $data['roleId'];
        unset($data['roleId']);
        if($roleId){//修改
            $status = UserManagerRole::where('roleId',$roleId)->update($data);
        }else{
            $data['createTime'] = date("Y-m-d H:i:s",time());
            $status = UserManagerRole::create($data);
        }
        if(!$status){
            return $this->apiResponse('', config('errorCode.ROLE_UPDATE_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }
    public function destroy($roleId)
    {
       /* $status = UserManager::where('roleId',$roleId)->first();
        if($status){
            return $this->apiResponse('', config('errorCode.ROLE_DELETE_FAILED'));
        }*/
        $status = UserManagerRole::destroy($roleId);
        if(!$status){
            return $this->apiResponse('', config('errorCode.ROLE_DELETE_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }

    public function resourcesAssign(Request $request, $roleId)
    {
        UserManagerRole::where('roleId',$roleId)->update(['resources'=>$request->resources]);
        return $this->apiResponse('');
    }
}