<?php
/**
 *管理员
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-05
 * Time: 14:52
 */

namespace App\Http\Controllers\Api\V1\Manager\User;


use App\Http\Controllers\Controller;
use App\Models\AdminGroup;
use App\Models\Group;
use App\Models\UserManager;
use App\Models\UserManagerLog;
use App\Models\UserManagerRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ManagerController extends Controller
{
    public function index(Request $request)
    {
        $keyWords = $request->keyWords;

        $sql = UserManager::select("*");
        if ($keyWords) {
            $sql->where(function ($query) use ($keyWords) {
                $query->where('userName', 'like', '%' . $keyWords . '%')
                    ->orWhere(function ($query) use ($keyWords) {
                        $query->where('realName', 'like', '%' . $keyWords . '%');
                    });
            });
        }
        $data = $sql->orderBy('createTime', 'desc')->get()->toArray();
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'managerId' => 'integer',
            'userName' => 'required',
            //   'password'      =>  'required',
            'realName' => 'required',
            'connectTel' => 'required'
//            'roleId' => 'integer',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $managerId = $data['managerId'];
        unset($data['managerId']);
        //判断roleId 存在数据
//        $status = UserManagerRole::find($data['roleId']);
//        if (!$status) {
//            return $this->apiResponse('', config('errorCode.ROLE_NOT_FAILED'));
//        }
        if ($managerId) {//修改
            if (@$data['password']) {
                unset($data['password']);
            }
            $status = UserManager::where('managerId', $managerId)->update($data);
        } else {
            $status = UserManager::where('userName', $data['userName'])->first();
            if ($status) {
                return $this->apiResponse('', config('errorCode.MANAGER_EXISTS_ALREADY'));
            }
            $data['createTime'] = date("Y-m-d H:i:s", time());
//            $data['accessToken'] = encrypt(time());
            $status = UserManager::create($data);
        }
        if (!$status) {
            return $this->apiResponse('', config('errorCode.MANAGER_ADD_FAILED'));
        } else {
            return $this->apiResponse(true);
        }
    }

    public function destroy($managerId)
    {
        $status = UserManager::destroy($managerId);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.MANAGER_DELETE_ALREADY'));
        } else {
            return $this->apiResponse(true);
        }
    }

    public function updateStates($managerId)
    {
        $status = UserManager::find($managerId);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.MANAGER_DELETE_ALREADY'));
        }
        $status = UserManager::where('managerId', $managerId)->update(['enabledStatus' => !($status['enabledStatus'])]);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.MANAGER_UPDATE_FAILED'));
        } else {
            return $this->apiResponse(true);
        }
    }

    public function Log(Request $request)
    {
        $pageSize = $request->pageSize ? $request->pageSize : 20;
        $keyWords = $request->keyWords;
        $startTime = $request->startTime;
        $endTime = $request->endTime;
        $sql = UserManagerLog::select("*");
        if($startTime){
            $sql->where('createTime','>=',$startTime);
        }
        if($endTime){
            $sql->where('createTime','<=',$endTime);
        }
        if ($keyWords) {
            $sql->where(function ($query) use ($keyWords) {
                $query->where('userName', 'like', '%' . $keyWords . '%')
                    ->orWhere(function ($query) use ($keyWords) {
                        $query->where('ip', 'like', '%' . $keyWords . '%');
                    })->orWhere(function ($query) use ($keyWords) {
                        $query->where('operation', 'like', '%' . $keyWords . '%');
                    });
            });
        }


        $data = $sql->  orderBy('createTime', 'DESC')->paginate($pageSize)->toArray();
        $result['log'] = $data['data'];
        $result['totalNum'] = $data['total'];
        return $this->apiResponse($result);
    }

    /**
     * 平台-管理员设置-课程负责人
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCourseStatus(Request $request)
    {
        $managerId = $request->managersId;
        $courseStatus = $request->courseStatus;
        $status = UserManager::where('managerId', $managerId)->update(['courseStatus' => $courseStatus]);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.MANAGER_UPDATE_FAILED'));
        }
        return $this->apiResponse('');
    }
}