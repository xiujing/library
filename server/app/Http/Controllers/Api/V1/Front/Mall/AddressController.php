<?php
/**
 *地址管理
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-15
 * Time: 15:06
 */

namespace App\Http\Controllers\Api\V1\Front\Mall;


use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class AddressController extends Controller
{
    public function index(Request $request)
    {
        $address = Address::where('userId',$request->userId)->get();
        return $this->apiResponse($address);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data,[
            'addressId'         =>'integer',
            'reciverName'       =>'required',
            'phoneNumber'       =>'regex:/^1[34578][0-9]{9}$/',
            'province'          =>'required',
            'city'              =>'required',
//            'district'          =>'required',
            'detailAddress'     =>'required'
        ]);
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $addressId = $data['addressId'];
        $userId = $request->userId;
//        unset($data['addressId']);
        try{
            \DB::beginTransaction();
            if($addressId){//修改
//                if($data['defaultStatus']){//证明要设置默认
//                    Address::where(['userId'=>userId])->update(['defaultStatus'=>0]);
////                    if(!$status){
////                        throw new \Exception("设置默认地址失败");
////                    }
//                }
                $status = Address::where(['userId'=>$userId,'addressId'=>$addressId])->update($data);
                if(!$status){
                    throw new \Exception("更新地址库失失败");
                }
                \DB::commit();
                return $this->apiResponse($addressId);
            }else{//新增
                $data['userId'] = $userId;
                $status = Address::create($data);
                if(!$status){
                    throw new \Exception("更新地址库失失败");
                }
                \DB::commit();
                return $this->apiResponse($status['addressId']);
            }
        }catch (\Exception $e){
            \DB::rollback();
            \Log::error("AddressController store".$e->getMessage());
            return $this->apiResponse('', config('errorCode.ADDRESS_UPDATE_FAILED'));
        }
    }

    public function update(Request $request, $addressId)
    {
        try{
            \DB::beginTransaction();
            $status = Address::where(['userId'=>$request->userId])->update(['defaultStatus'=>0]);
//            if(!$status){
//                throw new \Exception("更新地址库失败");
//            }
            $status = Address::where(['addressId'=>$addressId, 'userId'=>$request->userId])->update(['defaultStatus'=>1]);
            if(!$status){
                throw new \Exception("更新地址库失败");
            }
            \DB::commit();
            return $this->apiResponse(true);
        }catch (\Exception $e){
            \DB::rollback();
            \Log::error("AddressController update ".$e->getMessage());
            return $this->apiResponse('', config('errorCode.DEFAULTADDRESS_SET_FAILED'));
        }
    }

    public function show($addressId)
    {
        $data = Address::find($addressId);
        return $this->apiResponse($data);
    }

    public function destroy($addressId)
    {
        $status = Address::destroy($addressId);
        if(!$status){
            return $this->apiResponse('', config('errorCode.ADDRESS_UPDATE_FAILED'));
        }
        return $this->apiResponse(true);
    }
}