<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-15
 * Time: 17:18
 */

namespace App\Http\Controllers\Api\V1\Front\Mall;


use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Commodity;
use App\Models\CommoditySku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $data = Cart::select()
            ->from((new Cart())->getTable().' as t1')
            ->join((new Commodity())->getTable().' as t2','t1.commodityId','=','t2.commodityId')
            ->where(['t1.userId'=>$request->userId])->get();
        return $this->apiResponse($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data,[
            'cartId'        =>'integer',
            'commodityId'   =>'integer|required',
            'amounts'       =>'integer|required'
        ]);
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $commodity = Commodity::find($data['commodityId']);
        $userId = $request->userId;
        $data['price'] = $commodity['nowPrice'];
        $cartId = $data['cartId'];
        unset($data['cartId']);
        if($cartId){
            $status = Cart::where(['cartId'=>$cartId])->update($data);
        }else{
            $status = Cart::where(['commodityId'=>$data['commodityId'],'userId'=>$userId])->first();
            if($status){
                Cart::where(['commodityId'=>$data['commodityId'],'userId'=>$userId])->increment('amounts',$data['amounts']);
                return $this->apiResponse(true);
            }
            $data['userId'] = $userId;
            $status = Cart::create($data);
        }
        if(!$status){
            return $this->apiResponse('', config('errorCode.CATE_ADD_FAILED'));
        }
        return $this->apiResponse(true);
    }


    public function update(Request $request, $cartId)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'amounts' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        $status = Cart::where('cartId', $cartId)->update($data);
        if (!$status) {
            return $this->apiResponse('', config('errorCode.EDI_REQ_FAILED'));
        }else{
            return $this->apiResponse(true);
        }
    }

    public function moreDestroy(Request $request){
        $id=$request->cartId;

        $arr=explode(",",$id);
//
        $status = Cart::whereIn('cartId',$arr)->delete();
        if(!$status){
            return $this->apiResponse('', config('errorCode.CATE_DELETE_FAILED'));
        }
        return $this->apiResponse($status);

    }

    public function destroy($cartId)
    {
        $status = Cart::destroy($cartId);
        if(!$status){
            return $this->apiResponse('', config('errorCode.CATE_DELETE_FAILED'));
        }
        return $this->apiResponse(true);
    }

}