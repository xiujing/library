<?php
/**
 * 快递查看
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-16
 * Time: 11:11
 */

namespace App\Http\Controllers\Api\V1\Front\Mall;


use App\Http\Controllers\Controller;

class DeliverController extends Controller
{
    public function getDeliver($deliverCode, $deliverNo)
    {
        $data = app('kuaidi')->getData($deliverCode,$deliverNo);
        return $this->apiResponse($data);
    }
}