<?php
/**
 * 订单管理
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-07
 * Time: 19:17
 */

namespace App\Http\Controllers\Api\V1\Front\Mall;


use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderRefund;
use App\Services\Front\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    public function order(Request $request)
    {
        $orderService = new OrderService();
        $data = $request->all();
        $validator = Validator::make($data,[
            'province'      =>     'required',
            'city'          =>     'required',
            'district'      =>     'required',
            'detailAddress' =>     'required',
            'reciverName'   =>     'required',
            'phoneNumber'   =>      'required',
//            'commodityClass'=>      'required',
            'commodityList' =>      'required'
        ]);
        $userId = $request->userId;
        if($validator->fails()){
            return $this->apiResponse('', config('errorCode.INVALID_REQ_PARAMS'));
        }
        try{
            \DB::beginTransaction();

            //第一步 取出数据 skuId 、commodityId 、 cartId
            $result =  $orderService->createOrderStep1($data);

            //第2步 得到购买商品信息 计算出总金额 应得金额  运费 加上运费后的金额信息
            $commodity = $orderService->createOrderStep2($result);
//            第3步 生成订单 存储支付信息 订单信息 订单扩展信息 订单日志 订单商品
            $order = $orderService->createOrderStep3($commodity,$data,$userId);

            //            第4步 删除购物车、减少订库存、增加销量
            $orderService->createOrderStep4($commodity['commodity'],$result['cartId']);
            $orderResponse['orderNo'] = $order[0]['orderNo'];
            $orderResponse['totalFee'] = $order['totalFee'];

            \DB::commit();
            return $this->apiResponse($orderResponse);
        }catch (\Exception $e){
            \DB::rollback();
            \Log::error("Front OrderController order".$e);
            return $this->apiResponse('', config('errorCode.ORDER_PAY_FAILED'));
        }
    }

    public function orderList(Request $request)
    {
        $userId = $request->userId;
        $orderStatus = $request->orderStatus;
        $sql = Order::where('userId',$userId)->orderBy('createTime','DESC');
        if($orderStatus){
            $sql->where('orderStatus',$orderStatus)->orderBy('createTime','DESC');
        }
        $data = $sql->with('orderDetail')->paginate()->toArray();

        $result['orderList'] = $data['data'];
        $result['totalNum'] = $data['total'];



       return $this->apiResponse($result);
    }

    public function destroy($orderId)
    {
        $data = Order::destroy($orderId);
        return $this->apiResponse($data);
    }

    public function show($orderNo){
        $data = Order::select('orderStatus')->where('orderNo',$orderNo)->first();
        return $this->apiResponse($data);
    }
    /**
     * 确认收货
     * @param $orderNo
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($orderNo)
    {
        $status = Order::where(['orderNo'=>$orderNo, 'orderStatus'=>'WAITRECEIVE'])->update(['orderStatus'=>'WAITCOMMENT']);
        if(!$status){
            return $this->apiResponse('', config('errorCode.ORDER_DELIVER_FAILED'));
        }
        return $this->apiResponse(true);
    }
}