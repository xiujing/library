<?php

namespace App\Http\Controllers\Api\V1\Front\Wx;

use Illuminate\Http\Request;
use App\Libs\Contracts\PayMent\Wxpay\JsApiNativePay;
use App\Libs\Contracts\PayMent\Wxpay\ScanNativePay;
use App\Libs\Contracts\PayMent\Wxpay\NativePay;
use App\Libs\Contracts\PayMent\Wxpay\WeixinPay;
use App\Models\Business;
use App\Models\User;
use App\Models\MemberOrder;
use App\Models\ResumeOrder;
use App\Models\MemberWelfare;
use App\Models\Profit;
use App\Libs\Contracts\PayMent\Wxpay\WxPayApi;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SandboxController extends Controller
{
    /**
     * 获取沙箱秘钥
     * @return mixed
     */
    public function GetKey()
    {
        $test = NativePay::getkey();
        return $test;
    }

    /**
     * 1003用例测试---扫码支付（统一下单）
     */
    public function pay()
    {
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/unifiedorder";
        $model = new WxPayApi();
        $out_trade_no = date('YmdHis') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);
        $data['body'] = "1004用例测试";
        $data['out_trade_no'] = $out_trade_no;
        $data['total_fee'] = 552;
        $data['spbill_create_ip'] = getIp();
        $data['notify_url'] = 'https://30days-tech.com/v1/front/wx/sandbox/callback';
        $data['trade_type'] = 'NATIVE';
        $key = env('sandbox_signkey');
        $data['sign'] = $model->MakeSign($data, $key);
        $xml = $model->ToXml($data);
        $response = $model->postXmlCurl($xml, $url);
        $arr = xmlToArray($response);
        return $arr;
    }
    //写入日志
    function log2($arg)
    {
        $log = vsprintf('%s', print_r($arg, true));
        $log = date('[Y/m/d H:i:s]') .'---'. $log . PHP_EOL;
        $path = dirname(__FILE__) . '/log.log';
        $fp = file_put_contents( $path,$log, FILE_APPEND);
        return true;
    }
    public function callback()
    {
        $postXml = file_get_contents("php://input"); //接收微信参数
        $this->log2($postXml);
        //$result = xmlToArray($postXml);
    }

    /**
     * 查询订单
     */
    public function query()
    {
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/orderquery";
        $model = new WxPayApi();
        $data['out_trade_no'] = '2018050712021430758';
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);
        $key = env('sandbox_signkey');
        $data['sign'] = $model->MakeSign($data, $key);
        $xml = $model->ToXml($data);
        $response = $model->postXmlCurl($xml, $url);
        $this->log2($response);
//        $arr = xmlToArray($response);
//        return $arr;
    }
    /**
     * 1004用例测试
     */
    public function refund()
    {
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/refund";
        $model = new WxPayApi();
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);

        $data['out_trade_no'] = '2018050712021430758';
        $data['out_refund_no'] = 'refund'.date('YmdHis') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
        $data['total_fee'] = 552;
        $data['refund_fee'] = 552;
        $key = env('sandbox_signkey');
        $data['sign'] = $model->MakeSign($data, $key);
        $xml = $model->ToXml($data);
        $response = $model->postXmlCurl($xml, $url);
        $this->log2($response);


    }
    /**
     * 1004用例退款查询
     */
    public function RefundQuery()
    {
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/refundquery";
        $model = new WxPayApi();
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);
        $data['out_trade_no'] = '2018050712021430758';
        $key = env('sandbox_signkey');
        $data['sign'] = $model->MakeSign($data, $key);
        $xml = $model->ToXml($data);
        $response = $model->postXmlCurl($xml, $url);
        $this->log2($response);

    }
    /**
     * 1005用例下载交易对账单
     */
    public function downloadbill()
    {
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/downloadbill";
        $model = new WxPayApi();
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);
        $data['bill_date'] = date('Ymd');
        $data['bill_type'] = 'REFUND';
        $key = env('sandbox_signkey');
        $data['sign'] = $model->MakeSign($data, $key);
        $xml = $model->ToXml($data);
        $response = $model->postXmlCurl($xml, $url);
        return $response;
    }


}