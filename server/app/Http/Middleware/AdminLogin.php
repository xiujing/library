<?php


namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\UserManager;
use App\Models\UserManagerLog;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminLogin extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
       /* $accessToken = $request->header("accessToken");
        $manager = UserManager::where('accessToken', $accessToken)->first();

        if (!$manager) {
            return $this->apiResponse('', config('errorCode.PERMISSION_DENY'));
        }
        $userName = $manager->userName;
        $managerId = $manager->managerId;
        $request->managerId = $managerId;
        $request->managerName = $userName;
        $request->roleId = $manager->roleId;
        $request->managerCourseStatus = $manager->courseStatus;*/
        return $next($request);
    }
}
