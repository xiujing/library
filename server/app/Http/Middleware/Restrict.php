<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-05
 * Time: 12:12
 */

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class Restrict extends Controller
{
    /**
     * Handle an incoming request.
     *  前台通过 template+随机数 两次md5加密 形成头部Token   服务端通过返回的随机数+template 两次md5加密 比对是否正确
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      /*  $rand = $request->header("rand");
        $token = $request->header("Token");
        $okToken = md5(md5("template".$rand));
        if($token != $okToken){
            return $this->apiResponse('', config('errorCode.PERMISSION_DENY'));
        }*/
        return $next($request);
    }
}
