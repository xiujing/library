<?php


namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\UserManager;
use App\Models\UserManagerLog;
use Closure;
use Illuminate\Support\Facades\Auth;

class Operation extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $operation = $request->header("operation");
        $accessToken = $request->header("accessToken");
        $manager = UserManager::where('accessToken',$accessToken)->first();

        if(!$manager){
            return $this->apiResponse('', config('errorCode.PERMISSION_DENY'));
        }
        $managerId = $manager->managerId;
        $userName = $manager->userName;
        $ip = getIp();

        UserManagerLog::create(['ip'=>$ip, 'operation'=>"登陆成功",'userName'=>$userName,'managerId'=>$managerId]);
        $request->managerId = $managerId;
        return $next($request);
    }
}
