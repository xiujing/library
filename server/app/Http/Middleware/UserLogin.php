<?php


namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class UserLogin extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
          $userAccess = $request->header("userAccess");
          if(!$userAccess){
              return $this->apiResponse('', config('errorCode.PERMISSION_DENY'));
          }
          $user = Account::select("*")->where("userAccess",$userAccess)->first();
          if(!$user){
              return $this->apiResponse('', config('errorCode.PERMISSION_DENY'));
          }
          $request->userId = $user['userId'];
        return $next($request);
    }
}
