<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 16:06
 */

namespace App\Transformers;


use App\Models\OrderDetail;
use League\Fractal\TransformerAbstract;

class OrderDetailTransformer extends TransformerAbstract
{
    public function transform(OrderDetail $order)
    {
        return [
            'detailId'              =>$order->detailId,
            'skuId'                 =>$order->skuId,
            'commodityName'         =>$order->commodityName,
            'commodityPrice'        =>$order->commodityPrice,
            'commodityIcon'         =>$order->commodityIcon,
            'descPictures'          =>$order->descPictures,
            'amounts'               =>$order->amounts,
            'commodityId'           =>$order->commodityId,
            'skuDesc'               =>$order->skuDesc,
            'discount'              =>$order->discount,
            'totalPrice'            =>$order->totalPrice,
        ];
    }
}