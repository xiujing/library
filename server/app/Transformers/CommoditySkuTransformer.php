<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-06
 * Time: 12:11
 */

namespace App\Transformers;


use App\Models\CommoditySku;
use League\Fractal\TransformerAbstract;

class CommoditySkuTransformer extends TransformerAbstract
{
    public function transform(CommoditySku $sku)
    {
        return [
            "skuId" => $sku->skuId,
            "skuDesc" => unserialize($sku->skuDesc),
            "commodityPrice" => $sku->commodityPrice,
            "discount" => $sku->discount,
            "stock" => $sku->stock
        ];
    }
}


