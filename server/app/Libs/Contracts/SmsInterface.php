<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-04
 * Time: 15:08
 */

namespace App\Libs\Contracts;


interface SmsInterface
{
    /**
     * @param $mobile 手机号
     * @param $type 验证码类型
     * @return mixed true|false [$state, $msg]
     */
    public function send($mobile, $type);


    /**
     * @param $mobile  手机号
     * @param $type    发送类型
     * @param $smscode 验证码
     * @return mixed   验证是否成功
     */
    public function check($mobile, $type, $smscode);

    /**
     * @param $mobile  手机号
     * @param $type
     * @param $content   内容
     * @return mixed
     */
    public function sendOnly($mobile, $type, $content);

}