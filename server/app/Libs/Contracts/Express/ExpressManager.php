<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-04
 * Time: 14:55
 */

namespace App\Libs\Contracts\Express;

use Illuminate\Support\Manager;

class ExpressManager extends Manager
{
    public function __construct($app)
    {
        parent::__construct($app);
    }
    public function getDefaultDriver()
    {
        return "kuaidi";
    }
    public function createKuaidiDriver()
    {
        return new Kuaidimpl();
    }
}