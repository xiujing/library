<?php
/**
 * 阿里大于短信接口
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-04
 * Time: 15:08
 */

namespace App\Libs\Sms;

class AliSms
{
    public function send($mobile, $code, $content)
    {
        return $this->_sendAli($mobile, $code, $content);
    }

    /**
     * 阿里短信接口
     *
     * @param $mobile
     * @param $content
     */
    private function _sendAli($mobile, $code, $param)
    {
        if (!defined('SCRIPT_ROOT')) {
            define('SCRIPT_ROOT',  app_path().'/Libs/Sms/Aliapi/');
        }
        /**
         * SDK工作目录
         * 存放日志，TOP缓存数据
         */
        if (!defined("TOP_SDK_WORK_DIR"))
        {
            define("TOP_SDK_WORK_DIR", SCRIPT_ROOT."/tmp/");
        }
        require_once SCRIPT_ROOT.'top/TopClient.php';
        require_once SCRIPT_ROOT.'top/ResultSet.php';
        require_once SCRIPT_ROOT.'top/request/AlibabaAliqinFcSmsNumSendRequest.php';
        require_once SCRIPT_ROOT.'top/RequestCheckUtil.php';
        require_once SCRIPT_ROOT.'top/TopLogger.php';

        /**
         * appkey
         */
        $appkey = '23822985';

        /**
         * secretKey
         */
        $secretKey = '12f08439680decb4500399d45791f36d';


        /**
         * setSmsType
         */
        $setSmsType = 'normal';

        /**
         * 签名
         */
        $setSmsFreeSignName = '纳德光学';


        $client = new \TopClient($appkey, $secretKey);

        $req = new \AlibabaAliqinFcSmsNumSendRequest();

        $req->setSmsType($setSmsType);

        $req ->setSmsFreeSignName($setSmsFreeSignName);

        $req ->setSmsParam($param);

        $req->setRecNum($mobile);

        $req->setSmsTemplateCode($code);

        $resp = $client->execute($req);

        if (isset($resp->result->err_code)) {
            if ($resp->result->err_code == '0') {	//发送成功
                return true;
            } else {
                return false;
            }
        }
    }
}