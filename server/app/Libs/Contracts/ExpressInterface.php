<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-04
 * Time: 14:56
 */

namespace App\Libs\Contracts;



interface ExpressInterface
{
    /**
     *
     * @param $e_code
     * @param $shipping_code
     * @return mixed
     */
    public function getData($e_code, $shipping_code);
}