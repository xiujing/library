<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-04
 * Time: 15:11
 */

namespace App\Libs\Contracts\SMS;


use App\Libs\Contracts\SMS\CCPRestSmsSDK;
use App\Libs\Contracts\SmsInterface;
use Illuminate\Support\Facades\Redis;


class CcpImlp implements SmsInterface
{
    //主帐号,对应开官网发者主账号下的 ACCOUNT SID
    private $accountSid= '8aaf070867e8660f01680d4f4f3311ea';

    //主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
    private $accountToken= '385fd5f4a8f144449de651f9f8e3a5c1';

    //应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
    //在开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID
    private $appId='8aaf070867e8660f01680d4f4f8311f1';
    
    //请求地址
    //沙盒环境（用于应用开发调试）：sandboxapp.cloopen.com
    //生产环境（用户应用上线使用）：app.cloopen.com
    private $serverIP='app.cloopen.com';


    //请求端口，生产环境和沙盒环境一致
    private$serverPort='8883';

    //REST版本号，在官网文档REST介绍中获得。
    private $softVersion='2013-12-26';

    public function __construct()
    {

    }
    /**
     * @param $mobile 手机号
     * @param $type 验证码类型
     * @return mixed true|false [$state, $msg]
     */
    public function send($mobile, $code)
    {
        return $this->makesms($mobile, $code);
        // TODO: Implement send() method.
    }

    public function sendPassword($mobile, $password, $contractorAllName)
    {

        return $this->makePassword($mobile, $password, $contractorAllName);
        // TODO: Implement send() method.
    }

    public function sendContractorDate($mobile, $content,$contractorAllName )
    {

        return $this->makeContractorDate($mobile, $content, $contractorAllName);
        // TODO: Implement send() method.
    }

    /**
     * @param $mobile  手机号
     * @param $type    发送类型
     * @param $smscode 验证码
     * @return mixed   验证是否成功
     */
    public function check($mobile, $type, $smscode)
    {
        if($type === "code"){
           $code =  \RedisDB::get($mobile);
           if($code == $smscode){
               \RedisDB::del($mobile);
                return true;
           }else{
               return false;
           }
        }
        
        // TODO: Implement check() method.
    }

    /**
     * @param $mobile  手机号
     * @param $type
     * @param $content   内容
     * @return mixed
     */
    public function sendOnly($mobile, $type, $content)
    {
        // TODO: Implement sendOnly() method.
    }

    protected function makesms($phone, $code)
    {
        $rest = new CCPRestSmsSDK($this->serverIP,$this->serverPort,$this->softVersion);
        $rest->setAccount($this->accountSid,$this->accountToken);
        $rest->setAppId($this->appId);
        $result = $rest->sendTemplateSMS($phone,array($code,"30分钟"),"405845");
        if($result->statusCode == "160040" || $result->statusCode == "160032"){
            return -300;
        }

        \RedisDB::setex($phone,1800,$code);
        //Redis::set($phone,$code);
    //    Redis::expire($phone,1800);//30分钟后过期
        return  100;
    }

    /**
     * 验证短信校验码
     * @param $mobile
     * @param $type
     * @param $smscode
     * @return int
     */
    public function checksms($mobile, $smscode)
    {
        //检测手机校验码是否过期
        //$smstime = \Cache::pull($mobile);//获取缓冲值，并且删除
        $code =  \RedisDB::get($mobile);
        \RedisDB::del($mobile);
        if (!$code) {
            return -100;//已经过期
        }
        if($code === $smscode){
            return  100;
        }else{
            return -300;//已经验证过了
        }
    }

    /**
     * 发送修改密码通知
     * @param $phone
     * @param $password
     * @param $contractorAllName
     * @return int
     */
    protected function makePassword($phone, $password, $contractorAllName)
    {
        $rest = new CCPRestSmsSDK($this->serverIP,$this->serverPort,$this->softVersion);
        $rest->setAccount($this->accountSid,$this->accountToken);
        $rest->setAppId($this->appId);
        $result = $rest->sendTemplateSMS($phone,array($contractorAllName,$password),"225452");
        if($result->statusCode == "160040" || $result->statusCode == "160032"){
            return -300;
        }
        return  100;
    }

    /**
     * 发送修改资料通知
     * @param $phone
     * @param $password
     * @param $contractorAllName
     * @return int
     */
    protected function makeContractorDate($phone, $content,$contractorAllName)
    {
        $rest = new CCPRestSmsSDK($this->serverIP,$this->serverPort,$this->softVersion);
        $rest->setAccount($this->accountSid,$this->accountToken);
        $rest->setAppId($this->appId);
        $result = $rest->sendTemplateSMS($phone,array($contractorAllName, $content),"225485");
        if($result->statusCode == "160040" || $result->statusCode == "233949"){
            return -300;
        }
        return  100;
    }
}