<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-04
 * Time: 15:12
 */

namespace App\Libs\Contracts\SMS;


use Illuminate\Support\Manager;

class SMSManager extends Manager
{

    public function __construct($app)
    {
        parent::__construct($app);
    }

    public function getDefaultDriver()
    {
        return 'ccp';
    }

    /**
     * 阿里短信接口
     *
     * @return AliImpl
     */
    public function createAliDriver()
    {
        return new AliImpl();
    }


    /**
     * 腾讯短信接口
     *
     * @return TxImpl
     */
    public function createTxDriver()
    {
        return new TxImpl();
    }

    /**
     * 容联云
     * @return TxImpl
     */
    public function createCcpDriver()
    {
        return new CcpImlp();
    }
}