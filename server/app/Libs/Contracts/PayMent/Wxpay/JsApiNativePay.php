<?php
/**
 * 微信公众号支付
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-10-13
 * Time: 10:27
 */

namespace App\Libs\Contracts\PayMent\Wxpay;


class JsApiNativePay
{
    public static function JsApiNativePay($order)
    {
        return   NativePay::nativePay($order, "JSAPI");
    }
}