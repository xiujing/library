<?php
/**
 *扫码支付
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-07
 * Time: 19:02
 */

namespace App\Libs\Contracts\PayMent\Wxpay;


class ScanNativePay
{
    /**
     *
     * @param $order
     * @param $goodDetail  json  '{"goods_detail": [{"goods_id": 1,  "quantity": 1, "price": 105}]}';
     * @return mixed
     */
    public static function scanNativePay($order, $goodDetail)
    {
       return   NativePay::nativePay($order, "NATIVE", $goodDetail);
    }

}