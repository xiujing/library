<?php
/**
 * 微信H5支付
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-12
 * Time: 9:47
 */

namespace App\Libs\Contracts\PayMent\Wxpay;


class H5NativePay
{
    public static function H5NativePay($order)
    {
        return   NativePay::nativePay($order, "MWEB");
    }

}