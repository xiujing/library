<?php
/**
 * 所有支付的公共方法
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-07
 * Time: 19:14
 */

namespace App\Libs\Contracts\PayMent\Wxpay;


class NativePay
{

    /**
     * @param $order
     * @param $tradeType
     * @param $goodDetail  json  '{"goods_detail": [{"goods_id": 1,  "quantity": 1, "price": 105}]}';
     * @return mixed     {"goods_detail":[{"goods_id":11,"quantity":1,"price":1}]}
     * @throws \Exception
     */
    public static function nativePay($order, $tradeType, $goodDetail = '')
    {
        $notifyUrl = empty($order['notifyUrl']) ? env("NOTIFY_URL") : $order['notifyUrl'];
        $model = new WxPayApi();
        $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);
        $data['sign_type'] = "MD5";
        $data['body'] = $order['body'];
        $data['out_trade_no'] = $order['orderNo'];
        $data['total_fee'] = $order['totalFee'];
        $data['spbill_create_ip'] = getIp();
        $data['version'] = "1.0";
        if ($goodDetail) {
       //     $data['detail'] = $goodDetail;
        }

        $data['notify_url'] = $notifyUrl;
        $data['trade_type'] = $tradeType;
        $data['goods_tag'] = '110';
        if (!empty($order['openId'])) {
            $data['openid'] = $order['openId'];
        }
        $key = "";
        $data['sign'] = $model->MakeSign($data, $key);
//        dd($data);
        $xml = $model->ToXml($data);
//
        $response = $model->postXmlCurl($xml, $url);
        $arr = xmlToArray($response);

//        return $response;
        if (($arr['return_code'] === "SUCCESS") && ($arr['result_code'] === "SUCCESS")) {
            return $arr;
        } else {
            if ($arr['return_code'] === "FAIL") {
                throw new \Exception("预支付下单失败错误码:" . $arr['return_msg']);
            } else {
                throw new \Exception("预支付下单失败错误码:" . $arr['err_code_des']);
            }
        }
    }

    public static function orderquery($orderNo)
    {
        $model = new WxPayApi();
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/orderquery";
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);
        $data['sign_type'] = "MD5";
        $data['out_trade_no'] = $orderNo;
        //沙箱
        $signKeyParams['mch_id'] = env('MCHID');
        $signKeyParams['nonce_str'] = $data['nonce_str'];
        $temp = $model->MakeSign($signKeyParams);
        $signKeyParams['sign'] = $temp;
        $xmlTemp = $model->ToXml($signKeyParams);
        $responseTemp = $model->postXmlCurl($xmlTemp, "https://api.mch.weixin.qq.com/sandboxnew/pay/getsignkey");
        $arr = xmlToArray($responseTemp);

        $key = $arr['sandbox_signkey'];
        $data['sign'] = $model->MakeSign($data, $key);
        $xml = $model->ToXml($data);

        $response = $model->postXmlCurl($xml, $url);
        return $response;
    }

    public static function refundquery($orderNo)
    {
        $model = new WxPayApi();
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/refundquery";
        $data['appid'] = env('APPID');
        $data['mch_id'] = env('MCHID');
        $data['nonce_str'] = nonceStr(32);
        $data['sign_type'] = "MD5";
        $data['out_trade_no'] = $orderNo;
        //沙箱
        $signKeyParams['mch_id'] = env('MCHID');
        $signKeyParams['nonce_str'] = $data['nonce_str'];
        $temp = $model->MakeSign($signKeyParams);
        $signKeyParams['sign'] = $temp;
        $xmlTemp = $model->ToXml($signKeyParams);
        $responseTemp = $model->postXmlCurl($xmlTemp, "https://api.mch.weixin.qq.com/sandboxnew/pay/getsignkey");
        $arr = xmlToArray($responseTemp);

        $key = $arr['sandbox_signkey'];
        $data['sign'] = $model->MakeSign($data, $key);
        $xml = $model->ToXml($data);

        $response = $model->postXmlCurl($xml, $url);
        return $response;
    }


}