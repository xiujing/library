<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2017091208686535",

		//商户私钥，您的原始格式RSA私钥
		'merchant_private_key' => "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCbsGT9fz2p5GobQwo15u17MxBnb4ugt0u/StMWhcBDHbBAisjl3F8+/Cq1/cJGEGmIzb/lPcQoVAIxs8tzbPtfdhJqdXCJZ0/28b5xrGOUBeYcNDhQ3Fz2hG91qM7MCi0ZdtUiIvfEuJVhrHbx+oPazkjlUSjsZKZSIekra9yisscgNeZBDurIGbinUUjQ7C+dEB0rV+NVohC/mxd50LYDBfdL1Ctvt4TeXFbnBPPegMm3VpRAggcLRL7MGHY/39N5yJ+x7OX+4c89ufNvvQd2djglGR8uDEWHoOQ/yVcnJp659Fqrs3f2R8YbF9YzMMObZ3CxUURAMkQ74zXDhqHbAgMBAAECggEBAJkp7hCdjzKGbgSn7XGd3+AmGmtIB5ty9+gXWpd7uevNV0oHvyCzrlguiG5GWUvIdaqKj9Cc9U+fZhEzimHhw2Lkq7VkF/R1WOveY7c9GzHSo9D3ZrfWMvaqz2R3UArXsAqvJ3rP3+T+mqdenh3skK8/5eMD9zioxB9lERkQohzEL7m3KSkAU4bQWIptI4a8hhhroVH84tLKFAwSnROCFZSKReWEf1wTGfrMnOjmGPx+xzDuS51CRAaoADDT7k8OTh36bsu033pQ8wbgci/R5i5N4rzSEjpjru5uNpLOjckp3gSRpr8g2s1541KeRdg7QHOO6iwiRbwJh3bRT1QZ5HECgYEAza1h6/Nq2t+xY3r2gtQ7F6BLpvA+cduCI/UnFxVeXQOPUUWeBPTPIB3lLuG53gw+vukLGcI90gIB2KiHctamEQvyRM5388jJPcZe97UY5m5fdPIgMd6Eb/GgKpch+atmZOEKwTatSCx+NKbVgrSKC73jIDw/2cPAzHacqehI728CgYEAwcf+ZWXAmcUuUy1NBPogZuk7syQqF7wo93aTYWrTp5TWdxOpjltCpRmB8xYwKkJztSdDEQ1LrLrhuCXyxdK+44jkCN0Kqh/b5Gmgj0yPYPMLZP96f8i0nAML8S/9rPGrwcBDrdTDwkkxaXHGbQoeMM5sVT3d+TY45ZZ/uOax/lUCgYEAvLbtNfVugczlkgVs0bWxUog9YbrlUMq7qgpyB3gP0QBHK6bYymtk8G79rfpm9BoAKGLjUIss9dfpocVgIjpYvSNc800OOpxsKUYuNNHxOtzisQbIC3nDwoLNFsb84Xwmw0DJAWcNrz9DvUOme/ry/rsH/rfJTiGScQgTzsHWyfkCgYEAqKXzHLJSzkFS0OcTKeq6enYwE/e8fo2upbhYCieb7zRU9qtxHMkGFoQ9mZ9M6eqNozJ5ZwTIQJsgyVCVml+Q7Jv9wk0F9AvoJ0FakHyUKE8+Mnc6WK4HqePo587Zm0N1NRmsjUQn3xYuX9O6Wrm8jsOIeyNt5W+wUvgJTpXQgUUCgYBj8S3dAwXn8WkvKDFymui4xsNXFyxFxnK9EFfWlTPwl0UiMlw9leqrtZUoirrsvvW7GwpGuSRSmZ9LunWoEaQzzZdBw5vDLONwk2iJTYZu+QZAGDNJr/Mx4Gf7HC9lP++w6Dvt47ytTljYPNpX5tKPz13dzQX8Xpx8CD/GwFAPJQ==",
		
		//异步通知地址
		'notify_url' => "http://www.upygo.com",
		
		//同步跳转
		'return_url' => "http://www.upygo.com",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnBDKsP8CTjy1JP2V22KTMfe/FdUCZHf4/uYLQpkKzBfrOrer6D0Yuz+nsdQHXdpMSvaG8RizWh4RQ2QkPsN6dQZD+K0oU1SlSIYSyXBirSf9w/HhtACc2AilZM2zBA830024Ui6BWqOYQd9U/z5zdlImFpO5fdl0tB2dhI8tKfxuV7EjDG4+Uxa4VzJroDpkwyVMJtOtX26BCFxE5i+SDSJ4BQK7mVTQ4CZd7sbAJf0tXjiOTWjnAkw2yu0vVlF9ks8yZt4gHKYHLw1biRJSLTUZdRTyHhQyyxfz2p9qbzs4oF+3s1qt8zAkgrrHFdbh8LMhnMcxXRM6p5GN70o1+wIDAQAB",
		
	
);