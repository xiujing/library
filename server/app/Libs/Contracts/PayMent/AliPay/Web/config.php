<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2017062307555074",

		//商户私钥
		'merchant_private_key' => "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDpVefgzMlpKL9WnjESUUN9bjotspFuYtDieCWz/neyAH4OS/QKSdcFp/6FKeJ6NOe8UFs7bXbAvbscTm7virkahLJmX6N6LEkzBZm29FD1Qg7izxTbXTjbkuwx5AjTL9rlssdi/W66euWDlVaJrZUvLEDr7tU9zktCc5eBDQQyABudRaWV8NPu3YM13H7xJdHg3B58Svdx/EZzjeo6Mq1vhT6o8nhQVCBTnTxoa9BgTbfmtvWa4ivpwljyXU7V5AHhNa82TiFtUYdU2jS8nOYusDLFkY3pgLoCeojbNX4TU18AE6VUxFRVngcl1zRk1TaHdzDY49q0HwicP/KJ3X2JAgMBAAECggEBAKNqyeJUKn9ezVrRodaNWXIgX9ifZmFkDZ1+vhEomBXvaFmE8l6+Rjys4oHKytYB9Yaa1LpzOKdWehQiDmnvwyICa5ttQeYP3BhCxNwwkf6jDB/nHllgGbEWXhu29ENMEcerhR8cFY+/AL0+OwRjsMb//FwAwhHuSCE7KZF1V0nYGQl+ibZr3MHKQgBGDxzjBSgZzp9vKf0XetFgU0WlJz3gY154RBIeWpNSKcYeK6E4etbkh3HHH5YD31+Jz5RWqwii+XdcF8pKaidCaYnG4wT9b6osx0Ai/+Az3pnLJLNP6ueEpTFoUCw5JSUTmOEisAz8u8597ScR97FRXS9jfvECgYEA+suw22KHhgXoo1Pk3muOdf3SmwbDioTSIlklP5M1qdr8FDQfEvs6EE5X2nZ/yBw4NyBNofJ0B7LE/AYMYmY2agSPD8X/kdwKkWeW4SDIcYB7rpX4xo0daAXMV3dw4R30srCqfUFXgLu4GEWvahfQotr83bbqpWpCJyo5NlzJbnUCgYEA7i12DSFwA1Xtzx6TxvH+fGn5Epvi6+KCUM6tYJm5BnxknYgH8CdCHbWhwvgQJ0H7DeqP/8Eint43WvO0iVLp6r81F8xqYsz14lidZ1zvoVy7iwgZrVkbZhe4rf38fx3TRjqhs9XuRUt15YPwaAFP0kV8T7waQx7jRhP3t7zg2EUCgYBr8SVylyq7TWm+ekpETMipEhEGNlgNE3OZ4wJ8JXDwZVcfnvtM4SIxiHaZiW0gBL1RRK8FJ+Q2uAO+cR/9bf63jLTCVjbUYX+O66XtyX1cFK+nCddkJbJlZjF35+mGiq0aB52pgWkW+DSZBc0k/ZEHtvckuiit/1jvoGW1dyQfTQKBgEVgtKxqewhdtWHJlF41C14OOGE1S0pOtor3SQWEgaLkBSLmBvnrVZT13WK/g2UMEScilkN3bLA+Jn2103bU0hJxE/QUtUCGV3FRQrhjF+I28uIyv9gAxrxcWI1GGJCmjW49kfCidj91Fah/YD3LdAbZ+rX3Iuc+ThGRq2y39f7JAoGAKpTiZ6DtlVCOdXBFQMGNUjUdzxxT5qGQiVzfzYKfmNfP/vQTL3vT7NqRNpSp38BLdDo1DnyljDp7rQw2gsAHb41vCpu0u/v5jNki8CTj/xmawebcGNPXu065DHWoEIDdIzv6xr8NirbyT9iAU3/BFhH+i72C+R4pAnAqWAYH1wE=",
		
		//异步通知地址
		'notify_url' => "http://www.upygo.com/",
		
		//同步跳转
		'return_url' => "http://www.upygo.com/",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiWYuu5/zq7pnRK/L28LcltgRmLIkVW2GavSs3E1L8k3UfR6VWIxZOJrsrsED6NZtxJP4oHS1g9M/uP/wNvZe9QjXya8+MXoxuUySgxZkb26MFcdBRR3AYkvrQrAQPkSW/yG+jHi5/EWD9FtumuzcPb3/yYBx8cutF9A/BvhNqHvj58Hn8HR9OGBKqCWwkn1zq3p9uLmcZHjK2oozGjkp0OiF9WQy3s/BcX8h8FwgMVQZ+DVJsCTLQ8esb7KBf1jR/VH5KSBqA3F2WgcZsNzdDn2sIf4QNzb+RKOpANktTKdtGCNGOmSCLZZjgRS4WR+EWm/co5IO3MmwtCB6ZTE0GQIDAQAB",
);