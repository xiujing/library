<?php
/**
 *手机支付宝支付
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-12
 * Time: 17:27
 */

namespace App\Libs\Contracts\PayMent\AliPay;




use App\Libs\Contracts\PayMent\AliPay\Mobile\wappay\buildermodel\AlipayTradeWapPayContentBuilder;
use App\Libs\Contracts\PayMent\AliPay\Mobile\wappay\service\AlipayTradeService;

class AliMobile
{
    public function pay($order)
    {
        $config = getAliConfig();
        //构造参数
        //超时时间
        $timeout_express="1m";
        $payRequestBuilder = new AlipayTradeWapPayContentBuilder();
        $payRequestBuilder->setTimeExpress($timeout_express);

        $payRequestBuilder->setBody($order['body']);
        $payRequestBuilder->setSubject($order['subject']);
        $payRequestBuilder->setTotalAmount($order['totalFee']/100);
        $payRequestBuilder->setOutTradeNo($order['orderNo']);

        $payResponse = new AlipayTradeService($config);
        $response=$payResponse->wapPay($payRequestBuilder,$config['return_url'],$config['notify_url']);

        return $response;

    }
}