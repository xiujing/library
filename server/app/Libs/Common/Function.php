<?php
/**
 * 所有的公共函数
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-01
 * Time: 14:56
 */

function nonceStr($length = 8)
{
// 密码字符集，可任意添加你需要的字符
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $nonceStr = '';
    for ($i = 0; $i < $length; $i++) {
        $nonceStr .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    return $nonceStr;
}

//将XML转为array
function xmlToArray($xml)
{
    //禁止引用外部xml实体
    libxml_disable_entity_loader(true);
    $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    return $values;
}

/**
 * 获取客户端IP
 * @return array|false|string
 */
function getIp()
{
    if (getenv('HTTP_CLIENT_IP')) {
        $onlineip = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $onlineip = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('REMOTE_ADDR')) {
        $onlineip = getenv('REMOTE_ADDR');
    } else {
        $onlineip = "127.0.0.1";
    }
    return $onlineip;
}

/**
 * 获取订单编号
 * @param $type
 * @return string
 */
function getOrderSn($type)
{
    $orderSn = $type
        . sprintf('%010d', time() - 946656000)
        . sprintf('%03d', (float)microtime() * 1000)
        . mt_rand(10, 99);

    return $orderSn;
}

/**
 * 得到支付宝支付配置
 * @return array
 */
function getAliConfig()
{
    $config = array(
        //应用ID,您的APPID。
        'app_id' => env("ALI_APPID"),

        //商户私钥
        'merchant_private_key' => env("ALI_MERCHANT_PRIVATE_KEY"),
        //异步通知地址
        'notify_url' => env("ALI_NOTIFY_URL"),

        //同步跳转
        'return_url' => env("ALI_RETURN_RUL"),

        //编码格式
        'charset' => env("ALI_CHARSET"),

        //签名方式
        'sign_type' => env("ALI_SIGN_TYPE"),

        //支付宝网关
        'gatewayUrl' => env("ALI_GATEWAYURL"),

        //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
        'alipay_public_key' => env("ALI_PAY_PUBLIC_KEY")
    );
    return $config;
}

/**
 * 获得上个月日期
 * @param $date 2017-09-11
 * @return array
 */
function getlastMonthDays($date)
{
    $timestamp = strtotime($date);
    $firstday = date('Y-m-01', strtotime(date('Y', $timestamp) . '-' . (date('m', $timestamp) - 1) . '-01'));
    $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day"));
    return array($firstday, $lastday);
}

function birthday($birthday)
{
    $age = strtotime($birthday);
    if ($age === false) {
        return false;
    }
    list($y1, $m1, $d1) = explode("-", date("Y-m-d", $age));
    $now = strtotime("now");
    list($y2, $m2, $d2) = explode("-", date("Y-m-d", $now));
    $age = $y2 - $y1;
    if ((int)($m2 . $d2) < (int)($m1 . $d1))
        $age -= 1;
    return $age;
}

/**
 * 数组转对象
 * @param $array
 * @return StdClass
 */
function array2object($array)
{
    if (is_array($array)) {
        $obj = new StdClass();
        foreach ($array as $key => $val) {
            $obj->$key = $val;
        }
    } else {
        $obj = $array;
    }
    return $obj;
}

/**
 * 对象转数组
 * @param $object
 * @return mixed
 */
function object2array($object)
{
    if (is_object($object)) {
        foreach ($object as $key => $value) {
            $array[$key] = $value;
        }
    } else {
        $array = $object;
    }
    return $array;
}

function _ksort($arr, $key, $order = 'desc')
{
    if (!is_array($arr)) return false;

    array_values($arr);
    foreach ($arr as $k => $v) {
        $ks = (string)$v[$key];
        if (isset($array[$ks])) {
            $array[$ks . '.' . $k] = $array[$ks];
            $array[$ks . '.' . ($k + 1)] = $v;
            unset($array[$ks]);
            continue;
        }
        $array[$ks] = $v;
    }
    if ($order == 'asc') {
        ksort($array);
    } elseif ($order == 'desc') {
        krsort($array);
    }

    foreach ($array as $v) {
        $return[] = $v;
    }
    return $return;
}

