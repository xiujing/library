<?php
/**
 * 阿里OSS上传
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-04
 * Time: 10:28
 */
namespace App\Libs;


class httpUtil
{
    /**
     * POST 请求
     * @param string $url
     * @param array $param
     * @return string content
     */
        public function http_post($url,$param,$type=true){

        $oCurl = curl_init();
        if(stripos($url,"https://")!==FALSE){
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
        }
        if($type){
            if (is_string($param)) {
                $strPOST = $param;
            } else {
                $aPOST = array();
                foreach($param as $key=>$val){
                    $aPOST[] = $key."=".urlencode($val);
                }
                $strPOST =  join("&", $aPOST);
            }
        }else{
            $strPOST = json_encode($param);
        }

        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($oCurl, CURLOPT_POST,true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);

        if(intval($aStatus["http_code"])==200){
            return $sContent;
        }else{
            return false;
        }
    }

    /**
     * GET 请求
     * @param string $url
     */
    public function http_get($url){
        $oCurl = curl_init();
        if(stripos($url,"https://")!==FALSE){
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        if(intval($aStatus["http_code"])==200){
            return $sContent;
        }else{
            return false;
        }
    }

    public function postXmlCurl($xml, $url, $useCert = false, $second = 30)
    {
        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_HEADER,0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,1);//证书检查
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'pem');
        curl_setopt($ch,CURLOPT_SSLCERT,"/service/wxapp/groupon/public/cert/apiclient_cert.pem");
        curl_setopt($ch,CURLOPT_SSLCERTPASSWD,env('MCHID'));
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'pem');
        curl_setopt($ch,CURLOPT_SSLKEY,"/service/wxapp/groupon/public/cert/apiclient_key.pem");
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'pem');
        curl_setopt($ch,CURLOPT_CAINFO,"/service/wxapp/groupon/public/cert/rootca.pem");
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$xml);
        $data=curl_exec($ch);
        $error = curl_errno($ch);
        curl_close($ch);
        if($error){
            return $error;
        }else{
            return $data;
        }
    }
}