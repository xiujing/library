<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-09-19
 * Time: 15:38
 */
return [
    'MESSAGE_INSERT_FAILS' =>[
        'errorCode'             => "10015",
        'errorMessage'          =>'留言添加失败'
    ],
    'Usr_ALREADY' =>[
        'errorCode'             => "24009",
        'errorMessage'          =>'用户名已存在'
    ],
    'MESSAGE_INSERT_FAIL'=>[
        'errorCode'             => "000403",
        'errorMessage'          =>'留言添加失败'
    ],
    'USER_NOT_FIND'=>[
        'errorCode'             => "24008",
        'errorMessage'          =>'用户不存在'
    ],
    'PASSWORD_UPDATE_FAILED'=>[
        'errorCode'             => "24007",
        'errorMessage'          =>'与旧密码相同'
    ],
    'USERNAME_ALREADY' =>[
        'errorCode'             => "24007",
        'errorMessage'          =>'用户名已存在'
    ],
    'INSERT_REQ_FAILED' =>[
        'errorCode'             => "000403",
        'errorMessage'          =>'添加失败'
    ],
    'EDI_REQ_FAILED' =>[
        'errorCode'             => "000403",
        'errorMessage'          =>'未更改任何信息'
    ],
    'TIME_REQ_FORBID' =>[
        'errorCode'             => "000403",
        'errorMessage'          =>'距离上次添加时间没满足6小时'
    ],
    'INVALID_REQ_PARAMS' =>[
        'errorCode'             => "000400",
        'errorMessage'          =>'请求参数不合法'
    ],
    'MANAGER_LOGIN_FAILED' =>[
        'errorCode'             => "11007",
        'errorMessage'          =>'用户名或密码错误'
    ],
    'PERMISSION_DENY' =>[
        'errorCode'             => "000403",
        'errorMessage'          =>'禁止访问'
    ],
    'OLD_PASSWORD_FALSE' =>[
        'errorCode'             => "11011",
        'errorMessage'          =>'旧密码输入有误'
    ],
    'ROLE_UPDATE_FAILED' =>[
        'errorCode'             => "11001",
        'errorMessage'          =>'角色信息修改失败'
    ],
    'ROLE_ADD_FAILED' =>[
        'errorCode'             => "11002",
        'errorMessage'          =>'角色增加失败'
    ],
    'ROLE_DELETE_FAILED' =>[
        'errorCode'             => "11003",
        'errorMessage'          =>'角色删除失败，有管理员有此角色'
    ],
    'MANAGER_UPDATE_FAILED' =>[
        'errorCode'             => "11004",
        'errorMessage'          =>'管理员信息修改失败'
    ],
    'MANAGER_ADD_FAILED' =>[
        'errorCode'             => "11005",
        'errorMessage'          =>'管理员增加失败'
    ],
    'MANAGER_EXISTS_ALREADY' =>[
        'errorCode'             => "11006",
        'errorMessage'          =>'管理员登录名占用'
    ],
    'ROLE_NOT_FAILED' =>[
        'errorCode'             => "11012",
        'errorMessage'          =>'角色不存在，请正确填写角色'
    ],
    'CATEGORYNAME_ADD_FAILED' =>[
        'errorCode'             => "12002",
        'errorMessage'          =>'商品类别已经存在'
    ],
    'CATEGORY_ADD_FAILED' =>[
        'errorCode'             => "12001",
        'errorMessage'          =>'商品类别添加失败'
    ],
    'CATEGORY_UPDATE_FAILED' =>[
        'errorCode'             => "12003",
        'errorMessage'          =>'商品类别修改失败'
    ],
    'PRODUCT_ADD_FAILED' =>[
        'errorCode'             => "12004",
        'errorMessage'          =>'产品添加失败'
    ],
    'PRODUCT_UPDATE_FAILED' =>[
        'errorCode'             => "12005",
        'errorMessage'          =>'产品修改失败'
    ],
    'CATEGORY_DELETE_FAILED' =>[
        'errorCode'             => "12007",
        'errorMessage'          =>'商品类别删除失败,有商品属于该类别'
],
    'CATEGORY_NOR_FAILED' =>[
        'errorCode'             => "12008",
        'errorMessage'          =>'商品类别不存在'
    ],
    'PRODUCT_DELETE_FAILED' =>[
        'errorCode'             => "12009",
        'errorMessage'          =>'产品删除失败'
    ],
    'DELIVER_FAILED' =>[
        'errorCode'             => "12010",
        'errorMessage'          =>'发货失败'
    ],
    'COMMENT_REPLAY_FAILED' =>[
        'errorCode'             => "12011",
        'errorMessage'          =>'客服回复失败'
    ],


    'MANAGER_DELETE_ALREADY' =>[
        'errorCode'             => "11014",
        'errorMessage'          =>'管理员不存在'
    ],

     'REFUND_REFUSE_FAILED' =>[
    'errorCode'             => "13001",
    'errorMessage'          =>'退款请求拒绝失败,参数有误'
    ],


    'COURSE_UPDATE_FAILED' =>[
        'errorCode'             => "14000",
        'errorMessage'          =>'更新课程失败'
    ],

    'COURSE_DELETE_FAILED' =>[
        'errorCode'             => "14001",
        'errorMessage'          =>'请勿删除课程'
    ],
    'CONTRACTOR_COURSE__FAILED' =>[
        'errorCode'             => "14002",
        'errorMessage'          =>'审核失败'
    ],
    'CONTRACTOR_COURSE__USER_FAILED' =>[
        'errorCode'             => "14003",
        'errorMessage'          =>'更改跟课员失败'
    ],
    'ACTIVITY_UPDATE_FAILED' =>[
        'errorCode'             => "14004",
        'errorMessage'          =>'更新活动失败'
    ],
    'ACTIVITY_DELETE_FAILED' =>[
        'errorCode'             => "14005",
        'errorMessage'          =>'请勿删除活动'
    ],
    'RECRUIT_UPDATE_FAILED' =>[
        'errorCode'             => "14006",
        'errorMessage'          =>'更新招募失败'
    ],
    'RECRUIT_DELETE_FAILED' =>[
        'errorCode'             => "14007",
        'errorMessage'          =>'请勿删除招募'
    ],
    'UPDATE_FAILED' =>[
        'errorCode'             => "14008",
        'errorMessage'          =>'更新失败'
    ],

    'VALUE_EXIST' =>[
        'errorCode'             => "14009",
        'errorMessage'          =>'已经存在'
    ],
    'SURVER_DELETE_FAILED' =>[
        'errorCode'             => "14010",
        'errorMessage'          =>'请勿删除问卷'
    ],
    'CANCEL_ORDER_FAILED' =>[
        'errorCode'             => "14011",
        'errorMessage'          =>'取消订单失败'
    ],
    'ADDRESS_UPDATE_FAILED' =>[
        'errorCode'             => "21002",
        'errorMessage'          =>'未修改信息'
    ],
    'DEFAULTADDRESS_SET_FAILED' =>[
        'errorCode'             => "21003",
        'errorMessage'          =>'默认收货地址设置失败'
    ],

    'CATE_ADD_FAILED' =>[
        'errorCode'             => "22001",
        'errorMessage'          =>'添加购物车失败'
    ],
    'CATE_EXISTS' =>[
        'errorCode'             => "22002",
        'errorMessage'          =>'购物车已经存在'
    ],
    'CATE_DELETE_FAILED' =>[
        'errorCode'             => "22003",
        'errorMessage'          =>'删除购物车失败'
    ],
    'REGIST_FAILED' =>[
        'errorCode'             => "24003",
        'errorMessage'          =>'注册失败'
    ],
    'NAME_PASSWORD_NOT_MATCH' =>[
        'errorCode'             => "24006",
        'errorMessage'          =>'用户名或密码错误'
    ],
    'PHONENUMBER_EXISTA' =>[
        'errorCode'             => "24007",
        'errorMessage'          =>'手机号码已经注册过，不可重复注册'
    ],
    'CONTRACTOR_EXISTA' =>[
        'errorCode'             => "24008",
        'errorMessage'          =>'机构全称已经注册过，不可重复注册'
    ],
    'ORDER_PAY_FAILED' =>[
        'errorCode'             => "23002",
        'errorMessage'          =>'订单支付失败'
    ],
    'ORDER_DELIVER_FAILED' =>[
        'errorCode'             => "23002",
        'errorMessage'          =>'确认收货失败'
    ],

    'INVALID_CODE_PARAMS' =>[
        'errorCode'             => "30000",
        'errorMessage'          =>'code不合法'
    ],
    'USER_NO_USER' =>[
        'errorCode'             => "30001",
        'errorMessage'          =>'用户已经被禁用'
    ],
    'CARD_NO' =>[
        'errorCode'             => "30002",
        'errorMessage'          =>'照片模糊或不端正，请重新上传'
    ],
    'CARD_ERROR' =>[
        'errorCode'             => "30003",
        'errorMessage'          =>'身份证号错误'
    ],

    'CARD_EXIST' =>[
        'errorCode'             => "30004",
        'errorMessage'          =>'身份证号已经存在，请重新上传'
    ],

    'CODE_ERROR' =>[
        'errorCode'             => "30004",
        'errorMessage'          =>'验证码错误'
    ],
    'CODE_FAILS' =>[
        'errorCode'             => "30005",
        'errorMessage'          =>'获取验证码失败'
    ],
    'CERTIFICATES_FAILS' =>[
        'errorCode'             => "30006",
        'errorMessage'          =>'证件号错误'
    ],
    'ORDER_FAILS' =>[
        'errorCode'             => "30007",
        'errorMessage'          =>'订单已经存在'
    ],
    'ORDER_USER_LIMIT' =>[
        'errorCode'             => "30033",
        'errorMessage'          =>'你已超过报名人数限制'
    ],

    'NAME_ERROR' =>[
        'errorCode'             => "30034",
        'errorMessage'          =>'姓名必须与身份证姓名一致'
    ],
    'AGE_ERROR' =>[
        'errorCode'             => "30035",
        'errorMessage'          =>'不在规定的年龄范围内'
    ],
    'RECRUIT_REPLY_FAILS' =>[
        'errorCode'             => "30008",
        'errorMessage'          =>'请勿重复申请'
    ],
    'RECRUIT_REPLY_ERROR' =>[
        'errorCode'             => "30009",
        'errorMessage'          =>'招募申请失败'
    ],
    'REFUND_REPLY_FAILS' =>[
        'errorCode'             => "30010",
        'errorMessage'          =>'退款申请失败'
    ],
    'CONSULTATION_FAILS' =>[
        'errorCode'             => "30011",
        'errorMessage'          =>'咨询失败'
    ],
    'MODIFY_PWD_FAILS' =>[
        'errorCode'             => "30012",
        'errorMessage'          =>'修改密码失败'
    ],
    'PHONE_SAME' =>[
        'errorCode'             => "30013",
        'errorMessage'          =>'手机号相同'
    ],
    'ORDER_ERROR' =>[
        'errorCode'             => "30014",
        'errorMessage'          =>'订单错误'
    ],
    'ORDER_NOT_BELONG' =>[
        'errorCode'             => "30015",
        'errorMessage'          =>'订单不属于该承办方'
    ],
    'ORDER_NOT_TIME' =>[
        'errorCode'             => "30016",
        'errorMessage'          =>'订单不在时间范围内'
    ],
    'ORDER_END' =>[
        'errorCode'             => "30016",
        'errorMessage'          =>'课程已结束'
    ],
    'ORDER_FINISH' =>[
        'errorCode'             => "30016",
        'errorMessage'          =>'签到完成'
    ],

    'CONTRACTOR_ERROR' =>[
        'errorCode'             => "30017",
        'errorMessage'          =>'该承办方不是主账号'
    ],
    'CONTRACTOR_DATA_FAILS' =>[
        'errorCode'             => "30018",
        'errorMessage'          =>'承办方修改资料失败'
    ],
    'CONTRACTOR_COURSE_FAILS' =>[
        'errorCode'             => "30019",
        'errorMessage'          =>'承办方承办课程失败'
    ],
    'CONTRACTOR_COURSE_NUM_ERROR' =>[
        'errorCode'             => "30020",
        'errorMessage'          =>'承办方承办课程核心团队人数必须大于3'
    ],
    'CONTRACTOR_COURSEINT' =>[
        'errorCode'             => "30021",
        'errorMessage'          =>'此课程已在承办中，请勿重复承办'
    ],
    'SURVER_INFO_EXIST' =>[
        'errorCode'             => "30022",
        'errorMessage'          =>'此问卷已经提交，请勿重复'
    ],
    'SURVER_INFO_FAILS' =>[
        'errorCode'             => "30023",
        'errorMessage'          =>'提交问卷失败'
    ],
    'COURSE_NUM_FAILS' =>[
        'errorCode'             => "30024",
        'errorMessage'          =>'报名人数已满'
    ],

    'USER_NOT_USER' =>[
        'errorCode'             => "30025",
        'errorMessage'          =>'用户不存在'
    ],
    'SIGN_UP_NOT' =>[
        'errorCode'             => "30026",
        'errorMessage'          =>'报名截止'
    ],
    'CONTRACTOR_EXIST' =>[
        'errorCode'             => "30027",
        'errorMessage'          =>'承办方已经存在'
    ],

    'UPLOAD_FILE_ERROR' =>[
        'errorCode'             => "50000",
        'errorMessage'          =>'上传文件失败'
    ],

    "UPDATE_COURSE_ERROR"=>[
        'errorCode'             => "40000",
        'errorMessage'          =>'已发布的课程，请勿修改'
    ],
    "NOT_DELETE"=>[
        'errorCode'             => "40000",
        'errorMessage'          =>'请勿删除'
    ],
    "PROHIBIT_USER"=>[
        'errorCode'             => "10000",
        'errorMessage'          =>'禁止进入'
    ],
    "CONTRACTOR_PROHIBIT"=>[
        'errorCode'             => "10000",
        'errorMessage'          =>'正在审核资料'
    ],
];