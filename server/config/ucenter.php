<?php

/**
 * 为了方便，直接修改以下带【*】的配置即可
 */

return [
    'url'            => env('UC_URL', '/api/ucapi'),  //这里是你的项目所在的接口api的前缀，比如 /xx/api/uc 一般直接留空。
    'connect'        => env('UC_CONNECT', 'mysql'), //这里可以是 mysql或者null，null会通过socket远程请求接口的方式通信
    'dbhost'         => env('UC_DBHOST', '120.25.68.86:16810'),
    'dbuser'         => env('UC_DBUSER', 'root'),
    'dbpw'           => env('UC_DBPW', 'win2Tech30@SZ#2015$'),
    'dbname'         => env('UC_DBNAME', 'ailemao'),
    'dbcharset'      => env('UC_DBCHARSET', 'utf8'),
    'dbtablepre'     => env('UC_DBTABLEPRE', '`ailemao`.uc_'),
    'dbconnect'      => env('UC_DBCONNECT', '0'),
    //这个是通信密钥，必须和服务端统一【*】
    'key'            => env('UC_KEY', 'S0o0l2peT50d76vej7j2TdP6ebl1vdJfJ2JfQdI7q5p0z62ed0i2CcueQ5Iby123'),
    //这个是uc_server的服务端地址【*】
    'api'            => env('UC_API', 'http://ailemao-dz.30days-tech.com/uc_server'),
    //这个是uc_server的服务端地址【*】
    'ip'             => env('UC_IP', '120.25.68.86:16810'),
    'charset'        => env('UC_CHARSET', 'utf-8'),
    //这里是应用编号
    'appid'          => env('UC_APPID', '5'),
    'ppp'            => env('UC_PPP', '20'),
    //这里是uc_server调用你的程序的接口，配置成uc的话，将会和前面的UC_URL配置一起形成这样的地址 url/api/uc
    'apifilename'    => env('UC_APIFILENAME', 'uc'),
    //这里如果要异步登陆，可以直接继承这个类实现其中的方法，也可以创建app/Service/Ucenter.php(文件放哪里都可以，这里只是推荐) 实现该类实现的接口【*】
    'service'        => env('UC_SERVICE', 'Noxue\Ucenter\Services\Api'),
];
