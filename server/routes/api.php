<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {

});
Route::post('test', "Api\Common\CommonController@ossUpload"); //通用上传文件

//通用路由设置
Route::group(['prefix' => 'common', 'namespace' => 'Api\Common'], function () {
    Route::post('upload', "CommonController@ossUpload"); //通用上传文件
    Route::get('kuaidi', "CommonController@kuaidi"); //查询物流
    Route::post('code', "CommonController@code"); //短信验证码
    Route::get('cards', "CommonController@idcard"); //身份证识别
});
//word
Route::group(['prefix' => 'v1/word', 'namespace' => 'Api\V1\Manager\Word'], function () {


});
//后台用户模块
Route::group(['prefix' => 'v1/manager/user', 'namespace' => 'Api\V1\Manager\User'], function () {
    Route::post('login', "LoginController@login"); //管理员登陆
});

//后台用户模块  需要登录的操作
Route::group(['prefix' => 'v1/manager/user', 'middleware' => ['AdminLogin'], 'namespace' => 'Api\V1\Manager\User'], function () {
    Route::put('password', "LoginController@updatePassword"); //管理员修改密码
    Route::resource('role', "RoleController"); //角色管理
    Route::resource('account', "ManagerController"); //管理员管理
    Route::put('states/{managerId}', "ManagerController@updateStates"); //管理员管理---修改管理员状态
    Route::get('log', "ManagerController@Log"); //操作日志
});
Route::group(['prefix' => 'v1/manager', 'middleware' => ['AdminLogin'], 'namespace' => 'Api\V1\Manager\User'], function () {
    Route::put('/roles/{roleId}/resources/assign', "RoleController@resourcesAssign"); //分配资源
});


//后台商城模块
Route::group(['prefix' => 'v1/manager/mall', 'middleware' => ['AdminLogin'], 'namespace' => 'Api\V1\Manager\Mall'], function () {
    Route::resource('category', "CategoryController"); //商品分类列表
    Route::resource('commodity', "CommodityController"); //商品
    Route::put('commodity/onsale/{commodityId}', "CommodityController@updateSale"); //商品上下架
    Route::resource('order', "OrderController"); //订单管理
    Route::post('dilive', "OrderController@deliver"); //订单发货
    Route::post('comment/reply', "CommentController@commentReply"); //-商品评价---回复
    Route::resource('refund', "RefundController"); //退款管理
    Route::post('/refund/refuse', "RefundController@refundRefuse"); //拒绝退款
    Route::put('/refund/agree/{refundId}', "RefundController@refundAgree"); //同意退款
    Route::put('/refund/deliver/{refundId}', "RefundController@refundDeliver"); //退货---确认到货  直接退款
});

//后台系统模块
Route::group(['prefix' => 'v1/manager', 'middleware' => ['AdminLogin'], 'namespace' => 'Api\V1\Manager\System'], function () {


    Route::resource('notice', "NoticeController"); //公告
    Route::resource('banner', "IndexController"); //banner


});
Route::group(['prefix' => 'v1/manager', 'namespace' => 'Api\V1\Manager\System'], function () {

    Route::resource('setting', "SettingController"); //通用设置

});

//支付回调路由
Route::group(['prefix' => '/v1/server/pay/callback', 'namespace' => 'Api\V1\Manager\Mall'], function () {
    Route::post('wx', "PayCallbackController@wxCallback"); //微信支付回调
    Route::post('ali', "PayCallbackController@aliCallback"); //支付宝支付回调
});


//前台商城模块
Route::group(['prefix' => 'v1/front/mall', 'namespace' => 'Api\V1\Front\Mall'], function () {
    Route::post('/order', "OrderController@order"); //下订单
    Route::delete('/order/{orderId}',"OrderController@destroy");
    Route::get('/order/{orderNo}',"OrderController@show");
    Route::resource('address', "AddressController"); //收货地址管理
    Route::resource('cart', "CartController"); //购物车管理
    Route::post('cart/moreDestroy', "CartController@moreDestroy");
    Route::get('/order', "OrderController@orderList"); //我的订单
    Route::post('/order/refund', "RefundController@applyRefund"); //申请退款
    Route::get('/deliver/{deliverCode}/{deliverNo}', "DeliverController@getDeliver"); //查询快递信息
    Route::put('/order/{orderNo}', "OrderController@update"); //确认收货
    Route::post('/order/comment', "OrderCommentController@store"); //评价商品
    Route::put('/refund/{refundId}', "RefundController@cancelRefund"); //撤销退款请求
    Route::post('/refund/deliver', "RefundController@updateDeliver"); //退货物流
});


//前台用户模块
Route::group(['prefix' => 'v1/front/user', 'namespace' => 'Api\V1\Front\User'], function () {
    Route::post('/login', "UserController@login");//用户登录
    Route::post('/regist', "UserController@register"); //用户注册
    Route::post('/find', "UserController@findPassword"); //找回密码
    Route::post('/privilege', "ThreeUserController@privilege"); //第三方登陆
});





//后台用户模块  需要登录的操作
Route::group(['prefix' => 'v1/manager/user', 'namespace' => 'Api\V1\Manager\User'], function () {
    Route::post('token', ['as' => 'authorizations.token', 'uses' => 'AuthController@token']); //获取token
});


//JWT认证
Route::group(['prefix' => 'v1/manager/user', 'middleware' => ['auth:api'], 'namespace' => 'Api\V1\Manager\User'], function () {
    Route::post('get/user', 'AuthController@getUser');
});

//JWT刷新
Route::group(['prefix' => 'v1/manager/user', 'namespace' => 'Api\V1\Manager\User'], function () {
    Route::post('refresh/token', 'AuthController@refreshToken');
});


//微信接口
Route::group(['prefix' => 'v1/front/wx', 'namespace' => 'Api\V1\Front\Wx'], function () {
    Route::get('user', 'WxController@getUserInfoByCode'); //通过code获取用户详情
    Route::get('config', 'WxController@getWxConfig'); //微信JS权限配置
    Route::get('sandbox', 'SandboxController@GetKey');  //获取沙箱秘钥
    Route::get('sandbox/pay', 'SandboxController@pay'); //1003用例测试----统一下单
    Route::post('sandbox/callback', 'SandboxController@callback');  //1003用例测试回调
    Route::get('sandbox/query', 'SandboxController@query');  //查询订单
    //1004用例测试
    //1、进行5.52的支付。2、查询订单。3、退款。4、退款查询
    Route::get('sandbox/refund', 'SandboxController@refund'); //1004用例退款
    Route::get('sandbox/refundquery', 'SandboxController@RefundQuery'); //1004用例退款查询

    //1005用例--交易对账单下载
    Route::get('sandbox/downloadbill', 'SandboxController@downloadbill');//交易对账单下载


    Route::get('login', 'WxController@login');   //微信小程序登录
    Route::get('register', 'WxController@register');   //微信小程序注册

    Route::get('pushmessage', 'WxController@PushMessage'); //小程序推送消息
});

Route::group(['prefix' => 'v1/front/user', 'namespace' => 'Api\V1\Front\User'], function () {
    Route::post('bind', 'UserController@bind');
});

Route::group(['prefix' => 'v1/front', 'middleware' => ['UserLogin'], 'namespace' => 'Api\V1\Front'], function () {

    Route::get('banner', 'SettingController@banner');//获取banner
    /**
     * 公告
     */
    Route::resource('notice', 'NoticeController'); // 公告sss
});


Route::group(['prefix' => 'web', 'namespace' => 'Api\Web'], function () {
    Route::get('index', 'IndexController@index');//首页
    Route::get('about', 'AboutController@index');//关于我们
    Route::get('news', 'NewsController@index');//xinwen
    Route::get('news/{newsId}', 'NewsController@show');//xinwen
    Route::post('contact','ContactController@store');//联系我们在线留言
    Route::get('box/list','CommodityController@index');//盒子列表//四驱车
    Route::get('box/detail/{commodityId}','CommodityController@show');//盒子详情
    Route::post('commodity','CommodityController@store');//盒子详情


});
Route::group(['prefix' => 'backstage', 'middleware' => ['AdminLogin'], 'namespace' => 'Api\Backstage'], function () {
    Route::resource('teacher', "TeacherController"); //老师
    Route::resource('news', "NewsController"); //新闻

    Route::resource('questions', "QuestionsController"); //常见问题

    Route::resource('about', "AboutController"); //关于我们
    Route::resource('subscribe', "SubscribeController"); //预定留言
    Route::resource('online', "OnlineMessageController"); //在线留言
    Route::put('commodity/{commodityId}',"CommodityController@updateStatus");//盒子管理

    Route::resource('commodity',"CommodityController");//盒子管理

});
Route::group(['prefix' => 'v1/front/user','namespace' => 'Api\V1\Front\User'], function () {
    Route::resource('user','UserController');//用户
    Route::get('user/detail','UserController@userDetail');//后台用户详情
});
Route::post('ucenter/check','Api\V1\Front\User\UserController@check');//论坛登录验证