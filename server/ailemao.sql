/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : ailemao

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-12-21 18:46:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `about`
-- ----------------------------
DROP TABLE IF EXISTS `about`;
CREATE TABLE `about` (
  `aboutId` int(11) NOT NULL AUTO_INCREMENT COMMENT '关于我们',
  `companySyn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '公司简介',
  `idea` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '教育理念',
  PRIMARY KEY (`aboutId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of about
-- ----------------------------
INSERT INTO `about` VALUES ('1', '深圳市普赢创新科技股份有限公司成立于2012年，\r\n爱乐猫是普赢创新新型Steam创客教育品牌，针对3-12岁儿童和青少年；\r\n提供动手科学产品及趣味性的课程学习，课程覆盖物理、化学、技术、工程、数学、艺术、生物、天文等领域。', '爱乐猫实地走访了美国、香港、荷兰及国内一线城市的相关品牌进行了学术交流，引入国际最先进的STEAM+HS教育体系，通过提炼整合科学（Science）、技术（Technology）、工程（Engineering）、艺术（Art）、数学（Mathematics）五大领域的基础知识，引导兴趣，感知和探索，辅以奖赏性激励，让孩子在身心发育早期，就能感受到科学的魅力，感知学习的快乐。\r\n\r\n在以上五大领域基础上，又增加了人文（Humanity）社会（Society）领域，更符合中国孩子的需求。同时也更进一步完善了知');

-- ----------------------------
-- Table structure for `boxgroup`
-- ----------------------------
DROP TABLE IF EXISTS `boxgroup`;
CREATE TABLE `boxgroup` (
  `boxgroupId` int(11) NOT NULL AUTO_INCREMENT COMMENT '盒子套餐ID',
  `caption` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '盒子套餐标题',
  `price` double NOT NULL COMMENT '价格',
  `commodityId` int(11) NOT NULL COMMENT '套餐对应的盒子ID',
  PRIMARY KEY (`boxgroupId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of boxgroup
-- ----------------------------

-- ----------------------------
-- Table structure for `box_message`
-- ----------------------------
DROP TABLE IF EXISTS `box_message`;
CREATE TABLE `box_message` (
  `boxMessageId` int(11) NOT NULL AUTO_INCREMENT COMMENT '订购盒子留言ID',
  `name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '您的姓名',
  `contactTel` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '联系电话',
  `buyTheme` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '订购主题',
  `content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '留言内容',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`boxMessageId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of box_message
-- ----------------------------
INSERT INTO `box_message` VALUES ('1', '刘利达', '13111182325', '订购主题', '我想买东西', '2018-12-21 16:01:40');

-- ----------------------------
-- Table structure for `commodity`
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity` (
  `commodityId` int(11) NOT NULL AUTO_INCREMENT COMMENT '盒子ID',
  `caption` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '盒子标题',
  `originalPrice` double NOT NULL COMMENT '原价',
  `nowPrice` double NOT NULL COMMENT '现价',
  `content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '摘要',
  `bgPicture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '背景图片',
  `hsaPicture` varchar(1000) COLLATE utf8_bin NOT NULL COMMENT '型号图片(多图片用；隔开)',
  `boxVideo` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '盒子介绍视频',
  `boxContent` longtext COLLATE utf8_bin NOT NULL COMMENT '盒子介绍',
  `hasVideo` varchar(1000) COLLATE utf8_bin NOT NULL COMMENT '盒子内所含有的实验视频（多视频用;隔开）',
  `recommend` tinyint(1) DEFAULT '0' COMMENT '推荐（全表只能有一个为1,0为不推荐，1为推荐）',
  `recommendFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '本期盒子第一标题',
  `recommendSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '本期盒子第二标题',
  `recommendThird` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '本期盒子第三标题',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`commodityId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of commodity
-- ----------------------------
INSERT INTO `commodity` VALUES ('1', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '1', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 14:50:28');
INSERT INTO `commodity` VALUES ('3', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:00:59');
INSERT INTO `commodity` VALUES ('4', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:01:03');
INSERT INTO `commodity` VALUES ('5', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:01:05');
INSERT INTO `commodity` VALUES ('6', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:01:07');
INSERT INTO `commodity` VALUES ('7', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:01:09');
INSERT INTO `commodity` VALUES ('8', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:01:11');
INSERT INTO `commodity` VALUES ('9', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'https://www.belledu.com/Uploads/video/Ubot.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:01:12');
INSERT INTO `commodity` VALUES ('10', '【本期盒子】199四大主题科学盒子 免费快递到家', '450', '199', '主题盒子包含神奇的不倒翁、小小建筑师、海洋精灵、魅力四射之扎染四大主题，涉及工程、物理、空间结构、色彩搭配、图形对称性等丰富知识点。通过丰富的主题和课程内容，培养孩子耐力、机械性记忆力、逻辑思维能力和文化吸收能力，以进阶的方式一步步引导孩子们，推开科学之门。', 'http://taiyuan.30days-tech.com/ailemao/images/box3bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png;http://taiyuan.30days-tech.com/ailemao/images/detail.png', 'video.mp4', '', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png;http://taiyuan.30days-tech.com/ailemao/images/maker14.png', '0', '四大主题科学盒子', '免费快递到家', '购买四节课免费邮寄一个盒子包含四个主题', '2018-12-21 15:01:15');

-- ----------------------------
-- Table structure for `course`
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `courseId` int(11) NOT NULL AUTO_INCREMENT,
  `picture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '课程图片',
  `code` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '二维码',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`courseId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', 'http://taiyuan.30days-tech.com/ailemao/images/maker11.png', 'http://taiyuan.30days-tech.com/ailemao/images/code1.png', '2018-12-21 17:08:00');
INSERT INTO `course` VALUES ('2', 'http://taiyuan.30days-tech.com/ailemao/images/maker12.png', 'http://taiyuan.30days-tech.com/ailemao/images/code1.png', '2018-12-21 17:08:10');
INSERT INTO `course` VALUES ('3', 'http://taiyuan.30days-tech.com/ailemao/images/maker13.png', 'http://taiyuan.30days-tech.com/ailemao/images/code1.png', '2018-12-21 17:08:20');
INSERT INTO `course` VALUES ('4', 'http://taiyuan.30days-tech.com/ailemao/images/maker14.png', 'http://taiyuan.30days-tech.com/ailemao/images/code1.png', '2018-12-21 17:08:32');

-- ----------------------------
-- Table structure for `feedback`
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `feedbackId` int(11) NOT NULL AUTO_INCREMENT COMMENT '反馈ID',
  `name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '孩子姓名',
  `isParent` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '关系',
  `content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '反馈内容',
  `avatar` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '照片',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`feedbackId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of feedback
-- ----------------------------
INSERT INTO `feedback` VALUES ('1', '陈*渊', null, '我家孩子报了爱乐猫的《编程课》和《益智模型课课》，上了半年的课，能够发现他很大的进步，在爱乐猫跟着老师参加“全国车辆模型公开赛”还获得了三等奖，真的挺惊喜。\r\n\r\n在爱乐猫，孩子能够主动且快乐的学习，不是硬性的灌输， 而是由浅入深的引导孩子在学习，在编程学习的过程中，能够感受到孩子的创造力和探索能力被激发出来了。很感谢爱乐猫！', 'http://taiyuan.30days-tech.com/ailemao/images/index91.png', '2018-12-21 09:50:28');
INSERT INTO `feedback` VALUES ('2', '李*楷', null, '我家孩子之前在家就喜欢自己动手做一些手工，养一些小植物，所以看到爱乐猫的《科学课》和《创客设计课》，我立马就给她报名了，她非常开心！\r\n\r\n现在的小孩子都喜欢轻松的情境下去摄取各种信息和知识。爱乐猫正是了解孩子的这个特质，通过游戏的形式，学习科学知识，锻炼孩子各种能力：手眼协调能力，专注力，以及抗挫折能力。', 'http://taiyuan.30days-tech.com/ailemao/images/index92.png', '2018-12-21 09:51:48');
INSERT INTO `feedback` VALUES ('3', '陈*渊', '妈妈', '我家孩子报了爱乐猫的《编程课》和《益智模型课课》，上了半年的课，能够发现他很大的进步，在爱乐猫跟着老师参加“全国车辆模型公开赛”还获得了三等奖，真的挺惊喜。\r\n\r\n在爱乐猫，孩子能够主动且快乐的学习，不是硬性的灌输， 而是由浅入深的引导孩子在学习，在编程学习的过程中，能够感受到孩子的创造力和探索能力被激发出来了。很感谢爱乐猫！', 'http://taiyuan.30days-tech.com/ailemao/images/index91.png', '2018-12-21 09:53:31');
INSERT INTO `feedback` VALUES ('4', '李*楷', '爸爸', '我家孩子之前在家就喜欢自己动手做一些手工，养一些小植物，所以看到爱乐猫的《科学课》和《创客设计课》，我立马就给她报名了，她非常开心！\r\n\r\n现在的小孩子都喜欢轻松的情境下去摄取各种信息和知识。爱乐猫正是了解孩子的这个特质，通过游戏的形式，学习科学知识，锻炼孩子各种能力：手眼协调能力，专注力，以及抗挫折能力。', 'http://taiyuan.30days-tech.com/ailemao/images/index92.png', '2018-12-21 09:54:19');

-- ----------------------------
-- Table structure for `indexproduct`
-- ----------------------------
DROP TABLE IF EXISTS `indexproduct`;
CREATE TABLE `indexproduct` (
  `productId` int(11) NOT NULL AUTO_INCREMENT COMMENT '爱乐猫产品ID',
  `caption` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '摘要',
  `picture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '图片',
  `link` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '链接',
  `smallPicture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '小图片',
  PRIMARY KEY (`productId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of indexproduct
-- ----------------------------
INSERT INTO `indexproduct` VALUES ('1', '科学盒子', '“科学盒子”面向家庭提供的3-12岁儿童创客盒子，每月一个主题，免费邮寄到家。', 'http://taiyuan.30days-tech.com/ailemao/images/index21bg.png', 'http://taiyuan.30days-tech.com/ailemao/box.html', 'http://taiyuan.30days-tech.com/ailemao/images/index21.png');
INSERT INTO `indexproduct` VALUES ('2', '科学实验室', '科学盒子”面向家庭提供的3-12岁儿童创客盒子，每月一个主题，免费邮寄到家。', 'http://taiyuan.30days-tech.com/ailemao/images/index22bg.png', 'http://taiyuan.30days-tech.com/ailemao/labOne.html', 'http://taiyuan.30days-tech.com/ailemao/images/index22.png');
INSERT INTO `indexproduct` VALUES ('3', '创客课程', '“科学盒子”面向家庭提供的3-12岁儿童创客盒子，每月一个主题，免费邮寄到家。', 'http://taiyuan.30days-tech.com/ailemao/images/index23bg.png', 'http://taiyuan.30days-tech.com/ailemao/maker.html', 'http://taiyuan.30days-tech.com/ailemao/images/index23.png');

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `newsId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '新闻ID',
  `caption` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '标题',
  `picture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '图片',
  `time` date NOT NULL COMMENT '日期',
  `tag` varchar(11) COLLATE utf8_bin DEFAULT NULL COMMENT '新闻标签',
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '摘要',
  PRIMARY KEY (`newsId`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('2', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('3', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('1', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('4', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('5', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('6', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('7', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('8', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('9', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('10', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('11', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('12', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('13', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('14', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('15', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('16', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('17', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('18', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('19', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('20', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('21', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('22', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('23', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('24', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');
INSERT INTO `news` VALUES ('25', '终于盼到你~2018FIRST机器人挑战赛...', 'http://taiyuan.30days-tech.com/ailemao/images/index101.png', '2018-12-21', '赛事新闻', '北京时间2018年1月6日23点30分，由FIRST中国组委会举办的2018机器人挑战赛主题启动会在上海FIRST中国组委会举行');

-- ----------------------------
-- Table structure for `onlinemessage`
-- ----------------------------
DROP TABLE IF EXISTS `onlinemessage`;
CREATE TABLE `onlinemessage` (
  `onlineMessageId` int(11) NOT NULL AUTO_INCREMENT COMMENT '在线留言ID',
  `name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `tel` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '手机号',
  `email` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  `content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '留言内容',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`onlineMessageId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of onlinemessage
-- ----------------------------
INSERT INTO `onlinemessage` VALUES ('1', '刘利达', '13111182325', 'damonlld@126.com', '我就是个留言', '2018-12-21 17:49:56');

-- ----------------------------
-- Table structure for `questions_answers`
-- ----------------------------
DROP TABLE IF EXISTS `questions_answers`;
CREATE TABLE `questions_answers` (
  `qaId` int(11) NOT NULL AUTO_INCREMENT COMMENT '问答ID',
  `question` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '问题',
  `answers` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '答案',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`qaId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of questions_answers
-- ----------------------------
INSERT INTO `questions_answers` VALUES ('1', '多大的年龄适合爱乐猫科学盒子？', '3-12岁孩子，都能在爱乐猫科学盒子中得到乐趣和益处。自己动手、发挥创造、科学实验，每个孩子都会很喜欢的。', '2018-12-21 15:06:36');
INSERT INTO `questions_answers` VALUES ('2', '一般什么时候发货？', '爱乐猫科学盒子固定每月20日发货，默认百世快递。（待定）', '2018-12-21 15:06:48');
INSERT INTO `questions_answers` VALUES ('3', '爱乐猫科学盒子跟巧虎有什么区别？', '巧虎重在生活习惯、社会认知，而爱乐猫科学盒子主题是手工、科学，专业培养孩子动手能力、专注力、创造力。', '2018-12-21 15:07:00');
INSERT INTO `questions_answers` VALUES ('4', '可以购买一期先试玩下吗？', '可以的。您可以先选择一期您比较感兴趣的主题试用，之后再决定是否长期订购。但我们更鼓励1年左右系统性的学习，会让孩子有更多成长和收获。', '2018-12-21 15:07:13');
INSERT INTO `questions_answers` VALUES ('5', '家长自己不是很懂，怎么教小朋友？', '绘本和视频中都包含有制作过程的介绍，同时还有更多科学方面的趣味知识，家长可以和孩子一起观看、学习、制作。', '2018-12-21 15:07:23');
INSERT INTO `questions_answers` VALUES ('6', '材料包是否安全？？', '我们严格甄选环保材料，且都是经过认证的产品，让孩子玩得开心，家长放心。', '2018-12-21 15:07:43');

-- ----------------------------
-- Table structure for `review`
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `reviewId` int(11) NOT NULL AUTO_INCREMENT COMMENT ' 往期回顾ID',
  `picture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '图片',
  `link` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '链接',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`reviewId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of review
-- ----------------------------
INSERT INTO `review` VALUES ('1', 'http://taiyuan.30days-tech.com/ailemao/images/lab141.jpg', 'link', '2018-12-21 17:05:58');
INSERT INTO `review` VALUES ('2', 'http://taiyuan.30days-tech.com/ailemao/images/lab141.jpg', 'link', '2018-12-21 17:06:14');
INSERT INTO `review` VALUES ('3', 'http://taiyuan.30days-tech.com/ailemao/images/lab141.jpg', 'link', '2018-12-21 17:06:25');

-- ----------------------------
-- Table structure for `science`
-- ----------------------------
DROP TABLE IF EXISTS `science`;
CREATE TABLE `science` (
  `scienceId` int(11) NOT NULL AUTO_INCREMENT COMMENT '科学盒子ID',
  `caption` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '标题',
  `content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '摘要',
  `bgPicture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '背景图片',
  `smallPicture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '小图片',
  `commodityId` int(11) DEFAULT NULL COMMENT '对应的盒子ID(如果没有那么为 更多页面 使用Link)',
  `link` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '多更页面的跳转链接',
  PRIMARY KEY (`scienceId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of science
-- ----------------------------
INSERT INTO `science` VALUES ('1', '做科学实验', '“科学盒子”面向家庭提供的3-12岁儿童创客盒子，每月一个主题，免费邮寄到家。', 'http://taiyuan.30days-tech.com/ailemao/images/index21bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/boxIcon41.png', '1', null);
INSERT INTO `science` VALUES ('2', '读有趣绘本', '“科学盒子”面向家庭提供的3-12岁儿童创客盒子，每月一个主题，免费邮寄到家。', 'http://taiyuan.30days-tech.com/ailemao/images/index22bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/boxIcon42.png', '2', null);
INSERT INTO `science` VALUES ('3', '看指导视频', '“科学盒子”面向家庭提供的3-12岁儿童创客盒子，每月一个主题，免费邮寄到家。', 'http://taiyuan.30days-tech.com/ailemao/images/index43bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/boxIcon43.png', '3', null);
INSERT INTO `science` VALUES ('4', '更多盒子', '“科学盒子”面向家庭提供的3-12岁儿童创客盒子，每月一个主题，免费邮寄到家。', 'http://taiyuan.30days-tech.com/ailemao/images/index44bg.png', 'http://taiyuan.30days-tech.com/ailemao/images/boxIcon44.png', null, 'http://taiyuan.30days-tech.com/ailemao/four-wheel.html');

-- ----------------------------
-- Table structure for `subscribemessage`
-- ----------------------------
DROP TABLE IF EXISTS `subscribemessage`;
CREATE TABLE `subscribemessage` (
  `subscribeId` int(11) NOT NULL AUTO_INCREMENT COMMENT '预约课程ID',
  `childrenName` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '孩子姓名',
  `childrenAge` int(11) NOT NULL COMMENT '孩子年龄',
  `contactTel` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '联系电话',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`subscribeId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of subscribemessage
-- ----------------------------
INSERT INTO `subscribemessage` VALUES ('1', '龟龟', '11', '13111182325', '2018-12-21 16:34:22');

-- ----------------------------
-- Table structure for `swcamp`
-- ----------------------------
DROP TABLE IF EXISTS `swcamp`;
CREATE TABLE `swcamp` (
  `swcampId` int(11) NOT NULL AUTO_INCREMENT COMMENT '夏令营回顾ID',
  `picture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '图片',
  `llink` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '链接',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`swcampId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of swcamp
-- ----------------------------
INSERT INTO `swcamp` VALUES ('1', 'http://taiyuan.30days-tech.com/ailemao/images/maker31.jpg', 'link1', '2018-12-21 17:09:00');
INSERT INTO `swcamp` VALUES ('2', 'http://taiyuan.30days-tech.com/ailemao/images/maker31.jpg', 'link2', '2018-12-21 17:09:13');
INSERT INTO `swcamp` VALUES ('3', 'http://taiyuan.30days-tech.com/ailemao/images/maker31.jpg', 'link3', '2018-12-21 17:09:23');

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `teacherId` int(11) NOT NULL AUTO_INCREMENT COMMENT '教师ID',
  `name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '老师姓名',
  `subhead` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '老师职称',
  `content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '老师介绍',
  `picture` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '老师图片',
  `recommend` tinyint(1) DEFAULT '0' COMMENT '推荐(0代表不推荐1代表推荐)',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`teacherId`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('1', '吴启丽', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher1.png', '0', '2018-12-21 09:37:19');
INSERT INTO `teacher` VALUES ('2', '江北', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher2.png', '0', '2018-12-21 09:38:06');
INSERT INTO `teacher` VALUES ('3', '叶文宇', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher3.png', '0', '2018-12-21 09:39:00');
INSERT INTO `teacher` VALUES ('4', '赵瑾', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher4.png', '0', '2018-12-21 09:39:33');
INSERT INTO `teacher` VALUES ('5', '巫婷', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher5.png', '0', '2018-12-21 09:40:12');
INSERT INTO `teacher` VALUES ('6', '陈凌萍', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher6.png', '0', '2018-12-21 09:40:51');
INSERT INTO `teacher` VALUES ('7', '刘江峰', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher1.png', '0', '2018-12-21 09:41:29');
INSERT INTO `teacher` VALUES ('8', '易聃', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher2.png', '0', '2018-12-21 09:42:54');
INSERT INTO `teacher` VALUES ('9', '徐翠霞', '爱乐猫课程研发老师', '爱乐猫一线讲师，毕业于武汉大学，拥有五年教学经验，擅长PBL项目教学法和场景化教学，擅长通过游戏教学将课堂氛围调动起来，上课风格活力无限，教学语言直白易懂，本着以孩子为主的教学模式，培养孩子主动的学习能力，深受家长和孩子们的喜爱', 'http://taiyuan.30days-tech.com/ailemao/images/teacher3.png', '0', '2018-12-21 09:43:38');
INSERT INTO `teacher` VALUES ('10', '毛毛老师', '爱乐猫一线讲师', '北京理工大学工程管理学学士，\r\n\r\n曾获得具魅力授课奖，教师大赛三等奖。一副好嗓子，满脑子科学点子，是孩子们心目中最崇拜的“科学老师”！', 'http://taiyuan.30days-tech.com/ailemao/images/index81.png', '1', '2018-12-21 09:44:38');
INSERT INTO `teacher` VALUES ('11', '苹果老师', '爱乐猫一线讲师', '毕业于武汉大学，拥有5年教学经验，\r\n\r\n擅长PBL项目教学法和场景化教学，上课风格活力无限，深受家长和孩子们的喜爱！', 'http://taiyuan.30days-tech.com/ailemao/images/index82.png', '1', '2018-12-21 09:45:12');
INSERT INTO `teacher` VALUES ('12', '夏天老师', '爱乐猫一线讲师', '江西师范大学学前教育学士。\r\n\r\n乐于钻研STEAM教育体系，性格活泼开朗，讲课深入浅出，是深受孩子喜爱的夏天老师。', 'http://taiyuan.30days-tech.com/ailemao/images/index83.png', '1', '2018-12-21 09:45:42');

-- ----------------------------
-- Table structure for `user_account`
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `accountId` int(11) NOT NULL AUTO_INCREMENT COMMENT '账户ID',
  `userName` varchar(20) DEFAULT NULL COMMENT '用户名',
  `email` varchar(30) DEFAULT NULL COMMENT '用户注册邮箱',
  `password` varchar(35) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(16) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(256) DEFAULT NULL COMMENT '头像',
  `realName` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `gender` varchar(1) DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `phoneNumber` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `sina` varchar(255) DEFAULT NULL COMMENT '新浪微博',
  `QQ` int(11) DEFAULT NULL COMMENT 'QQ号码',
  `registTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  `privilege` varchar(8) DEFAULT NULL COMMENT '第三方登录类别',
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `detailAddress` varchar(255) DEFAULT NULL,
  `userAccess` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of user_account
-- ----------------------------

-- ----------------------------
-- Table structure for `user_account_info`
-- ----------------------------
DROP TABLE IF EXISTS `user_account_info`;
CREATE TABLE `user_account_info` (
  `infoId` int(11) NOT NULL AUTO_INCREMENT,
  `openId` varchar(128) NOT NULL,
  `unionId` varchar(128) DEFAULT NULL,
  `accountId` int(11) NOT NULL,
  PRIMARY KEY (`infoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户三方信息表';

-- ----------------------------
-- Records of user_account_info
-- ----------------------------

-- ----------------------------
-- Table structure for `user_manager`
-- ----------------------------
DROP TABLE IF EXISTS `user_manager`;
CREATE TABLE `user_manager` (
  `managerId` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `userName` varchar(32) NOT NULL COMMENT '登录名',
  `realName` varchar(20) DEFAULT NULL,
  `connectTel` varchar(20) DEFAULT NULL,
  `password` varchar(64) NOT NULL COMMENT '登录密码',
  `roleId` int(11) DEFAULT '0' COMMENT '用户所属角色ID',
  `enabledStatus` tinyint(1) NOT NULL DEFAULT '1' COMMENT '账号状态（1为可使用，0为禁用）',
  `createTime` datetime DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`managerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Records of user_manager
-- ----------------------------

-- ----------------------------
-- Table structure for `user_manager_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_manager_role`;
CREATE TABLE `user_manager_role` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(32) NOT NULL COMMENT '角色名称',
  `roleDesc` varchar(256) NOT NULL COMMENT '角色描述',
  `resources` varchar(2048) DEFAULT NULL COMMENT '拥有资源，多个资源之间用英文分号;隔开',
  `createTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of user_manager_role
-- ----------------------------
