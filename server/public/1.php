<?php
/**
 *
 * User: 梁晓伟  lxw11109@gmail.com
 * Date: 2017-10-18
 * Time: 14:43
 */

$dir = "/service/taiyuan/east-institute/weixin";

/**
 *  递归修改文件权限
 * @param $dir 目录或文件
 * @param $mode 权限
 * @param bool $flag 如果是文件 则为false
 * @return bool
 */

function readFileFromDir($dir,$mode=0777,$flag = true)
{
    if(!$flag){
         @chmod("$dir",$mode);
    }
    if(!is_dir($dir))
        return false;
    $handle=opendir($dir);          //打开目录
    while(($file=readdir($handle))!==false)
    {

        if($file=='.'||$file=='..')
        {
            continue;
        }
        $file=$dir.DIRECTORY_SEPARATOR.$file;
        if(is_file($file))                 //是文件就输出
        {
            @chmod($file,$mode);
        }
        elseif(is_dir($file))
        {

            readFileFromDir($file);          //递归查询
        }
    }
    closedir($dir);                 //关闭目录
}

readFileFromDir($dir);
