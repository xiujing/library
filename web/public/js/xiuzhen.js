var url = "http://apilibrary.com/";



$(function(){
    // 验证手机号
    function isPhoneNo(phone) {
        var pattern = /^1[34578]\d{9}$/;
        return pattern.test(phone);
    }

    $("#appointment .make").click(function(){
        if($("#appointment .childrenName").val() == ""){
            $(".xz-alert").html("姓名不能为空!");
            $('.xz-alert').show();
            setTimeout(function () {
                $(".xz-alert").hide();
            }, 2000);
        }else if($("#appointment .childrenAge").val() == ""){
            $(".xz-alert").html("不能为空!");
            $('.xz-alert').show();
            setTimeout(function () {
                $(".xz-alert").hide();
            }, 2000);
        }else if($("#appointment .contactTel").val() == ""){
            $(".xz-alert").html("联系电话不能为空!");
            $('.xz-alert').show();
            setTimeout(function () {
                $(".xz-alert").hide();
            }, 2000);
        }else if(isPhoneNo($.trim($("#appointment .contactTel").val())) == false){
            $(".xz-alert").html("联系电话不正确!");
            $('.xz-alert').show();
            setTimeout(function () {
                $(".xz-alert").hide();
            }, 2000);
        }else{
            $.ajax({
                type: "post",
                dataType: "json", // submit info
                url: url + '/web/subscribe',
                data:$("#appointment").serializeArray(),
                contentType:"application/x-www-form-urlencoded",
                success: function (res) {
                    if (!res.resultStatus) {
                        $(".xz-alert").html(res.errorMessage);
                        $('.xz-alert').show();
                        setTimeout(function () {
                            $(".xz-alert").hide();
                        }, 2000);
                        return
                    }
                    $(".xz-alert").html("提交成功！");
                    $('.xz-alert').show();
                    setTimeout(function () {
                        $(".xz-alert").hide();
                    }, 2000);
                    $("#appointment .childrenName").val("");
                    $("#appointment .contactTel").val("");
                    $("#appointment .childrenAge").val(""); 
                },
                error: function (e) {
                    $(".xz-alert").html("请求数据失败");
                    $('.xz-alert').show();
                    setTimeout(function () {
                        $(".xz-alert").hide();
                    }, 2000);
                }
        
            })
        }
        
    })

    // 检测是否登陆
    if($.cookie('uid')){
        $(".header .nav .right .login").css("display","none");
        $(".header .nav .right .leaguer").css("display","inline-block");
		$("#username").css("display","block");
		$("#username").html($.cookie('username'));
    }
    if(!$.cookie('uid')){
        $(".indexCart").attr('href','./login.html'); 
    }

    // 点击退出
    $(".loginOut").click(function(){
        $.cookie('uid', '',{path: '/' }); 
        // $.cookie('username', ''); 
        location.href = './index.html';
    })


    // 点击logo跳转引导页
    $(".header .nav .logo").click(function(){
        location.href="/index.html";
    })
});








    

